//
//  Evento.swift
//  Camnet
//
//  Created by giulio piana on 16/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation


class Evento{
	
	/*
	
	<events>
		<event>
			<eventId>58585</eventId>
			<deviceId>10633</deviceId>
			<deviceName>001Cam03</deviceName>
			<time>1422604830</time>
			<eventDetails>Ingresso</eventDetails>
			<image>http://sede.b4web.biz:12080/demo.jpg</image>
			<mode>generic/realtime</mode>
			<type>B</type>
			<video/>
		</event>
	</events>
	*/
	
	var id : String
	var deviceId : String
	var device : Cam?
	var deviceName : String
	var eventDetail : String
	var imageUrl : String?
	var mode : String
	var type : String
	var urlVideo : String?
	var time : String
	
	
	init ()
	{
		 id = ""
		 deviceId = ""
		 deviceName = ""
		 eventDetail = ""
		 imageUrl = nil
		 mode = ""
		 type = ""
		 urlVideo = nil
		time = ""
	}
	
	init (_id:String , _deviceId:String , _deviceName:String , _eventDetail:String , _imageUrl: String?, _mode:String, _type:String, _urlVideo:String?)
	{
		id = _id
		deviceId = _deviceId
		deviceName = _deviceName
		eventDetail = _eventDetail
		imageUrl = _imageUrl
		mode = _mode
		type = _type
		urlVideo = _urlVideo
		time = ""
	}
	
}