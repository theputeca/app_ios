//
//  BarraPlayer.swift
//  Camnet
//
//  Created by giulio piana on 02/03/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation

class BarraPlayer: UIView  {

	@IBOutlet weak var PlayeButton: UIButton!
	
	@IBOutlet weak var Slider: UISlider!
	
	@IBOutlet weak var ImgBack: UIImageView!
	
	@IBOutlet weak var BarrView: UIView!
	@IBOutlet weak var TimeLbl: UILabel!
	
	var _sliderDurationCurrentTouched :Bool = false
	
	var player : VKPlayerController?
	var storico : StoricoPlayerVC?
	
	
	var   _timerDuration : NSTimer? = nil
	var _durationCurrent : Float!
	var _durationTotal : Float!
	
	var isPaused :Bool = false
	override func awakeFromNib()  {
		
		Slider.addTarget(self, action: Selector("onSliderCurrentDurationTouched:") , forControlEvents:UIControlEvents.TouchDown)
		Slider.addTarget(self, action:Selector("onSliderCurrentDurationTouchedOut:") ,forControlEvents:UIControlEvents.TouchUpInside)
		Slider.addTarget(self ,action:Selector("onSliderCurrentDurationTouchedOut:"), forControlEvents:UIControlEvents.TouchUpOutside)
		Slider.addTarget( self , action:Selector("onSliderCurrentDurationChanged:") ,forControlEvents:UIControlEvents.ValueChanged)
		
		Slider.setMinimumTrackImage(UIImage(), forState: UIControlState.Normal)
		Slider.setMaximumTrackImage(UIImage(), forState: UIControlState.Normal)
		
		PlayeButton.addTarget(self, action: Selector("TogglePause") , forControlEvents: UIControlEvents.TouchUpInside)
		PlayeButton.setImage(UIImage(named: "pausa.png"), forState: UIControlState.Normal)
		PlayeButton.setTitle("", forState: UIControlState.Normal)
	}
	
	func update ( )  {
		
	}
	
	func setCurrentTime( time : String  )	{
		TimeLbl.text = time
	}
	
	func setCurrentTime( timestamp : NSTimeInterval  )	{
		
		TimeLbl.text = ""
	}
	
	
	
	///
	
	//GESTIONE BARRA
	
	func startDurationTimer ()  {
		
		
		self.Slider.maximumValue = player!.sliderCurrentDurationEmbedded.maximumValue
		
		self.stopDurationTimer()
		_timerDuration = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("onTimerDurationFired:" ), userInfo: nil, repeats: true	)
	
		if( !player!.durationCurrent.isNaN  ){
			TimeLbl.text = "\(Int(floor(player!.durationCurrent / 60)) ) : \(Int(floor(player!.durationCurrent % 60)))";
		}
	}
	
	func stopDurationTimer() {
		if( _timerDuration != nil  )
		{
			if ( _timerDuration!.valid ) {
				_timerDuration!.invalidate()
			}
		}
		
		_timerDuration = nil;
	}
	
	func onTimerDurationFired (timer : NSTimer) {
		if (!_sliderDurationCurrentTouched ) {
			self.Slider.value = player!.sliderCurrentDurationEmbedded.value
			
			if( !player!.durationCurrent.isNaN  ){
				TimeLbl.text = "\(Int(floor(player!.durationCurrent / 60)) ) : \(Int(floor(player!.durationCurrent % 60)))";
			}
			
		}
	}
	
	//SLIDER EVENT
	
	func onSliderCurrentDurationTouched( sender : AnyObject	)  {
		_sliderDurationCurrentTouched = true
		self.stopDurationTimer()
	}
	
	func onSliderCurrentDurationTouchedOut( sender : AnyObject) {
		if ( player!.decoderState.value == kVKDecoderStatePlaying.value || player!.decoderState.value == kVKDecoderStatePaused.value ){
			_sliderDurationCurrentTouched = false
			player!.setStreamCurrentDuration( self.Slider.value) // player!.sliderCurrentDurationEmbedded.value )
			self.startDurationTimer();
		}
	}
	
	func onSliderCurrentDurationChanged(sender : AnyObject) {
		
		if( !player!.durationCurrent.isNaN  ){
			TimeLbl.text = "\(Int(floor(player!.durationCurrent / 60)) ) : \(Int(floor(player!.durationCurrent % 60)))";
		}
	}

	//BUTTON BARRA
	func TogglePause () {
		
		player!.togglePause()
		
		//se pausa metti
		if (!isPaused){
			isPaused = true
			PlayeButton.setImage(UIImage(named: "play.png"), forState: UIControlState.Normal)
		}
		else{
			isPaused = false
			PlayeButton.setImage(UIImage(named: "pausa.png"), forState: UIControlState.Normal)
		}
		
	}
	
}