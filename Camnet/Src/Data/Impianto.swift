//
//  Evento.swift
//  Camnet
//
//  Created by giulio piana on 16/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation


class Impianto{
	
	/*
		<install>
			<id>1</id>
			<name>B4web sede amministrativa</name>
			<description>valmadonna test</description>
	
	
	<views_auto>
	<views_user>
	<devices>
	<maps>
	
	</install>
*/


	var id :String = ""
	var name :String = ""
	var description :String? = ""
	var isPro :String = ""

    var storedVideo : String = ""

	var views_auto = [String: Vista] ()
	var views_user = [String: Vista] ()
	var devices = [String: Cam] ()
	var maps = [String: Mappa] ()
	
	 init (_id:String , _name:String? , _description :String?)
	{
		self.id=_id
		
		name = ""
		if(_name != nil ){  self.name=_name!}
		self.description=_description
	}
	
	
}