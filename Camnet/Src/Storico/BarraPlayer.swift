//
//  BarraPlayer.swift
//  Camnet
//
//  Created by giulio piana on 02/03/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation

class BarraPlayer: UIView  {
	
	@IBOutlet weak var PlayeButton: UIButton!
	
	@IBOutlet weak var Slider: UISlider!
	
	@IBOutlet weak var ImgBack: UIImageView!

	@IBOutlet weak var BarrView: UIView!
	@IBOutlet weak var TimeLbl: UILabel!
	
	var _sliderDurationCurrentTouched :Bool = false
	
	//var player : VKPlayerController?
    var player : VLCMediaPlayer?

	var isStorico : Bool = false
	var storico : StoricoPlayerVC?
	
	var eventi : EventiPlayerView?
	
	var   _timerDuration : NSTimer? = nil
	var _durationCurrent : Float!
	var _durationTotal : Float!
	
	var isPaused :Bool = false
	var isVoidVideo = false
	override func awakeFromNib()  {
		
		Slider.addTarget(self, action: Selector("onSliderCurrentDurationTouched:") , forControlEvents:UIControlEvents.TouchDown)
		Slider.addTarget(self, action:Selector("onSliderCurrentDurationTouchedOut:") ,forControlEvents:UIControlEvents.TouchUpInside)
		Slider.addTarget(self ,action:Selector("onSliderCurrentDurationTouchedOut:"), forControlEvents:UIControlEvents.TouchUpOutside)
		Slider.addTarget( self , action:Selector("onSliderCurrentDurationChanged:") ,forControlEvents:UIControlEvents.ValueChanged)
		
		Slider.setMinimumTrackImage(UIImage(), forState: UIControlState.Normal)
		Slider.setMaximumTrackImage(UIImage(), forState: UIControlState.Normal)
		
		PlayeButton.addTarget(self, action: Selector("TogglePause") , forControlEvents: UIControlEvents.TouchUpInside)
		PlayeButton.setImage(UIImage(named: "pausa.png"), forState: UIControlState.Normal)
		PlayeButton.setTitle("", forState: UIControlState.Normal)


        if (!isStorico){

        }
    }
	
	
	// CREA BARRA STORICO
	
	func creaBarraStorico()	{

        cantouchBar = true;
        self.Slider.userInteractionEnabled = true
		self.Slider.maximumValue = Float (storico!.barLenght)
		Slider.value = Float (storico!.barOfset)
		
		
	}
	
	func setTimeLabel(){
		
		if( isStorico	)
		{
			
			let orafrom : Double = storico!.minTime
			let oracurrent  : Double = orafrom + Double	(Slider.value)
			let datafrom = NSDate(timeIntervalSince1970: oracurrent)
			
			let dateFormatter: NSDateFormatter = NSDateFormatter()
			dateFormatter.dateFormat = "HH:mm:ss"
			TimeLbl.text =  dateFormatter.stringFromDate(datafrom)
			
		}
		else{
			let orafrom : Double = ( eventi!.evento!.time as NSString).doubleValue
			let oracurrent  : Double = orafrom + Double	(Slider.value)
			let datafrom = NSDate(timeIntervalSince1970: oracurrent)
			
			let dateFormatter: NSDateFormatter = NSDateFormatter()
			dateFormatter.dateFormat = "HH:mm:ss"
			TimeLbl.text =  dateFormatter.stringFromDate(datafrom)
		}
	}
	
	
	
	//GESTIONE BARRA
	//chiamata da evento del player
	func startDurationTimer ()  {
		
		
		
		
		if(isStorico)
		{
			self.stopDurationTimer()
			_timerDuration = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("onTimerDurationFired:" ), userInfo: nil, repeats: true	)
			setTimeLabel()
		}
		else{//eventi
            cantouchBar = false;
            self.Slider.userInteractionEnabled = false

	//		self.Slider.maximumValue = player!.sliderCurrentDurationEmbedded.maximumValue
			self.Slider.maximumValue = 1

			self.stopDurationTimer()
			_timerDuration = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("onTimerDurationFired:" ), userInfo: nil, repeats: true	)

		}
	}
	
	func stopDurationTimer() {
		if( _timerDuration != nil  )
		{
			if ( _timerDuration!.valid ) {
				_timerDuration!.invalidate()
			}
			
				_timerDuration = nil;			
		}	
	}
	
	func onTimerDurationFired (timer : NSTimer) {
        if(isStorico)
		{
			if (!_sliderDurationCurrentTouched  && !isVoidVideo) {
				
				//self.Slider.value =  Float (storico!.selectedFilm.startTimeTs - storico!.minTime )  + player!.sliderCurrentDurationEmbedded.value
                self.Slider.value =  Float (storico!.selectedFilm.startTimeTs - storico!.minTime )  + (player!.position * Float (storico!.segmentLengt!))

                setTimeLabel()
				
				if (!cantouchBar){
					cantouchBar = true
					Slider.userInteractionEnabled = true
				}
			}

			if(isVoidVideo)
			{
                // se sono giaa nel viedo vuoto controllo se ce il video
				self.Slider.value++
				setTimeLabel()
				
				if( checkFilm() )		{
                     print("Timer Fired video vuoto trovato nuovo")
                    storico!.barOfset = storico!.segmentLengt * storico!.selectedFilmNumber
					storico!.isOfsetBar = true
					storico!.addLoading()

                    isVoidVideo = false
                    player!.stop()
                    player!.delegate  = nil
                    player = nil
					storico!.player = nil
					storico!.creaNuovoPlayer(storico!.selectedFilmNumber)
					storico!.removeLblVoidVideo()
				}
				else // se non ce metto scritta video non presente
				{
					 print("Timer Fired video vuoto lascio vuoto")
				}
				return
			}
			

            if( player!.position >= 0.99 ) // cambia video quando arrivato a fine
            {
                playerAllafine()
            }
		}
		else{//eventi
			if (!_sliderDurationCurrentTouched ) {
				self.Slider.value = player!.position

				setTimeLabel()
				if (Slider.value >= Slider.maximumValue || player!.position  >= 1  ||  player!.state == VLCMediaPlayerState.Ended)
				{

                 //   player!.position = 0
				//	player!.stop()
					//player!.play()
					//stopDurationTimer()
				}
			}
		}
	}

    func playerAllafine(){


        print("Timer Fired video alla fine")
        if( storico!.selectedFilmNumber < storico!.films.count - 1	)
        {

            //se trovo il video
            if( checkFilm() )
            {
                print("Timer Fired video alla fine trovato")
                storico!.selectedFilmNumber =  storico!.selectedFilmNumber + 1
                storico!.selectedFilm = storico!.films[storico!.selectedFilmNumber]
                storico!.barOfset = storico!.segmentLengt * storico!.selectedFilmNumber
                storico!.isOfsetBar = true
                storico!.addLoading()
                player!.stop()
                player!.delegate = nil

                player = nil
                storico!.player = nil
                storico!.creaNuovoPlayer(storico!.selectedFilmNumber)
                storico!.removeLblVoidVideo()
            }
            else // se non ce metto scritta video non presente
            {
                print("Timer Fired video alla fine video vuoto")
                isVoidVideo = true
                player!.stop()
                storico!.createLblVoidVideo()
            }
            
        }

    }
	
	//SLIDER EVENT
	
	
	func checkFilm()->Bool
	{
		
		var i = 0
		for f in storico!.films
		{
			let from    = Int ( f.startTimeTs - storico!.minTime )
			let to    = Int	( f.endTimeTS - storico!.minTime )
			
			let myTime : Int  =   Int  ( Int (Slider.value))
			
			if (  myTime >= from  && myTime < to ) {
				print( "SELECTED")
				storico!.selectedFilmNumber = i
				storico!.selectedFilm = f
				return true				
			}
			i++
		}
	
			return false
	}
	
	func onSliderCurrentDurationTouched( sender : AnyObject	)  {

        if (!isStorico){
            cantouchBar = false;
        }
        else {
            cantouchBar = true;
        }

		if(cantouchBar){
			_sliderDurationCurrentTouched = true
			//self.stopDurationTimer()
		}
	}
	
	
	var cantouchBar : Bool = true
	func onSliderCurrentDurationTouchedOut( sender : AnyObject) {
		
		if(isStorico)
		{
			if(!cantouchBar) { return	}
			
			cantouchBar = false
			Slider.userInteractionEnabled = false
			
			_sliderDurationCurrentTouched = false
			if ( player!.state.rawValue == VLCMediaPlayerState.Playing.rawValue    ||  player!.state.rawValue == VLCMediaPlayerState.Paused.rawValue   ||  player!.state.rawValue == VLCMediaPlayerState.Buffering.rawValue   || isVoidVideo)
			{
				print("touched out")
				
				if ( (storico!.minTime + Double (Slider.value)) < storico!.selectedFilm.startTimeTs || (storico!.minTime + Double (Slider.value)) > storico!.selectedFilm.endTimeTS  || isVoidVideo || player!.position >= 1 )
				{
					print("touched out cambia video")

					var i = 0
					for f in storico!.films
					{
						let from    = Int ( f.startTimeTs - storico!.minTime )
						let to    = Int	( f.endTimeTS - storico!.minTime )
						let myTime : Int  =   Int  ( Int (Slider.value))
						
						if (  myTime >= from  && myTime < to ) {
							print("touched out video selected")
							storico!.selectedFilmNumber = i
							storico!.selectedFilm = f
							break
						}
						i++
					}
					
					if(   i < storico!.films.count  )
					{
						isVoidVideo = false
						storico!.isOfsetBar = true
						storico!.addLoading()
						player!.stop()
						player!.delegate = nil

						player = nil
						storico!.player = nil
						storico!.creaNuovoPlayer(storico!.selectedFilmNumber)
						storico!.removeLblVoidVideo()
					}
					else{
						print("touched out video vuoto")
						player!.stop()
						isVoidVideo = true
						
						cantouchBar = true
						Slider.userInteractionEnabled = true
						self.startDurationTimer()
						storico!.createLblVoidVideo()
					}
				}
				else{ //continua sullo stesso video
					print("touched out continua su video")
					let x = Slider.value - Float ( storico!.selectedFilm.startTimeTs - storico!.minTime     )
                    self.player!.position = x /  Float ( storico!.segmentLengt)
					self.startDurationTimer()
                    cantouchBar = true
                    Slider.userInteractionEnabled = true

                    self.player?.play()
                }
			}
            else
            {
                print("player was stopped  ?");
                self.startDurationTimer()
                cantouchBar = true
                Slider.userInteractionEnabled = true
                storico!.creaNuovoPlayer(storico!.selectedFilmNumber)
            }
			
		}
			
		else{ // eventi
			_sliderDurationCurrentTouched = false
			if ( player!.state.rawValue == VLCMediaState.Playing.rawValue ) // || player!.decoderState.rawValue == kVKDecoderStatePaused.rawValue ){
            {
                //self.player!.setStreamCurrentDuration( self.Slider.value) // player!.sliderCurrentDurationEmbedded.value )
               // self.startDurationTimer();
			}
		}
	}
	
	func onSliderCurrentDurationChanged(sender : AnyObject) {
		
		if(isStorico)
		{
			setTimeLabel()
		}
		else{
			setTimeLabel()
		}
	}
	
	//BUTTON BARRA
	func TogglePause () {
		
		player!.pause()

		
		
		//se pausa metti
		if (!isPaused){
			isPaused = true
			
			if(isStorico){
				storico!.isPaused = true
			}
			
			PlayeButton.setImage(UIImage(named: "play.png"), forState: UIControlState.Normal)
		}
		else{
			isPaused = false
            if(isStorico){
                storico!.isPaused = false
            }
			PlayeButton.setImage(UIImage(named: "pausa.png"), forState: UIControlState.Normal)
		}
		
	}
	
	
	
}