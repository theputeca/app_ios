//
//  NSDateExtension.swift
//  Camnet
//
//  Created by giulio piana on 22/03/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

extension NSDate {

	
	func getcomponent()-> NSDateComponents
	{
	
		let cal = NSCalendar.currentCalendar()
		let comp = cal.components([.NSMonthCalendarUnit, .NSDayCalendarUnit], fromDate: self)
		return comp
		
	}
	
	func day () -> Int {
		
		return getcomponent().day
	}
	
	func month () -> Int {
		
		return getcomponent().month
	}
	
}
