//
//  SelectionCamMap.swift
//  Camnet
//
//  Created by giulio piana on 05/01/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation
import UIKit

class SelectionCamMap: UIViewController ,UIWebViewDelegate  , UITableViewDelegate, UITableViewDataSource,UIGestureRecognizerDelegate {
	
	var delegate: callBackFromSelectionProtocol?
	
	
	var mapWebView: UIWebView!
	var MenuBtn: UIButton!
	
	var selectedImpianto : Impianto?
	var selectedMap : Mappa?
	var selectedCamId : String?
	
	var menuTable : UITableView!
	var isMenuopen : Bool = false
	
	var loadInd : UIActivityIndicatorView?
	
	var isStorico : Bool = false
	var isModifica : Bool = false
	
	var border: CALayer!
	//VIEW LOAD
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.tabBarController?.tabBar.hidden=true
		self.navigationController?.interactivePopGestureRecognizer!.enabled = false;
		self.navigationController?.interactivePopGestureRecognizer!.delegate = self
		
		//titolo della barra
		//	sleziona la prima mappa
		
		if(selectedImpianto!.maps.count == 0)
		{
			selectedMap = nil
			for  imp : Impianto in DataManager.Instance.impianti {
				if (imp.maps.count >  0){
					selectedImpianto = imp
					let  map  = [Mappa] (selectedImpianto!.maps.values) [0]
					selectedMap = map
					break
				}
			}
		}
		else{
			let  map  = [Mappa] (selectedImpianto!.maps.values) [0]
			selectedMap = map
		}
		
		self.title = " seleziona camera " + selectedMap!.name;
		
		border  = CALayer()
		border.backgroundColor = UIColor.sfondoChiaro().CGColor
		border.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 66 , width: 1024, height: 1)
		//border.borderWidth = 1
		view.layer.addSublayer(border)
		
		//creo MenuBtn
		
		MenuBtn = UIButton(type: UIButtonType.System) //UIButton()
		MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
		
		MenuBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
		MenuBtn.tintColor = UIColor.sfondoScuro()
		view.addSubview(MenuBtn)
		
		MenuBtn.addTarget(self, action: Selector("mappeListClick:"), forControlEvents: UIControlEvents.TouchUpInside)
		
		
		MenuBtn.setTitle("\(selectedMap!.name) (\(selectedImpianto!.maps.count))" , forState: UIControlState.Normal)
		
		
		/*if(selectedMap!.contentIds.count > 0){
		selectedVista = selectedImpianto!.views_user[ selectedMap!.contentIds[0] ]
		MenuBtn.setTitle(selectedVista!.name + "(" +  selectedVista!.size + " Viste )" , forState: UIControlState.Normal)
		}
		else
		{
		MenuBtn.userInteractionEnabled	 = false
		MenuBtn.alpha = 0.5
		MenuBtn.setTitle("Nessuna vista" , forState: UIControlState.Normal)
		}*/
		
		//creo la tableview del menu
		
		menuTable = UITableView()
		if(self.IsPad()){
			menuTable.frame = CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width/5 	, height: 0)
		}
		else{
			menuTable.frame = CGRect(x: 0 , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width 	, height: 0)
			menuTable.separatorStyle = UITableViewCellSeparatorStyle.None
		}
		
		menuTable.separatorStyle = UITableViewCellSeparatorStyle.None
		
		self.view.addSubview(menuTable)
		menuTable.backgroundColor = UIColor.whiteColor()

        menuTable.layer.masksToBounds = true
        menuTable.layer.borderColor =  UIColor.sfondoScuro().CGColor
        menuTable.layer.borderWidth = 0.5

		
		self.title = selectedMap?.name;
		
		menuTable.delegate = self
		menuTable.dataSource = self
		
		//WEBVIEW
		mapWebView = UIWebView(frame: CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.width, height: self.frameBetweenBar().height - 66))
		mapWebView.backgroundColor = UIColor.sfondoChiaro()
		view.addSubview(mapWebView)
		mapWebView.scalesPageToFit = true
		
		
		
		//LOAD URL
		if(selectedMap != nil)
		{
			let url = NSURL(string: selectedMap!.url)
			let request = NSURLRequest(URL: url!)
			mapWebView.loadRequest(request)
			mapWebView.delegate = self
		}
		
		// loading indicator
		loadInd  = UIActivityIndicatorView(frame: view.frame)
		loadInd!.center = mapWebView.center
		loadInd!.hidesWhenStopped = true
		loadInd!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray // .WhiteLarge
		loadInd!.color = UIColor.arancio()
		loadInd!.startAnimating()
		
		view.addSubview(loadInd!)
		self.view.bringSubviewToFront(loadInd!)
		
		self.view.bringSubviewToFront(menuTable)
	}
	
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		if(self.IsPad())
		{
			border.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 66 , width: 1024, height: 1)
			
			MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
			menuTable.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67  , width:  self.view.frame.width/5 	, height: 0)
			mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.width, height: self.frameBetweenBar().height - 66)
			loadInd!.center = mapWebView.center
			
			
		}
		else
		{
			border.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 66 , width: 1024, height: 1)
			
			MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
			menuTable.frame = CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width 	, height: 0)
			mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.width, height: self.frameBetweenBar().height - 66)
			loadInd!.center = mapWebView.center
		}
		
	}
	
	//ORIENTATION
	override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		
		if(self.IsPad())
		{
			if (  toInterfaceOrientation.isPortrait		){
				MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
				menuTable.frame = CGRect(x: 0 , y: self.frameBetweenBar().origin.y + 67  , width:  self.view.frame.width/5 	, height: 0)
				mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.height, height: self.frameBetweenBar().width - 66)
				loadInd!.center = mapWebView.center
				
			}
			else{
				
				MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
				menuTable.frame = CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width/5 	, height: 0)
				mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.height, height: self.frameBetweenBar().width - 66)
				loadInd!.center = mapWebView.center
			}
			
		}
		else{//iphone
			
			
			
		}
        isMenuopen = false
	}
	
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {

        if(!self.IsPad())
		{
			border.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 66 , width: 1024, height: 1)
			
			MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
			menuTable.frame = CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width 	, height: 0)
			mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.width, height: self.frameBetweenBar().height - 66)
			loadInd!.center = mapWebView.center
		}

	}
	
	
	//BUTTON CLICK
	func mappeListClick(sender: UIButton) {



//self.MenuBtn.frame.origin.y + self.MenuBtn.frame.height
		if ( isMenuopen ){
			isMenuopen = false
			
			UIView.animateWithDuration(0.4, animations: { () -> Void in
				
				if(self.IsPad()){
                    //self.MenuBtn.frame.origin.x
					self.menuTable.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67,  width:self.view.frame.width/4 	, height:0)
				}
				else{
					self.menuTable.frame = CGRect(x: 0, y: self.MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width 	, height: 0)
				}
				
				
				}, completion: nil)
		}
		else{
			isMenuopen = true
			
			UIView.animateWithDuration(0.4, animations: { () -> Void in
				if(self.IsPad()){

                    let menuheight : CGFloat = CGFloat( self.selectedImpianto!.maps.count * 65)

					self.menuTable.frame = CGRect(x: 0 , y: self.frameBetweenBar().origin.y + 67 , width:self.view.frame.width/4 , height: menuheight) // self.view.frame.height/2
					
				}
				else{
					self.menuTable.frame = CGRect(x: 0 , y: self.MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width 	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height - self.MenuBtn.frame.height  )
				}
				
				
				}, completion: nil)
		}
		
	}
	
	/// TABLE VIE DELEGATE
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		
		
		selectedMap = [Mappa] (selectedImpianto!.maps.values) [indexPath.row ]
		let string = selectedMap!.url
		let url = NSURL(string: string)
		let request = NSURLRequest(URL: url!)
		mapWebView.loadRequest(request)
		
		firstTime = true
		mappeListClick( MenuBtn)
		
		loadInd!.startAnimating()
		view.addSubview(loadInd!)
		
	}
	
	//TABLE VIEW DATASURCE
	
	// number of section sezione degli  impianti
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		//ritorna il numero delle mappe
		return selectedImpianto!.maps.count
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		/*let cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier("mappCell", forIndexPath: indexPath) as UITableViewCell
		//set the title
		let title : UILabel = cell.viewWithTag(1) as UILabel
		let  map  = [Mappa] (selectedImpianto!.maps.values) [indexPath.row ]
		title.text = map.name
		
		return cell
		*/
		
		var cell :UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("CellTitleDescription") as UITableViewCell?
		if(cell == nil)
		{
			let nib = UINib(nibName: "CellTitleDescription", bundle: nil)
			tableView.registerNib(nib, forCellReuseIdentifier: "CellTitleDescription")
			cell = tableView.dequeueReusableCellWithIdentifier("CellTitleDescription") as UITableViewCell?
		}
		
		let bgColorView = UIView()
		bgColorView.backgroundColor = UIColor.arancioSelezione()
		cell.selectedBackgroundView = bgColorView
		
		let title : UILabel =  cell.viewWithTag(1) as! UILabel
		let  map  = [Mappa] (selectedImpianto!.maps.values) [indexPath.row ]
		title.text = map.name
		
		let desc : UILabel = cell.viewWithTag(2) as! UILabel
		desc.text = "description"
		
		
		cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, tableView.frame.width  , 65 )
		
		let border : CALayer = CALayer()
		border.borderColor = UIColor.sfondoChiaro().CGColor
		border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: 1)
		border.borderWidth = 1
		cell.layer.addSublayer(border)
		
		
		return cell
	}
	
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 65
		
	}
	
	
	
	//WEBVIEW
	// web view map event
	
	func webViewDidFinishLoad(webView: UIWebView) {
		self.loadInd!.stopAnimating()
	}

	func webViewDidStartLoad(webView: UIWebView) {
	}
	
	var firstTime : Bool = true
	func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
		if (firstTime)
		{
			firstTime = false
		}
		else{
			print( "web view start load" )
			print(request.URL  )
			
			if(request.URL == nil ) {return false}
			
			parseUrlCamSelected(request.URL!.absoluteString)
			//webView.stopLoading()

            return false
		}
		
		return true
	}
	
	
	//selected cam from map
	
	func btnTestCam(sender: UIButton) {
		selectedCam()
	}
	
	func parseUrlCamSelected(url :String)	{

        firstTime=true
        mapWebView.reload()
        loadInd!.startAnimating()
        view.addSubview(loadInd!)

		let myStringArr = url.componentsSeparatedByString("/")
		if let id = myStringArr.last {
			
			//let  cam  = [Cam](selectedImpianto!.devices.values) [0 ]
			selectedCamId = id //selectedImpianto!.devices[ id ]?.id
			selectedCam()

		}
	}
	
	func selectedCam()	{
		if(isStorico)
		{
			let storicoPlayerController : StoricoPlayerVC = self.storyboard?.instantiateViewControllerWithIdentifier("StoricoPlayerVC") as! StoricoPlayerVC
			storicoPlayerController.selectedCamId = selectedCamId!

			let cam : Cam = selectedImpianto!.devices[ selectedCamId! ]!
			
			storicoPlayerController.selectedCam = cam
			
			self.navigationController?.pushViewController(storicoPlayerController, animated: true)
		}
		else
		{
			let cam : Cam = selectedImpianto!.devices[ selectedCamId! ]!
			delegate?.callBackFromSelection( cam )
			
			if(self.IsPad()){
				if(isModifica){
					let vc = self.navigationController!.viewControllers [2] 
					self.navigationController?.popToViewController(  vc , animated: true)
				}
				else{
					let vc = self.navigationController!.viewControllers [1] 
					self.navigationController?.popToViewController(  vc , animated: true)
				}
			}
			else{//iphone
				if(isModifica){
					let vc = self.navigationController!.viewControllers [2] 
					self.navigationController?.popToViewController(  vc , animated: true)
				}
				else{
					let vc = self.navigationController!.viewControllers [1] 
					self.navigationController?.popToViewController(  vc , animated: true)
				}
			}
		}
	}
	
}