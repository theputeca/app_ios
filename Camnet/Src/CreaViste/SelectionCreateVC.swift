//
//  SelectionCreateVC.swift
//  Camnet
//
//  Created by giulio piana on 28/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation
import UIKit

protocol callBackFromSelectionProtocol {
	func callBackFromSelection(cam:Cam)
	
}

class SelectionCreateVC : UIViewController , UITableViewDataSource ,UITableViewDelegate , UICollectionViewDataSource , UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
	
	
	
	var delegate: callBackFromSelectionProtocol?
	
	
	var ImpiantiTable2 : UITableView!
	var CamCollectionView2: UICollectionView!
	
	
	var selectedImpianto : Impianto?
	var selectedIndex : Int = 0
	
	var isShowCollection : Bool = false
	
	var isModifica : Bool = false
	
	//immagini
	let kBgQueue  = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
	
	var imageCahce : [Int : UIImage] = [Int : UIImage]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		self.title =  NSLocalizedString("creaVista", comment:  "")
		
		
		
		if(!self.IsPad()){   "scegliImpianto".loc() }
		
		//create che collection view
		
		CamCollectionView2 = NSBundle.mainBundle().loadNibNamed("CVSelection", owner: self, options: nil)[0] as? UICollectionView
		CamCollectionView2.frame = CGRect(x: 300 , y: 0, width: self.view.frame.width - 300	, height:self.view.frame.height)
		CamCollectionView2.backgroundColor = UIColor.whiteColor()
		if(self.IsPad()){
			self.automaticallyAdjustsScrollViewInsets = false
			
			let nib = UINib(nibName: "CVHeaderCell", bundle: nil)
			CamCollectionView2.registerNib(nib , forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
		}
		else{//IPHIONE
			
			self.automaticallyAdjustsScrollViewInsets = false
			
			let nib = UINib(nibName: "CVHeaderCellIphone", bundle: nil)
			CamCollectionView2.registerNib(nib , forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
			
		}
		
		self.view.addSubview(CamCollectionView2)
		CamCollectionView2.delegate = self
		CamCollectionView2.dataSource = self
		
		//table view
		ImpiantiTable2   = UITableView()
		if(self.IsPad()){
			ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 300	, height: self.view.frame.height)
			ImpiantiTable2.separatorStyle = UITableViewCellSeparatorStyle.None
		}
		else{//IPHIONE
			ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: self.view.frame.width	, height: self.view.frame.height)
			ImpiantiTable2.separatorStyle = UITableViewCellSeparatorStyle.None
			
			
		}
		self.view.addSubview(ImpiantiTable2)
		self.view.bringSubviewToFront(ImpiantiTable2)
		
		//bordo
		ImpiantiTable2.layer.masksToBounds = true
		ImpiantiTable2.layer.borderWidth = 1
		ImpiantiTable2.layer.borderColor =  UIColor.sfondoHeader().CGColor
		
		//ImpiantiTable2.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		
		ImpiantiTable2.delegate=self
		ImpiantiTable2.dataSource = self
		
		if ( isShowCollection ){
			CamCollectionView2.frame = CGRect(x: 0 , y: 0 , width: self.view.frame.width	, height: self.view.frame.height)
			ImpiantiTable2.alpha = 0
			
			if(selectedImpianto != nil){
				self.title =  "creaVista".loc() + ": " + selectedImpianto!.name
			}

		}
		else{
			
			for  imp : Impianto in DataManager.Instance.impianti {
				if (imp.devices.count >  0){
					selectedImpianto = imp
					break
				}
			}
			
			if(selectedImpianto != nil){
				self.title =  "creaVista".loc()
			}

		}
	}
	
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

		if(self.IsPad())
		{
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 300	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height  )
				CamCollectionView2.frame = CGRect(x: 300, y:  self.navigationController!.navigationBar.frame.height  , width:  self.view.frame.width - 300	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height )
				
			}
			else{
				ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 200	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height )
				CamCollectionView2.frame = CGRect(x: 200 , y:  self.navigationController!.navigationBar.frame.height  , width:  self.view.frame.width - 200	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height )
				
			}
			
			ImpiantiTable2.reloadData()
			CamCollectionView2.reloadData()
		}
		else{//IPHONE
			if(isShowCollection){
				
			}
			else{
				
			}
			ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: self.view.frame.width	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height)
			CamCollectionView2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: self.view.frame.width	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height)
			ImpiantiTable2.reloadData()
			CamCollectionView2.reloadData()
			
			
		}
	}
	
	override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		if (self.IsPad()){
			if (  toInterfaceOrientation.isPortrait		){
				ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 200	, height: self.view.frame.width - self.navigationController!.navigationBar.frame.height   )
				CamCollectionView2.frame = CGRect(x: 200 , y: self.navigationController!.navigationBar.frame.height, width:  self.view.frame.height - 200	, height: self.view.frame.width  - self.navigationController!.navigationBar.frame.height  )
				
			}
			else{
				ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 300	, height: self.view.frame.width - self.navigationController!.navigationBar.frame.height  )
				CamCollectionView2.frame = CGRect(x: 300, y: self.navigationController!.navigationBar.frame.height , width:  self.view.frame.height - 300	, height: self.view.frame.width  - self.navigationController!.navigationBar.frame.height )
				
			}
			
			ImpiantiTable2.reloadData()
			CamCollectionView2.reloadData()
		}
		else{//IPHONE
			if(isShowCollection){
			}
			else{
			}
		}
	}
	
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
		if (self.IsPad()){
		}
		else{//IPHONE
			if(isShowCollection){
				
			}
			else{
				
			}
			ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: self.view.frame.width	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height)
			CamCollectionView2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: self.view.frame.width	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height)
			ImpiantiTable2.reloadData()
			CamCollectionView2.reloadData()
			
			
		}
	}
	
	@IBAction func selectMappeClick (sender : UIButton)	{
		
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		
		let selectionMappeVC = storyboard.instantiateViewControllerWithIdentifier("selectionMappeVC") as! SelectionCamMap
		
		selectionMappeVC.selectedImpianto = self.selectedImpianto
		selectionMappeVC.delegate = self.delegate
		
		if(isModifica) { selectionMappeVC.isModifica	= isModifica   }
		
		navigationController?.pushViewController(selectionMappeVC, animated: true)
		
		
		
	}
	
	//TABEL VIEW
	
	//number row
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return DataManager.Instance.impianti.count
	}
	//cellforrow at index
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		var cell :UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("CellTitleDescription") as UITableViewCell?
		if(cell == nil)
		{
			let nib = UINib(nibName: "CellTitleDescription", bundle: nil)
			tableView.registerNib(nib, forCellReuseIdentifier: "CellTitleDescription")
			cell = tableView.dequeueReusableCellWithIdentifier("CellTitleDescription") as UITableViewCell?
		}
		
		
		
		
		let bgColorView = UIView()
		bgColorView.backgroundColor = UIColor.arancioSelezione()
		cell.selectedBackgroundView = bgColorView
		
		let title : UILabel =  cell.viewWithTag(1) as! UILabel
		title.text = DataManager.Instance.impianti[ indexPath.row ].name
		
		let desc : UILabel = cell.viewWithTag(2) as! UILabel
		desc.text = DataManager.Instance.impianti[ indexPath.row ].description
		
		if( DataManager.Instance.impianti[ indexPath.row ].devices.count == 0 	) {
			if (selectedIndex == indexPath.row) {selectedIndex++ }
			
			
			title.alpha = 0
			desc.alpha = 0
			let freccia  : UIImageView = cell.viewWithTag(3) as! UIImageView
			freccia.alpha = 0
        }
        else
        {
            title.alpha = 1
            desc.alpha = 0
            cell.alpha = 1
            let freccia  : UIImageView = cell.viewWithTag(3) as! UIImageView
            freccia.alpha = 1
        }


		//if(!self.IsPad()){//IPHONE
		cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, tableView.frame.width  , 65 )
		
		
		let border : CALayer = CALayer()
		border.borderColor = UIColor.sfondoChiaro().CGColor
		border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: 1)
		border.borderWidth = 1
		cell.layer.addSublayer(border)
		//}
		
		if(self.IsPad()){
			if(indexPath.row == selectedIndex )	{
				tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
			}
		}
		return cell
	}
	//heigt row
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		if( DataManager.Instance.impianti[ indexPath.row ].devices.count == 0 	) { return 0 }
		return 65
	}
	// table view header
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		
		//	if(self.IsPad()){
		let headerview : UIView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
		headerview.backgroundColor = UIColor.sfondoHeader()
		
		let title : UILabel = UILabel (frame: headerview.frame)
		title.text = NSLocalizedString("impianti", comment:  "")
		title.textAlignment = NSTextAlignment.Center
		title.textColor = UIColor.sfondoChiaro()
		
		headerview.addSubview(title)
		
		return  headerview
		//	}
		
	}
	
	func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return ""
	}
	
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		if(self.IsPad()){
			return 30
		}
		else{
			return 0
		}
	}
	//tableView Delegate
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		
		
		
		if(!self.IsPad()){
			
			selectedIndex = indexPath.row
			selectedImpianto = DataManager.Instance.impianti[ indexPath.row ]
			
			CamCollectionView2.reloadData()
			
			imageCahce  = [Int : UIImage]()
			
			let storicoVc = SelectionCreateVC()
			storicoVc.selectedImpianto = selectedImpianto
			storicoVc.isShowCollection = true
			storicoVc.delegate = self.delegate
			
			if (isModifica ){	storicoVc.isModifica = true }
			self.navigationController?.pushViewController(storicoVc , animated: true)
			
		}
		else{
			if(selectedIndex == indexPath.row) { return }
			
			selectedIndex = indexPath.row
			selectedImpianto = DataManager.Instance.impianti[ indexPath.row ]
			
			CamCollectionView2.reloadData()
			
			imageCahce  = [Int : UIImage]()
			
			
		}
		
		
	}
	
	
	
	// COLLECTION VIEW
	//Data
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	//number of item
	
	
	// ordino le cam del imp in ordine alfa
	var  cams :[Cam]  = [Cam]()//(selectedImpianto!.devices.values))
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		if (section == 0 ){ // cam
			if (selectedImpianto != nil)
			{
				
				cams = ([Cam](selectedImpianto!.devices.values))
				cams.sortInPlace({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })
				
				return  cams.count //selectedImpianto!.devices.count
			}
			else{
				return 0
			}
		}
		else{ // mappa
			return selectedImpianto!.maps.count
		}
		
	}
	//cellforitem
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		
		//	if (indexPath.section == 0){ //cell cam
		
		if (selectedImpianto != nil){
			
			if(self.IsPad()){
				
				var nib = UINib(nibName: "CollCellWithImage", bundle: nil)
				collectionView.registerNib(nib, forCellWithReuseIdentifier: "CollCellWithImage")
				var cell :UICollectionViewCell!  = collectionView.dequeueReusableCellWithReuseIdentifier("CollCellWithImage", forIndexPath: indexPath) as? UICollectionViewCell
				//resize the cell frame
				
				if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
					cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, CamCollectionView2.frame.width / 2 - 10 , 125 )
				}
				else{
					cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, CamCollectionView2.frame.width / 2 - 10 , 125 )
				}
				
				//arrotonda bordi
				cell.layer.masksToBounds = true;
				cell.layer.cornerRadius = 25;
				
				//set the title of cam
				let title : UILabel = cell.viewWithTag(1) as! UILabel
				
				let  cam  = cams[indexPath.row] //  [Cam](selectedImpianto!.devices.values) [indexPath.row ]
				title.text = cam.name
				
				//set the description of cam
				let desc : UILabel = cell.viewWithTag(3) as! UILabel
				desc.text = cam.description
				
				
				//set the image of the cell
				let imageView : UIImageView = cell.viewWithTag(2) as! UIImageView
				
				if let image = UIImage(named: "ITEM-3.png") {
					imageView.image = image
				}
				
				if (cam.urlImg != nil){
					if let img = DataManager.Instance.imgCache[ cam.id ]
					{
						imageView.image = img
					}
					else{
						
						let url = NSURL(string: cam.urlImg! )
						var timeoutInterval: NSTimeInterval = 5
						var chachepol : NSURLRequestCachePolicy = NSURLRequestCachePolicy.UseProtocolCachePolicy
						let request = NSURLRequest(URL: url!, cachePolicy: chachepol, timeoutInterval: timeoutInterval)
						
						NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
							
							if (data != nil){
								if let image = UIImage(data: data!){
									 DataManager.Instance.imgCache[ cam.id ] = image
									if let updateCell : UICollectionViewCell = self.CamCollectionView2.cellForItemAtIndexPath(indexPath) { //  tableView cellForRowAtIndexPath:indexPath];
										(updateCell.viewWithTag(2) as! UIImageView).image = image;
									}
								}
							}
						}
					}
				}
				
				
				
				
				
				return cell;
			}
			else{//IPHONE
				var nib = UINib(nibName: "CollCellWithImageIphone", bundle: nil)
				collectionView.registerNib(nib, forCellWithReuseIdentifier: "CollCellWithImageIphone")
				var cell :UICollectionViewCell!  = collectionView.dequeueReusableCellWithReuseIdentifier("CollCellWithImageIphone", forIndexPath: indexPath) as? UICollectionViewCell
				//resize the cell frame
				
				//	if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, CamCollectionView2.frame.width  ,65)
				/*}
				else{
				cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, CamCollectionView2.frame.width , CamCollectionView2.frame.height / 6 )
				}*/
				
				//set the title of cam
				let title : UILabel = cell.viewWithTag(1) as! UILabel
				let  cam  = cams[indexPath.row]// [Cam](selectedImpianto!.devices.values) [indexPath.row ]
				title.text = cam.name
				
				//set the description of cam
				let desc : UILabel = cell.viewWithTag(4) as! UILabel
				desc.text = cam.description

                let data : UILabel = cell.viewWithTag(3) as! UILabel
                data.alpha = 0

				//set the image of the cell
				let imageView : UIImageView = cell.viewWithTag(2) as! UIImageView
				
				if let image = UIImage(named: "ITEM-3.png") {
					imageView.image = image
				}
				
				if (cam.urlImg != nil){
					if let img =  DataManager.Instance.imgCache[ cam.id ]
					{
						imageView.image = img
					}
					else{
						
						let url = NSURL(string: cam.urlImg! )
						var timeoutInterval: NSTimeInterval = 5
						var chachepol : NSURLRequestCachePolicy = NSURLRequestCachePolicy.UseProtocolCachePolicy
						let request = NSURLRequest(URL: url!, cachePolicy: chachepol, timeoutInterval: timeoutInterval)
						
						NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in
							
							if (data != nil){
								if let image = UIImage(data: data!){
									 DataManager.Instance.imgCache[ cam.id ] = image
									if let updateCell : UICollectionViewCell = self.CamCollectionView2.cellForItemAtIndexPath(indexPath) { //  tableView cellForRowAtIndexPath:indexPath];
										(updateCell.viewWithTag(2) as! UIImageView).image = image;
									}
								}
							}
						}
					}
				}
				
				let border : CALayer = CALayer()
				border.borderColor = UIColor.sfondoChiaro().CGColor
				border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: cell.frame.size.height)
				border.borderWidth = 1
				cell.layer.addSublayer(border)
				
				return cell;
			}
			
			
		}
		else {
			return UICollectionViewCell( )
		}
			}
	//size for row
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		if(self.IsPad()){
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				return CGSizeMake(CamCollectionView2.frame.width / 2 - 10  , 125 )
			}
			else{
				return CGSizeMake(CamCollectionView2.frame.width / 2 - 10  , 125)
				
			}
		}
		else
		{
			//if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
			return CGSizeMake(collectionView.frame.width   , 65 )
			/*}
			else{
			return CGSizeMake(collectionView.frame.width   , collectionView.frame.height / 6 )
			
			}*/
		}
		
	}
	
	//header footer
	
	
	func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
		
		if (kind == UICollectionElementKindSectionHeader){
			let header :UICollectionReusableView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView", forIndexPath: indexPath) 
			
			//header.sizeThatFits(CGSize(width: collectionView.frame.width, height: 100))
			
			if (indexPath.section==0){
				
				if(self.IsPad()){
					
					let title : UILabel = header.viewWithTag(1) as! UILabel
					let button : UIButton = header.viewWithTag(2) as! UIButton
					
					title.text = NSLocalizedString("telecamere", comment:  "")
					
					button.layer.masksToBounds = true;
					button.layer.cornerRadius = ( 72 / 2 ) - 2;
					button.layer.borderWidth = 2
					button.layer.borderColor =  UIColor.arancio().CGColor
					
					button.addTarget(self, action: Selector("selectMappeClick:") , forControlEvents: UIControlEvents.TouchUpInside)
					button.setTitle(NSLocalizedString("selezionaMapp", comment:  ""), forState: UIControlState.Normal)
					
				}
				else{
					header.frame = CGRectMake(header.frame.origin.x, header.frame.origin.y, CamCollectionView2.frame.width , 65 )
					
					
					let button : UIButton = header.viewWithTag(2) as! UIButton
					button.addTarget(self, action: Selector("selectMappeClick:") , forControlEvents: UIControlEvents.TouchUpInside)
					button.setTitle(NSLocalizedString("selezionaMapp", comment:  ""), forState: UIControlState.Normal)
					
					let border : CALayer = CALayer()
					border.borderColor = UIColor.sfondoChiaro().CGColor
					border.frame = CGRect(x: 0, y: header.frame.size.height - 1, width:  header.frame.size.width, height: header.frame.size.height)
					border.borderWidth = 1
					header.layer.addSublayer(border)
				}
			}
			
			return header
		}
		/*else if (kind == UICollectionElementKindSectionFooter ){
		
		let footer : UICollectionReusableView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionFooter, withReuseIdentifier: "footerView", forIndexPath: indexPath) as UICollectionReusableView
		
		if let title : UILabel = footer.viewWithTag(1) as UILabel?			{
		title.removeFromSuperview()
		}
		footer.frame = CGRectMake(footer.frame.origin.x, footer.frame.origin.y, footer.frame.width  , 4 )
		footer.sizeThatFits(CGSize(width: 1000, height: 4))
		footer.backgroundColor=UIColor.grayColor()
		return footer
		}*/
		
		return UICollectionReusableView()
	}
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		if(self.IsPad()){
			
			return CGSizeMake(CamCollectionView2.frame.width  , 85 )
			
		}
			//IPHONE
		else{
			return CGSizeMake(CamCollectionView2.frame.width   , 65 )
			
		}
		
	}
	
	
	
	//HIGHLIGHT
	func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
		let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
		cell.contentView.backgroundColor = UIColor.arancioSelezione()
	}
	func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
		if(self.IsPad())
		{
			let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
			cell.contentView.backgroundColor = UIColor.sfondoChiaro()
		}
		else{
			let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
			cell.contentView.backgroundColor = UIColor.whiteColor()
		}
	}
	
	// CollectionView Delegate
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		if (indexPath.section == 0){ //cell cam
			//var selectedCam =  [Cam] (selectedImpianto!.devices.values)
			delegate?.callBackFromSelection(  cams[indexPath.row ]) //selectedCam[ indexPath.row ] )
			//	self.navigationController?.popViewControllerAnimated(true)
			
			if(self.IsPad()){
				self.navigationController?.popViewControllerAnimated(true)
				
			}
			else{
				if(isModifica){
					let vc = self.navigationController!.viewControllers [2] 
					self.navigationController?.popToViewController(  vc , animated: true)
				}
				else{
					let vc = self.navigationController!.viewControllers [1] 
					self.navigationController?.popToViewController(  vc , animated: true)
				}
				
			}
			
		}
	}
	
	
	
}












