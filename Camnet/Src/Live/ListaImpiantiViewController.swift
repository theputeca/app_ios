//
//  ListaImpiantiViewController.swift
//  Camnet
//
//  Created by giulio piana on 21/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation
import UIKit
//,UICollectionViewDelegate ,UICollectionViewDataSource
class ListaImpiantiViewController: UICollectionViewController   {
	
	
	var impianti : [Impianto] =  [Impianto] ()
	
	
	//	var collectionView :UICollectionView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		self.navigationItem.title = "impianti".loc()
		
		
		/*collectionView	= NSBundle.mainBundle().loadNibNamed("CVSelection", owner: self, options: nil)[0] as? UICollectionView
		collectionView.frame = CGRect(x: 0 , y: 0 , width: self.frame().width 	, height: self.frame().height)
		collectionView.backgroundColor = UIColor.whiteColor()
		
		self.view.addSubview(collectionView)
		collectionView.delegate = self
		collectionView.dataSource = self*/
		
		if(!self.IsPad())
		{
			self.collectionView!.backgroundColor = UIColor.whiteColor()
			let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
			layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
			self.collectionView!.setCollectionViewLayout(layout, animated: false, completion: nil	)
			
			layout.minimumInteritemSpacing = 0 //1.0
			layout.minimumLineSpacing = 0 //1.0
		}
		
		for i in DataManager.Instance.impianti
		{
			if( i.views_auto.count != 0 	) { impianti.append(i) }
			
		}
	}
	
	override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		collectionView?.reloadData()
	}
	
	
	override func viewWillAppear(animated: Bool) {
		collectionView?.reloadData()
		super.viewWillAppear(animated)
		self.tabBarController?.tabBar.hidden = false
		
		/*if(self.IsPad())
		{
		collectionView.frame = CGRect(x: 0, y:  self.navigationController!.navigationBar.frame.height  , width:  self.view.frame.width 	, height: self.view.frame.height)
		}
		else{//IPHONE
		collectionView.frame = CGRect(x: 0 , y: collectionView.frame.origin.y , width: self.view.frame.width	, height: self.view.frame.height)
		
		}
		collectionView?.reloadData()*/
	}
	
	//NUMBER OF ROW
	override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return impianti.count
	}
	
	//CELL
	override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		
		
		//get the cell
		
		if(self.IsPad()){
			let nib = UINib(nibName: "CollCellTitleDes", bundle: nil)
			collectionView.registerNib(nib, forCellWithReuseIdentifier: "CollCellTitleDes")
			let cell :UICollectionViewCell!  = collectionView.dequeueReusableCellWithReuseIdentifier("CollCellTitleDes", forIndexPath: indexPath) as? UICollectionViewCell
			
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, self.collectionView!.frame.width / 3 - 10 , 125)
			}
			else{
				cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, self.collectionView!.frame.width / 2 - 10 , 125 )
			}
			
			
			//arrotonda bordi
			cell.layer.masksToBounds = true;
			cell.layer.cornerRadius = 25;
			
			//set the title of impianto
			let title : UILabel = cell.viewWithTag(1) as! UILabel
			title.frame = CGRect (x: 0, y: 0, width: 50, height: 30)
			title.text = impianti[ indexPath.row ].name
			
			let desc : UILabel = cell.viewWithTag(3) as! UILabel
			desc.text = impianti[ indexPath.row ].description
			
			return cell;
		}
		else //Iphone
		{
			let nib = UINib(nibName: "CollCellTitleDesIphone", bundle: nil)
			collectionView.registerNib(nib, forCellWithReuseIdentifier: "CollCellTitleDesIphone")
			let cell :UICollectionViewCell!  = collectionView.dequeueReusableCellWithReuseIdentifier("CollCellTitleDesIphone", forIndexPath: indexPath) as? UICollectionViewCell
			
			cell.frame = CGRectMake(  cell.frame.origin.x  , cell.frame.origin.y, (collectionView.frame.width  ) ,65)
			
			cell.backgroundColor = UIColor.whiteColor()
			
			let border : CALayer = CALayer()
			border.borderColor = UIColor.sfondoChiaro().CGColor
			border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: cell.frame.size.height)
			border.borderWidth = 1
			cell.layer.addSublayer(border)
			
			//set the title of impianto
			let title : UILabel = cell.viewWithTag(1) as! UILabel
			title.frame = CGRect (x: 0, y: 0, width: 50, height: 30)
			title.text = impianti[ indexPath.row ].name
			
			let desc : UILabel = cell.viewWithTag(3) as! UILabel
			desc.text = impianti[ indexPath.row ].description
			
			return cell;
		}
		
	}
	
	//SIZE
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		
		
		if(self.IsPad()){
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				return CGSizeMake(self.collectionView!.frame.width / 3 - 10  ,125)
			}
			else{
				return CGSizeMake(self.collectionView!.frame.width / 2 - 10  , 125 )
			}
		}
		else{ // iphone
			return CGSizeMake((view.frame.width  )  , 65)
		}
		
	}
	
	//HIGHLIGHT
	override func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
		let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
		cell.contentView.backgroundColor = UIColor.arancioSelezione()
	}
	
	override func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
		if(self.IsPad())
		{
			let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
			cell.contentView.backgroundColor = UIColor.sfondoChiaro()
		}
		else{
			let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
			cell.contentView.backgroundColor = UIColor.whiteColor()
		}
	}
	
	//delegate
	
	override func  collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
	{
		let visteViewController = self.storyboard?.instantiateViewControllerWithIdentifier("visteViewController") as!  VisteViewController
		visteViewController.visteList = [Vista ] ( impianti[indexPath.row].views_auto.values )
		visteViewController.currentVista = visteViewController.visteList[0]
		navigationController?.pushViewController(visteViewController , animated: true) //showViewController(visteViewController, sender: nil)
	}
	
	
}





























