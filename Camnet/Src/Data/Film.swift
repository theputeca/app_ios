//
//  Evento.swift
//  Camnet
//
//  Created by giulio piana on 16/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation


class Film{
	
	
	var startTime :String
	var endTime : String
	var videoUrl : String
	
	var isvoidVideo = false;
	
	//
	
	var dataFrom : NSDate  {
		get  { return  NSDate(timeIntervalSince1970: startTimeTs) }
	}
	var dataTo : NSDate {
		get  { return  NSDate(timeIntervalSince1970: endTimeTS) }
	}
	
	var dataFromString : String  {
		get  {
		
			let dateFormatter: NSDateFormatter = NSDateFormatter()
			dateFormatter.dateFormat = "MM-dd-yyyy HH-mm"
			return "\(dateFormatter.stringFromDate(dataFrom))"
		}
	}
	var dataToString : String  {
		get  {
			let dateFormatter: NSDateFormatter = NSDateFormatter()
			dateFormatter.dateFormat = "MM-dd-yyyy HH-mm"
			return "\(dateFormatter.stringFromDate(dataTo))"
		}
	}
	
	var startTimeTs : NSTimeInterval  {
		get  { return  (startTime as NSString).doubleValue}
	}
	var endTimeTS : NSTimeInterval  {
		get { return (endTime as NSString).doubleValue }
	}
	
	init ()
	{
		startTime = ""
		endTime = ""
		videoUrl = ""
	}
	
	init (_startTime: String , _endTime:String , _videoUrl : String)
	{
		startTime = _startTime
		endTime = _endTime
		videoUrl = _videoUrl
	}
	
}