//
//  MyView.swift
//  Camnet
//
//  Created by giulio piana on 02/02/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation
class PlayerLiveGrandeVC: UIViewController , UIScrollViewDelegate,UIWebViewDelegate,VLCMediaPlayerDelegate,VLCMediaDelegate {
	
	
	
	
	var selectedCam : Cam?
	var urlViedeo : String = ""
	
	var commView : LiveCommandView!
	var isPtz : Bool = false
	var ptzView : UIWebView?
	
//	var player : VKPlayerController = VKPlayerController();

    var player : VLCMediaPlayer?
    var playerview : UIView?

	var actInd : UIActivityIndicatorView?

    var playerWebView : UIWebView? = nil
    var isWebView : Bool = false
	var   _timerDuration : NSTimer? = nil
	var _liveStreamDurationTimer : NSTimer? = nil
	
	var scrollview : UIScrollView = UIScrollView()
	////////////////
	
	//chiamate audiovideo
	
	var canCallVideoRecComm : Bool = true
	var canCallAudioRecComm : Bool = false
	
	
	
	override func viewDidLoad() {


        UIApplication.sharedApplication().idleTimerDisabled = false
        UIApplication.sharedApplication().idleTimerDisabled = true
        
		self.title = selectedCam!.name  //"eventi".loc()
		
		//crea player
		
     //creo una scrollvie per gestire lo zoom

        let aggiornaBtn = UIBarButtonItem(title: "aggiorna".loc() , style: UIBarButtonItemStyle.Plain , target: self, action: Selector("aggiornaClick:"))
        self.navigationItem.rightBarButtonItem = aggiornaBtn

		self.automaticallyAdjustsScrollViewInsets = false
		
		scrollview = UIScrollView()
		scrollview.backgroundColor =   UIColor.sfondoScuro()  //UIColor.greenColor() 	//
		
		self.view.addSubview(scrollview);

		scrollview.delegate = self
		scrollview.bounces=true
		scrollview.bouncesZoom=true
		
		let tapGesture :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onPlayerTapped:")
		tapGesture.numberOfTapsRequired = 1
		scrollview.addGestureRecognizer(tapGesture)
		
        self.view.backgroundColor = UIColor.sfondoScuro()
		
		//comandi
		if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
			commView = LiveCommandView(frame: CGRect (x: self.frame().width - 66 , y:  self.frameBetweenBar().origin.y , width: 66, height: self.frameBetweenBar().height ))
			self.view.addSubview(commView)
		}
		else{
			commView = LiveCommandView(frame: CGRect (x: 0 , y: self.frame().height - 66 , width: self.frame().width , height: 66 ))
			self.view.addSubview(commView)
		}
		
		commView.audioBtn.addTarget(self, action: Selector("btnCommandAudio:"), forControlEvents: UIControlEvents.TouchUpInside)
		commView.videoBtn.addTarget(self, action: Selector("btnCommandVideo:"), forControlEvents: UIControlEvents.TouchUpInside)
		commView.ptzBtn.addTarget(self, action: Selector("BtnPtzClick:"), forControlEvents: UIControlEvents.TouchUpInside)
		commView.lightBtn.addTarget(self, action: Selector("btnCommandLight:"), forControlEvents: UIControlEvents.TouchUpInside)
		commView.extBtn.addTarget(self, action: Selector("btnCommandEst:"), forControlEvents: UIControlEvents.TouchUpInside)
		
		commView.infoBtn.addTarget(self, action: Selector("btnCommandInfo:"), forControlEvents: UIControlEvents.TouchUpInside)
		commView.mappaBtn.addTarget(self, action: Selector("btnCommandMappa:"), forControlEvents: UIControlEvents.TouchUpInside)
		commView.storicoBtn.addTarget(self, action: Selector("btnCommandStorico:"), forControlEvents: UIControlEvents.TouchUpInside)
		commView.volumeBtn.addTarget(self, action: Selector("btnCommandVolume:"), forControlEvents: UIControlEvents.TouchUpInside)
		commView.zoomBtn.addTarget(self, action: Selector("btnCommandZoom:"), forControlEvents: UIControlEvents.TouchUpInside)
		
        
        
        let instal = DataManager.Instance.installs[  selectedCam!.install! ]
        
        if (  instal!.isPro == "false"  ){
            commView.storicoBtn.enable(false)
            commView.mappaBtn.enable(false)
        }else{
            commView.storicoBtn.enable(true)
            commView.mappaBtn.enable(true)
        }
        


     //   controllo audio
		if( selectedCam?.recordingAudioOn == nil)
        {
			commView.audioBtn.enable(false)
            commView.volumeBtn.enable(false)
		}
        else
        {
			commView.audioBtn.enable(true)
			commView.volumeBtn.enable(true)

			if(selectedCam!.status["audio"]  == "off")
            {
				commView.audioBtn.acceso(false)
                audioOn = false
                commView.volumeBtn.acceso(false)
			}
			else
            {
				commView.audioBtn.acceso(true)
                audioOn = false
                commView.volumeBtn.acceso(false)
			}

		}
		
		if( selectedCam?.recordingVideoOn == nil)
        {
			commView.videoBtn.enable(false)
		}
        else
        {
			commView.videoBtn.enable(true)
			
			if(selectedCam!.status["video"]  == "off")
            {
				commView.videoBtn.acceso(false)
					canCallAudioRecComm = false
			}
			else
            {
				commView.videoBtn.acceso(true)
					canCallAudioRecComm = true
			}
			
		}
		
		if( selectedCam?.viewPtz == nil)
        {
			commView.ptzBtn.enable(false)
		}else
        {
			commView.ptzBtn.enable(true)
		}
		
		if( selectedCam?.lightOn == nil){
			commView.lightBtn.enable(false)
		}
        else
        {
			commView.lightBtn.enable(true)
		}
		
		if( selectedCam?.externalCommand == nil){
			commView.extBtn.enable(false)
		}
        else
        {
			commView.extBtn.enable(true)
		}
		
		if(DataManager.Instance.installs [ selectedCam!.install! ]!.maps[ selectedCam!.mapId ] != nil)
		{
			commView.mappaBtn.enable(true)
		}
		else
        {
			commView.mappaBtn.enable(false)
		}
		
	}
	

    func aggiornaClick(sender : UIBarButtonItem ){


    if(!isWebView)
    {
        if(self.player != nil)
        {
           self.player?.stop()
            playerview?.removeFromSuperview()
            self.player = nil
        }

     initializePlayer()

        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){//landscape

            var barHeight :CGFloat = 66
            if( IsPad() )
            {
                barHeight  = 66
            }
            else{
                barHeight = 124
            }

        playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight , height:  self.frameBetweenBar().height )
        let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
        scrollview.contentSize = scrollSize
        }
        else{
            playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
            let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
            scrollview.contentSize = scrollSize
        }
        actInd!.center = playerview!.center
    }
    else{ // web View
        playerWebView?.reload()
    }

}



    func initializePlayer()
	{

        if(selectedCam?.urlVideo_1 != nil ) // METTO PLEYER
        {

		urlViedeo = selectedCam!.urlVideo_1!

		// inizializza il player
		let arr = ["--extraintf=" ] as NSArray //"--avformat-format=mxg"
        player = VLCMediaPlayer(options: arr as [AnyObject])
        playerview = UIView()

         playerview!.tag = 1
        self.player?.delegate = self
        self.player?.drawable = playerview!


            // crea il playere
            print("video player granda")
            print(urlViedeo)

            let nsurl = NSURL(string: urlViedeo)
            let media = VLCMedia (URL: nsurl)
            media.synchronousParse()

            self.player?.media = media
           // player?.media.delegate = self

            //setta parametri del media tipo cahce
            if ( urlViedeo.rangeOfString("mjpg") != nil )
            {
                media.addOptions(mediaOptionDictionary())
                liveStreamTimer()
            }
            else
            {
                media.addOptions(mediaOptionDictionaryRtsp() )
            }

            self.player?.rate = 0
            self.player?.setDeinterlaceFilter("blend")  ;
            player?.audio?.muted = true
            player!.play();


            playerview!.userInteractionEnabled = true
            let tapGesture :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onPlayerTapped:")
            tapGesture.numberOfTapsRequired = 1
            playerview!.addGestureRecognizer(tapGesture)
            scrollview.addSubview(playerview!)

		// loading indicator
        if(actInd != nil )
        {
            actInd?.removeFromSuperview()
        }
		actInd  = UIActivityIndicatorView(frame: playerview!.frame)
		actInd!.center = playerview!.center
		actInd!.hidesWhenStopped = true
		actInd!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
		actInd!.color = UIColor.arancio()
		actInd!.startAnimating()
		actInd!.alpha = 1
        actInd?.userInteractionEnabled = false
		playerview!.addSubview(actInd!)

        playerview!.bringSubviewToFront(actInd!)

        }
        else if (selectedCam?.webViewVideoVideo_1 != nil){ // METTO WEB VIEW

              let url =  selectedCam?.webViewVideoVideo_1!
            isWebView = true

            playerWebView  = UIWebView()
            playerWebView!.backgroundColor = UIColor.sfondoPlayer()
            playerWebView!.userInteractionEnabled = false
            //LOAD URL

            let urlR = NSURL(string: url!	)
            let request = NSURLRequest(URL: urlR!)
            playerWebView!.loadRequest(request)
            playerWebView!.delegate = self
            scrollview.addSubview(playerWebView!)

            // loading indicator
            actInd  = UIActivityIndicatorView(frame: view.frame)
            actInd!.center = playerWebView!.center
            actInd!.hidesWhenStopped = true
            actInd!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
            actInd!.color = UIColor.arancio()
            actInd!.startAnimating()
            actInd!.alpha = 0
            view.addSubview(actInd!)
        }
	}

    override func viewDidAppear(animated: Bool) {


		initializePlayer()

		self.automaticallyAdjustsScrollViewInsets = false
		if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){//landscape
			
			var barHeight :CGFloat = 66
			if( IsPad() )
			{
				 barHeight  = 66
			}
			else{
				 barHeight = 124
			}
			
			commView.frame = CGRect (x: self.view.frame.width - barHeight , y:  self.frameBetweenBar().origin.y , width: barHeight, height: self.frameBetweenBar().height )
            scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width - barHeight , height: self.view.frame.height)

            if(!isWebView){
                playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight , height:  self.frameBetweenBar().height )
                let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
                scrollview.contentSize = scrollSize
            }
            else{
                playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight , height:  self.frameBetweenBar().height )
                let scrollSize = CGSize(width: playerWebView!.frame.size.width , height: playerWebView!.frame.size.height)
                scrollview.contentSize = scrollSize
            }
		}
		else{//PROTRAIT
			
			var barHeight :CGFloat = 66
			if( IsPad() )
			{
				barHeight  = 66
			}
			else{
				barHeight = 124
			}

            commView.frame =  CGRect (x: 0 , y: self.view.frame.height - barHeight , width: self.view.frame.width , height: barHeight )
			scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width, height: self.view.frame.height )
			


            if(!isWebView){
                playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
                scrollview.contentSize = scrollSize
            }
            else{
                playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                let scrollSize = CGSize(width: playerWebView!.frame.size.width , height: playerWebView!.frame.size.height)
                scrollview.contentSize = scrollSize

            }
		}
		
		
		let scrollViewFrame = scrollview.frame
		let scaleWidth = scrollViewFrame.size.width / scrollview.contentSize.width
		let scaleHeight = scrollViewFrame.size.height / scrollview.contentSize.height
		let minScale = min(scaleHeight, scaleWidth)
		
		scrollview.minimumZoomScale = minScale
		scrollview.maximumZoomScale = 10
		scrollview.zoomScale = minScale
		
        if(!isWebView){
            setplayerAspectRatio(player!)

            actInd!.center = playerview!.center
        }
        else{
             actInd!.center = playerWebView!.center
        }

   
	//	commView.rotate()
        commView.rotate()
	}
	
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
		
		scrollview.zoomScale = 1
		var barHeight :CGFloat = 66
		if( IsPad() )
		{
			barHeight  = 66
		}
		else{
			barHeight = 124
		}
		
		let ptzDim : CGFloat = 180
		//se non e ptz
		if(!isPtz){
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){//landscape
				
				commView.frame = CGRect (x: self.view.frame.width - barHeight , y:  self.frameBetweenBar().origin.y , width: barHeight, height: self.frameBetweenBar().height )
				

				scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width - barHeight , height: self.view.frame.height)
				
				if(ptzView != nil){
					self.ptzView!.frame = CGRect(x: self.view.frame.width  , y:  self.frameBetweenBar().origin.y  , width: ptzDim, height: self.view.frame.height)
				}


                if(!isWebView){
                        playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight , height:  self.frameBetweenBar().height )
                        let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
                        scrollview.contentSize = scrollSize
                }
                else{
                    playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight , height:  self.frameBetweenBar().height )
                    let scrollSize = CGSize(width:  playerWebView!.frame.size.width , height:  playerWebView!.frame.size.height)
                    scrollview.contentSize = scrollSize
                }

				
				
			}
			else{
				
				commView.frame =  CGRect (x: 0 , y: self.view.frame.height - barHeight , width: self.view.frame.width , height: barHeight )
				scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width, height: self.view.frame.height )// self.view.frame.width * 3 / 4 )
				if( ptzView != nil){
					self.ptzView!.frame = CGRect(x: 0  , y:  self.view.frame.height   , width: self.view.frame.width , height: ptzDim )
				}

                if(!isWebView){
                        playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
				
                        let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
                        scrollview.contentSize = scrollSize
                }
                else{
                     playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )

                    let scrollSize = CGSize(width:  playerWebView!.frame.size.width , height:  playerWebView!.frame.size.height)
                    scrollview.contentSize = scrollSize
                }
				
			}
		}
			//se e ptz
		else
		{
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				
			self.commView.frame = CGRect (x: self.view.frame.width - barHeight , y:  self.frameBetweenBar().origin.y , width: barHeight, height: self.frameBetweenBar().height )
				self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width - barHeight - ptzDim , height: self.view.frame.height)
				
				if( ptzView != nil){
					self.ptzView!.frame = CGRect(x: self.view.frame.width - ptzDim - barHeight , y:  self.frameBetweenBar().origin.y  , width: ptzDim, height: self.view.frame.height)
				}

                if(!isWebView){
                    self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight - ptzDim , height: self.view.frame.height)
				
                    let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                    self.scrollview.contentSize = scrollSize

                }
                else{
                     self.playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight - ptzDim , height: self.view.frame.height)

                    let scrollSize = CGSize(width: self.playerWebView!.frame.size.width , height: self.playerWebView!.frame.size.height)
                    self.scrollview.contentSize = scrollSize                }
				
			}
			else{
				
				self.commView.frame =  CGRect (x: 0 , y: self.view.frame.height - barHeight  , width: self.view.frame.width , height: barHeight )
				

				scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width, height: self.view.frame.height )// self.view.frame.width * 3 / 4 )
				
				if( ptzView != nil){
					self.ptzView!.frame = CGRect(x: 0  , y:  self.view.frame.height  - ptzDim - barHeight  , width: self.view.frame.width , height: ptzDim )
				}

                if(!isWebView){
                    playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                    let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
                    scrollview.contentSize = scrollSize

                }
                else{
                    playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                    let scrollSize = CGSize(width: playerWebView!.frame.size.width , height: playerWebView!.frame.size.height)
                    scrollview.contentSize = scrollSize
                }
				
			}
		}
		
		let scrollViewFrame = scrollview.frame
		let scaleWidth = scrollViewFrame.size.width / scrollview.contentSize.width
		let scaleHeight = scrollViewFrame.size.height / scrollview.contentSize.height
		let minScale = min(scaleHeight, scaleWidth)
		
		scrollview.minimumZoomScale = minScale
		scrollview.maximumZoomScale = 10
		scrollview.zoomScale = minScale
		
        if(!isWebView){
                actInd!.center = playerview!.center
                setplayerAspectRatio(player!)
        }
        else{
                actInd!.center = playerWebView!.center
        }
		
		commView.rotate()
		
	
	}
	
	//bottoni
	
	var audioOn :Bool = false
	
	func btnCommandVideo(sender: UIButton) {
		
		
	
		print("comando video CLICk")
		
		if( !canCallVideoRecComm ) {
		
			//self.alertMessage("", message: "msgCallDisable".loc(), button: "ok")
			
			return
		}
		
		commView.addLoadingOnButtonVideo()
		
		print("comando puo fare comando video")
		
		canCallVideoRecComm = false
		canCallAudioRecComm = false
		
		if(selectedCam!.status["video"] == "on") {
			callApiAudioVideo(selectedCam!.recordingVideoOff!)
		}
		else{
			callApiAudioVideo(selectedCam!.recordingVideoOn!)
		}
	}
	
	func btnCommandAudio(sender: UIButton) {
		
		print("comando CLICk")
		
		if( !canCallAudioRecComm && canCallVideoRecComm ){ 
			self.alertMessage("", message: "msgCallDisable".loc(), button: "ok")
			return
		}
		
		commView.addLoadingOnButtonAudio()
		
		canCallAudioRecComm = false
		canCallVideoRecComm = false

		if(selectedCam!.status["audio"] == "on") {
			callApiAudioVideo( selectedCam!.recordingAudioOff! )
		}
		else{
			callApiAudioVideo( selectedCam!.recordingAudioOn! )
		}

	}
	
	func BtnPtzClick(sender: UIButton) { //btn7
		print("PTZ CLICk")
		showPtz( sender )
	}
	
	func btnCommandLight(sender: UIButton) {
		print("comando CLICk")
		
		if(selectedCam!.lightOn != nil )
		{
			if(selectedCam!.status["luce"] == "on") {
				
				callApi( selectedCam!.lightOff! )
				selectedCam!.status["luce"] = "off"
				
				commView.lightBtn.acceso(false)
			}
			else{
				callApi( selectedCam!.lightOn! )
				selectedCam!.status["luce"] = "on"
				
				commView.lightBtn.acceso(true)
			}
		}
		/*no return*/
	}
	
	func btnCommandEst(sender: UIButton) {
		print("comando CLICk")
		
		/*no return*/
	}
	
	func btnCommandInfo(sender: UIButton) {
		print("info CLICk")
		
		
		showInfoView()
		
		/*no return*/
	}
	
	func btnCommandStorico(sender: UIButton) {
		print("storico CLICk")
		
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		let storicoPlayerController : StoricoPlayerVC = storyboard.instantiateViewControllerWithIdentifier("StoricoPlayerVC") as! StoricoPlayerVC
		
		let  cam  = selectedCam! //  [Cam](selectedImpianto!.devices.values) [indexPath.row ]
		
		storicoPlayerController.selectedCamId = cam.id
		storicoPlayerController.selectedCam = cam
		self.navigationController?.pushViewController(storicoPlayerController, animated: true)
	}
	
	func btnCommandMappa(sender: UIButton) {
		print("mappa CLICk" + selectedCam!.mapId)
		
		
		if(selectedCam!.mapId != "" )
		{
			let storyboard = UIStoryboard(name: "Main", bundle: nil)
			
			let detailMap = storyboard.instantiateViewControllerWithIdentifier("detailMappa") as! DetailMappaVC

			let imp  = DataManager.Instance.installs [ selectedCam!.install! ]
			
			let  map   =  imp!.maps[ selectedCam!.mapId ]
			detailMap.selectedMap = map
			detailMap.selectedImpianto = imp
			detailMap.selectedCamId  = selectedCam!.id
			navigationController?.pushViewController(detailMap, animated: true)
		}
		

	}
	
	func btnCommandVolume(sender: UIButton) {
		print("volume CLICk")
		
		/*no return*/
		if(audioOn)
		{
			audioOn = false
            player?.audio?.muted=true
		}
		else{
			audioOn = true
            player?.audio?.muted=false
		}
		
		commView.volumeBtn.acceso(audioOn)
		
		
	}
	
	func btnCommandZoom(sender: UIButton) {
		print("zoom CLICk")
		
		setTabBarVisible(false, animated: true)
		

	}
	
	
	
	
	
	func showPtz( sender : UIButton ) {
		
		
		var barHeight :CGFloat = 66
		if( IsPad() )
		{
			barHeight  = 66
		}
		else{
			barHeight = 124
		}
		
		let ptzdim :CGFloat = 180
		if(!isPtz)
		{
			let cgrect = self.view.convertRect(sender.frame, fromView: sender)
			
			ptzView = UIWebView()
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				ptzView!.frame =  CGRect(x: self.view.frame.width  , y:  self.frameBetweenBar().origin.y  , width: ptzdim, height: self.view.frame.height)
			}else{
				self.ptzView!.frame = CGRect(x: 0  , y:  self.view.frame.height     , width: self.view.frame.width , height: ptzdim )
			}
			self.view.addSubview(ptzView!)
			self.view.bringSubviewToFront(ptzView!)
            self.view.bringSubviewToFront(self.commView!)

			ptzView!.backgroundColor = UIColor.sfondoScuro()

            ptzView!.tag=10
            ptzView!.delegate=self

			/*crea webview */
			
			let url = NSURL(string: selectedCam!.viewPtz!	)
			let request = NSURLRequest(URL: url!)
			ptzView!.loadRequest(request)
			
			isPtz = true
			
			//
			
			UIView.animateWithDuration(0.4, animations: { () -> Void in
				
				if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){

                    self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width - barHeight - 1 , height: self.view.frame.height)

                    if(!self.isWebView){
                        self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight - ptzdim , height: self.view.frame.height)
                        let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                        self.scrollview.contentSize = scrollSize

                    }
                    else{
                        self.playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight - ptzdim , height: self.view.frame.height)
                        let scrollSize = CGSize(width: self.playerWebView!.frame.size.width , height: self.playerWebView!.frame.size.height)
                        self.scrollview.contentSize = scrollSize
                        
                    }

                    //PTZ
                       self.ptzView!.frame = CGRect(x: self.view.frame.width - ptzdim - barHeight  , y: self.frameBetweenBar().origin.y   , width: ptzdim, height: self.view.frame.height)
				}
				else{
					
					self.ptzView!.frame = CGRect(x: 0  , y:  self.view.frame.height  - ptzdim  - barHeight , width: self.view.frame.width , height: ptzdim )
					
				}
				
				return
				}, completion: { (bool) -> Void in
					self.commView.rotate()
			})
			
		}
		else{
			isPtz = false
			
			UIView.animateWithDuration(0.4, animations: { () -> Void in
				
				if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
                    self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width - barHeight  , height: self.view.frame.height)

                    if(!self.isWebView){
                        self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight  , height: self.view.frame.height)

                        let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                        self.scrollview.contentSize = scrollSize
                    }
                    else{
                        self.playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight  , height: self.view.frame.height)

                        let scrollSize = CGSize(width: self.playerWebView!.frame.size.width , height: self.playerWebView!.frame.size.height)
                        self.scrollview.contentSize = scrollSize
                    }


                    //ptz
					self.commView.frame = CGRect (x: self.view.frame.width - barHeight  , y:  self.frameBetweenBar().origin.y , width: barHeight, height: self.frameBetweenBar().height )
					self.ptzView!.frame = CGRect(x: self.view.frame.width  , y:  self.frameBetweenBar().origin.y  , width: ptzdim, height: self.view.frame.height)


				}
				else{
					
					self.commView.frame =  CGRect (x: 0 , y: self.view.frame.height - barHeight  , width: self.view.frame.width , height: barHeight )
					self.ptzView!.frame = CGRect(x: 0  , y:  self.view.frame.height   , width: self.view.frame.width , height: ptzdim )
					
				}
				
				return
				}, completion: { (bool) -> Void in
					
					self.ptzView!.removeFromSuperview()
					
			})
		}

        if(!isWebView){
            setplayerAspectRatio(player!)
        }
	}
	
	func callApi(urlStr: String)	{
		
		let url = NSURL(string: urlStr )!
		
		print( url )
		
		let request1 = NSMutableURLRequest(URL: url )
		var session1 = NSURLSession.sharedSession()
		request1.HTTPMethod = "GET"
		
		let request = NSURLRequest(URL: url)
		let connection = NSURLConnection(request: request, delegate:nil, startImmediately: true)
		
		
		
	}
	
	func callApiAudioVideo(urlStr: String)	{
		
		let url = NSURL(string: urlStr )!

		print( url )
		

	//	dispatch_async(dispatch_get_main_queue(), {
		
		let request1 = NSMutableURLRequest(URL: url )
		let session1 = NSURLSession.sharedSession()
		request1.HTTPMethod = "GET"
		
		let task = session1.dataTaskWithRequest(request1, completionHandler: {data, response, error -> Void in
			
			
			print(response)

			//var error: NSError?
			do {
				let xmlDoc = try AEXMLDocument(xmlData: data!)
				
				print(xmlDoc.xmlString)
			
				if let result =  xmlDoc.root["result"].value as String? {
					
					
					print(result)
					
					if (result == "false"){
						//errore comando fail
					}
					else{
						
						dispatch_async(dispatch_get_main_queue(), {
						self.canCallVideoRecComm = true

						self.commView.RemoveLoadingOnButton()
						
						if let video =  xmlDoc.root["data"]["video"].value as String? {
							
							print(video)
							if(video == "1")
							{
								self.canCallAudioRecComm = true
								
								self.selectedCam!.status["video"] = "on"			
								self.commView.videoBtn.acceso(true)
								
							}
							else{
								self.canCallAudioRecComm = false
								
								self.selectedCam!.status["video"] = "off"			
								self.commView.videoBtn.acceso(false)
							}
						}
						
						if let audio =  xmlDoc.root["data"]["audio"].value as String? {
							print(audio)
												
							if(audio == "1")
							{
								self.selectedCam!.status["audio"] = "on"
								self.commView.audioBtn.acceso(true)
                                //self.commView.volumeBtn.acceso(true)
							}
							else{
								self.selectedCam!.status["audio"] = "off"
								self.commView.audioBtn.acceso(false)
                               // self.commView.volumeBtn.acceso(false)
							}
							
							
						}
						})
					}
				}
				
			} catch _ {
			}
		})
			
				task.resume()
			//self.removeLoading()
		//})
		
	}
	



    func mediaMetaDataDidChange(aMedia: VLCMedia!)
    {

        let  currentstate : VLCMediaState = player!.media.state
        switch (currentstate.rawValue)
        {
        case  VLCMediaState.Error.rawValue:
            print("media state error")
        case  VLCMediaState.Buffering.rawValue:
            print("media state buffering")
        case  VLCMediaState.Playing.rawValue:
            print("media state playng")
        default:print("media default")

        }
    }

    func liveStreamTimer(){
        _liveStreamDurationTimer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("liveStreamDurationFired:" ), userInfo: nil, repeats: true	)
    }

    func liveStreamDurationFired (timer : NSTimer) {

        UIApplication.sharedApplication().idleTimerDisabled = false
        UIApplication.sharedApplication().idleTimerDisabled = true



            player?.gotoNextFrame()

    }


// VKPLAYER DELEGATE
    func setplayerAspectRatio( player: VLCMediaPlayer )
    {
     /* if   let rect : CGRect = playerview?.frame
      {

        let h = (rect.height / rect.height) * 100
        let w = (rect.width / rect.height) * 100
        let ratio = "\( Int (w) ):\(  Int(h) )"

        CriptString.setaspectRatio(player , ratio)
        }*/
    }

    func mediaOptionDictionary()-> [NSObject:AnyObject]
    {
        let dict = ["network-caching" :  1111 ]// , "audio-time-stretch" : "0" ,"subsdec-encoding":"Windows-1252","avcodec-skiploopfilter":3 ];
        return dict
    }
    func mediaOptionDictionaryRtsp()-> [NSObject:AnyObject]
    {
        let dict = ["network-caching" :  2111 ]// , "audio-time-stretch" : "0" ,"subsdec-encoding":"Windows-1252","avcodec-skiploopfilter":3 ];
        return dict
    }

    func mediaOptionDictionary(netCache : Int )-> [NSObject:AnyObject]
    {

        let dict = ["network-caching" : netCache ] ; //, "audio-time-stretch" : "0",  "subsdec-encoding" : "Windows-1252" ,  "avcodec-skiploopfilter" : 1
        return dict as [NSObject : AnyObject]
    }



    func mediaPlayerStateChanged(aNotification: NSNotification!)
    {
        let  currentstate : VLCMediaPlayerState = player!.state
        switch (currentstate.rawValue)
        {

        case  VLCMediaPlayerState.Error.rawValue  :
            print("player error " )
            self.removeLoading()
            self.alertMessage("ErrorTitle".loc(), message: "ErrorMessage".loc(), button: "annulla".loc())

        case  VLCMediaPlayerState.Buffering.rawValue  :
           print("player bufering " )


             buffertimer()


            if(audioOn)
            {
                player?.audio?.muted=false
            }
            else{
                player?.audio?.muted=true
            }

        case  VLCMediaPlayerState.Playing.rawValue  :
            print("player playng " )


            //actInd!.stopAnimating()

            if(audioOn)
            {
                player?.audio?.muted=false
            }
            else{
                player?.audio?.muted=true
            }


        case VLCMediaPlayerState.Ended.rawValue:
            print("player ended")

        case VLCMediaPlayerState.Paused.rawValue :
            print ("player paused")

        case VLCMediaPlayerState.Stopped.rawValue :
            print("player stoppeds " )

            print("Erore - non sono riuscito a eseguire il video hd uso quello piu piccolo" )
            urlViedeo = selectedCam!.urlVideo_4!


            let nsurl = NSURL(string: urlViedeo)
            let media = VLCMedia (URL: nsurl)
            self.player?.media = media
            self.player?.play()

        default:print("default")
        }
    }



    var buffernumber : Int = 2
    var currentBufferNum : Int = 0
    var isPlayengFirstTime:Bool = false
    func buffertimer(){
        actInd!.startAnimating()
      //  self.addLoading()
        if( _timerDuration == nil  )
        {
         //   self.addLoading()
            _timerDuration = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("onTimerDurationFired:" ), userInfo: nil, repeats: true	)
        }
        else{
            if ( _timerDuration!.valid )
            {
                _timerDuration!.invalidate()
            }
            _timerDuration = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: Selector("onTimerDurationFired:" ), userInfo: nil, repeats: true	)

        }
    }

    func onTimerDurationFired (timer : NSTimer) {
        let  currentstate : VLCMediaState = (player?.media!.state)!
        if(currentstate != VLCMediaState.Buffering  )
        {
            currentBufferNum++
            if(currentBufferNum >=  buffernumber){
                            actInd!.stopAnimating()
            }

           // self.removeLoading()
            //stop timer
            if( _timerDuration != nil  )
            {
                if ( _timerDuration!.valid )
                {
                    _timerDuration!.invalidate()
                }
                _timerDuration = nil;
            }
        }
    }

	
	override func viewWillDisappear(animated: Bool) {
		super.viewWillDisappear(animated)

        if(!isWebView){
            player!.stop()
            player!.delegate = nil
            player = nil
		}
		if( _timerDuration != nil  )
		{
			if ( _timerDuration!.valid ) {
				_timerDuration!.invalidate()
			}
		}
		
		_timerDuration = nil;
		
	}
	
	
	
	//SCROLL VIEW DELEGATE
	
	func centerScrollViewContents(){
		
	}
	
	func scrollViewDidZoom(scrollView: UIScrollView) {
		centerScrollViewContents()
	}
	
	func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {

        if(!isWebView){

                return playerview!

        }
        else{
            return playerWebView
        }
       
	}
	
	func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat) {
		if(scale == 1)
		{
			print("EndZoom")
		}
	}
	
	
	/**/
	
	///
	func onPlayerTapped(sender:UITapGestureRecognizer)	{
		toggleBarVisible()
	}
	
	func  toggleBarVisible()	{
		setTabBarVisible(!tabBarIsVisible(), animated: true)
	}
	
	func hideBar()	{
		setTabBarVisible(false, animated: true)
	}
	
	func setTabBarVisible(visible:Bool, animated:Bool) {
		
		// bail if the current state matches the desired state
		if (tabBarIsVisible() == visible) { return }
		

		
		let frame2 = self.navigationController?.navigationBar.frame
		let height2 = frame2!.size.height 
		
		
		let duration2:NSTimeInterval = (animated ? 0.3 : 0.0)
		
		var barHeight :CGFloat = 66
		if( IsPad() )
		{
			barHeight  = 66
		}
		else{
			barHeight = 124
		}
		let ptzdim :CGFloat = 180
		
		if (visible){ // VISIbile
			UIView.animateWithDuration(duration2, animations: { () -> Void in
				self.navigationController?.navigationBar.frame = CGRectOffset(frame2!, 0, height2)
				
				if(!self.isPtz){
				if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){//landscape
					
					self.commView.frame = CGRect (x: self.view.frame.width - barHeight , y:  self.frameBetweenBar().origin.y , width: barHeight, height: self.frameBetweenBar().height )
					self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width - barHeight , height: self.view.frame.height)
					
					if(self.ptzView != nil){
						self.ptzView!.frame = CGRect(x: self.view.frame.width  , y:  self.frameBetweenBar().origin.y  , width: ptzdim, height: self.view.frame.height)
					}
				}
				else{
					self.commView.frame =  CGRect (x: 0 , y: self.view.frame.height - barHeight , width: self.view.frame.width , height: barHeight )
						self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width, height: self.view.frame.height )// self.view.frame.width * 3 / 4 )
					
					if( self.ptzView != nil){
						self.ptzView!.frame = CGRect(x: 0  , y:  self.view.frame.height   , width: self.view.frame.width , height: ptzdim )
					}
					}
				}
				else{ //ptz
					if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
						
						self.commView.frame = CGRect (x: self.view.frame.width - barHeight  , y:  self.frameBetweenBar().origin.y , width: barHeight, height: self.frameBetweenBar().height )
						self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width - barHeight - ptzdim , height: self.view.frame.height)
						
						if( self.ptzView != nil){
							self.ptzView!.frame = CGRect(x: self.view.frame.width - ptzdim - barHeight  , y:  self.frameBetweenBar().origin.y  , width: ptzdim, height: self.view.frame.height)
						}
						
					}
					else{
						
						self.commView.frame =  CGRect (x: 0 , y: self.view.frame.height - barHeight  , width: self.view.frame.width , height: barHeight )
						self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width, height: self.view.frame.height )// self.view.frame.width * 3 / 4 )
						
						if( self.ptzView != nil){
							self.ptzView!.frame = CGRect(x: 0  , y:  self.view.frame.height  - ptzdim  - barHeight , width: self.view.frame.width , height: ptzdim )
						}

					}
				}
				
				
				return
				}, completion: { (bool) -> Void in
					self.scrollview.zoomScale = 1
					
					var ptzDim : CGFloat = 0
					if(self.isPtz){
						ptzDim = 150
					}
					if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){//landscape

                        if(!self.isWebView){
                                self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight - ptzDim, height:  self.frameBetweenBar().height )
                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize

                        }
                        else{
                            self.playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight - ptzDim, height:  self.frameBetweenBar().height )
                            let scrollSize = CGSize(width: self.playerWebView!.frame.size.width , height: self.playerWebView!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }


						
						
					}
					else{

                        if(!self.isWebView){
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize

                        }
                        else{
                            self.playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                            let scrollSize = CGSize(width: self.playerWebView!.frame.size.width , height: self.playerWebView!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }
						
						
					}
					
		
					
			})
			
		}
		else{ // NON VISIBILE
			UIView.animateWithDuration(duration2, animations: { () -> Void in
				self.navigationController?.navigationBar.frame = CGRectOffset(frame2!, 0, -height2)
				
				if(!self.isPtz){
				
					if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){//landscape
						
						self.commView.frame = CGRect (x: self.view.frame.width  , y:  self.frameBetweenBar().origin.y , width: barHeight, height: self.frameBetweenBar().height )
						self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width  , height: self.view.frame.height)
						
						if(self.ptzView != nil){
							self.ptzView!.frame = CGRect(x: self.view.frame.width  , y:  self.frameBetweenBar().origin.y  , width: ptzdim, height: self.view.frame.height)
						}
					}
					else{
						self.commView.frame =  CGRect (x: 0 , y: self.view.frame.height  , width: self.view.frame.width , height: barHeight )
						self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width, height: self.view.frame.height )// self.view.frame.width * 3 / 4 )
						
						if( self.ptzView != nil){
							self.ptzView!.frame = CGRect(x: 0  , y:  self.view.frame.height   , width: self.view.frame.width , height: ptzdim )
						}
					
					}
					
				}
				else{ //  ptz
					
					if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
						
						self.commView.frame = CGRect (x: self.view.frame.width   , y:  self.frameBetweenBar().origin.y , width: barHeight, height: self.frameBetweenBar().height )
						self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width  - ptzdim  , height: self.view.frame.height)
						
						if( self.ptzView != nil){
							self.ptzView!.frame = CGRect(x: self.view.frame.width - ptzdim - barHeight , y:  self.frameBetweenBar().origin.y  , width: ptzdim, height: self.view.frame.height)
						}
						
					}
					else{
						
						self.commView.frame =  CGRect (x: 0 , y: self.view.frame.height   , width: self.view.frame.width , height: barHeight )
						self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width, height: self.view.frame.height )// self.view.frame.width * 3 / 4 )
						
						if( self.ptzView != nil){
							self.ptzView!.frame = CGRect(x: 0  , y:  self.view.frame.height  - ptzdim   , width: self.view.frame.width , height: ptzdim )
						}
						
					}
					
					
				}
				return
				}, completion: { (bool) -> Void in
					self.scrollview.zoomScale = 1
					
					var ptzDim : CGFloat = 0
					if(self.isPtz){
						ptzDim = 150
					}
					if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){//landscape

                        if(!self.isWebView){

                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - ptzDim , height:  self.frameBetweenBar().height )
                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }
                        else{
                            self.playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - ptzDim , height:  self.frameBetweenBar().height )
                            let scrollSize = CGSize(width: self.playerWebView!.frame.size.width , height: self.playerWebView!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }

						
					}
					else{

                        if(!self.isWebView){
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize

                        }
                        else{
                            self.playerWebView!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                            let scrollSize = CGSize(width: self.playerWebView!.frame.size.width , height: self.playerWebView!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }

						
					}
					
				
					
			})
		}


        if(!isWebView){
            setplayerAspectRatio(player!)
        }
	}
	
	func tabBarIsVisible() ->Bool {
		return !(self.navigationController?.navigationBar.frame.origin.y < 0 )
	}
	
	func navbarvisible() -> Bool{
		
		return self.navigationController!.navigationBar.hidden
	}
	
	//
	
	func showInfoView(){
		
		
		
		let str : String =  selectedCam!.description + "\n \n"  + "impianti".loc()+": " + DataManager.Instance.installs[ selectedCam!.install! ]!.name
		
		self.alertMessage( "cam".loc()+": " + selectedCam!.name	, message: str , button: "Ok".loc())
	}
	
	func onTimerFired(timer : NSTimer)
	{
        if(!isWebView){
		print("reload video")
		
//		player.stop()
//		player.delegate = nil
//		playerview!.removeFromSuperview()
//
//		
//		
//		urlViedeo = selectedCam!.urlVideo_1!
//		
//		var codecType  = "rtsp"
//		if ( urlViedeo.rangeOfString("mjpg") != nil ){
//			codecType = "mjpg"
//		}
//		
//		// inizializza il player
//		
//		player = VKPlayerController();
//		
//		var option : NSDictionary = NSDictionary ()
//		if (codecType == "rtsp"){
//			option = NSDictionary(object: VKDECODER_OPT_VALUE_RTSP_TRANSPORT_TCP, forKey: VKDECODER_OPT_KEY_RTSP_TRANSPORT)
//		}
//		else if (codecType == "mjpg")	{
//			option = NSDictionary(object: "1", forKey: VKDECODER_OPT_KEY_FORCE_MJPEG)
//		}
//		
//		player.controlStyle = kVKPlayerControlStyleNone;
//		player.statusBarHidden = true;
//		player.contentURLString = urlViedeo;
//		player.decoderOptions = option as [NSObject : AnyObject];
//		player.backgroundColor = UIColor.sfondoScuro()
//		player.showPictureOnInitialBuffering = true
//		//player.scale()
//		player.setVolumeLevel(0)
//		player.setMute(true)
//		player.play();
//		player.delegate = self
//		playerview!.userInteractionEnabled = false
//		scrollview.addSubview(playerview!)

		
		var barHeight :CGFloat = 66
		if( IsPad() )
		{
			barHeight  = 66
		}
		else{
			barHeight = 124
		}
		
		
		if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){//landscape
			
			playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width - barHeight , height:  self.frameBetweenBar().height )
			let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
			scrollview.contentSize = scrollSize
			
		}
		else{
			playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
			let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
			scrollview.contentSize = scrollSize
			
		}
        }
        else{

            playerWebView!.reload()
        }
	}


    //WEB VIEW DELEGATE
    func webViewDidFinishLoad(webView: UIWebView) {


        let contentSize :CGSize = webView.scrollView.contentSize;
        let viewSize:CGSize = webView.bounds.size;

        let rw  = viewSize.width / contentSize.width;

        webView.scrollView.minimumZoomScale = rw;
        webView.scrollView.maximumZoomScale = rw;
        webView.scrollView.zoomScale = rw;

        if(webView.tag != 10)
        {
        actInd!.stopAnimating()
        }
    }

	
}