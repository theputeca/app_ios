//
//  SDraggingCell.swift
//  Camnet
//
//  Created by giulio piana on 21/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation


class SDraggingCell : UICollectionViewCell
{
	
	

	
	
	func getRasterizedImageCopy () -> UIImage
	{
		
		UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque , 0.0)
		self.layer.renderInContext(UIGraphicsGetCurrentContext()!)
		let img : UIImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return img
		
	}
	
}