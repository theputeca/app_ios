
//
//  LoginViewController.swift
//  Camnet
//
//  Created by giulio piana on 11/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import UIKit


//import CryptoSwift

class LoginViewController:   UIViewController ,UITextFieldDelegate
{
	
	
	
	@IBOutlet weak var defaultServerLbl: UILabel!
	@IBOutlet weak var userText: UITextField!
	@IBOutlet weak var passwordText: UITextField!
	@IBOutlet weak var serverText: UITextField!
	
	@IBOutlet weak var loginBtn: UIButton!
	@IBOutlet weak var serverBtn: UISwitch!
	
	var scrollView: UIScrollView = UIScrollView()
	var contentView: UIView!
	
	var loginResults :String = "";
	
	override func viewDidLoad() {
		
		super.viewDidLoad()
		
		
		
		
		
		self.scrollView = UIScrollView(frame: self.view.frame )
		self.scrollView.scrollEnabled = true
		self.scrollView.contentSize = CGSize(width:  1 , height:    self.view.frame.height )
		self.view.addSubview(scrollView)
		
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
		NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
		
		
		contentView  = NSBundle.mainBundle().loadNibNamed("login", owner: self, options: nil)[0] as! UIView
		contentView.frame =  self.view.frame
		scrollView.addSubview(contentView)
		
		
		defaultServerLbl = scrollView.viewWithTag(4) as! UILabel
		userText = scrollView.viewWithTag(1) as! UITextField
		passwordText = scrollView.viewWithTag(2) as! UITextField
		serverText = scrollView.viewWithTag(3) as! UITextField
		
		loginBtn = scrollView.viewWithTag(6) as! UIButton
		serverBtn = scrollView.viewWithTag(5) as! UISwitch
		
		loginBtn.addTarget(self, action: Selector("loginBtnClick:"), forControlEvents: UIControlEvents.TouchUpInside)
		serverBtn.addTarget(self, action:  Selector("servrSwitchChange:" ), forControlEvents: UIControlEvents.ValueChanged )
		
		
		//set server disabled
		serverBtn.setOn(true, animated: false)
		serverText.userInteractionEnabled = false
		serverText.alpha = 0.2
		
		
		// arrotonda btnLogin
		if(IsPad()){
			loginBtn.layer.frame = CGRect(x: loginBtn.layer.frame.origin.x , y: loginBtn.layer.frame.origin.y, width: loginBtn.layer.frame.width, height: 80)
			
		}
		else{
			loginBtn.layer.frame = CGRect(x: loginBtn.layer.frame.origin.x , y: loginBtn.layer.frame.origin.y, width: loginBtn.layer.frame.width, height: 60)
			
		}
		
		loginBtn.layer.masksToBounds = true;
		loginBtn.layer.cornerRadius = ( loginBtn.frame.height / 2 ) - 2
		loginBtn.setTitle( NSLocalizedString("login", comment:  "") , forState: UIControlState.Normal)
		
		
		
		userText.delegate = self
		passwordText.delegate = self
		serverText.delegate = self
		
		//userText.text = NSLocalizedString("nomeUtente", comment:  "")
		userText.placeholder = "nomeUtente".loc()
		passwordText.placeholder = "password".loc()
		serverText.placeholder = "server".loc()
		defaultServerLbl.text = "defaultserver".loc()
		
		//controlla se ce gia stato login
		let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
		if let loginCode = defaults.objectForKey("loginCode") as? String {
			print(loginCode)
			
			if let serverStr = defaults.objectForKey("userServer") as? String {
				User.Instance.serverUrl = serverStr
			}
			else {
				User.Instance.serverUrl = User.Instance.defaultServer
			}
			
			self.loginCall(loginCode )
			self.addLoadingView()
			
		}
		else // se nn ce salvato loghin
		{
			print("NOLOGIN")
			
			userText.becomeFirstResponder()
			userText.text = ""
		}
		
		
		
		
		
		
		//First get the nsObject by defining as an optional anyObject
		
		let versionLabel :UILabel! = scrollView.viewWithTag(10) as! UILabel
		
		let nsObject :AnyObject? = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] //  NSBundle.mainBundle().infoDictionary["CFBundleShortVersionString"] CFBundleVersion
		let version = nsObject as! String
		
		let nsObject2 :AnyObject? = NSBundle.mainBundle().infoDictionary!["CFBundleVersion"] //  NSBundle.mainBundle().infoDictionary["CFBundleShortVersionString"] CFBundleVersion
		let build = nsObject2 as! String
		
		
		//versionLabel = UILabel( frame: CGRect(x: loginBtn.frame.origin.x , y: loginBtn.frame.origin.y + loginBtn.frame.height  , width: loginBtn.frame.width, height: 100 ))
		
		versionLabel.text = "Camnet -  Versione: \(version)  Build: \( build ) "
	//	versionLabel.textAlignment = NSTextAlignment.Center
		
		//view.addSubview(versionLabel)
		
		
	}
	
	var border : CALayer!
	var border2 : CALayer!
	var border3 : CALayer!
	
	override func viewDidAppear(animated: Bool) {
        
        if(self.IsPad())
        {
            
            
            
            
            //  self.scrollView.frame  = CGRect(x: 0 , y: 0 , width: self.view.frame.height	, height: self.view.frame.width    )
            //  contentView.frame = CGRect(x: 0 , y: 0, width:  self.view.frame.height 	, height: self.view.frame.width )
            
        }
        else{
            
            if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
                contentView.frame = CGRect(x: 0 , y: 200 , width: self.view.frame.width		, height: self.view.frame.height)
                self.scrollView.frame = CGRect(x: 0, y: 0 , width:  self.view.frame.width		, height: self.view.frame.height )
                self.scrollView.scrollEnabled = true
                self.scrollView.contentSize = CGSize(width:  1 , height:    self.view.frame.height + 300 )
            }
            else{
                self.scrollView.frame  = CGRect(x: 0 , y: 0 , width: self.view.frame.width	, height: self.view.frame.height    )
                contentView.frame = CGRect(x: 0 , y: 0, width:  self.view.frame.width 	, height: self.view.frame.height )
                
                self.scrollView.contentSize = CGSize(width:  1 , height:    self.view.frame.height + 200 )
                
            }
            
        }
		
		border  = CALayer()
		border.borderColor = UIColor.sfondoPlayer().CGColor
		border.frame = CGRect(x: userText.frame.origin.x - 5  , y: userText.frame.origin.y , width:  userText.frame.width + 5  , height: userText.frame.height )
		border.borderWidth = 1
		border.backgroundColor = UIColor.whiteColor().CGColor
		border.cornerRadius = ( userText.frame.height / 2 ) - 2
		contentView.layer.addSublayer(border)
		
		border2  = CALayer()
		border2.borderColor = UIColor.sfondoPlayer().CGColor
		border2.frame = CGRect(x: passwordText.frame.origin.x - 5  , y: passwordText.frame.origin.y , width:  passwordText.frame.width + 5  , height: passwordText.frame.height )
		
		border2.borderWidth = 1
		border2.backgroundColor = UIColor.whiteColor().CGColor
		border2.cornerRadius = ( userText.frame.height / 2 ) - 2
		contentView.layer.addSublayer(border2)
		
		border3 = CALayer()
		border3.borderColor = UIColor.sfondoPlayer().CGColor
		border3.frame = CGRect(x: serverText.frame.origin.x - 5  , y: serverText.frame.origin.y , width:  serverText.frame.width + 5  , height: serverText.frame.height )
		border3.borderWidth = 1
        border3.opacity = 0;
		border3.backgroundColor = UIColor.whiteColor().CGColor
		border3.cornerRadius = ( userText.frame.height / 2 ) - 2
		contentView.layer.addSublayer(border3)

serverText.alpha = 0

		contentView.bringSubviewToFront(userText)
		contentView.bringSubviewToFront(passwordText)
		contentView.bringSubviewToFront(serverText)
        
        
      
	}
	
	override func  didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {

			
			border.frame = CGRect(x: userText.frame.origin.x - 5  , y: userText.frame.origin.y , width:  userText.frame.width + 5  , height: userText.frame.height )
			
			border2.frame = CGRect(x: passwordText.frame.origin.x - 5  , y: passwordText.frame.origin.y , width:  passwordText.frame.width + 5  , height: passwordText.frame.height )
			
			border3.frame = CGRect(x: serverText.frame.origin.x - 5  , y: serverText.frame.origin.y , width:  serverText.frame.width + 5  , height: serverText.frame.height )

	}
	
	override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		
        if(self.IsPad())
        {
            
            
            
            
            self.scrollView.frame  = CGRect(x: 0 , y: 0 , width: self.view.frame.height	, height: self.view.frame.width    )
            contentView.frame = CGRect(x: 0 , y: 0, width:  self.view.frame.height 	, height: self.view.frame.width )
            
        }
        else{
            
            if (  toInterfaceOrientation.isLandscape		){
                contentView.frame = CGRect(x: 0 , y: 100 , width: self.view.frame.height		, height: self.view.frame.width)
                self.scrollView.frame = CGRect(x: 0, y: 0 , width:  self.view.frame.height		, height: self.view.frame.width )
                self.scrollView.scrollEnabled = true
                self.scrollView.contentSize = CGSize(width:  1 , height:    self.view.frame.width + 300  )
            }
            else{
                
                self.scrollView.frame  = CGRect(x: 0 , y: 0 , width: self.view.frame.height	, height: self.view.frame.width    )
                contentView.frame = CGRect(x: 0 , y: 0, width:  self.view.frame.height 	, height: self.view.frame.width  )
                
                self.scrollView.contentSize = CGSize(width:  1 , height:    self.view.frame.width + 200 )

            }
            
        }
			
		
	}
	
	/// ACTIOMN
	
	@IBAction func loginBtnClick(sender: UIButton) {
		
		/*
		self.loginResults="true"
		//Xml TEST
		let bundle = NSBundle.mainBundle()
		let path : String? = bundle.pathForResource("LANIPIER", ofType: "xml")  //testXmlFile
		var dataTest : NSData? = NSData(contentsOfFile: path!) as NSData?
		
		var error: NSError?
		if let xmlDoc = AEXMLDocument(xmlData: dataTest!, error: &error) {
		DataManager.Instance.ParseXmlData(xmlDoc)
		}
		self.enterLogin()
		return

		// */
		// /*

		
		self.view.endEditing(true);
		self.addLoadingView ()
		
		let username = userText.text;
		let password = passwordText.text;
		
		let criptedCode = criptaCodice(username!,password:password!)
		
		//setta server o default
		if(!serverBtn.on)
		{

            let servText = serverText.text!.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
			User.Instance.serverUrl = "http://" + servText + "/camnet/dashboard/api/"
		}
		else{
			User.Instance.serverUrl = User.Instance.defaultServer
		}
		
		loginCall(criptedCode )
		//  */
    
	}
	

    var isServerPER : Bool = false;
	@IBAction func servrSwitchChange(sender: UISwitch) {
		
		if ( !sender.on){
            isServerPER = true;
			serverText.userInteractionEnabled = true
			serverText.alpha = 1
            border3.opacity = 1;

			serverText.becomeFirstResponder()
			serverText.text = ""
		}
		else
		{
            isServerPER = false;
			serverText.userInteractionEnabled = false
			serverText.alpha = 0
			serverText.text = ""
            border3.opacity = 0;

            

		}

		
	}
	
	
	
	func loginCall(loginCode : String )	{
		
		let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
		
		var token = "0"

        //let devtoken =		defaults.stringForKey("deviceToken")  as? String
		if let devtoken =		defaults.stringForKey("deviceToken")  as String?
		{

            token = devtoken.stringByReplacingOccurrencesOfString("<", withString: "")
            token = token.stringByReplacingOccurrencesOfString(">", withString: "")
            token = token.stringByReplacingOccurrencesOfString(" ", withString: "")
			//token =  devtoken
		}
		
		
		
		let sistemversion = UIDevice.currentDevice().systemVersion;
		let devicename =  UIDevice.currentDevice().modelName
		let trimmedString = devicename.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())


        let nsObject :AnyObject? = NSBundle.mainBundle().infoDictionary!["CFBundleShortVersionString"] //  NSBundle.mainBundle().infoDictionary["CFBundleShortVersionString"] CFBundleVersion
        let version = nsObject as! String
        let sistemLanguage: AnyObject = NSLocale.currentLocale().objectForKey(NSLocaleLanguageCode)!

		print ("\(devicename)  debug log \(sistemversion) ")
        print ("LIngua sistema \(sistemLanguage)  versione app \(version) ")





        let str = "\(User.Instance.serverUrl)api_mobile.php?login=\(loginCode)&udid=\(token)&device=ios:\(trimmedString):\(sistemversion):\(version):\(sistemLanguage)"
		let url = NSURL(string:  str)!
		
		print("LOGIN URL ")
		print(url)
		
		
		var request = NSMutableURLRequest(URL: url)
		var session = NSURLSession.sharedSession()

        session.configuration.timeoutIntervalForResource = 30;
        session.configuration.timeoutIntervalForRequest = 30;

		request.HTTPMethod = "GET"
		
		loginResults=""
		var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
			
			print("Response: \(response)")
			//	println( NSString(data: data, encoding: NSUTF8StringEncoding ))
			var error: NSError?
			
			if ( response == nil )
			{
                if( self.isServerPER == true)
                {
                    self.alertErrorLogin("errorServerMessagePER".loc()  ) // server error
                }
                else{
                    self.alertErrorLogin("errorServerMessageDEF".loc()  ) // server error
                }

				return
			}
			
			do {
				let xmlDoc = try AEXMLDocument(xmlData: data!)
				
				print(xmlDoc.xmlString)

                let errorMesage = xmlDoc.root["error"].value
                let errorCode = xmlDoc.root["error_code"].value

				if let result =  xmlDoc.root["result"].value as String? {

					if(xmlDoc.root.name != "camnet")
					{

                        self.alertErrorLogin( errorMesage!)
						return
					}
					
					if (result == "false"){
						  //self.alertErrorLogin() // login error
                          self.alertErrorLogin( errorMesage!)
					}
					else	{
						
						DataManager.Instance.ParseXmlData(xmlDoc)
						
						/*salva valori di login*/
						let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
						if(loginCode != ""){
							defaults.setObject(loginCode, forKey: "loginCode")
							defaults.setObject( User.Instance.serverUrl , forKey: "userServer")
						}

						// crea l user per tenere dati
						User.Instance.token = xmlDoc.root["token"].value!
						User.Instance.prouser = ""
						User.Instance.paypage = xmlDoc.root["paypage"].value!
						User.Instance.newspage = xmlDoc.root["newspage"].value!

                        if (xmlDoc.root["appleRefresh"].name != "error" )
                        {
                            let str : String? = xmlDoc.root["appleRefresh"].value!
                            User.Instance.refreshTime =  Int(str!)!
                        }

						self.loginResults = "true"
						self.enterLogin()
					}
				}
				else
				{
                    self.alertErrorLogin( errorMesage!)
				}
			} catch _ {

			}
			
		})
		task.resume()
	}
	
	
	//cript code
	func criptaCodice (username :String, password :String ) -> String{
		
		//cripta la pass conRIPEMD160
		let passRipemd :String = RIPEMD.asciiDigest(password).description;
		
		//rimuove caratteri sbagliati
		var newString = passRipemd.stringByReplacingOccurrencesOfString(" ", withString: "");
		newString = newString.stringByReplacingOccurrencesOfString("<", withString: "");
		newString = newString.stringByReplacingOccurrencesOfString(">", withString: "");
		
		
		//passRipemd
		
		let code = username+":"+newString;
		
		let  cr :CriptString = CriptString()
		
		if let codesh1 = cr.sha1(code){
			return codesh1
		}
		else {
			print ("error cripting SHA!")
			return ""
		}
		
	}
	
	
	/// login result ok
	func enterLogin(){
		//per chiamate da un secondo thread task
		dispatch_async(dispatch_get_main_queue(), {
			
			if( self.loginResults=="true")
			{
				//self.removeLoadinView ()
				let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MainViewController") as! MainViewController
				self.presentViewController(secondViewController, animated: true, completion: nil)
			}
			else{
				self.alertErrorLogin()
			}
		});
	}
	
	
	
	// error login
	func alertErrorLogin(){
		self.removeLoadinView ()
		
		dispatch_async(dispatch_get_main_queue(), {
		
			self.alertMessage("errorLoginTitle".loc(), message: "errorLoginMessage".loc() , button: "ok")
		});
		
	}
	
	func alertErrorLogin(message:String){
		self.removeLoadinView ()
		
		dispatch_async(dispatch_get_main_queue(), {
			
			self.alertMessage("errorLoginTitle".loc(), message: message , button: "ok")
		});
		
	}

	
	
	// TEXT FIELD DELEGATE
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		print("RETURN")
		
		if (textField.tag == 1 ){
			passwordText.becomeFirstResponder()
			passwordText.text = ""
			passwordText.secureTextEntry=true
		}
		else if (textField.tag == 2){
			if(serverText.userInteractionEnabled)
			{
				serverText.becomeFirstResponder()
				serverText.text = ""
				//asdserverText.secureTextEntry=true
			}
			else{
				loginBtnClick(UIButton())
			}
			
		}
		else if (textField.tag == 3){
			loginBtnClick(UIButton())
		}
		return true
	}
	
	func textFieldDidBeginEditing(textField: UITextField) {
		
		
		//self.anima
		/*if (textField.tag == 1 ){
		if(	userText.text == NSLocalizedString("nomeUtente", comment:  "") ){
		userText.text = ""
		}
		}
		else if (textField.tag == 2){
		if(	passwordText.text == NSLocalizedString("password", comment:  "")) {   // default text
		passwordText.text = ""
		}
		}
		else if (textField.tag == 3){
		if(	serverText.text == NSLocalizedString("server", comment:  "") ) {   // default text
		serverText.text = ""
		}
		}*/
	}
	
	
	func keyboardWillShow(notification: NSNotification) {
		var info = notification.userInfo!
		let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
		
		self.scrollView.scrollEnabled = true
        
        if(self.IsPad())
        {
            
          self.scrollView.contentSize = CGSize(width:  1 , height:    self.view.frame.height + keyboardFrame.size.height + 20 )
        }
		
	}
	
	
	func keyboardWillHide(notification: NSNotification) {
		var info = notification.userInfo!
		var keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
		
		self.scrollView.scrollEnabled = false
		self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
        if(self.IsPad())
        {
            
           self.scrollView.contentSize = CGSize(width:  1, height:  self.view.frame.height   )
        }
		
		
	}
}
