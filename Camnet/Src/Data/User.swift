//
//  Evento.swift
//  Camnet
//
//  Created by giulio piana on 16/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation


private let _InstanceUser = User()

class User{
	
	
	var token :String = ""
	var prouser : String = ""
	var paypage : String = ""
	var newspage : String = ""
    var refreshTime : Int = 360;
	var isProUser : Bool = false
	
	
	
	
	var serverUrl : String =  "https://app.cestelsrl.com/camnet/dashboard/api/"
	var defaultServer : String =   "https://app.cestelsrl.com/camnet/dashboard/api/"
	
	
	//app.cestelsrl.com
	
	//var serverUrl : String =  "http://sede.b4web.biz:12080/camnet/dashboard/api/"
    //var defaultServer : String = "http://sede.b4web.biz:12080/camnet/dashboard/api/"
	
	
	
	
	class var Instance : User {
		return _InstanceUser
	}
	
	func resetInstance () {
	
		 token  = ""
		prouser  = ""
		 paypage  = ""
		 isProUser  = false

		
		serverUrl  = "https://app.cestelsrl.com/camnet/dashboard/api/"
		defaultServer  =  "https://app.cestelsrl.com/camnet/dashboard/api/"
		
		//serverUrl  = "http://     sede.b4web.biz:12080     /camnet/dashboard/api/"
		//defaultServer  = "http://sede.b4web.biz:12080/camnet/dashboard/api/"

	}
	
	init() {
		
	}
	
	
	class func	logout()
	{
		print(" CALL LOGOUT API ")
		
		
		
		let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
		var devtoken = defaults.stringForKey("deviceToken")
		
		if(devtoken == nil)
		{
			devtoken = "0"
		}
		
		let url = NSURL(string:  User.Instance.serverUrl + User.mobileLogOutApiUrl(devtoken!) )!
	
		let request1 = NSMutableURLRequest(URL: url )
		let session1 = NSURLSession.sharedSession()
		request1.HTTPMethod = "GET"
		
		let task = session1.dataTaskWithRequest(request1, completionHandler: {data, response, error -> Void in
			var error: NSError?
			
			
		})
		
		task.resume()
	}
	
	//CLASS FUNCTION
	//Save VIew
	class func saveViewApiUrl(installId:String, viewId:String? , attr:String) -> String {
		let  cr :CriptString = CriptString()
		
		if ( viewId != nil ) {
			
			let tok = cr.sha1( User.Instance.token )
			let str = "api_mobile.php?token=\(tok)&op=saveView&install_id=\(installId)&view_id=\(viewId!)&attr=\(attr)"
			return str
		}
		else{
			return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=saveView&install_id=" + installId +  "&attr=" + attr
		}
		//api_mobile.php?token=<sha1(TOKEN)>&op=saveView&install_id=<install_id>&view_id=<view_id>&attr=<attr>
		
		/*
		attr
			{
			"view_name": "pippo",
			"view_mode": "51",
			"view_details": "descrizione vista",
			"hide_to_operator": "0",
			"map_id": "",
			"view_prop": "0",
			"cams": {
				"1": "10632",
				"2": "10633"
				}
			}
			installId	String	"1"	
			obbligatori view_name, view_mode, cams
		*/
		
		/*positive
		
		<camnet>
		<result>true</result>
		<data>ID della vista creata/modificata</data>
		</camnet>
		
		negative
		
		<camnet>
		<result>false</result>
		<error>descrizione errore</error>
		<error_code>codice errore</error_code>
		</camnet>
		
		*/
	}
	
	class func deleteViewApiUrl( viewId:String ) -> String {
		let  cr :CriptString = CriptString()
		return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=deleteView&view_id=" + viewId
		
		//api_mobile.php?token=<sha1(TOKEN)>&op=deleteView&view_id=<view_id>
	
		/* +
			<camnet>
			<result>true</result>
			</camnet>
		
			-
			<camnet>
			<result>false</result>
			<error>descrizione errore</error>
			<error_code>codice errore</error_code>
			</camnet>
			
		*/

	}
	
	
	//cmdvideobig
	class func cmdLightApiUrl(deviceId:String, action:Bool)-> String {
		
		let  cr :CriptString = CriptString()
		if (action)		{
			return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=cmdLight&device_id=" + deviceId + "&action=" + "on"
		}
		else		{
			return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=cmdLight&device_id=" + deviceId + "&action=" + "off"
		}
		
		/*positive
		
		<camnet>
		<result>true</result>
		</camnet>
		
		negative
		
		<camnet>
		<result>false</result>
		<error>descrizione errore</error>
		<error_code>codice errore</error_code>
		</camnet>
		
		*/
	}
	class func cmdEsternalActionApiUrl(deviceId:String)-> String {
		let  cr :CriptString = CriptString()
		return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=cmdEsternalAction&device_id=" + deviceId
		
		/*positive
		
		<camnet>
		<result>true</result>
		</camnet>
		
		negative
		
		<camnet>
		<result>false</result>
		<error>descrizione errore</error>
		<error_code>codice errore</error_code>
		</camnet>
		*/
	}
	class func cmdRecordingVideoApiUrl( deviceId:String,action:Bool)-> String {
		let  cr :CriptString = CriptString()
		if (action)		{
			return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=cmdRecordingVideo&device_id=" + deviceId + "&action=" + "on"
		}
		else		{
			return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=cmdRecordingVideo&device_id=" + deviceId + "&action=" + "off"
		}
		
		/*positive
		
		<camnet>
		<result>true</result>
		</camnet>
		
		negative
		
		<camnet>
		<result>false</result>
		<error>descrizione errore</error>
		<error_code>codice errore</error_code>
		</camnet>
		
		*/
		
	}
	class func cmdRecordingAudioApiUrl(deviceId:String, action:Bool) -> String{
		let  cr :CriptString = CriptString()
		if (action)		{
			return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=cmdRecordingAudio&device_id=" + deviceId + "&action=" + "on"
		}
		else		{
			return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=cmdRecordingAudio&device_id=" + deviceId + "&action=" + "off"
		}
		
		/*positive
		
		<camnet>
		<result>true</result>
		</camnet>
		
		negative
		
		<camnet>
		<result>false</result>
		<error>descrizione errore</error>
		<error_code>codice errore</error_code>
		</camnet>
		
		*/
	}
	
	//getevent
	class func getEventsApiUrl( installId:String  ) -> String{
		let  cr :CriptString = CriptString()
	//	return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=getEvents&device_id=" + deviceId
		
		return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=getEvents&install_id=" + installId
		
		//api_mobile.php?token=<sha1(TOKEN)>&op=getEvents&device_id=<device_id>
		
		/*positive
		
		<events>
		<event>
		<eventId></eventId>
		<deviceId></deviceId>
		<deviceName></deviceName>
		<eventDetails></eventDetails>
		<image></image>
		<mode>generic/realtime</mode>
		<type>T/D/C/U/K</type>
		<video>url video dell'evento</video>
		</event>
		</events>
		
		negative
		
		<camnet>
		<result>false</result>
		<error>descrizione errore</error>
		<error_code>codice errore</error_code>
		</camnet>
		*/
	}
	//getvideo
	class func getStoredVideoApiUrl(deviceId:String , dateFrom:String )-> String { //, dateTo:String
		let  cr :CriptString = CriptString()
		return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=getStoredVideo&device_id=" + deviceId + "&date=" + dateFrom //+ "&date_to=" + dateTo // "<timestamp>"
		
		/*api_mobile.php?token=ead647429f5d030d2040f1b1fd6931bef68ee8d9&op=getStoredVideo&device_id=10633&date_from=1426584562&date_to=1426588162
		a
		api_mobile.php?token=ead647429f5d030d2040f1b1fd6931bef68ee8d9&op=getStoredVideo&device_id=10633&date=1426586422
		*/
		/*positive
		
		<films>
		<film>
		<startTime></startTime>
		<endTime></endTime>
		<videoUrl></videoUrl>
		</film>
		</films>
		
		negative
		
		<camnet>
		<result>false</result>
		<error>descrizione errore</error>
		<error_code>codice errore</error_code>
		</camnet>
		*/
	}
	
	
	// keepAlive
	class func keepAliveApiUrl() -> String{
		let  cr :CriptString = CriptString()
		
		return "api_mobile.php?token=" + cr.sha1(User.Instance.token) + "&op=keepAlive"
		
		/*positive
		
		<camnet>
			<result>true</result>
		</camnet>
		
		negative
		
		<camnet>
			<result>false</result>
			<error>descrizione errore</error>
			<error_code>codice errore</error_code>
		</camnet>
		
		*/
	}
	
	//ptz webview
	class func viewPtzApiUrl(deviceId:String ) -> String {
		let  cr :CriptString = CriptString()
		return "viewPtz.php?token=" +  cr.sha1( User.Instance.token ) + "&device_id=" + deviceId
		
		/*positive
		Ritorna pagina html con comandi o errori
		*/
	}
	//mapp
	class func viewMapApiUrl(mapId :String) -> String{
		let  cr :CriptString = CriptString()
		return "viewMap.php?token=" +  cr.sha1( User.Instance.token ) + "&map_id=" + mapId
		
		
		/*positive
		Ritorna pagina html con comandi o errori
		*/
	}
	//logout
	class func mobileLogOutApiUrl(UDID:String) -> String{
		let  cr :CriptString = CriptString()
		return "api_mobile.php?token=" +  cr.sha1( User.Instance.token ) + "&op=mobileLogOut&udid=" + UDID
		
		/*positive*/
		/*
		<camnet>
		<result>true</result>
		</camnet>
		*/
		/*negative*/
		/*
		<camnet>
		<result>false</result>
		<error>descrizione errore</error>
		<error_code>codice errore</error_code>
		</camnet>
		*/

	}
	

}



























