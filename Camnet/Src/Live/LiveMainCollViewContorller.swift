//
//  MainViewController.swift
//  Camnet
//
//  Created by giulio piana on 12/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//


import UIKit

private var _Instance : LiveMainCollViewContorller? = nil
//UICollectionViewDataSource,UICollectionViewDelegate
class LiveMainCollViewContorller: UICollectionViewController,UICollectionViewDelegateFlowLayout,   UIGestureRecognizerDelegate, UIAlertViewDelegate
{

    var  canAutostart : Bool = true

    class var Instance : LiveMainCollViewContorller? {
        return _Instance
    }

	var visteUser : [Vista] = [Vista]()

	override func viewDidLoad() {
		super.viewDidLoad()

                _Instance = self

        MainViewController.Instance?.mainNavigationController = self.navigationController

        let nib = UINib(nibName: "CVHeaderEventiCellIphone", bundle: nil) //CVHeaderCellIphone
		self.collectionView!.registerNib(nib , forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
		
		
		
		if(!self.IsPad())
		{
			self.collectionView!.backgroundColor = UIColor.whiteColor()
			let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
			layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
			self.collectionView!.setCollectionViewLayout(layout, animated: false, completion: nil	)
			
			layout.minimumInteritemSpacing = 0 //1.0
			layout.minimumLineSpacing = 0 //1.0
		}
	}
	
	override func viewWillAppear(animated: Bool) {

        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()

        if(defaults.boolForKey("deleteViewMessage")  == false)
        {
            defaults.setBool(true, forKey: "deleteViewMessage")
            self.alertMessage("tutorialMessageTitle".loc(), message: "deleteViewMessage".loc(), button: "OK")

        }

		self.removeLoading()
		self.tabBarController?.tabBar.hidden=false

		visteUser = [Vista]()
		for imp in DataManager.Instance.impianti
		{
			visteUser = visteUser + ([Vista] (imp.views_user.values ))
			
		}
		
		
		//visteUser[0].name
		
		visteUser.sortInPlace({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })
		
		//self.events.sort({ ($0.time as NSString).doubleValue < ($1.time as NSString).doubleValue })

        checkAutostart()
		
		
		
		collectionView?.reloadData()
		
		
		let lpgr :UILongPressGestureRecognizer		= UILongPressGestureRecognizer(target: self, action: Selector("handleLongPress:"))
		
		lpgr.minimumPressDuration = 0.5 //seconds
		lpgr.delegate = self
		collectionView!.addGestureRecognizer(lpgr)
		
	}

     override func viewDidAppear(animated: Bool) {
self.tabBarController?.tabBar.hidden=false
        canAutostart = true;
    }

    override func viewWillDisappear(animated: Bool) {
    //    canAutostart = false;
    }

	@IBAction func onLogOutBtnClick(sender: UIButton) {
		
        print("LOGOUT CLICK ")
		
		User.logout()
		
		
		let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
		defaults.removeObjectForKey("loginCode")
		defaults.removeObjectForKey("userServer")
		
		User.Instance.resetInstance()
		DataManager.Instance.resetInstance()
		
		
		let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
		self.presentViewController(secondViewController, animated: true, completion: nil)
		
	}
	
	
	
	// MARK: UICollectionViewDataSource
 
	//
	override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return DataManager.Instance.impianti.count + 1
	}
	
	override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		//var numero = visteUser.count + 2
		//return numero
		
		/*	if( section == 0 )
		{
		return 1
		}
		else */
		if( section == DataManager.Instance.impianti.count   ){
			return 1
		}
		else{
			if( DataManager.Instance.impianti[ section].views_auto.count != 0){
				return DataManager.Instance.impianti[ section ].views_user.count + 1
			}
			else {
				return DataManager.Instance.impianti[ section ].views_user.count
			}
		}
		
		
		
	}
	//cellforitem
	override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		
		var cell :UICollectionViewCell;
		
		
		
		
		if ( indexPath.section ==  DataManager.Instance.impianti.count   ){ // crea nuova vista cell
			cell = collectionView.dequeueReusableCellWithReuseIdentifier("CellCrea", forIndexPath: indexPath) 
			
			//set teh dimension of the cell
			if(self.IsPad()){
				
				cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y , collectionView.frame.width - 20 ,125)
				
				//	cell.layer.masksToBounds = true
				cell.layer.cornerRadius = 25
				cell.layer.borderWidth = 2
				cell.layer.borderColor =  UIColor.arancio().CGColor
				
			}
			else{//iphone
				if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
					
					cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, collectionView.frame.width - 20 ,65)
				}
				else{
					cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, collectionView.frame.width - 20 ,65)
				}
				cell.backgroundColor = UIColor.whiteColor()
				
				let title : UILabel = cell.viewWithTag(1) as! UILabel
				
				title.layer.cornerRadius = 55 / 1.9
				title.layer.borderWidth = 2
				title.layer.borderColor =  UIColor.arancio().CGColor

			}
			
			let title : UILabel = cell.viewWithTag(1) as! UILabel
			title.text = NSLocalizedString("creaVista", comment:  "")
			
		}
		else  { //CELLA VIEW
			
			if ( indexPath.row == 0 && ( DataManager.Instance.impianti[indexPath.section].views_auto.count != 0) ){ //   indexPath.row == 0) { //celle tutte le vste // viste auto
				
				//if ( DataManager.Instance.impianti[indexPath.section].views_auto.count != 0) {// tutte le cam
				
				if(self.IsPad()){
					cell = collectionView.dequeueReusableCellWithReuseIdentifier("CellTutte", forIndexPath: indexPath) 
					
					if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
						cell.frame = CGRectMake(  cell.frame.origin.x  , cell.frame.origin.y,(collectionView.frame.width / 3 ) - 15, 125)
					}
					else{
						cell.frame = CGRectMake(  cell.frame.origin.x  , cell.frame.origin.y,(collectionView.frame.width / 2 ) - 15, 125)
					}
					
					cell.layer.masksToBounds = true;
					cell.layer.cornerRadius = 25;
				}
				else{ // iphone
					cell = collectionView.dequeueReusableCellWithReuseIdentifier("VisteCellIphone", forIndexPath: indexPath) 
					
					
					if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
						cell.frame = CGRectMake(  cell.frame.origin.x  , cell.frame.origin.y,(collectionView.frame.width  ) , 65)
					}
					else{
						cell.frame = CGRectMake(  cell.frame.origin.x  , cell.frame.origin.y,(collectionView.frame.width  ) , 65)
					}
					
					let border : CALayer = CALayer()
					border.borderColor = UIColor.sfondoChiaro().CGColor
					border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: cell.frame.size.height)
					border.borderWidth = 1
					cell.layer.addSublayer(border)
					
					cell.backgroundColor = UIColor.whiteColor()
				}
				
				let title : UILabel = cell.viewWithTag(1) as! UILabel
				title.text = NSLocalizedString("tutteLeCam", comment:  "")
				
				
				let desc : UILabel = cell.viewWithTag(2) as! UILabel
				desc.text =  NSLocalizedString("tutteLeCamDesc", comment:  "")
				
				let imageView : UIImageView = cell.viewWithTag(3) as! UIImageView
				let bundle = NSBundle.mainBundle()
				
				let path : String? = bundle.pathForResource("16", ofType: "png")
				
				if let image = UIImage(named: "16.png") {
					imageView.image = image
				}
				
				
			}
				
			else{
				////
				
				var numero = 0 ;
				if ( DataManager.Instance.impianti[indexPath.section].views_auto.count != 0)
				{
					numero = 1;
				}
				
				if(self.IsPad()){
					
					cell = collectionView.dequeueReusableCellWithReuseIdentifier("CellTutte", forIndexPath: indexPath) 
					
					if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
						cell.frame = CGRectMake(  cell.frame.origin.x  , cell.frame.origin.y,(collectionView.frame.width / 3 ) - 15, 125)
					}
					else{
						cell.frame = CGRectMake(  cell.frame.origin.x  , cell.frame.origin.y,(collectionView.frame.width / 2 ) - 15, 125)
					}
					
					cell.layer.masksToBounds = true;
					cell.layer.cornerRadius = 25;
					
					let title : UILabel = cell.viewWithTag(1) as! UILabel
					
					
					
					var  viste = ([Vista] (DataManager.Instance.impianti[indexPath.section ].views_user.values))
					viste.sortInPlace({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })
					
					if let  v  = viste[indexPath.row - numero ] as Vista?  //visteUser[indexPath.row ] as Vista?  //  indexPath.row - 1
					{
						
						title.text = v.name
						
						let imageView : UIImageView = cell.viewWithTag(3) as! UIImageView
						let bundle = NSBundle.mainBundle()
						
						//	let path : String? = bundle.pathForResource( String(v.size) , ofType: "png")
						
						if let image = UIImage(named: "\(String(v.size)).png") {
							imageView.image = image
						}
					}
					
					let desc : UILabel = cell.viewWithTag(2) as! UILabel
					desc.text = NSLocalizedString("vistaImpianto", comment:  "")
				}
				else{ // iphone
					
					cell = collectionView.dequeueReusableCellWithReuseIdentifier("VisteCellIphone", forIndexPath: indexPath) 
					
					if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
						cell.frame = CGRectMake(  cell.frame.origin.x  , cell.frame.origin.y,(collectionView.frame.width  ) , 65)
					}
					else{
						cell.frame = CGRectMake(  cell.frame.origin.x  , cell.frame.origin.y,(collectionView.frame.width  ) , 65)
					}
					
					
					let title : UILabel = cell.viewWithTag(1) as! UILabel
					let imageView : UIImageView = cell.viewWithTag(3) as! UIImageView
					let desc : UILabel = cell.viewWithTag(2) as! UILabel
					
					var viste = ([Vista] (DataManager.Instance.impianti[indexPath.section ].views_user.values))
					viste.sortInPlace({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })
					
					if let  v  = viste[indexPath.row - numero ] as Vista?  //visteUser[indexPath.row ] as Vista?  //  indexPath.row - 1
					{
						
						title.text = v.name
						let bundle = NSBundle.mainBundle()
						//	let path : String? = bundle.pathForResource( String(v.size) , ofType: "png")
						if let image = UIImage(named: "\(String(v.size)).png") {
							imageView.image = image
						}
					}
					
					desc.text = NSLocalizedString("vistaImpianto", comment:  "")
					
					let border : CALayer = CALayer()
					border.borderColor = UIColor.sfondoChiaro().CGColor
					border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: cell.frame.size.height)
					border.borderWidth = 1
					cell.layer.addSublayer(border)
					
					cell.backgroundColor = UIColor.whiteColor()
				}
				
			}
			
		}
		return cell;
	}
	
	//sizeforitem
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		if(self.IsPad()){
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				
				/*	if (indexPath.row == 0) {
				return CGSizeMake((collectionView.frame.width / 3 ) - 15, 125)
				}
				else*/  if (indexPath.section ==  DataManager.Instance.impianti.count  ){
					return CGSizeMake(collectionView.frame.width - 20 , 125 )
				}
				else  {
					return CGSizeMake((collectionView.frame.width / 3 ) - 15, 125)
				}
			}
			else{
				/*if (indexPath.section == 0) {
				return CGSizeMake((collectionView.frame.width / 2 ) - 15, 125)
				}
				else */ if (indexPath.section == DataManager.Instance.impianti.count  ){
					return CGSizeMake(collectionView.frame.width - 20 , 125)
				}
				else  {
					return CGSizeMake((collectionView.frame.width / 2 ) - 15, 125)
				}
			}
		}
		else{ // iphone
			/*if (indexPath.row == 0) {
			return CGSizeMake((collectionView.frame.width  )  , 65)
			}
			else */ if (indexPath.section ==  DataManager.Instance.impianti.count  ){
				return  CGSizeMake(collectionView.frame.width - 20 , 65)
			}
			else  {
				return CGSizeMake((collectionView.frame.width  )  ,65)
				
			}
		}
	}
	
	
	//header footer
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		
		/*if( section == 0 )
		{
		return CGSizeMake(collectionView.frame.width  , 0 )
		}
		else */	if( section == DataManager.Instance.impianti.count   ){
			return CGSizeMake(collectionView.frame.width  , 0 )
		}
		else{
			return CGSizeMake(collectionView.frame.width  , 32 )
		}
		
		/*if(self.IsPad()){
		
		if( section==0){
		
		return CGSizeMake(collectionView.frame.width  , 108 )
		}
		else{
		
		return CGSizeMake(collectionView.frame.width  , 32 )
		}
		
		//	return CGSizeMake(CamCollectionView.frame.width  , 85 )
		
		}
		//IPHONE
		else{
		return CGSize(width: 0, height: 32)
		
		}*/
		
	}
	
	override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
		
		if (kind == UICollectionElementKindSectionHeader){
			let header :UICollectionReusableView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView", forIndexPath: indexPath) 
			
			
		if( indexPath.section == DataManager.Instance.impianti.count   ){
				let titledata : UILabel = header.viewWithTag(1) as! UILabel
				titledata.alpha =  0
			}
			else{
				let titledata : UILabel = header.viewWithTag(1) as! UILabel
				titledata.text = DataManager.Instance.impianti[indexPath.section].name
			}
			
			return header
		}
		
		return UICollectionReusableView()
	}
	
	
	//HIGHLIGHT
	override func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
		let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
		cell.contentView.backgroundColor = UIColor.arancioSelezione()
	}
	override func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
		if(self.IsPad())
		{
			let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
			cell.contentView.backgroundColor = UIColor.sfondoChiaro()
		}
		else{
			let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
			cell.contentView.backgroundColor = UIColor.whiteColor()
		}
	}
	
	
	//delegate
	
	override func  collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath)
	{
		/*	if (indexPath.row == 0) {// tutte le cam
		
		let listaImpiantiViewController = self.storyboard?.instantiateViewControllerWithIdentifier("listaImpiantiViewController") as ListaImpiantiViewController
		navigationController?.pushViewController(listaImpiantiViewController , animated: true) //showViewController(listaImpiantiViewController, sender: nil)
		
		}
		else  if (indexPath.row ==  collectionView.numberOfItemsInSection(0)	 - 1 ){ // crea vista
		
		self.addLoading()
		
		let creaVistaViewController = self.storyboard?.instantiateViewControllerWithIdentifier("creaVistaViewController") as CreaVistaViewController
		navigationController?.pushViewController(creaVistaViewController, animated: true)
		}
		else  {// apri vista id
		if let  v  =  visteUser[indexPath.row - 1] as Vista?
		{
		let visteViewController = self.storyboard?.instantiateViewControllerWithIdentifier("visteViewController") as  VisteViewController
		visteViewController.visteList = visteUser
		visteViewController.currentVista = v
		visteViewController.currentVistaId = indexPath.row - 1
		navigationController?.pushViewController(visteViewController , animated: true)
		}
		
		}
		*/
		
		/*if (indexPath.section == 0) {// tutte le cam
		
		let listaImpiantiViewController = self.storyboard?.instantiateViewControllerWithIdentifier("listaImpiantiViewController") as ListaImpiantiViewController
		navigationController?.pushViewController(listaImpiantiViewController , animated: true) //showViewController(listaImpiantiViewController, sender: nil)
		
		}
		else */

         canAutostart = false;

        if (indexPath.section ==  DataManager.Instance.impianti.count ){ // crea vista
			
			self.addLoading()
			
			let creaVistaViewController = self.storyboard?.instantiateViewControllerWithIdentifier("creaVistaViewController") as! CreaVistaViewController
			navigationController?.pushViewController(creaVistaViewController, animated: true)
		}
		else  {// apri vista id
			
			if (indexPath.row == 0 && ( DataManager.Instance.impianti[indexPath.section].views_auto.count != 0) ) {// tutte le cam				
				
				
				let visteViewController = self.storyboard?.instantiateViewControllerWithIdentifier("visteViewController") as!  VisteViewController
				var viste : [Vista] = [Vista ] ( DataManager.Instance.impianti[indexPath.section].views_auto.values )
				
				
				if( viste[0].id.componentsSeparatedByString("_").count > 2  ){
					viste.sortInPlace({ Int(($0.id.componentsSeparatedByString("_")[2]  as  String))  < Int(($1.id.componentsSeparatedByString("_")[2]  as String))  })
				}
				else {
					viste.sortInPlace({ Int(($0.id as  String)) < Int(($1.id as String)) })
				}

				
				visteViewController.visteList = viste//[Vista ] ( DataManager.Instance.impianti[indexPath.section].views_auto.values )
				visteViewController.currentVista = visteViewController.visteList[0]
				navigationController?.pushViewController(visteViewController , animated: true)
				
			}
			else{
				
				var numero = 0 ;
				if ( DataManager.Instance.impianti[indexPath.section].views_auto.count != 0)
				{
					numero = 1;
				}
				
				var viste = ([Vista] (DataManager.Instance.impianti[indexPath.section ].views_user.values))
				
				viste.sortInPlace({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })
				
				
				if let  v  =  viste[indexPath.row - numero  ] as Vista?
				{
					let visteViewController = self.storyboard?.instantiateViewControllerWithIdentifier("visteViewController") as!  VisteViewController
					visteViewController.visteList =  viste //visteUser
					visteViewController.currentVista = v
					visteViewController.currentVistaId = indexPath.row - numero
					navigationController?.pushViewController(visteViewController , animated: true)
				}
			}
		}
	}
	
	
	//orientation
	
	override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		collectionView?.reloadData()
	}
	
	
	//long press
	func handleLongPress (gestureRecognizer: UILongPressGestureRecognizer )
	{

		if ( gestureRecognizer.state != UIGestureRecognizerState.Ended) {
			return;
		}
		let  p : CGPoint = gestureRecognizer.locationInView(collectionView!);
		
		let  indexPath: NSIndexPath? = collectionView!.indexPathForItemAtPoint(p)
		if (indexPath == nil){
			print("couldn't find index path");
		}
		else {
			// get the cell at indexPath (the one you long pressed)
			//let  cell : UICollectionViewCell? = collectionView!.cellForItemAtIndexPath(indexPath!)
			// do stuff with the cell
			
			if (indexPath!.row == 0) {// tutte le cam
                    print("path 0 ");

			}
			/*else  if (indexPath!.row ==  collectionView!.numberOfSections() - 1){ // crea vista
                println("path  \(collectionView!.numberOfItemsInSection(0)	 - 1) ");
			}*/
			else  {// apri vista id
				
				deleteindex = indexPath!.row - 1

                deleteSection = indexPath!.section


				//self.alertMessage("eliminaVistaTitle".loc(), message: "eliminaVistaMessage".loc() , button: "annulla".loc() ,button2: "elimina".loc() , delegate: self , _tag: 20 )

                var  viste = ([Vista] (DataManager.Instance.impianti[deleteSection ].views_user.values))
                viste.sortInPlace({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })
                let  v  = viste[deleteindex  ] as Vista?

                dispatch_async(dispatch_get_main_queue(), {
                    let alert: UIAlertView = UIAlertView()
                    alert.delegate = self
                    alert.title = "opzioniVistaTitle".loc()
                    alert.message = "opzioniVistaMessage".loc()






                    let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
                    if( defaults.stringForKey("autostartCamId")  == v!.id )
                    {
                        alert.addButtonWithTitle("autostartOff".loc())
                    }
                    else{
                        alert.addButtonWithTitle("autostartOn".loc())
                    }

                    alert.addButtonWithTitle("modifica".loc())
                    alert.addButtonWithTitle("elimina".loc())
                    alert.addButtonWithTitle("annulla".loc() )

                    alert.delegate = self
                    alert.tag = 20
                    alert.show()
                });

			}
			
		}
	}
	
    var deleteindex : Int = 0
	var deleteSection : Int = 0
	
	func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
		
		if (alertView.tag == 20)
		{
			if(buttonIndex == 3){ //annulla

			}
            else if(buttonIndex == 1){ // modifica
                print("modifica vista", terminator: "");
                openModifica()
            }
            else if(buttonIndex == 0){  //autostart
                print("autostar button", terminator: "");
                setAutostart()
            }
			else if(buttonIndex == 2){  // elimina
				print("button1")
				
				self.addLoading()
				deleteView()
			}
			
		}
		if (alertView.tag == 1) // error
		{
			
		}
		if (alertView.tag == 2 )
		{
			dispatch_async(dispatch_get_main_queue(), {
				
				self.removeLoading()
				self.view.bringSubviewToFront(self.collectionView!)
			});
		}
		
	}
	
	func deleteView()
	{

       var  viste = ([Vista] (DataManager.Instance.impianti[deleteSection ].views_user.values))
        viste.sortInPlace({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })

         let  v  = viste[deleteindex  ] as Vista?  //visteUser[indexPath.row ] as Vista?  //  indexPath.row - 1

        //delete from iew
		deleteCall( v!.id )

		//delete from data manager
		
		DataManager.Instance.removeViewFromImp(v!.install! , viewId: v!.id)

        collectionView?.reloadData()
		
	}
	
	func deleteCall( viewId: String)
	{
		let asd = "\(User.Instance.serverUrl)\(User.deleteViewApiUrl(viewId))"
		
		
		let saveUrl : NSURL? = NSURL(string: asd)
		print ( saveUrl )
		
		if(saveUrl != nil ){
			
			var request1 = NSMutableURLRequest (URL: saveUrl! )
			var session1 = NSURLSession.sharedSession()
			request1.HTTPMethod = "GET"
			
			var task = session1.dataTaskWithRequest(request1, completionHandler: {data, response, error -> Void in
				
				//self.removeLoading()
				
				var error: NSError?
				do {
					let xmlDoc = try AEXMLDocument(xmlData: data!)
					print(xmlDoc.xmlString)
					
					if(xmlDoc.root["result"].value == "true") {
						//self.alertMessage( "cancellaOkTitle".loc() , message:"cancellaOkMessage".loc() , button:"ok" )
						
						self.alertMessage("cancellaOkTitle".loc() , message: "cancellaOkMessage".loc() , button: "ok", delegate: self, _tag: 2)
						//self.removeLoading()
					}
					else{
						//self.alertMessage( "cancellaErrorTitle".loc() , message:"cancellaErrorMessage".loc() , button:"ok" )

                        let errorMesage = xmlDoc.root["error"].value
                        let errorCode = xmlDoc.root["error_code"].value
                        
                        //"cancellaErrorMessage".loc()
						self.alertMessage("cancellaErrorTitle".loc() , message: errorMesage! , button: "ok", delegate: self, _tag: 2)

					}
					
				} catch _ {
				}
			})
			
			task.resume()
		}
		else{
			
		}
	}
	


    func openModifica()
    {


        self.addLoading()


        var  viste = ([Vista] (DataManager.Instance.impianti[deleteSection ].views_user.values))
        viste.sortInPlace({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })

        let  v  = viste[deleteindex  ] as Vista?

        dispatch_async(dispatch_get_main_queue(), {

            let creaVistaViewController = self.storyboard?.instantiateViewControllerWithIdentifier("creaVistaViewController") as! CreaVistaViewController
            creaVistaViewController.isEdit = true
            creaVistaViewController.selectedVista = v
            self.navigationController?.pushViewController(creaVistaViewController, animated: true)

        });

    }

    func setAutostart(){

        var  viste = ([Vista] (DataManager.Instance.impianti[deleteSection ].views_user.values))
        viste.sortInPlace({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })

        let  v  = viste[deleteindex  ] as Vista?


        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()

        if( defaults.stringForKey("autostartCamId")  == v!.id )
        {
            defaults.setValue(  "" , forKey: "autostartCamId")
              defaults.setValue(  "" , forKey: "autostartImpianto")
        }
        else{
            defaults.setValue(   v!.id , forKey: "autostartCamId")
               defaults.setValue(  v!.install , forKey: "autostartImpianto")
        }

    }
	
	
    func checkAutostart(){

        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()

        if(defaults.boolForKey("doneAutostart"))
        {
            return;
        }

        defaults.setBool( true , forKey: "doneAutostart")

        let autostartCamId = defaults.stringForKey("autostartCamId")
        let autostartImpId = defaults.stringForKey("autostartImpianto")

        print("autostart ID  \( autostartCamId  )", terminator: "");

        if( autostartCamId != nil )
        {
            if( autostartCamId! != "")
            {
                if( autostartImpId == nil ){
                    defaults.setValue(  "" , forKey: "autostartCamId")
                    defaults.setValue(  "" , forKey: "autostartImpianto")
                    return;
                }



                var numero = 0

                if let Imp = DataManager.Instance.installs[ autostartImpId! ]
                {
                var viste = ([Vista] (DataManager.Instance.installs[ autostartImpId! ]!.views_user.values))
                viste.sortInPlace({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })


               var i = 0;
               for  v in viste
               {
                    if ( v.id == autostartCamId  ){

                        break;
                    }
                i++;
                }

                if let  v  =  DataManager.Instance.allUserView[ autostartCamId! ] as Vista?
                {
                    let visteViewController = self.storyboard?.instantiateViewControllerWithIdentifier("visteViewController") as!  VisteViewController
                    visteViewController.visteList =  viste //visteUser
                    visteViewController.currentVista = v
                    visteViewController.currentVistaId = i
                    navigationController?.pushViewController(visteViewController , animated: true)
                }
                }
                else
                {
                    defaults.setValue(  "" , forKey: "autostartCamId")
                    defaults.setValue(  "" , forKey: "autostartImpianto")
                    return
                }

            }
        }
    }

}






















