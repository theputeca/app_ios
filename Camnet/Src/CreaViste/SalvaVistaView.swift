//
//  SalvaVistaView.swift
//  Camnet
//
//  Created by giulio piana on 29/01/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation

protocol SaveViewProtocol{
	func callBackSave(vistaName :String ,  vistaid :String)
    func callBackSaveCancel()
}

class SalvaVistaView : UIView, UITextFieldDelegate, UIAlertViewDelegate
{
	
	var delegate : SaveViewProtocol?
	
	var viewController : UIViewController?
	
	@IBOutlet weak var nomeText: UITextField!
	@IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var escLbl: UITextField!
	@IBOutlet weak var annullaBtn: UIButton!
	@IBOutlet weak var salvaBtn: UIButton!
	
	
	var currentInstallId = ""
	var currentViewId : String?
	var currentViewName : String?
	
	var currentViewAttr : String = ""
	
	
	@IBAction func annullaBtnClick(sender: UIButton) {
		self.delegate?.callBackSaveCancel(  )
		self.removeFromSuperview()
	}
	
	@IBAction func salvaBtnClick(sender: UIButton) {
		
		print(" salva vista con nome \(nomeText.text )")
		
		if(nomeText.text != ""){
			salvaCall()
		}
		else{
			nomeText.backgroundColor = UIColor.orangeColor()
		}
	}
	
	
	override func awakeFromNib() {
		
		titleLbl.text = "salvaVistaTitle".loc()
		nomeText.placeholder = "salvaVistaText".loc()
		escLbl.placeholder = "salvaVistaDesc".loc()
		
		escLbl.alpha = 0
		
		if ( currentViewName != nil ) {
			nomeText.text = currentViewName
		}
		else{
			nomeText.text = ""
		}
		
		//	escLbl.text = "salvaVistaDesc".loc()
		
		
		
		annullaBtn.setTitle("annulla".loc() , forState: UIControlState.Normal)
		salvaBtn.setTitle("salva".loc() , forState: UIControlState.Normal)
		
		super.awakeFromNib()
		nomeText.delegate = self
		nomeText.becomeFirstResponder()
		//nomeText.text = ""
		
		let viewSalva = self.viewWithTag(1)
		viewSalva!.layer.masksToBounds = true
		viewSalva!.layer.cornerRadius = 25
		
		let border : CALayer = CALayer()
		border.borderColor = UIColor.sfondoPlayer().CGColor
		border.frame = CGRect(x: 0 /*viewSalva!.frame.origin.x*/  , y: annullaBtn.frame.origin.y  , width:  viewSalva!.frame.size.width, height: 1)
		border.borderWidth = 1
		viewSalva!.layer.addSublayer(border)
		
		let border2 : CALayer = CALayer()
		border2.borderColor = UIColor.sfondoPlayer().CGColor
		border2.frame = CGRect(x: viewSalva!.frame.width / 2   , y: annullaBtn.frame.origin.y  , width: 1 , height:  annullaBtn.frame.height )
		border2.borderWidth = 1
		viewSalva!.layer.addSublayer(border2)
	}
	
	
	
	// TEXT FIELD DELEGATE
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		print("RETURN")
		
		salvaBtnClick(UIButton())
		
		return true
	}
	
	func textFieldDidBeginEditing(textField: UITextField) {
		
		//textField.text = ""
		
		if ( currentViewName != nil ) {
			textField.text = currentViewName
		}
		else{
			textField.text = ""
		}
	}
	
	//salva
	func salvaCall()	{
		
		
		self.viewController!.addLoading()
		
		currentViewAttr = currentViewAttr.stringByReplacingOccurrencesOfString("<viewName>", withString: nomeText.text!, options: [], range: nil)
		
		//creo url corretto
		let asd = "\(User.Instance.serverUrl)\(User.saveViewApiUrl(currentInstallId, viewId: currentViewId, attr: currentViewAttr))"
		let parsedJson = asd.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
		
		//creo nsurl per la chiamata
		let saveUrl : NSURL? = NSURL(string: parsedJson!)
		if(saveUrl != nil ){
			
			var request1 = NSMutableURLRequest (URL: saveUrl! )
			var session1 = NSURLSession.sharedSession()
			request1.HTTPMethod = "GET"
			
			//eseguo la richiesta
			var task = session1.dataTaskWithRequest(request1, completionHandler: {data, response, error -> Void in
				
				var error: NSError?
				do {
					let xmlDoc = try AEXMLDocument(xmlData: data!)
					print(xmlDoc.xmlString)
					
					//se va a buon fine
					if(xmlDoc.root["result"].value == "true") {

                         self.IDCamSaved = xmlDoc.root["data"].value
						self.viewController!.alertMessage( "salvaOkTitle".loc() , message:"salvaOkMessage".loc() , button:"ok" ,delegate: self , _tag: 0 )
					}
						//se non va a buon fine
					else{
						self.viewController!.alertMessage( "salvaErrorTitle".loc() , message:"salvaErrorMessage".loc() , button:"ok" ,delegate: self ,_tag: 1)
					}
					
				} catch _ {
				}
			})
			
			task.resume()
		}
		else{
			
		}
		
	}
	
	func goBack()	{

          self.delegate?.callBackSaveCancel(  )

        dispatch_async(dispatch_get_main_queue(), {


			self.removeFromSuperview()
		});
		
		
	}


    var IDCamSaved : String? = "0"
	
	func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
		
		if (alertView.tag == 0)
		{
			
			self.delegate?.callBackSave( self.nomeText.text!, vistaid: IDCamSaved! )
            //self.delegate?.callBackSave(<#vistaName: String#>, vistaid: <#String#>)
		}
		if (alertView.tag == 1) // error
		{
			
			dispatch_async(dispatch_get_main_queue(), {
				self.delegate?.callBackSaveCancel(  )
				self.viewController!.removeLoading()
				self.removeFromSuperview()
			});
			
		}
	}
	
	
	
}