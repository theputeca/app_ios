//
//  UiButtonExtension.swift
//  Camnet
//
//  Created by giulio piana on 19/04/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation

import UIKit

extension UIButton {


	func enable(_enable : Bool )
	{
		self.enabled = _enable
		
		if(_enable){
			self.alpha = 1
			
		}
		else{
			self.alpha = 0.5
			
			self.tintColor = UIColor.sfondoHeader()
		}
	}
	
	func acceso(_acceso : Bool )
	{

		if(_acceso){
			self.tintColor = UIColor.arancio()
		}
		else{
			self.tintColor = UIColor.sfondoHeader()
		}
	}
	

}
	