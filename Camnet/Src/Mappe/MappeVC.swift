//
//  FirstViewController.swift
//  Camnet
//
//  Created by giulio piana on 11/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import UIKit




class MappeVC:  UIViewController , UITableViewDataSource ,UITableViewDelegate , UICollectionViewDataSource , UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
	
	var ImpiantiTable2 : UITableView!
	var CamCollectionView2: UICollectionView!
	
	var selectedImpianto : Impianto?
	var selectedIndex : Int = 0
	
	
	var isShowCollection : Bool = false
	
	@IBAction func logOutClick(sender: UIButton) {
		self.logout()
	}
	
	///////
	override func viewDidLoad() {
		super.viewDidLoad()
		
		MainViewController.Instance?.mainNavigationController = self.navigationController
		self.title = "mappe".loc()
		
		
		self.navigationController!.navigationBar.tintColor = UIColor.arancio()
		
		CamCollectionView2 = NSBundle.mainBundle().loadNibNamed("CVSelection", owner: self, options: nil)[0] as? UICollectionView
		CamCollectionView2.frame = CGRect(x: 300 , y: 0 , width: self.view.frame.width - 300	, height:  self.view.frame.height )
		CamCollectionView2.backgroundColor = UIColor.whiteColor()
		
		if(self.IsPad()){
			self.automaticallyAdjustsScrollViewInsets = false
			let nib = UINib(nibName: "CVHeaderCell", bundle: nil)
			CamCollectionView2.registerNib(nib , forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
		}
		else{//IPHIONE
			let nib = UINib(nibName: "CVHeaderCellIphone", bundle: nil)
			CamCollectionView2.registerNib(nib , forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
			
		}
		self.view.addSubview(CamCollectionView2)
		CamCollectionView2.delegate = self
		CamCollectionView2.dataSource = self
		
		//table view
		ImpiantiTable2   = UITableView()
		if(self.IsPad()){
			ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 300	, height: self.view.frame.height)
			ImpiantiTable2.separatorStyle = UITableViewCellSeparatorStyle.None
			
		}
		else{//IPHIONE
			ImpiantiTable2.frame = CGRect(x: 0 , y:  self.navigationController!.navigationBar.frame.height  , width: self.frame().width	, height: self.frame().height)
			ImpiantiTable2.separatorStyle = UITableViewCellSeparatorStyle.None
			
		}
		self.view.addSubview(ImpiantiTable2)
		self.view.bringSubviewToFront(ImpiantiTable2)
		
		//ImpiantiTable2.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
		
		ImpiantiTable2.delegate=self
		ImpiantiTable2.dataSource = self
		
		//bordo
		ImpiantiTable2.layer.masksToBounds = true
		ImpiantiTable2.layer.borderWidth = 1
		ImpiantiTable2.layer.borderColor =  UIColor.sfondoHeader().CGColor
		
		
		
		
		if ( isShowCollection ){
			
			CamCollectionView2.frame = CGRect(x: 0 , y: 0 , width: self.frame().width	, height: self.frame().height)
			ImpiantiTable2.alpha = 0
			CamCollectionView2.alpha = 1
			
			if(selectedImpianto != nil){
				self.title =  "mappe".loc() + ": " + selectedImpianto!.name
			}
		}
		else{
			if (!IsPad() ) {
				CamCollectionView2.alpha = 0
			}
			for  imp : Impianto in DataManager.Instance.impianti {
				if (imp.maps.count >  0){
					selectedImpianto = imp
					break
				}
			}
			
			
		}
		
	}
	
	 override func viewDidAppear(animated: Bool) {
		
		self.tabBarController?.tabBar.hidden = false
		
		if(self.IsPad())
		{
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 300	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height)
				CamCollectionView2.frame = CGRect(x: 300, y:  self.navigationController!.navigationBar.frame.height  , width:  self.view.frame.width - 300	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height - self.tabBarController!.tabBar.frame.height)
			}
			else{
				ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 200	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height - self.tabBarController!.tabBar.frame.height)
				CamCollectionView2.frame = CGRect(x: 200 , y:  self.navigationController!.navigationBar.frame.height  , width:  self.view.frame.width - 200	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height - self.tabBarController!.tabBar.frame.height )
				
			}
			
			ImpiantiTable2.reloadData()
			CamCollectionView2.reloadData()
			
		}
		else{//IPHONE
			if(isShowCollection){
				
			}
			else{
				
			}
			ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: self.view.frame.width	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height)
			CamCollectionView2.frame = CGRect(x: 0 , y: CamCollectionView2.frame.origin.y , width: self.view.frame.width	, height: self.view.frame.height)
			ImpiantiTable2.reloadData()
			CamCollectionView2.reloadData()
			
			
		}
		
	}
	
	override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		
		if(self.IsPad())
		{
			if (  toInterfaceOrientation.isPortrait		){
				ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 200	, height: self.view.frame.width - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height )
				CamCollectionView2.frame = CGRect(x: 200 , y: self.navigationController!.navigationBar.frame.height, width:  self.view.frame.height - 200	, height: self.view.frame.width  - self.navigationController!.navigationBar.frame.height - self.tabBarController!.tabBar.frame.height )
				
			}
			else{
				ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 300	, height: self.view.frame.width - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height)
				CamCollectionView2.frame = CGRect(x: 300, y: self.navigationController!.navigationBar.frame.height , width:  self.view.frame.height - 300	, height: self.view.frame.width  - self.navigationController!.navigationBar.frame.height - self.tabBarController!.tabBar.frame.height)
				
			}
			
			
			ImpiantiTable2.reloadData()
			CamCollectionView2.reloadData()
		}
	}
	
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
		if (self.IsPad()){
		}
		else{//IPHONE
			if(isShowCollection){
				
			}
			else{
				
			}
			ImpiantiTable2.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: self.view.frame.width	, height: self.view.frame.height)
			CamCollectionView2.frame = CGRect(x: 0 , y:  0 , width: self.view.frame.width	, height: self.view.frame.height)
			ImpiantiTable2.reloadData()
			CamCollectionView2.reloadData()
			
			
		}
	}
	
	
	
	//ACTION
	
	@IBAction func selectMappeClick (sender : UIButton)
	{
		
		
	}
	
	//TABEL VIEW
	
	//number row
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return DataManager.Instance.impianti.count
	}
	//cell for row row at index
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		
		var cell :UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("CellTitleDescription") as UITableViewCell?
		if(cell == nil)
		{
			let nib = UINib(nibName: "CellTitleDescription", bundle: nil)
			tableView.registerNib(nib, forCellReuseIdentifier: "CellTitleDescription")
			cell = tableView.dequeueReusableCellWithIdentifier("CellTitleDescription") as UITableViewCell?
		}
		
		let bgColorView = UIView()
		bgColorView.backgroundColor = UIColor.arancioSelezione()
		cell.selectedBackgroundView = bgColorView
		
		let title : UILabel =  cell.viewWithTag(1) as! UILabel
		title.text = DataManager.Instance.impianti[ indexPath.row ].name
		
		let desc : UILabel = cell.viewWithTag(2)as! UILabel
		desc.text = DataManager.Instance.impianti[ indexPath.row ].description
		
        if( DataManager.Instance.impianti[ indexPath.row ].id == "0" 	)
        {

            if (selectedIndex == indexPath.row) { selectedIndex++ }

            title.alpha = 0
            desc.alpha = 0
            cell.alpha = 0
            let freccia  : UIImageView = cell.viewWithTag(3) as! UIImageView
            freccia.alpha = 0
        }
        else
        {
            title.alpha = 1
            desc.alpha = 0
            cell.alpha = 1
            let freccia  : UIImageView = cell.viewWithTag(3) as! UIImageView
            freccia.alpha = 1
        }


		cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, tableView.frame.width  , 65 )
		
		
		let border : CALayer = CALayer()
		border.borderColor = UIColor.sfondoChiaro().CGColor
		border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: 1)
		border.borderWidth = 1
		cell.layer.addSublayer(border)
		//}
		
		if(self.IsPad()){
			if(indexPath.row == selectedIndex )	{
				tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
			}
		}
		
		return cell
	}
	//heigt row
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		
		if( DataManager.Instance.impianti[ indexPath.row ].id == "0" 	) { return 0 }
		
		return 65
	}
	
	// table view header
	
	func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		
		let headerview : UIView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
		headerview.backgroundColor = UIColor.sfondoHeader()
		
		let title : UILabel = UILabel (frame: headerview.frame)
		title.text = "IMPIANTI"
		title.textAlignment = NSTextAlignment.Center
		title.textColor = UIColor.sfondoChiaro()
		
		headerview.addSubview(title)
		
		return  headerview
		
	}
	
	func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return ""
	}
	
	func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		if(self.IsPad()){
			return 30
		}
		else{
			return 0
		}
	}
	//tableView Delegate
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		
		
		
		if(!self.IsPad()){
			selectedIndex = indexPath.row
			selectedImpianto = DataManager.Instance.impianti[ indexPath.row ]
			CamCollectionView2.reloadData()
			let mappeVc = MappeVC()
			mappeVc.selectedImpianto = selectedImpianto
			mappeVc.isShowCollection = true
			self.navigationController?.pushViewController(mappeVc , animated: true)
			
		}
		else{
			selectedIndex = indexPath.row
			selectedImpianto = DataManager.Instance.impianti[ indexPath.row ]
			CamCollectionView2.reloadData()
			if(selectedIndex == indexPath.row) { return }
			
		}
		
	}
	
	
	
	// COLLECTION VIEW
	//Data
	
	func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
		return 1
	}
	//number of item
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if (selectedImpianto != nil)
		{
			
		return selectedImpianto!.maps.count
		}
		else {
			return 0
		}
	}
	
	//cellforitem
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		
		// mappa
		if(self.IsPad()){
			
			let nib = UINib(nibName: "CollCellTitleDes", bundle: nil) //CollCellWithImage
			collectionView.registerNib(nib, forCellWithReuseIdentifier: "CollCellTitleDes")
			let cell :UICollectionViewCell!   = collectionView.dequeueReusableCellWithReuseIdentifier("CollCellTitleDes", forIndexPath: indexPath) as? UICollectionViewCell
			
			//resize the cell frame
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, CamCollectionView2.frame.width / 2 - 10 , 125)
				
			}
			else{
				cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, CamCollectionView2.frame.width / 2 - 10 , 125)
				
			}
			
			//arrotonda bordi
			cell.layer.masksToBounds = true;
			cell.layer.cornerRadius = 25;
			
			//set the title
			let title : UILabel = cell.viewWithTag(1) as! UILabel
			let  map  = [Mappa] (selectedImpianto!.maps.values) [indexPath.row ]
			title.text = map.name
			
			let desc : UILabel = cell.viewWithTag(3) as! UILabel
			desc.text = map.description
			
			return cell
			
		}
		else{
			
			let nib = UINib(nibName: "CollCellTitleDesIphone", bundle: nil) //CollCellWithImageIphone
			collectionView.registerNib(nib, forCellWithReuseIdentifier: "CollCellTitleDesIphone")
			let cell :UICollectionViewCell!  = collectionView.dequeueReusableCellWithReuseIdentifier("CollCellTitleDesIphone", forIndexPath: indexPath) as? UICollectionViewCell
			//resize the cell frame
			cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, CamCollectionView2.frame.width  ,65 )
			
			//set the title
			let title : UILabel = cell.viewWithTag(1) as! UILabel
			let  map  = [Mappa] (selectedImpianto!.maps.values) [indexPath.row ]
			title.text = map.name
			
			let desc : UILabel = cell.viewWithTag(3) as! UILabel
			desc.text = map.description
			
			let border : CALayer = CALayer()
			border.borderColor = UIColor.sfondoChiaro().CGColor
			border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: cell.frame.size.height)
			border.borderWidth = 1
			cell.layer.addSublayer(border)
			
			
			
			return cell
			
		}
	}
	//size for row
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		if(self.IsPad()){
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				return CGSizeMake(CamCollectionView2.frame.width / 2 - 10  , 125 )
				
			}
			else{
				return CGSizeMake(CamCollectionView2.frame.width / 2 - 10  , 125 )
				
			}
		}
			//IPHONE
		else{
			return CGSizeMake(CamCollectionView2.frame.width   , 65 )
			
			
		}
		
	}
	
	
	//HEADER
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
		if(self.IsPad()){
			return CGSizeMake(CamCollectionView2.frame.width  , 85 )
			
		}
			//IPHONE
		else{
			return CGSize(width: 0, height: 0)
			
		}
		
	}
	
	func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
		
		
		if (kind == UICollectionElementKindSectionHeader){
			
			let header :UICollectionReusableView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView", forIndexPath: indexPath) 
			if(self.IsPad()){
				
				let title : UILabel = header.viewWithTag(1) as! UILabel
				let button : UIButton = header.viewWithTag(2) as! UIButton
				
				button.alpha = 0
				button.enabled = false
				
				if (indexPath.section==0){
					
					title.text = "mappe".loc() + " (" +  String(collectionView.numberOfItemsInSection(0)) + ")"
					
				}
			}
			else{//iphone
				
			}
			return header
		}
		
		
		
		return UICollectionReusableView(	)
	}
	
	//HIGHLIGHT
	func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
		let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
		cell.contentView.backgroundColor = UIColor.arancioSelezione()
	}
	func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
		if(self.IsPad())
		{
			let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
			cell.contentView.backgroundColor = UIColor.sfondoChiaro()
		}
		else{
			let cell : UICollectionViewCell  = collectionView.cellForItemAtIndexPath(indexPath)!
			cell.contentView.backgroundColor = UIColor.whiteColor()
		}
	}
	
	// CollectionView Delegate
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		if (indexPath.section == 0){
			
			let storyboard = UIStoryboard(name: "Main", bundle: nil)
			
			let detailMap = storyboard.instantiateViewControllerWithIdentifier("detailMappa") as! DetailMappaVC
			
			let  map  = [Mappa] (selectedImpianto!.maps.values) [indexPath.row ]
			detailMap.selectedMap = map
			detailMap.selectedImpianto = selectedImpianto!
			
			navigationController?.pushViewController(detailMap, animated: true)
			
		}
		
	}
	
}
