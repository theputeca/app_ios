//
//  StoricoPlayerVC.swift
//  Camnet
//
//  Created by giulio piana on 20/01/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation
import MediaPlayer


class StoricoPlayerVC: UIViewController , UIPickerViewDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate,UIAlertViewDelegate,VLCMediaPlayerDelegate,VLCMediaDelegate {
	
//	var player : VKPlayerController?
    var player : VLCMediaPlayer?
	var playerview : UIView?

	var selectedCamId : String?
	var selectedCam : Cam?
	
	var dataPickerView : UIDatePicker?// = UIDatePicker()
	var oraPickerView : UIDatePicker?// = UIDatePicker()
	
	var dataPikerOpen : Bool = false
	var oraPickerOpen : Bool = false
	
	var dataBtn : UIButton?
	var oraBtn : UIButton?
	
	var imageNullView : UIImageView?
	
	var films : [Film] = [Film]()
	
	var labelSelectFilm : UILabel?
	
	
	var comandiView : UIView = UIView()
	
	//barra player
	var barraPlayer : BarraPlayer = BarraPlayer()
	var barraPlayerHeigth : Float = 70
	//zoom scrollview
	var scrollview : UIScrollView = UIScrollView()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.view.backgroundColor = UIColor.sfondoScuro()
		
		//nasconde barra
		self.tabBarController?.tabBar.hidden = true
		//toglie la gesture del navigation contoller
		self.navigationController?.interactivePopGestureRecognizer!.enabled = false;
		self.navigationController?.interactivePopGestureRecognizer!.delegate = self
		
		
		self.title = /*"storicoPlayerTitle".loc() +*/ selectedCam!.name
		
		
		
		//var url :String="rtsp://live1.wm.skynews.servecast.net/skynews_wmlz_live300k"
		//crea video

//		player = VKPlayerController();
//		player!.view.alpha = 0


        let arr = ["--extraintf="] as NSArray
        player =  VLCMediaPlayer(options: arr as [AnyObject])
		playerview = UIView()
		
		//aggiungo scrollview per zoom
		
		scrollview = UIScrollView()
		scrollview.backgroundColor = UIColor.sfondoScuro()
		self.view.addSubview(scrollview);
	//	scrollview.addSubview(player!.view)
        scrollview.addSubview(playerview!)
		scrollview.delegate = self
		scrollview.bounces=true
		scrollview.bouncesZoom=true
		
		
		let tapGesture :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onPlayerTapped:")
		tapGesture.numberOfTapsRequired = 1
		scrollview.addGestureRecognizer(tapGesture)
		playerview!.addGestureRecognizer(tapGesture)
		// Crea Barra PLAYER
		
		barraPlayer   = NSBundle.mainBundle().loadNibNamed("BarraPlayer", owner: self, options: nil)[0] as! BarraPlayer
		barraPlayer.frame = CGRect(x:0 , y: self.frame().height - 80  , width: self.frame().width, height: 80 )
		
//		barraPlayer.player = player!
		self.view.addSubview(barraPlayer)
		self.view.bringSubviewToFront(barraPlayer)
		
		barraPlayer.alpha = 0
		
		//crea barra comandi
		
		comandiView   = NSBundle.mainBundle().loadNibNamed("StoricoComandiView", owner: self, options: nil)[0] as! UIView
		comandiView.frame = CGRect(x:0 , y:  self.frameBetweenBar().origin.y , width: self.frame().width, height: 48)
		
		self.view.addSubview(comandiView)
		self.view.bringSubviewToFront(comandiView)
		
		

		dataBtn = comandiView.viewWithTag(3) as! UIButton?
		oraBtn  = comandiView.viewWithTag(5) as! UIButton?
		let confirmBtn : UIButton = comandiView.viewWithTag(6) as! UIButton
		
		
		confirmBtn.setTitle("aggiorna".loc() , forState: UIControlState.Normal)
		
		dataBtn!.addTarget(self, action: Selector("dataSelectorClick:") , forControlEvents: UIControlEvents.TouchUpInside)
		oraBtn!.addTarget(self, action: Selector("oraSelectorClick:") , forControlEvents: UIControlEvents.TouchUpInside)
		confirmBtn.addTarget(self, action: Selector("confirmButttonClick:") , forControlEvents: UIControlEvents.TouchUpInside)
		
		//DATA
		dataPickerView = UIDatePicker()
		if ( self.IsPad() )	{
			dataPickerView!.frame = CGRect(x: dataBtn!.frame.origin.x , y: comandiView.frame.origin.y + dataBtn!.frame.origin.y + dataBtn!.frame.height   , width: 300, height: 0)
		}
		else		{
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				dataPickerView!.frame = CGRect(x: dataBtn!.frame.origin.x , y: comandiView.frame.origin.y + comandiView.frame.height  , width: 320, height: 0)
			}else{
				dataPickerView!.frame = CGRect(x: 0, y: comandiView.frame.origin.y + comandiView.frame.height  , width: self.view.frame.width , height: 0)
			}
		}
		
		dataPickerView?.backgroundColor = UIColor.sfondoChiaro()
		dataPickerView!.datePickerMode = UIDatePickerMode.Date
		dataPickerView!.date = NSDate()
		dataPickerView!.addTarget(self, action: Selector("dateValueChanged:"), forControlEvents: UIControlEvents.ValueChanged )
		dataPickerView!.alpha = 0
		
		
		//ORA
		oraPickerView = UIDatePicker()
		if ( self.IsPad() )		{
			oraPickerView!.frame = CGRect(x: oraBtn!.frame.origin.x , y: comandiView.frame.origin.y + oraBtn!.frame.origin.y + oraBtn!.frame.height  , width: 300, height: 0)
		}
		else		{
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				oraPickerView!.frame = CGRect(x:  oraBtn!.frame.origin.x , y: comandiView.frame.origin.y + comandiView.frame.height  , width: 320, height: 0)
			}else{
				oraPickerView!.frame = CGRect(x: 0 , y: comandiView.frame.origin.y + comandiView.frame.height  , width: self.view.frame.width , height: 0)
			}
		}
		
		oraPickerView?.backgroundColor = UIColor.sfondoChiaro()
		oraPickerView!.datePickerMode = UIDatePickerMode.Time
		oraPickerView!.locale = NSLocale(localeIdentifier: "en_GB")
		oraPickerView!.date = NSDate()
		oraPickerView!.addTarget(self, action: Selector("oraValueChanged:"), forControlEvents: UIControlEvents.ValueChanged )
		oraPickerView!.alpha = 0
		
		dataBtn?.setTitle(  dateformatterDate(dataPickerView!.date) as String , forState: UIControlState.Normal)
		oraBtn?.setTitle( oraformatterDate(oraPickerView!.date) as String , forState: UIControlState.Normal)
		
		//contorno
		if ( self.IsPad() )
		{
			dataBtn?.layer.masksToBounds = true
			dataBtn?.layer.borderWidth = 1
			dataBtn?.layer.borderColor =  UIColor.sfondoChiaro().CGColor
			
			oraBtn?.layer.masksToBounds = true
			oraBtn?.layer.borderWidth = 1
			oraBtn?.layer.borderColor =  UIColor.sfondoChiaro().CGColor
		}
		
		self.view.addSubview(dataPickerView!)
		self.view.addSubview(oraPickerView!)
		
		
		
		
		labelSelectFilm = self.view.viewWithTag(10) as! UILabel?
		labelSelectFilm!.frame = self.view.frame
		labelSelectFilm!.text = "selectStoricoDate".loc()
		self.view.bringSubviewToFront(labelSelectFilm!)
		labelSelectFilm!.removeFromSuperview()
		
	}

	var lblVoidVideo : UILabel?
	func createLblVoidVideo() {
		
		if(lblVoidVideo != nil )
		{
			lblVoidVideo?.alpha = 1
		}
		else{
			lblVoidVideo = UILabel(frame: self.view.frame)
		
			lblVoidVideo!.tag = 30
			lblVoidVideo!.text = "msgNoVideoStorico".loc() // "nessun video presente"
			lblVoidVideo!.textAlignment = NSTextAlignment.Center
			
			lblVoidVideo!.textColor = UIColor.arancio()
			self.view.addSubview( lblVoidVideo! )
			
			self.view.bringSubviewToFront(dataPickerView!)
			self.view.bringSubviewToFront(oraPickerView!)
		}
	}

	func removeLblVoidVideo() {
		
		if(lblVoidVideo != nil )
		{
			lblVoidVideo?.alpha = 0
			lblVoidVideo?.removeFromSuperview(  )
			lblVoidVideo = nil
		}
		else{
			
		}
		
		
		
	}
	
	override func viewWillDisappear(animated: Bool) {
		
        if( _timerDuration != nil  )
        {
            if ( _timerDuration!.valid )
            {
                _timerDuration!.invalidate()
            }
            _timerDuration = nil;
        }



		self.barraPlayer.stopDurationTimer()
		
        if let p = self.player
		{
            p.stop()
            p.delegate=nil
            playerview?.removeFromSuperview()
            player = nil
            self.barraPlayer.player = nil

	}

		if let timer = timerPlay{
			if ( timer.valid ) {
				timer.invalidate()
			}
		}
		
	}
	
	override func viewWillAppear(animated: Bool) {
		
		
		super.viewWillAppear(animated)
		
		self.automaticallyAdjustsScrollViewInsets = false



        if(self.films.count == 0 && player == nil)
       {
          MainViewController.Instance?.mainNavigationController?.popToRootViewControllerAnimated(false)
          return
       }

       if( player == nil    )
        {
        creaNuovoPlayer(selectedFilmNumber)
       }

		dataBtn?.setTitle( "" + (dateformatterDate(dataPickerView!.date) as String) , forState: UIControlState.Normal)
		oraBtn?.setTitle( "" + (oraformatterDate(oraPickerView!.date) as String) , forState: UIControlState.Normal)
		
		if(self.IsPad())
		{
			self.automaticallyAdjustsScrollViewInsets = false
			
			comandiView.frame = CGRect(x:0 , y:  self.frameBetweenBar().origin.y , width: self.view.frame.width, height: 48)
			barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 70  , width: self.view.frame.width, height: 70 )
			
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){

                playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height - 70 - 48 )
                scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y  + 48 , width: self.view.frame.width  , height: self.view.frame.height - 70 - 48 )

                let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
                scrollview.contentSize = scrollSize
			}
			else{
                playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width, height: self.view.frame.height - 70 - 48 )

                let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
                scrollview.contentSize = scrollSize

            }
			
			let scrollViewFrame = scrollview.frame
			let scaleWidth = scrollViewFrame.size.width / scrollview.contentSize.width
			let scaleHeight = scrollViewFrame.size.height / scrollview.contentSize.height
			let minScale = min(scaleHeight, scaleWidth)
			
			scrollview.minimumZoomScale = minScale
			scrollview.maximumZoomScale = 10
			scrollview.zoomScale = minScale
			
			
		}
		else{ // iphone
			
			let dimeBarra : CGFloat = 50
			
			comandiView.frame = CGRect(x:0 , y:  self.frameBetweenBar().origin.y , width: self.view.frame.width, height: 48)
			
			barraPlayer.frame = CGRect ( x:0 , y: self.view.frame.height - 56  , width: self.view.frame.width, height: dimeBarra )
			
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){

                playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  ( self.frameBetweenBar().height - dimeBarra - 48 ) )
                scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y  + 48 , width: self.view.frame.width  , height: self.view.frame.height - 56 - 48 )

                let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
                scrollview.contentSize = scrollSize
			}
			else{
				
			    playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
				scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width, height: self.view.frame.height - 56 - 48 )
				
				let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
				scrollview.contentSize = scrollSize
			}
			
			let scrollViewFrame = scrollview.frame
			let scaleWidth = scrollViewFrame.size.width / scrollview.contentSize.width
			let scaleHeight = scrollViewFrame.size.height / scrollview.contentSize.height
			let minScale = min(scaleHeight, scaleWidth)
			
			scrollview.minimumZoomScale = minScale
			scrollview.maximumZoomScale = 10
			scrollview.zoomScale = minScale
			
		}

         setplayerAspectRatio(player!)
	}
	
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
		
		//setTabBarVisible(true, animated: false)
		
		
		if let loadingView = self.view.viewWithTag(95) 
		{
		 	loadingView.frame =  self.view.frame
		}
		
		self.dataPickerView!.alpha = 0
		self.oraPickerView!.alpha = 0
		oraPickerOpen = false
		dataPikerOpen = false
		
		scrollview.zoomScale = 1
		
		if(self.IsPad())
		{
			comandiView.frame = CGRect(x:0 , y:  self.frameBetweenBar().origin.y , width: self.view.frame.width, height: 48)
			barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 70  , width: self.view.frame.width, height: 70 )
			
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height - 70 - 48 )
				scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y  + 48 , width: self.view.frame.width  , height: self.view.frame.height - 70 - 48 )
				
				let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
				scrollview.contentSize = scrollSize
			}
			else{
				playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
				scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width, height: self.view.frame.height - 70 - 48 )
				
				let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
				scrollview.contentSize = scrollSize
			}
			
			
			let scrollViewFrame = scrollview.frame
			let scaleWidth = scrollViewFrame.size.width / scrollview.contentSize.width
			let scaleHeight = scrollViewFrame.size.height / scrollview.contentSize.height
			let minScale = min(scaleHeight, scaleWidth)
			
			scrollview.minimumZoomScale = minScale
			scrollview.maximumZoomScale = 10
			scrollview.zoomScale = minScale
			
		}
		else{ // Iphone
			
			comandiView.frame = CGRect(x:0 , y:  self.frameBetweenBar().origin.y , width: self.view.frame.width, height: 48)
			
			playerview!.frame = CGRect(x: 0,y: 0,width: self.view.frame.width , height: self.frameBetweenBar().height - 56 - 48 );
			scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width  , height: self.frameBetweenBar().height - 56 - 48 );
			
			barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 56  , width: self.view.frame.width, height: 56 )
			
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height - 56 - 48 )
				scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y  + 48 , width: self.view.frame.width  , height: self.view.frame.height - 56 - 48 )
				
				let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
				scrollview.contentSize = scrollSize
			}
			else{
				playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
				scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width, height: self.view.frame.height - 56 - 48 )
				
				let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
				scrollview.contentSize = scrollSize
			}
			
			
			let scrollViewFrame = scrollview.frame
			let scaleWidth = scrollViewFrame.size.width / scrollview.contentSize.width
			let scaleHeight = scrollViewFrame.size.height / scrollview.contentSize.height
			let minScale = min(scaleHeight, scaleWidth)
			
			scrollview.minimumZoomScale = minScale
			scrollview.maximumZoomScale = 10
			scrollview.zoomScale = minScale
		}
		
		if(lblVoidVideo != nil )
		{
			lblVoidVideo!.frame = self.view.frame
		}
        if(player != nil){
         setplayerAspectRatio(player!)
        }
	}
	
	override  func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		setTabBarVisible( true  , animated: true)
	}
	
	override func didReceiveMemoryWarning() {
		print("memory bad access")
	}
	
	
	//BUTTON EVENT
	
	
	@IBAction func confirmButttonClick(sender: UIButton)	{
		self.dataPickerView!.alpha = 0
		self.oraPickerView!.alpha = 0
		oraPickerOpen = false
		dataPikerOpen = false
		isOfsetBar = false

        seekFromBar = false
        seekFromSearch = true

		CallApi()
	}
	
	@IBAction func dataSelectorClick(sender: UIButton) {
		
		if ( self.IsPad() )	{
			dataPickerView!.frame = CGRect(x: dataBtn!.frame.origin.x , y: comandiView.frame.origin.y + dataBtn!.frame.origin.y + dataBtn!.frame.height   , width: 300, height: 250)
		}
		else		{
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				dataPickerView!.frame = CGRect(x: dataBtn!.frame.origin.x , y: comandiView.frame.origin.y + comandiView.frame.height  , width: 320, height: 250)
			}else{
				dataPickerView!.frame = CGRect(x: 0, y: comandiView.frame.origin.y + comandiView.frame.height  , width: self.view.frame.width , height: 250)
			}
		}
		
		
		self.oraPickerView!.alpha = 0
		oraPickerOpen = false
		
		if ( dataPikerOpen ){
			dataPikerOpen = false
			self.dataPickerView!.alpha = 0
		}
		else{
			dataPikerOpen = true
			self.dataPickerView!.alpha = 1
		}

        self.view.bringSubviewToFront(dataPickerView!)


	}
	
	@IBAction func oraSelectorClick(sender: UIButton) {
		
		if ( self.IsPad() )		{
			oraPickerView!.frame = CGRect(x: oraBtn!.frame.origin.x , y: comandiView.frame.origin.y + oraBtn!.frame.origin.y + oraBtn!.frame.height  , width: 300, height: 250)
		}
		else		{
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				oraPickerView!.frame = CGRect(x:  oraBtn!.frame.origin.x , y: comandiView.frame.origin.y + comandiView.frame.height  , width: 320, height: 250)
			}else{
				oraPickerView!.frame = CGRect(x: 0 , y: comandiView.frame.origin.y + comandiView.frame.height  , width: self.view.frame.width , height: 250)
			}
		}
		
		self.dataPickerView!.alpha = 0
		dataPikerOpen = false
		
		if ( oraPickerOpen ){
			oraPickerOpen = false
			self.oraPickerView!.alpha = 0
			
		}
		else{
			oraPickerOpen = true
			self.oraPickerView!.alpha = 1
			
		}
	}
	
	
	// Piker Event
	
	@IBAction func dateValueChanged(sender:UIDatePicker)	{
		
		let str = dateformatterDate(dataPickerView!.date)
		dataBtn?.setTitle( "" + (dateformatterDate(dataPickerView!.date) as String) , forState: UIControlState.Normal)
		
	}
	
	@IBAction func oraValueChanged(sender:UIDatePicker)	{
		let str = oraformatterDate(oraPickerView!.date)
		oraBtn?.setTitle("" + (oraformatterDate(oraPickerView!.date) as String) , forState: UIControlState.Normal)
		
	}
	
	
	
	//date utils
	func dateformatterDate(date: NSDate) -> NSString	{
		let dateFormatter: NSDateFormatter = NSDateFormatter()
		dateFormatter.dateFormat = "dd-MM-yyyy"
		dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
		
		return dateFormatter.stringFromDate(date)
		
	}
	
	func oraformatterDate(date: NSDate) -> NSString	{
		let dateFormatter: NSDateFormatter = NSDateFormatter()
		dateFormatter.dateFormat = "HH-mm"
		//dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC")
		
		return dateFormatter.stringFromDate(date)
		
	}
	
	func formatDate(date: NSDate) -> NSString	{
		let dateFormatter: NSDateFormatter = NSDateFormatter()
		dateFormatter.dateFormat = "MM-dd-yyyy HH-mm"
		return dateFormatter.stringFromDate(date)
		
	}
	
	
	//
	func CallApi()	{
		
		self.addLoading()
		
		self.barraPlayer.stopDurationTimer()
		

        if let p = self.player
        		{
        			p.stop()
        			p.delegate = nil
        			//playerview.removeFromSuperview()
        			player = nil
        
        			self.barraPlayer.player = nil
        		}

		
		films = [Film]()
		
		print("CALL FOR Storico ")
		
		let data : NSDate = dataPickerView!.date;
		let ora : NSDate = oraPickerView!.date;
		
		// creo la data dalle cose selezionate
		let dateStr = (dateformatterDate(data) as String) + " " + (oraformatterDate(ora) as String)
		
		let  dateFormat : NSDateFormatter = NSDateFormatter()
		dateFormat.dateFormat = "dd-MM-yyyy HH-mm"
		let date : NSDate?  = dateFormat.dateFromString(dateStr)
		
		//setto my time come timestamp
		if(date != nil){
			mytime = ( "\(date!.timeIntervalSince1970)" as NSString).doubleValue
		}
		else {
			let  dateFormat : NSDateFormatter = NSDateFormatter()
			dateFormat.dateFormat = "MM-dd-yyyy HH-mm"
			let date : NSDate?  = dateFormat.dateFromString(dateStr)
			
				mytime = ( "\(date!.timeIntervalSince1970)" as NSString).doubleValue
		}
		
		
		//eseguo la chiamata
		let url = NSURL(string:  User.Instance.serverUrl + User.getStoredVideoApiUrl( selectedCamId! , dateFrom: "\(date!.timeIntervalSince1970)" )  ) //+"&udid=1234")
		
		var request1 = NSMutableURLRequest(URL: url! )
		var session1 = NSURLSession.sharedSession()
		request1.HTTPMethod = "GET"
		
		var task = session1.dataTaskWithRequest(request1, completionHandler: {data, response, error -> Void in
			
			var error: NSError?
			do {
				let xmlDoc = try AEXMLDocument(xmlData: data!)
				
				print(xmlDoc.xmlString)
				
				
				if( xmlDoc.root["film"].all != nil ){
					for film in xmlDoc.root["film"].all!
					{
						if( film["startTime"].name != "error" ) {
							let f = Film(_startTime: film["startTime"].value! , _endTime: film["endTime"].value! , _videoUrl: film["videoUrl"].value!)
							self.films.append(f)
						}
					}
				}	

				if( self.films.count > 0){
					dispatch_async(dispatch_get_main_queue(), {
					self.selectVideo()
					})
				}
				else	{
					dispatch_async(dispatch_get_main_queue(), {
					self.removeLoading()
					self.alertMessage("ErrorTitleStorico".loc() , message: "ErrorStorico".loc(), button: "annulla".loc()  , delegate: self, _tag: 21)
					})
					
				}
			} catch _ {
			}
		})
		
		task.resume()
		
		print("RESUME TASK")
		
	}
	
	
	
	func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
		self.removeLoading()
	
	}	
	
	
	
	var mytime : Double = 0
	var from : Double = 0
	var to : Double = 0
	var selectedFilm : Film!
	var selectedFilmNumber : Int! = 0
	
	var numeroVideo : Int!
	var minTime : Double = 0
	var maxTime : Double = 0
	var barLenght : Int!
	
	var segmentLengt : Int!
	var barOfset : Int!
	
	var isOfsetBar : Bool = false
	var isPaused : Bool = false


    var seekFromSearch : Bool = false
    var seekFromBar : Bool = false
	 var isVoidVideo : Bool = false
	func selectVideo(){
		
		selectedFilmNumber = -1
		films.sortInPlace({ ($0.startTime as NSString).doubleValue < ($1.startTime as NSString).doubleValue })
		
		var i = 0
		for f in films
		{
			from  = f.startTimeTs
			to  = f.endTimeTS
			
			if(  mytime >= from && mytime < to ) {
				print( "SELECTED")
				selectedFilmNumber = i
				selectedFilm = f
				break
			}
			i++
		}
		
		if(selectedFilmNumber != -1 &&   selectedFilmNumber < films.count - 1 )
		{
            isVoidVideo = false
			creaBarraGiusta()				
			creaNuovoPlayer(selectedFilmNumber)			
		}
		else	{
            print("VIDEO VUOTO ")
			isVoidVideo = true
			self.creaBarraGiusta()
			

            if( self.player != nil )
            {
               // self.player!.stop()
                self.playerview!.alpha = 0
            }

            self.removeLoading()

            self.creaNuovoPlayer(0)
			self.selectedFilmNumber = 0
			self.selectedFilm = self.films[0]

			self.barraPlayer.isVoidVideo = true
			
			self.createLblVoidVideo()

			self.barraPlayer.alpha = 1
			self.barraPlayer.startDurationTimer()
			self.barraPlayer.cantouchBar = true
			self.barraPlayer.Slider.userInteractionEnabled = true
		}
		
	}
	
	func creaBarraGiusta(){
		
		numeroVideo = films.count
		
		
		minTime =  films[0].startTimeTs // films[0].startTimeTs       // films[0].startTimeTs  // (films[0].startTime as NSString).doubleValue
		maxTime = films[numeroVideo - 1 ].endTimeTS      ///(films[numeroVideo - 1 ].endTime  as NSString).doubleValue

        if(selectedFilmNumber != -1)
        {
            let minTimeSegment = films[selectedFilmNumber ].startTimeTs
            let maxTimeSegment = films[selectedFilmNumber ].endTimeTS
            segmentLengt =  Int(maxTimeSegment - minTimeSegment)
        }

		barLenght =  Int (maxTime - films[0].startTimeTs )
		barOfset =    Int ( mytime - minTime	)    //  segmentLengt * selectedFilmNumber
		
		print("AAAAA \n numeroVideo \( numeroVideo ) \n minTime \( minTime ) \n maxTime \( maxTime ) \n barOfset \( barOfset ) \n segmentLengt \( segmentLengt ) \n barLenght \( barLenght ) \n ")
		
		barraPlayer.isStorico = true
		barraPlayer.storico = self
		
		
		barraPlayer.creaBarraStorico()
	}
	
	func playPlayer()
	{
		
        self.player!.play()

	}
	
	var timerPlay : NSTimer?


	func creaNuovoPlayer(num : Int)
	{

        //12/1/16
        playerview?.removeFromSuperview()
        scrollview.removeFromSuperview()


        playerview = UIView()

        //aggiungo scrollview per zoom

        scrollview = UIScrollView()
        scrollview.backgroundColor = UIColor.sfondoScuro()
        self.view.addSubview(scrollview);
        //	scrollview.addSubview(player!.view)
        scrollview.addSubview(playerview!)
        scrollview.delegate = self
        scrollview.bounces=true
        scrollview.bouncesZoom=true


        let tapGesture :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onPlayerTapped:")
        tapGesture.numberOfTapsRequired = 1
        scrollview.addGestureRecognizer(tapGesture)
        playerview!.addGestureRecognizer(tapGesture)

        if(self.IsPad())
        {

        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
            playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height - 70 - 48 )
            scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y  + 48 , width: self.view.frame.width  , height: self.view.frame.height - 70 - 48 )

            let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
            scrollview.contentSize = scrollSize
        }
        else{
            playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
            scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width, height: self.view.frame.height - 70 - 48 )

            let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
            scrollview.contentSize = scrollSize

            }
        }
        else
        { // Iphone

            if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
                playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height - 56 - 48 )
                scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y  + 48 , width: self.view.frame.width  , height: self.view.frame.height - 56 - 48 )

                let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
                scrollview.contentSize = scrollSize
            }
            else{
                playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width, height: self.view.frame.height - 56 - 48 )
                
                let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
                scrollview.contentSize = scrollSize
            }
        }


        scrollview.zoomScale = 1
        scrollview.setContentOffset(CGPoint(x: 0,y: 0), animated: false)

        let scrollViewFrame = scrollview.frame
        let scaleWidth = scrollViewFrame.size.width / scrollview.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / scrollview.contentSize.height
        let minScale = min(scaleHeight, scaleWidth)

        scrollview.minimumZoomScale = minScale
        scrollview.maximumZoomScale = 10
        scrollview.zoomScale = minScale

        self.view.bringSubviewToFront(barraPlayer)
        self.view.bringSubviewToFront(dataPickerView!)
        self.view.bringSubviewToFront(oraPickerView!)
        self.view.bringSubviewToFront(labelSelectFilm!)

self.addLoading()

/////////////////////////////

        if( _timerDuration != nil  )
        {
            if ( _timerDuration!.valid )
            {
                _timerDuration!.invalidate()
            }
            _timerDuration = nil;
        }

		//dispatch_async(dispatch_get_main_queue(), {

		
		self.barraPlayer.stopDurationTimer()

        if let p = self.player
        		{
        		//	self.player!.stop()
        			self.player!.delegate = nil
        			//self.playerview!.removeFromSuperview()
        			self.player = nil
                    self.barraPlayer.player = nil
        		}



		if let timer = self.timerPlay{
			if ( timer.valid ) {
				timer.invalidate()
			}
		}

        

        let arr = ["--extraintf="] as NSArray
       self.player  =  VLCMediaPlayer(options: arr as [AnyObject])

        self.player?.delegate = self
        self.player?.drawable = playerview!

        let url = NSURL(string: self.films[num].videoUrl)
        let media = VLCMedia (URL: url)
        self.player?.media = media

         self.player?.media.delegate  = self
        print("VIDEO STORICO")
        print(self.films[num].videoUrl);

        self.player?.media.addOptions(mediaOptionDictionary())

        self.player?.rate = 0
        self.player?.setDeinterlaceFilter("blend")  ;


        //setto il paler per la barra
        self.barraPlayer.player = self.player!


			spostaOfset()
	        setplayerAspectRatio(player!)




	}
	
	
	//CHIAMATO DA EVENTO DEL PLAYER
	func spostaOfset() {
		
		
		if( barraPlayer.isVoidVideo  )
		{
			return
		}
		
		
		if(isOfsetBar)
		{
                seekFromBar = true
				labelSelectFilm!.alpha = 0
				barraPlayer.alpha = 1
				self.barraPlayer.Slider.userInteractionEnabled = true
				self.player!.play()
				self.playerview!.alpha = 1
		}
		else{

			print("  mytime \(mytime) \n from \(from) \n to \(to) "   )

            if(selectedFilm != nil )
            {

                labelSelectFilm!.alpha = 0
                barraPlayer.alpha = 1
                barraPlayer.Slider.userInteractionEnabled = true
                self.player!.play()

            }
            else
            {
              //  self.alertMessage("ErrorTitleStorico".loc(), message: "ErrorStorico2".loc(), button: "annulla".loc())
            }
		}
		
		self.playerview!.alpha = 1
	}

	var canSeek : Bool = true

    func setplayerAspectRatio( player: VLCMediaPlayer )
    {
        if   let rect : CGRect = playerview?.frame
        {

            let h = (rect.height / rect.height) * 100
            let w = (rect.width / rect.height) * 100
            let ratio = "\( Int (w) ):\(  Int(h) )"

            CriptString.setaspectRatio(player , ratio)
        }
    }
	//PLAYER DELEGATE

    func mediaOptionDictionary()-> [NSObject:AnyObject] {
        let dict = ["network-caching" :  10000, "audio-time-stretch" : "0" ,"subsdec-encoding":"Windows-1252","avcodec-skiploopfilter":1 ];
        return dict
    }


    var   _timerDuration : NSTimer? = nil

    var buffernumber : Int = 3
    var currentBufferNum : Int = 0
    func buffertimer(){
        currentBufferNum = 0
        if( _timerDuration == nil  )
        {
            self.addLoading()
            _timerDuration = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("onTimerDurationFired:" ), userInfo: nil, repeats: true	)
        }
        else{
            if ( _timerDuration!.valid )
            {
                _timerDuration!.invalidate()
            }
            _timerDuration = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("onTimerDurationFired:" ), userInfo: nil, repeats: true	)

        }
    }

    func onTimerDurationFired (timer : NSTimer) {
        let  currentstate : VLCMediaState = (player?.media!.state)!
        if(currentstate != VLCMediaState.Buffering  )
        {
            currentBufferNum++
            if(currentBufferNum >=  buffernumber){

                self.barraPlayer.startDurationTimer()
                self.removeLoading()
                barraPlayer.cantouchBar = true

                //stop timer
                if( _timerDuration != nil  )
                {
                    if ( _timerDuration!.valid )
                    {
                        _timerDuration!.invalidate()
                    }
                    _timerDuration = nil;
                }
            }


           
        }
    }



    func mediaPlayerStateChanged(aNotification: NSNotification!) {
        let  currentstate : VLCMediaPlayerState = player!.state
        switch (currentstate.rawValue)
        	{
                case  VLCMediaPlayerState.Error.rawValue  :
                    print("player error " )
                    self.removeLoading()
            		self.alertMessage("ErrorTitleStorico".loc(), message: "ErrorStorico2".loc(), button: "annulla".loc())

                case  VLCMediaPlayerState.Buffering.rawValue  :
                    print("player bufering " )
                    canSeek = false
                    barraPlayer.cantouchBar = false
                    buffertimer()
                case  VLCMediaPlayerState.Opening.rawValue  :
                print("player opening " )
                case  VLCMediaPlayerState.Playing.rawValue  :

                    print("player playng " )

                   // self.removeLoading()
            		//self.barraPlayer.startDurationTimer()
                    if(  seekFromSearch)
                        {
                           print("  start time  \(self.selectedFilm.startTimeTs) \n mintime  \(self.minTime  ) \n to \(self.barraPlayer.Slider.value ) "   )
            
                            let x = self.barraPlayer.Slider.value - Float ( self.selectedFilm.startTimeTs - self.minTime     )
                            let minTimeSegment = films[selectedFilmNumber ].startTimeTs
                            let maxTimeSegment = films[selectedFilmNumber ].endTimeTS
                            segmentLengt =  Int(maxTimeSegment - minTimeSegment)

                            let position = x /  Float ( segmentLengt)
                            player!.position = position;

                            canSeek = true
                            isPaused = false
            
                            seekFromSearch = false
                        }
            
                        if(seekFromBar )
                        {
                            let x = self.barraPlayer.Slider.value - Float ( self.selectedFilm.startTimeTs - self.minTime     )
                            //self.player!.setStreamCurrentDuration( Float (x))
                            let minTimeSegment = films[selectedFilmNumber ].startTimeTs
                            let maxTimeSegment = films[selectedFilmNumber ].endTimeTS
                            segmentLengt =  Int(maxTimeSegment - minTimeSegment)
                            
                            let position = x /  Float ( segmentLengt)
                            player!.position = position;

                            canSeek = true
                            isPaused = false
                            seekFromBar = false
                        }
            
            			if (isPaused == false && canSeek){
            				canSeek = false
            				barraPlayer.cantouchBar = false
            				barraPlayer.Slider.userInteractionEnabled = true
            			}
            			else{
            				canSeek = true
            				isPaused = false		
            			}



        case VLCMediaPlayerState.Stopped.rawValue :
            print("player stopped" )

                if( player!.position >= 0.99 ) // cambia video quando arrivato a fine
                {
                  self.barraPlayer.playerAllafine()
                }
            


        case VLCMediaPlayerState.Ended.rawValue :
            print("player ended" )

        case VLCMediaPlayerState.Paused.rawValue :
            print("player paaused" )
            barraPlayer.stopDurationTimer()

                default:print("")
            }
    }

    //MEDIA DELEGATE
    func mediaDidFinishParsing(aMedia: VLCMedia!) {
        print("Finish parse");

    }

    func mediaMetaDataDidChange(aMedia: VLCMedia!) {

        let  currentstate : VLCMediaState = player!.media.state

        switch (currentstate.rawValue)
        {
            case  VLCMediaState.Error.rawValue  :
                print("media error");

            break

            case  VLCMediaState.Buffering.rawValue  :
                print ("media buffer")
            break
            case  VLCMediaState.Playing.rawValue  :
                print("media play");
            break
        default:print("")
        }

    }



	
	//SCROLL VIEW DELEGATE
	
	func centerScrollViewContents(){
		
	}
	
	func scrollViewDidZoom(scrollView: UIScrollView) {
		centerScrollViewContents()
	}
	
	func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
		if(player != nil){
			return playerview!
		}
        return nil
		//return self.view
	}
	
	func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat) {
		
		
		if(scale == 1)
		{
			print("EndZoom")
		}
		
	}
	
	
	
	///
	func onPlayerTapped(sender:UITapGestureRecognizer)	{
		if(self.player != nil){
            toggleBarVisible()
		}
	}
	
	func  toggleBarVisible()	{
		setTabBarVisible(!tabBarIsVisible(), animated: true)
	}
	
	func hideBar()	{
		setTabBarVisible(false, animated: true)
	}
	
	func setTabBarVisible(visible:Bool, animated:Bool) {
		
		// bail if the current state matches the desired state
		if (tabBarIsVisible() == visible) { return }
		
		self.dataPickerView!.alpha = 0
		self.oraPickerView!.alpha = 0
		oraPickerOpen = false
		dataPikerOpen = false
		
		let frame2 = self.navigationController?.navigationBar.frame
		let height2 = frame2!.size.height + self.comandiView.frame.height
		
		
		let duration2:NSTimeInterval = (animated ? 0.3 : 0.0)
		
		if (visible){ // VISIbile
			UIView.animateWithDuration(duration2, animations: { () -> Void in
				self.navigationController?.navigationBar.frame = CGRectOffset(frame2!, 0, height2)
				
				if(self.IsPad())
				{
					self.automaticallyAdjustsScrollViewInsets = false
					self.comandiView.frame = CGRect(x:0 , y:  self.frameBetweenBar().origin.y , width: self.view.frame.width, height: 48)
					self.barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 70  , width: self.view.frame.width, height: 70 )
					
					self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width , height: self.frameBetweenBar().height - 70 - 48 );
					
				}
				else{
					self.comandiView.frame = CGRect(x:0 , y:  self.frameBetweenBar().origin.y , width: self.view.frame.width, height: 48)
					self.barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 56  , width: self.view.frame.width, height: 56 )

                    self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width, height: self.view.frame.height - 56 - 48 )


				//	self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width , height: self.frameBetweenBar().height - 70 - 48 );
				}
				
				
				return
				}, completion: { (bool) -> Void in
					self.scrollview.zoomScale = 1
					
					if(self.IsPad())
					{
						if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
							self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height  - 70 - 48 )
							let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
							self.scrollview.contentSize = scrollSize
						}
						else{
							self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
							let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
							self.scrollview.contentSize = scrollSize
						}
					}
					else{
						
						if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
							//self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height - 56 - 48 )
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height - 56 - 48 )

							let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
							self.scrollview.contentSize = scrollSize
						}
						else{
							
						//	self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )


							let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
							self.scrollview.contentSize = scrollSize
						}
					}
					self.setplayerAspectRatio(self.player!)
			})
			
		}
		else{ // NON VISIBILE
			UIView.animateWithDuration(duration2, animations: { () -> Void in
				self.navigationController?.navigationBar.frame = CGRectOffset(frame2!, 0, -height2)
				
				if(self.IsPad())
				{
					self.automaticallyAdjustsScrollViewInsets = false
					self.comandiView.frame = CGRect(x:0 , y:  self.frameBetweenBar().origin.y -  frame2!.size.height, width: self.view.frame.width, height: 48)
					self.barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 70   , width: self.view.frame.width, height: 70 )
					
					self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width , height: self.view.frame.height );
				}
				else{
					self.comandiView.frame = CGRect(x:0 , y:  self.frameBetweenBar().origin.y - frame2!.size.height, width: self.view.frame.width, height: 48)
					self.barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 56  , width: self.view.frame.width, height: 56 )
					
					//self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 48 , width: self.view.frame.width , height: self.view.frame.height );
                    self.scrollview.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height  - 56 )

                }
				
				
				return
				}, completion: { (bool) -> Void in
					self.scrollview.zoomScale = 1
					
					if(self.IsPad())
					{
						if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
							self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height  )
							let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
							self.scrollview.contentSize = scrollSize
						}
						else{
							self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
							let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
							self.scrollview.contentSize = scrollSize
						}
					}
					else{
						
						if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
							//self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height  )
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.view.frame.height - 56  )

							let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
							self.scrollview.contentSize = scrollSize
						}
						else{
							
							if(	self.player != nil){
								//self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                                self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )

							let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
							self.scrollview.contentSize = scrollSize
							}
						}

					}
					 self.setplayerAspectRatio(self.player!)
			})
		}


	}
	
	func tabBarIsVisible() ->Bool {
		return !(self.navigationController?.navigationBar.frame.origin.y < 0 )
	}
	
	func navbarvisible() -> Bool{
		
		return self.navigationController!.navigationBar.hidden
	}
	
}