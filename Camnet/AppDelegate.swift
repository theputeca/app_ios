//
//  AppDelegate.swift
//  Camnet
//
//  Created by giulio piana on 11/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import UIKit

//import CryptoSwift

//import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
    var mainController :  MainViewController?

  //  var _notificationData : NSDictionary? = nil

	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
		
		
		//Parse.setApplicationId("dCoa6Yru5tgxbwJScq1v8GbOGSVdokZNOOAeN90x", clientKey: "XTkWcOfCoA8XOjRE4nSJyrxoE8g2nZUUJzAJyX2M")


        // Register for Push Notitications
        if application.applicationState != UIApplicationState.Background {
            // Track an app open here if we launch with a push, unless
            // "content_available" was used to trigger a background push (introduced in iOS 7).
            // In that case, we skip tracking here to avoid double counting the app-open.

            let preBackgroundPush = !application.respondsToSelector("backgroundRefreshStatus")
            let oldPushHandlerOnly = !self.respondsToSelector("application:didReceiveRemoteNotification:fetchCompletionHandler:")
            var pushPayload = false
            if let options = launchOptions {
                pushPayload = options[UIApplicationLaunchOptionsRemoteNotificationKey] != nil
            }
            if (preBackgroundPush || oldPushHandlerOnly || pushPayload) {
                //se apro la app con la push notification


            }
        }

		//register for push notification


        if (UIApplication.sharedApplication().respondsToSelector(Selector("registerForRemoteNotifications")))
        {
            if #available(iOS 8.0, *) {
                let types: UIUserNotificationType = [UIUserNotificationType.Badge, UIUserNotificationType.Alert, UIUserNotificationType.Sound]
                let settings: UIUserNotificationSettings = UIUserNotificationSettings( forTypes: types, categories: nil )
                application.registerUserNotificationSettings( settings )
                application.registerForRemoteNotifications()
            } else {
                // Fallback on earlier versions
            }

        }
            // ios7
        else
        {
            application.registerForRemoteNotificationTypes(  [UIRemoteNotificationType.Alert, UIRemoteNotificationType.Badge, UIRemoteNotificationType.Sound]	)
        }

		return true
	}


	//DELEGATE PUSH NOTIFICATION
	
	func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
		
        print("Did Register for Remote Notifications with Device Token \(deviceToken)")


        let stringToken = "\(deviceToken)"

        var newString = stringToken.stringByReplacingOccurrencesOfString("<", withString: "")
        newString = newString.stringByReplacingOccurrencesOfString(">", withString: "")
        newString = newString.stringByReplacingOccurrencesOfString(" ", withString: "")

		let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
		defaults.setObject(newString, forKey: "deviceToken")

       defaults.synchronize()

		
	}
	
	func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
		  print("Did Fail to Register for Remote Notifications")
	}
	
	
	
	func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
		print("RECIVED PUSH")
	}





	func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
		print("RECIVED PUSH HANDLER")


        if(mainController != nil ){
            mainController?.mainNavigationController?.popToRootViewControllerAnimated(false)
            mainController?.tabBar.hidden = false
        }
		
		let dic : NSDictionary = userInfo as NSDictionary
        let n : NSDictionary  = dic.objectForKey("aps") as! NSDictionary
        /*
        sound default
        content id event o url news
        alert message
        mode event
        badge
        */
        let notificationText : String = n.objectForKey("alert") as! String
        let notificationMode : String = n.objectForKey("mode") as! String
        let notificationContent : String = n.objectForKey("content") as! String
        let notificationInstall : String = n.objectForKey("install") as! String


		print(notificationText)
		print(notificationMode)
		print(notificationContent)
        print(notificationInstall)


     //   _notificationData = n

        MainViewController.SetNotificationData( n )
      //  NSUserDefaults.standardUserDefaults().setObject(n, forKey:"notification")

        UIApplication.sharedApplication().applicationIconBadgeNumber = 0


        if application.applicationState == UIApplicationState.Active
        {

            print("PUSH NOTIFICATION ACTIVE")
            if  (MainViewController.notificationData != nil )// let  notificationDictionary : NSDictionary  = NSUserDefaults.standardUserDefaults().objectForKey("notification") as! NSDictionary?
            {

                //_notificationData = notificationDictionary
                if(mainController != nil ){

                if( MainViewController.notificationData!.objectForKey("mode" ) as! String == "event"){
                    mainController!.openEventFromNotificationIfActive()
                }
                else if (MainViewController.notificationData!.objectForKey("mode") as! String == "news")
                {
                    mainController!.openNewsFromNotificationIfActive()
                }
                }
            }

        }
	}
	

	
	
	func applicationWillResignActive(application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	}

	func applicationDidEnterBackground(application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

        if(mainController != nil ){
            mainController?.mainNavigationController?.popToRootViewControllerAnimated(true)
        }

	}

	func applicationWillEnterForeground(application: UIApplication) {
		// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	}

	func applicationDidBecomeActive(application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool( false , forKey: "doneAutostart")




        if(mainController != nil ){

            if(mainController!.selectedIndex == 0)
            {
            if( LiveMainCollViewContorller.Instance != nil)
            {
                //var  eventiVc : EventiVC = self.selectedViewController as! EventiVC
                LiveMainCollViewContorller.Instance!.checkAutostart()
            }
            }

            mainController!.selectedIndex = 0
        }

        UIApplication.sharedApplication().applicationIconBadgeNumber = 0




       /* if( EventiVC.Instance != nil)
        {
            //var  eventiVc : EventiVC = self.selectedViewController as! EventiVC
            EventiVC.Instance!.reloadEventFromNotification()
        }*/


	}

	func applicationWillTerminate(application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	}




}

