//
//  Evento.swift
//  Camnet
//
//  Created by giulio piana on 16/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation


class Mappa{
	
	var id : String
	var name : String
	var description : String
	var url : String
	var content = [Vista] ()
	var contentIds = [String]()
	
	/*
	<map>
	<id>157</id>
	<name>b4web</name>
	<description>sede</description>
	<viewMap>http://sede.b4web.biz:12080/camnet/ipad-map.php?token=47f3d93eef18c581aa9803121045bce3eadec6b8&id=157</viewMap>
	<content />
	</map>
	*/
	
	init ()
	{
		id =  ""
		name = ""
		description = ""
		url = ""
		
	}
	
	init (_id : String , _name : String? ,  _desc : String? , _url : String?   )
	{
		id = _id
		name = ""
		if(_name != nil ){	name = _name!}
		
		description = ""
		if (_desc != nil){	description = _desc!}
		
		url = ""
		if (_url != nil ) {url = _url!}
	}
	
}