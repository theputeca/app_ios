//
//  VisteViewController.swift
//  Camnet
//
//  Created by giulio piana on 15/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation

import UIKit


class VisteViewController: UIViewController , UIScrollViewDelegate , UIGestureRecognizerDelegate	,UIWebViewDelegate,VLCMediaPlayerDelegate {


    var visteList = [Vista] ()									// lista delle altre viste per gestire lo swipe left e right
    var currentVista : Vista?									// vista selezionata
    var currentVistaId : Int = 0

    var scroolViewPages : [UIView] =  [UIView]()

    var _playerList = [NSObject ]()		// VKPlayerController	// array con le playerview
    var viewList  = [UIView]()								// array con le viste che intercettano i gesti sui player

    var activityList  = [UIActivityIndicatorView]()  //array con i loadingview
    var nullViewList = [UIView]()							//

    var _playerListTemp = [NSObject]() //VKPlayerController
    var viewListTemp  = [UIView]()								// array con le viste che intercettano i gesti sui player

    var activityListTemp  = [UIActivityIndicatorView]()	//

    var scrollView :UIScrollView = UIScrollView()
    var touchView : UIView!

    var isNavBarVisible : Bool = true
    var currentPage : Int = 0


    var   _timerLastFrame : NSTimer? = nil
    var   _timerRefresh : NSTimer? = nil
    //
    override func viewDidLoad() {


        UIApplication.sharedApplication().idleTimerDisabled = false
        UIApplication.sharedApplication().idleTimerDisabled = true

        super.viewDidLoad()

        self.tabBarController?.tabBar.hidden = true

        self.navigationController?.interactivePopGestureRecognizer!.enabled = false;
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self

        self.title = currentVista!.name

        self.view.backgroundColor = UIColor.sfondoScuro()


        //creo la scrollview
        scrollView  = UIScrollView(frame: self.frame() )
        scrollView.pagingEnabled = true
        scrollView.delegate = self
        scrollView.contentSize = CGSize(width: self.frame().width * CGFloat(visteList.count) , height: scrollView.contentSize.height )
        scrollView.contentOffset.x = scrollView.frame.width * CGFloat(currentVistaId)

        scrollView.backgroundColor =  UIColor.sfondoScuro()

        currentPage = Int (scrollView.contentOffset.x /  scrollView.frame.size.width)

        var num : Int = 0
        for vista in self.visteList{

            if (num == currentVistaId){
                let quante = Int(vista.size)

                //array delle camere della vista selezionata
                let cams : [Cam] = [Cam] (vista.content.values)

                for (var i = 0; i < quante; i++) {
                    if ( i <  cams.count )
                    {
                        let str : String = vista.contentIds[i]

                        if(vista.content.count <= 0) {return}

                        //do {

                        let camtest : Cam? =  vista.content[ str ]

                        if(camtest != nil){
                            let cam : Cam =  vista.content[ str ]!

                            if( cam.urlVideo_1 != nil )  // SE HO IL PLAYER
                            {
                                var url =  cam.urlVideo_1

                                if (vista.size == "4") {  url = cam.urlVideo_4 }
                                else if (vista.size == "6") { url = cam.urlVideo_6  }
                                else if ( vista.size == "9") { url = cam.urlVideo_9 }
                                else{ url = cam.urlVideo_16 }


                                // inizializza il player

                                let arr = ["--extraintf="] as NSArray
                                let player =  VLCMediaPlayer(options: arr as [AnyObject])
                                player.audio?.muted=true;
                                let playerview = UIView()

//                                //setta la dimensione del player
                                if (vista.size == "4") { playerview.frame = init4Player(i, frame: scrollView.frame )}
                               else if (vista.size == "6") { playerview.frame = init6Player(i)}
                                else if ( vista.size == "9") { playerview.frame = init9Player(i)}
                               else	{ playerview.frame = init16Player(i) }
//
                               playerview.frame = CGRect(x: playerview.frame.origin.x + (self.frame().width * CGFloat(currentVistaId))  , y: playerview.frame.origin.y , width: playerview.frame.width, height: playerview.frame.height)


                                playerview.tag = i

                                let nsurl = NSURL(string: url!)
                                    let media = VLCMedia (URL: nsurl)
                                    player.media = media



                              //  player.media.addOptions(mediaOptionDictionary())

                                if ( url!.rangeOfString("mjpg") != nil )
                                {
                                    media.addOptions(mediaOptionDictionary())
                                   // liveStreamTimer()
                                }
                                else
                                {
                                    media.addOptions(mediaOptionDictionaryRtsp() )
                                }


                                    player.delegate = self
                                 player.drawable = playerview

                                

                                let tapGesture :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onPlayerTapped:")
                                tapGesture.numberOfTapsRequired = 1
                                playerview.addGestureRecognizer(tapGesture)

                                setplayerAspectRatio(player)


                                print("url dela cam  " + url!)
                               player.play();


                                _playerList.append(player)
                                scrollView.addSubview(playerview);
//
//                                // add view on player for  recognizer
                               let pView = UIView(frame: CGRect(x: 0, y : 0 , width:  playerview.frame.width, height : playerview.frame.height ) )
                                pView.tag = i
                               playerview.addSubview(pView);

                                pView.userInteractionEnabled=false
                                viewList.append(pView)

                                // loading indicator
                                let actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: pView.frame) as UIActivityIndicatorView
                                actInd.center = pView.center
                                actInd.hidesWhenStopped = true
                                actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
                                actInd.color = UIColor.arancio()
                                actInd.startAnimating()
                                actInd.userInteractionEnabled=false

                                pView.addSubview(actInd)
                                activityList.append(actInd)
                            }
                            else if( cam.webViewVideoVideo_1 != nil ){  // SE HO LA WEBWIEW CON LA CAM
                                // var url =  cam.webViewVideoMedium

                                var url =  cam.webViewVideoVideo_1

                                if (vista.size == "4") {  url = cam.webViewVideoVideo_4 }
                                else if (vista.size == "6") { url = cam.webViewVideoVideo_6  }
                                else if ( vista.size == "9") { url = cam.webViewVideoVideo_9 }
                                else{ url = cam.webViewVideoVideo_16 }

                                //creo la webView

                                let  playerWebView : UIWebView  = UIWebView()
                                playerWebView.backgroundColor = UIColor.arancio() //UIColor.sfondoScuro()
                                playerWebView.tag    = i


                                //setta la dimensione del player
                                if (vista.size == "4") { playerWebView.frame = init4Player(i, frame: scrollView.frame )}
                                else if (vista.size == "6") { playerWebView.frame = init6Player(i)}
                                else if ( vista.size == "9") { playerWebView.frame = init9Player(i)}
                                else	{ playerWebView.frame = init16Player(i) }

                                playerWebView.frame = CGRect(x: playerWebView.frame.origin.x + (self.frame().width * CGFloat(currentVistaId))  , y: playerWebView.frame.origin.y , width: playerWebView.frame.width, height: playerWebView.frame.height)

                                //LOAD URL

                                let urlR = NSURL(string: url!	)
                                let  request = NSURLRequest(URL: urlR!)
                                playerWebView.loadRequest(request)

                                scrollView.addSubview(playerWebView);
                                self.view.bringSubviewToFront(playerWebView)
                                playerWebView.delegate = self
                                _playerList.append(playerWebView)
                                //setta la dimensione della Web VIEW
                                
                                
                                // add view on player for  recognizer
                                let pView = UIView(frame: CGRect(x: 0, y : 0 , width:  playerWebView.frame.width, height : playerWebView.frame.height ) )
                                pView.tag = i
                                playerWebView.addSubview(pView);
                                viewList.append(pView)
                                
                                // loading indicator
                                let actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: pView.frame) as UIActivityIndicatorView
                                actInd.center = pView.center
                                actInd.hidesWhenStopped = true
                                actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
                                actInd.color = UIColor.arancio()
                                actInd.startAnimating()
                                actInd.alpha = 0
                                
                                pView.addSubview(actInd)
                                activityList.append(actInd)
                                
                                
                            }
                            
                        
                        } //end cam test


                    }

                }
            }

            num = num+1
        }




        //touchView.use
        touchView = UIView(frame : self.frame())

        let tapGesture :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onPlayerTapped:")
        tapGesture.numberOfTapsRequired = 1
        touchView.addGestureRecognizer(tapGesture)

        let doubleTapGesture :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onPlayerDoubleTapped:")
        doubleTapGesture.numberOfTapsRequired = 2
        touchView.addGestureRecognizer(doubleTapGesture)


        tapGesture.requireGestureRecognizerToFail(doubleTapGesture)

        touchView.backgroundColor = UIColor.sfondoScuro()
        self.view.bringSubviewToFront(touchView)
        self.view.addSubview(touchView)

        touchView.addSubview(scrollView)


        //button Modifica

        if(currentVista!.type != "auto")
        {
            let editButton = UIBarButtonItem(title: "modifica".loc() , style: UIBarButtonItemStyle.Plain , target: self, action: Selector("modificaClick:"))
            self.navigationItem.rightBarButtonItem = editButton
        }
    }


    override func viewDidAppear(animated: Bool) {
            super.viewDidAppear(animated)

        let timerefresh : Double =  Double (User.Instance.refreshTime * 60 )

    //    _timerDuration = NSTimer.scheduledTimerWithTimeInterval(  timerefresh , target: self, selector: Selector("onTimerFired:" ), userInfo: nil, repeats: true	)
        _timerRefresh = NSTimer.scheduledTimerWithTimeInterval(  timerefresh , target: self, selector: Selector("onTimerFired:" ), userInfo: nil, repeats: true	)

        self.removeLoading()

        self.automaticallyAdjustsScrollViewInsets = false

        scrollView.frame = CGRect(x: 0, y: 0, width: self.frameBetweenBar().width , height: self.frameBetweenBar().height )   // self.view.frame // self.view.frame
        touchView.frame = self.frameBetweenBar() //self.view.frame

        scrollView.contentOffset.x = self.view.frame.width * CGFloat(currentPage)

        var i = 0
        for pl in _playerList
        {
            if (pl.isKindOfClass(UIWebView))
            {
                let p = pl as! UIWebView

                if (currentVista!.size == "4") { p.frame = init4Player(i,frame:scrollView.frame)}
                else if (currentVista!.size == "6") { p.frame = init6Player(i)}
                else if ( currentVista!.size == "9") { p.frame = init9Player(i)}
                else	{ p.frame = init16Player(i) }

                p.frame = CGRect(x: p.frame.origin.x + (self.view.frame.width * CGFloat(currentPage))  , y: p.frame.origin.y , width: p.frame.width, height: p.frame.height)
                
                activityList[i].frame =  CGRect(x: 0, y: 0, width:  p.frame.width, height:  p.frame.height) // p.view.frame
                activityList[i].startAnimating()
            }
            else{

                let p = pl as! VLCMediaPlayer

                if (currentVista!.size == "4") { (p.drawable as! UIView ).frame  = init4Player(i,frame:scrollView.frame)}
                else if (currentVista!.size == "6") { (p.drawable as! UIView ).frame = init6Player(i)}
                else if ( currentVista!.size == "9") { (p.drawable as! UIView ).frame = init9Player(i)}
                else	{ (p.drawable as! UIView ).frame = init16Player(i) }

                let  isIOS8 : Bool =  SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("8.0");

                if(isIOS8)
                {
                  (p.drawable as! UIView ).frame = CGRect(x: (p.drawable as! UIView ).frame.origin.x + (self.view.frame.width * CGFloat( currentPage ))  , y: (p.drawable as! UIView ).frame.origin.y /*- self.navigationController!.navigationBar.frame.height */, width: (p.drawable as! UIView ).frame.width, height: (p.drawable as! UIView ).frame.height)
                }
                else{
                    (p.drawable as! UIView ).frame = CGRect(x: (p.drawable as! UIView ).frame.origin.x + (self.view.frame.width * CGFloat( currentPage ))  , y: (p.drawable as! UIView ).frame.origin.y , width: (p.drawable as! UIView ).frame.width, height: (p.drawable as! UIView ).frame.height)
                }

               activityList[i].frame =  CGRect(x: 0, y: 0, width:  (p.drawable as! UIView ).frame.width, height:  (p.drawable as! UIView ).frame.height) // p.view.frame

                activityList[i].startAnimating()

                p.play()
                p.delegate = self
                setplayerAspectRatio(p)

            }
            i = i + 1

                    }


        if(scrollView.subviews.count<3 )
        {
            for pl in _playerList
            {
                if (pl.isKindOfClass(UIWebView))
                {
                    let p = pl as! UIWebView
                    scrollView.addSubview(p)
                    p.delegate = self
                }
                else{

                    let p = pl as! VLCMediaPlayer
                    p.play()
                    p.delegate = self
                }
            }
        }

        buffertimer()
    }

    // ORIENTATION

    override  func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {


        scrollView.frame = CGRect(x: 0, y: 0, width: self.frameBetweenBar().width , height: self.frameBetweenBar().height )   // self.view.frame // self.view.frame
        touchView.frame = self.frameBetweenBar() //self.view.frame



        print ( "ROTATION with" )
        print (  self.view.frame.width )

        print ( "CURRENT OPAGE " )
        print (  currentPage )



        scrollView.contentSize = CGSize(width: self.view.frame.width * CGFloat(visteList.count) , height: scrollView.contentSize.height )
        scrollView.contentOffset.x = self.view.frame.width * CGFloat(currentPage)


        var i = 0
        for pl in _playerList
        {
            if (pl.isKindOfClass(UIWebView))
            {
                let p = pl as! UIWebView

                if (currentVista!.size == "4") { p.frame = init4Player(i,frame:scrollView.frame)}
                else if (currentVista!.size == "6") { p.frame = init6Player(i)}
                else if ( currentVista!.size == "9") { p.frame = init9Player(i)}
                else	{ p.frame = init16Player(i) }

                p.frame = CGRect(x: p.frame.origin.x + (self.frame().width * CGFloat(currentVistaId))  , y: p.frame.origin.y , width: p.frame.width, height: p.frame.height)

                activityList[i].frame =  CGRect(x: 0, y: 0, width:  p.frame.width, height:  p.frame.height)

                p.reload()
            }
            else{
                let p = pl as! VLCMediaPlayer
            if (currentVista!.size == "4") { (p.drawable as! UIView ).frame = init4Player(i,frame:scrollView.frame)}
            else if (currentVista!.size == "6") { (p.drawable as! UIView ).frame = init6Player(i)}
            else if ( currentVista!.size == "9") { (p.drawable as! UIView ).frame = init9Player(i)}
            else	{ (p.drawable as! UIView ).frame = init16Player(i) }

            let  isIOS8 : Bool =  SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("8.0");

            if(isIOS8)
            {
                (p.drawable as! UIView ).frame = CGRect(x: (p.drawable as! UIView ).frame.origin.x + (self.view.frame.width * CGFloat( currentPage ))  , y: (p.drawable as! UIView ).frame.origin.y /*- self.navigationController!.navigationBar.frame.height */, width: (p.drawable as! UIView ).frame.width, height: (p.drawable as! UIView ).frame.height)
            }
            else{
                (p.drawable as! UIView ).frame = CGRect(x: (p.drawable as! UIView ).frame.origin.x + (self.view.frame.width * CGFloat( currentPage ))  , y: (p.drawable as! UIView ).frame.origin.y , width: (p.drawable as! UIView ).frame.width, height: (p.drawable as! UIView ).frame.height)
            }

            activityList[i].frame =  CGRect(x: 0, y: 0, width:  (p.drawable as! UIView ).frame.width, height:  (p.drawable as! UIView ).frame.height) // p.view.frame
                 setplayerAspectRatio(p)
            }
            i = i + 1
        }
    }

    //SCROLL VIEW DELEGATE


    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        print("play Player")

        // calcola il numero della pagina
        if(IsDragging )
        {
            IsDragging = false
            let  pagenumber : Int = Int (scrollView.contentOffset.x /  scrollView.frame.size.width)

            if ( pagenumber != currentPage )
            {
                //stoppa i player vecchi

                for pl in _playerList
                {
                    if (pl.isKindOfClass(UIWebView))
                    {
                        let p = pl as! UIWebView
                        p.removeFromSuperview()
                        p.delegate = nil
                    }
                    else
                    {
                        let p = pl as! VLCMediaPlayer
                        p.stop()
                        p.delegate = nil


                        if ( activityList[(p.drawable as! UIView).tag] as UIActivityIndicatorView? != nil )
                        {
                            activityList[(p.drawable as! UIView).tag].removeFromSuperview()
                        }
                    }
                }

                //setta la current vista
                currentVista = self.visteList [pagenumber]

                _playerListTemp  = [VLCMediaPlayer]()
                activityListTemp = [UIActivityIndicatorView]()
                addPlayerIntoVista(scrollView , vista: currentVista! , pagenum: pagenumber ,playerContainer : false	, viewContainer : false)
                _playerList = _playerListTemp
                activityList = activityListTemp

                self.title = currentVista!.name

                 toggleBarVisible()
            }
            else
            {
                for pl in _playerList
                {
                    if (pl.isKindOfClass(UIWebView))
                    {
                      //  var p = pl as! UIWebView
                    }
                    else
                    {
                        //let p = pl as! VLCMediaPlayer
                        //p.pause()
                        //p.play()
                    }
                }


            }

            currentPage = pagenumber

        }

    }


    var  IsDragging : Bool = false;

    func scrollViewWillBeginDragging(scrollView: UIScrollView) {

        //let  pagenumber : CGFloat = scrollView.contentOffset.x /  scrollView.frame.size.width


        for pl in _playerList		{
            if (pl.isKindOfClass(UIWebView))
            {
             //   var p = pl as! UIWebView

            }
            else{
              //  let p = pl as! VLCMediaPlayer
              //  p.pause()
            }
        }

        IsDragging = true;

    }

    func  addPlayerIntoVista( view : UIView , vista: Vista, pagenum: Int , playerContainer : Bool, viewContainer :Bool)	{

        let quante = Int(vista.size)

        //array delle camere della vista selezionata
        let cams : [Cam] = [Cam] (vista.content.values)

        for (var i = 0; i < quante; i++) {
            //se ce la cam
            if ( i <  cams.count )
            {
                let str : String = vista.contentIds[i]

                let camtest : Cam? =  vista.content[ str ]

                if(camtest != nil)
                {

                let cam : Cam = vista.content[ str ]!


                if( cam.urlVideo_1 != nil )  // SE HO IL PLAYER
                {

                    //var url =  cam.urlSmallVideo
                    var url =  cam.urlVideo_1

                    if (vista.size == "4") {  url = cam.urlVideo_4 }
                    else if (vista.size == "6") { url = cam.urlVideo_6  }
                    else if ( vista.size == "9") { url = cam.urlVideo_9 }
                    else{ url = cam.urlVideo_16 }

                    // inizializza il player

                    let arr = ["--extraintf="] as NSArray
                    let player =  VLCMediaPlayer(options: arr as [AnyObject])
                    player.audio?.muted=true;
                    let playerview = UIView()

                    playerview.userInteractionEnabled = false
                    if (playerContainer){
                        _playerList.append(player)
                    }
                    else	{
                        _playerListTemp.append(player)
                    }

                    if (vista.size == "4") { playerview.frame = init4Player(i, frame: scrollView.frame )}
                    else if (vista.size == "6") { playerview.frame = init6Player(i)}
                    else if ( vista.size == "9") { playerview.frame = init9Player(i)}
                    else	{ playerview.frame = init16Player(i) }

//
                    let  isIOS8 : Bool =  SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("8.0");

                    if(isIOS8)
                    {
                        // - self.navigationController!.navigationBar.frame.height
                        playerview.frame = CGRect(x: playerview.frame.origin.x + (self.view.frame.width * CGFloat( pagenum ))  , y: playerview.frame.origin.y /* - self.navigationController!.navigationBar.frame.height */ , width: playerview.frame.width, height: playerview.frame.height)
                    }
                    else{
                        playerview.frame = CGRect(x: playerview.frame.origin.x + (self.view.frame.width * CGFloat( pagenum ))  , y: playerview.frame.origin.y , width: playerview.frame.width, height: playerview.frame.height)
                    }
//
                      playerview.tag = i

                    let nsurl = NSURL(string: url!)

                    let media = VLCMedia (URL: nsurl)
                    player.media = media
                 //   player.media.addOptions(mediaOptionDictionary())

                    if ( url!.rangeOfString("mjpg") != nil )
                    {
                        media.addOptions(mediaOptionDictionary())
                        // liveStreamTimer()
                    }
                    else
                    {
                        media.addOptions(mediaOptionDictionaryRtsp() )
                    }


                    player.delegate = self
                    player.drawable = playerview
                    playerview.userInteractionEnabled = false

                    player.play();

                    setplayerAspectRatio(player)

                    scrollView.addSubview(playerview);

                    // add view on player for  recognizer

                    let pView = UIView(frame: CGRect(x: 0, y : 0 , width:  playerview.frame.width, height : playerview.frame.height ) )
                    pView.tag = i
                    playerview.addSubview(pView);
                    pView.userInteractionEnabled=true

                    if (viewContainer){
                        viewList.append(pView)
                    }else {
                        viewListTemp.append(pView)
                    }


                    // loading indicator
                    let actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: pView.frame) as UIActivityIndicatorView
                    actInd.center = pView.center
                    actInd.hidesWhenStopped = true
                    actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
                    actInd.color = UIColor.arancio()
                    actInd.startAnimating()

                    pView.addSubview(actInd)

                    if (viewContainer){
                        activityList.append(actInd)
                    }else {
                        activityListTemp.append(actInd)
                    }
                }
                else if (cam.webViewVideoVideo_1 != nil) {  // SE HO LA WEBWIEW CON LA CAM
                    var url =  cam.webViewVideoVideo_1

                    if (vista.size == "4") {  url = cam.webViewVideoVideo_4 }
                    else if (vista.size == "6") { url = cam.webViewVideoVideo_6  }
                    else if ( vista.size == "9") { url = cam.webViewVideoVideo_9 }
                    else{ url = cam.webViewVideoVideo_16 }


                    //creo la webView
                    let playerWebView : UIWebView  = UIWebView()
                    playerWebView.backgroundColor = UIColor.arancio() //UIColor.sfondoScuro()
                    playerWebView.tag    = i


                    //setta la dimensione del player
                    if (vista.size == "4") { playerWebView.frame = init4Player(i, frame: scrollView.frame )}
                    else if (vista.size == "6") { playerWebView.frame = init6Player(i)}
                    else if ( vista.size == "9") { playerWebView.frame = init9Player(i)}
                    else	{ playerWebView.frame = init16Player(i) }

                    //playerWebView.frame = CGRect(x: playerWebView.frame.origin.x + (self.frame().width * CGFloat(currentVistaId))  , y: playerWebView.frame.origin.y , width: playerWebView.frame.width, height: playerWebView.frame.height)

                    playerWebView.frame = CGRect(x: playerWebView.frame.origin.x + (self.view.frame.width * CGFloat( pagenum ))  , y: playerWebView.frame.origin.y , width: playerWebView.frame.width, height: playerWebView.frame.height)
                    //LOAD URL

                    let urlR = NSURL(string: url!	)
                    let request = NSURLRequest(URL: urlR!)
                    playerWebView.loadRequest(request)

                    scrollView.addSubview(playerWebView);
                    self.view.bringSubviewToFront(playerWebView)
                    playerWebView.delegate = self

                    if (playerContainer){
                        _playerList.append(playerWebView)
                    }
                    else	{
                        _playerListTemp.append(playerWebView)
                    }

                    // add view on player for  recognizer
                    let pView = UIView(frame: CGRect(x: 0, y : 0 , width:  playerWebView.frame.width, height : playerWebView.frame.height ) )
                    pView.tag = i
                    playerWebView.addSubview(pView);
                    viewList.append(pView)

                    if (viewContainer){
                        viewList.append(pView)
                    }else {
                        viewListTemp.append(pView)
                    }

                    // loading indicator
                    let actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: pView.frame) as UIActivityIndicatorView
                    actInd.center = pView.center
                    actInd.hidesWhenStopped = true
                    actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
                    actInd.color = UIColor.arancio()
                    actInd.startAnimating()
                    actInd.alpha = 0

                    pView.addSubview(actInd)
                    if (viewContainer){
                        activityList.append(actInd)
                    }else {
                        activityListTemp.append(actInd)
                    }



                }

                }
            }

        }

    }


////////////////////////

    func buffertimer(){


            _timerLastFrame = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("onTimerDurationFired:" ), userInfo: nil, repeats: true	)
           }

    func onTimerDurationFired (timer : NSTimer) {

        UIApplication.sharedApplication().idleTimerDisabled = false
        UIApplication.sharedApplication().idleTimerDisabled = true

        for pl in _playerList
        {
            if (pl.isKindOfClass(UIWebView))
            {

            }
            else{

                let p = pl as! VLCMediaPlayer
                let urlString: String = p.media.url.absoluteString

                if ( urlString.rangeOfString("mjpg") != nil )
                {
                    p.gotoNextFrame();
                }


            }
        }
    }

   


    // VKPLAYER DELEGATE
    func setplayerAspectRatio( player: VLCMediaPlayer )    {
// setplayerAspectRatio(p, rect: (p.drawable as! UIView ).frame)

        let rect : CGRect = (player.drawable as! UIView ).frame
        let h = (rect.height / rect.height) * 100
        let w = (rect.width / rect.height) * 100
        let ratio = "\( Int (w) ):\(  Int(h) )"


        print(ratio)

        CriptString.setaspectRatio(player , ratio)


    }

    func mediaOptionDictionary()-> [NSObject:AnyObject] {

        let dict = ["network-caching" : 0 ];


        return dict
    }



    func mediaOptionDictionaryRtsp()-> [NSObject:AnyObject]
    {
        let dict = ["network-caching" :  2111 ]// , "audio-time-stretch" : "0" ,"subsdec-encoding":"Windows-1252","avcodec-skiploopfilter":3 ];
        return dict
    }


    func mediaPlayerStateChanged(aNotification: NSNotification!) {
      //  print("statechanged"  )

        let player =         aNotification.object as! VLCMediaPlayer



        let  currentstate : VLCMediaPlayerState = player.state
        switch (currentstate.rawValue)
        {
        case  VLCMediaPlayerState.Error.rawValue  :
            print("player error " )
          //  self.removeLoading()
          //  self.alertMessage("ErrorTitle".loc(), message: "ErrorNoEvent".loc(), button: "annulla".loc() , delegate: self, _tag: 21)
        case  VLCMediaPlayerState.Buffering.rawValue  :
           // print("player bufering " )

            player.audio?.muted=true;
        case  VLCMediaPlayerState.Playing.rawValue  :
            print("player playng " )

            player.audio?.muted=true;
            activityList[(player.drawable as! UIView).tag].stopAnimating()
            activityList[(player.drawable as! UIView).tag].removeFromSuperview()

         //   buffertimer()
         //   self.removeLoading()
        case VLCMediaPlayerState.Stopped.rawValue :
            print("player stopped  " )

        default:print("")
        }
    }

    // RITORNA LE DIMENSIONI DEL PLAYER

    func init4Player(i : Int , frame : CGRect ) ->CGRect	{
        let w = frame.width
        let h = frame.height

        var result : CGRect = CGRect()

        self.automaticallyAdjustsScrollViewInsets = false

        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
            switch (i)
            {
            case 0 :result =  CGRect(x: 0,y: 0,width: w / 2 ,height: h / 2 );//CGRectMake(10.0, 20.0, widthLabel, heightLabel);
            case 1 :result = CGRect(x: w / 2 ,y: 0 ,width: w / 2 ,height: h / 2 ); //CGRectMake(163.0, 20.0, widthLabel, heightLabel);
            case 2 :result = CGRect(x: 0,y: h / 2 ,width: w / 2 , height: h / 2 ); // CGRectMake(10.0, 144.0, widthLabel, heightLabel);
            case 3 :result = CGRect(x: w/2,y: h/2,width: w / 2 ,height: h / 2 );  //CGRectMake(163.0, 144.0, widthLabel, heightLabel);
            default :result =  CGRect(x: 0, y: 0, width: w / 2,height: h / 2 );
            }
        }
        else{

            let heigh43 = (w / 2 ) * (3 / 4)
            switch (i)
            {
            case 0 :result =  CGRect(x: 0, y: 0,width: w / 2 ,height: heigh43 );//CGRectMake(10.0, 20.0, widthLabel, heightLabel);
            case 1 :result = CGRect(x: w / 2 , y: 0 ,width: w / 2 ,height: heigh43 ); //CGRectMake(163.0, 20.0, widthLabel, heightLabel);
            case 2 :result = CGRect(x: 0,y:  heigh43 ,width: w / 2 , height: heigh43 ); // CGRectMake(10.0, 144.0, widthLabel, heightLabel);
            case 3 :result = CGRect(x: w/2, y: heigh43 ,width: w / 2 ,height: heigh43 );  //CGRectMake(163.0, 144.0, widthLabel, heightLabel);
            default :result =  CGRect(x: 0, y: 0, width: w / 2,height: h / 2 );
            }
        }

        return result
    }
    func init6Player(i : Int) ->CGRect	{

        self.automaticallyAdjustsScrollViewInsets = false

        let w = self.view.frame.width
        let h =  self.frameBetweenBar().height //self.view.frame.height

        var result : CGRect = CGRect()
        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){

            switch (i)
            {
            case 0 :result =  CGRect(x: 0, y: 0, width: w / 3,height: h / 2 );
            case 1 :result = CGRect(x: w * (1/3) ,y: 0 ,width: w / 3 ,height: h / 2 );
            case 2 :result = CGRect(x: w * (2/3) ,y:0 ,width: w / 3 , height: h / 2 );
            case 3 :result = CGRect(x: 0, y: h / 2 ,width: w / 3 , height: h / 2 );
            case 4 :result = CGRect(x: w * (1/3),y: h / 2 ,width: w / 3 , height: h / 2 );
            case 5 :result = CGRect(x: w * (2/3),y: h/2, width: w / 3 ,height: h / 2 );
            default :result =  CGRect(x: 0, y: 0, width: w / 3,height: h / 2 );
            }
        }
        else{
            let heigh43 = (w / 2 ) * (3 / 4)
            switch (i)
            {
            case 0 :result =  CGRect(x: 0, y: 0, width: w / 2,height: heigh43 );
            case 1 :result = CGRect(x: w * (0.5) ,y: 0 ,width: w / 2 ,height: heigh43 );
            case 2 :result = CGRect(x: 0 ,y:heigh43 ,width: w / 2 , height: heigh43 );
            case 3 :result = CGRect(x: w * (0.5), y: heigh43 ,width: w / 2 , height: heigh43 );
            case 4 :result = CGRect(x: 0 ,y: heigh43 * 2 ,width: w / 2 , height: heigh43 );
            case 5 :result = CGRect(x: w * (0.5),y: heigh43 * 2 , width: w / 2 ,height: heigh43 );
            default :result =  CGRect(x: 0, y: 0, width: w / 2,height: heigh43 );
            }
        }
        return result

    }
    func init9Player(i : Int) ->CGRect	{

        let w = self.view.frame.width
        let h = self.frameBetweenBar().height //self.view.frame.height
        self.automaticallyAdjustsScrollViewInsets = false
        var result : CGRect = CGRect()
        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){

            switch (i)
            {
            case 0 :result =  CGRect(x: 0, y: 0, width: w / 3,height: h / 3 );
            case 1 :	result = CGRect(x: w * (1/3) ,y: 0 ,width: w / 3 ,height: h / 3);
            case 2 :result = CGRect(x: w * (2/3) ,y:0 ,width: w / 3 , height: h / 3 );
            case 3 :result = CGRect(x: 0, y: h * (1/3) ,width: w / 3 , height: h / 3 );
            case 4 :result = CGRect(x: w * (1/3),y: h * (1/3) ,width: w / 3 , height: h / 3 );
            case 5 :result = CGRect(x: w * (2/3),y: h * (1/3) ,width: w / 3 , height: h / 3 );
            case 6 :result = CGRect(x:  0,y: h * (2/3) ,width: w / 3 , height: h / 3 );
            case 7 :result = CGRect(x: w * (1/3),y: h * (2/3) ,width: w / 3 , height: h / 3 );
            case 8 :	result = CGRect(x: w * (2/3),y: h * (2/3) , width: w / 3 ,height: h / 3 );
            default :result =  CGRect(x: 0, y: 0, width: w / 3,height: h / 3 );
            }
        }
        else{
            let heigh43 = (w / 3 ) * (3 / 4)
            switch (i)
            {
            case 0 :result =  CGRect(x: 0, y: 0, width: w / 3,height: heigh43 );
            case 1 :	result = CGRect(x: w * (1/3) ,y: 0 ,width: w / 3 ,height: heigh43);
            case 2 :result = CGRect(x: w * (2/3) ,y:0 ,width: w / 3 , height: heigh43 );
            case 3 :result = CGRect(x: 0, y: heigh43 ,width: w / 3 , height: heigh43 );
            case 4 :result = CGRect(x: w * (1/3),y: heigh43 ,width: w / 3 , height: heigh43 );
            case 5 :result = CGRect(x: w * (2/3),y: heigh43 ,width: w / 3 , height: heigh43 );
            case 6 :result = CGRect(x:  0,y: heigh43 * (2) ,width: w / 3 , height: heigh43 );
            case 7 :result = CGRect(x: w * (1/3),y: heigh43 * (2) ,width: w / 3 , height: heigh43 );
            case 8 :	result = CGRect(x: w * (2/3),y: heigh43 * (2) , width: w / 3 ,height:heigh43 );
            default :result =  CGRect(x: 0, y: 0, width: w / 3,height: heigh43 );
            }
        }
        return result

    }
    func init16Player(i : Int) ->CGRect	{

        let w = self.view.frame.width
        let h = self.frameBetweenBar().height // self.view.frame.height
        self.automaticallyAdjustsScrollViewInsets = false
        var result : CGRect = CGRect()
        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
            switch (i)
            {
            case 0 :result =  CGRect( x: 0 , y: 0 , width:  (w / 4)    , height: (h / 4 )  );
            case 1 :	result = CGRect(x: w * (1/4) ,y: 0 ,width: (w / 4) ,height: h / 4);
            case 2 :result = CGRect(x: w * (2/4) ,y:0 ,width: w / 4 , height: h / 4 );
            case 3 :result = CGRect(x: w * (3/4) , y: 0 ,width: w / 4 , height: h / 4 );
            case 4 :result = CGRect(x: 0,y: h * (1/4) ,width: w / 4 , height: h / 4 );
            case 5 :result = CGRect(x: w * (1/4),y: h * (1/4) ,width: w / 4 , height: h / 4 );
            case 6 :result = CGRect(x:  w * (2/4) ,y: h * (1/4) ,width: w / 4 , height: h / 4 );
            case 7 :result = CGRect(x: w * (3/4),y: h * (1/4) ,width: w / 4 , height: h / 4 );
            case 8 :	result = CGRect(x: 0 ,y: h * (2/4) , width: w / 4 ,height: h / 4 );
            case 9 :result = CGRect(x: w * (1/4) ,y:h * (2/4) ,width: w / 4 , height: h / 4 );
            case 10 :result = CGRect(x: w * (2/4) , y: h * (2/4) ,width: w / 4 , height: h / 4 );
            case 11 :result = CGRect(x: w * (3/4),y: h * (2/4) ,width: w / 4 , height: h / 4 );
            case 12 :result = CGRect(x: 0 ,y: h * (3/4) ,width: w / 4 , height: h / 4 );
            case 13 :result = CGRect(x:  w * (1/4) ,y: h * (3/4) ,width: w / 4 , height: h / 4 );
            case 14 :result = CGRect(x: w * (2/4),y: h * (3/4) ,width: w / 4 , height: h / 4 );
            case 15 :	result = CGRect(x: w * (3/4),y: h * (3/4) , width: w / 4 ,height: h / 4 );
            default :result =  CGRect( x: 0 , y: 0 , width:  (w / 4)    , height: (h / 4 )  );
            }
        }
        else{
            let heigh43 = (w / 4 ) * (3 / 4)
            switch (i)
            {
            case 0 :result =  CGRect( x: 0 , y: 0 , width:  (w / 4)    , height:heigh43  );
            case 1 :	result = CGRect(x: w * (1/4) ,y: 0 ,width: (w / 4) ,height: heigh43);
            case 2 :result = CGRect(x: w * (2/4) ,y:0 ,width: w / 4 , height: heigh43 );
            case 3 :result = CGRect(x: w * (3/4) , y: 0 ,width: w / 4 , height: heigh43 );
            case 4 :result = CGRect(x: 0,y: heigh43  ,width: w / 4 , height: heigh43 );
            case 5 :result = CGRect(x: w * (1/4),y: heigh43  ,width: w / 4 , height: heigh43 );
            case 6 :result = CGRect(x:  w * (2/4) ,y: heigh43  ,width: w / 4 , height: heigh43 );
            case 7 :result = CGRect(x: w * (3/4),y: heigh43  ,width: w / 4 , height: heigh43 );
            case 8 :	result = CGRect(x: 0 ,y: heigh43 * (2) , width: w / 4 ,height: heigh43 );
            case 9 :result = CGRect(x: w * (1/4) ,y:heigh43 * (2) ,width: w / 4 , height: heigh43 );
            case 10 :result = CGRect(x: w * (2/4) , y: heigh43 * (2) ,width: w / 4 , height: heigh43 );
            case 11 :result = CGRect(x: w * (3/4),y: heigh43 * (2) ,width: w / 4 , height: heigh43);
            case 12 :result = CGRect(x: 0 ,y: heigh43 * (3) ,width: w / 4 , height: heigh43 );
            case 13 :result = CGRect(x:  w * (1/4) ,y: heigh43 * (3) ,width: w / 4 , height: heigh43 );
            case 14 :result = CGRect(x: w * (2/4),y: heigh43 * (3) ,width: w / 4 , height: heigh43 );
            case 15 :	result = CGRect(x: w * (3/4),y: heigh43 * (3) , width: w / 4 ,height: heigh43 );
            default :result =  CGRect( x: 0 , y: 0 , width:  (w / 4)    , height: heigh43  );
            }
        }
        return result
    }

    //
    func onPlayerTapped(sender:UITapGestureRecognizer)	{
        
        toggleBarVisible()
        
    }
    
    func onPlayerDoubleTapped(sender:UITapGestureRecognizer)	{
        
        print("doubletap")
        
        var playernumber = 0
        for pl in _playerList		{
            if (pl.isKindOfClass(UIWebView))
            {
                let p = pl as! UIWebView
                let location = sender.locationInView( p )

                if(location.x >= 0 && location.x < p.frame.width && location.y >= 0 && location.y < p.frame.height)
                {
                    break
                }
                playernumber = playernumber + 1


            }
            else{
                let p = pl as! VLCMediaPlayer
                let location = sender.locationInView(  (p.drawable as! UIView ) )

                //se e compreso nella vie
                if(location.x >= 0 && location.x <  (p.drawable as! UIView ).frame.width && location.y >= 0 && location.y <  (p.drawable as! UIView ).frame.height)
                {
                    break
                }
                playernumber = playernumber + 1
            }
        }
        
        
        if (playernumber < _playerList.count ){
            
            print(playernumber)
            sendPlayerFullScreen(playernumber)
        }
    }

    // hide bar

    func  toggleBarVisible()	{
        setTabBarVisible(!tabBarIsVisible(), animated: true)
    }
    
    func hideBar()	{
        setTabBarVisible(false, animated: true)
    }
    
    func setTabBarVisible(visible:Bool, animated:Bool) {
        
        // bail if the current state matches the desired state
        if (tabBarIsVisible() == visible) { return }
        
        let frame2 = self.navigationController?.navigationBar.frame
        let height2 = frame2?.size.height
        let offsetY2 = (visible ? height2 : -height2!)
        
        
        
        
        // zero duration means no animation
        let duration2:NSTimeInterval = (animated ? 0.3 : 0.0)
        
        //  animate the tabBar
        if frame2 != nil {
            
            UIView.animateWithDuration(duration2, animations: { () -> Void in
                self.navigationController?.navigationBar.frame = CGRectOffset(frame2!, 0, offsetY2!)
                return
                }, completion: { (bool) -> Void in
                    
                    self.scrollView.frame = CGRect(x: 0, y: 0, width: self.frameBetweenBar().width , height: self.frameBetweenBar().height )   // self.view.frame // self.view.frame
                    self.touchView.frame = self.frameBetweenBar()
                    self.scrollView.contentOffset.x = self.view.frame.width * CGFloat(self.currentPage)
                    
                    
                    var i = 0
                    for pl in self._playerList		{
                        if (pl.isKindOfClass(UIWebView))
                        {
                            let p = pl as! UIWebView

                            if (self.currentVista!.size == "4") { p.frame = self.init4Player(i,frame:self.scrollView.frame)}
                            else if (self.currentVista!.size == "6") { p.frame = self.init6Player(i)}
                            else if ( self.currentVista!.size == "9") { p.frame = self.init9Player(i)}
                            else	{ p.frame = self.init16Player(i) }

                            p.frame = CGRect(x: p.frame.origin.x + (self.view.frame.width * CGFloat( self.currentPage )), y: p.frame.origin.y , width: p.frame.width, height: p.frame.height)
                        }
                        else{
                            let p = pl as! VLCMediaPlayer
                            if (self.currentVista!.size == "4") {  (p.drawable as! UIView ).frame = self.init4Player(i,frame:self.scrollView.frame)}
                            else if (self.currentVista!.size == "6") {  (p.drawable as! UIView ).frame = self.init6Player(i)}
                            else if ( self.currentVista!.size == "9") {  (p.drawable as! UIView ).frame = self.init9Player(i)}
                            else	{  (p.drawable as! UIView ).frame = self.init16Player(i) }

                            let  isIOS8 : Bool =  self.SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("8.0");
                            if(isIOS8)	{

                                 (p.drawable as! UIView ).frame = CGRect(x:  (p.drawable as! UIView ).frame.origin.x + (self.view.frame.width * CGFloat( self.currentPage ))  , y:  (p.drawable as! UIView ).frame.origin.y , width:  (p.drawable as! UIView ).frame.width, height:  (p.drawable as! UIView ).frame.height)
                            }
                            else{
                                 (p.drawable as! UIView ).frame = CGRect(x:  (p.drawable as! UIView ).frame.origin.x + (self.view.frame.width * CGFloat( self.currentPage ))  , y:  (p.drawable as! UIView ).frame.origin.y , width:  (p.drawable as! UIView ).frame.width, height:  (p.drawable as! UIView ).frame.height)
                            }

                             self.setplayerAspectRatio(p)
                           //  setplayerAspectRatio(p)
                        }
                        i = i + 1

                    }
                    
            })
            
        }
    }
    
    func tabBarIsVisible() ->Bool {
        return !(self.navigationController?.navigationBar.frame.origin.y < 0 )// CGRectGetMaxY(self.view.frame)
    }
    
    // PLAYER FULL SCREEN
    
    func sendPlayerFullScreen(id : Int)	{
        
        setTabBarVisible(true, animated: true)
        
        for pl in _playerList		{
            if (pl.isKindOfClass(UIWebView))
            {
                var p = pl as! UIWebView
            }
            else{
                let p = pl as! VLCMediaPlayer
                p.pause()
            }
        }

        let playerGreandeVc = PlayerLiveGrandeVC()

        let camID = currentVista!.contentIds[id]
        let cam : Cam  = DataManager.Instance.allCam[camID]!;

        if(cam.urlVideo_1 != nil){
            playerGreandeVc.urlViedeo =  cam.urlVideo_1!
        }

        playerGreandeVc.selectedCam =  cam // cams[id]
        navigationController?.pushViewController(playerGreandeVc , animated: true)  //showViewController(playerGreandeVc, sender: nil)
        
    }
    
    
    
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        _timerLastFrame?.invalidate()


        for pl in _playerList
        {
            if (pl.isKindOfClass(UIWebView))
            {
                let p = pl as! UIWebView
                p.delegate = nil
                p.removeFromSuperview()
            }
            else{
                let p = pl as! VLCMediaPlayer
               /* if(p.media  != nil)
                {
                    p.media.delegate = nil
                    p.media = nil
                }*/
                p.delegate = nil
               
            //    p.stop()
                //p.view.removeFromSuperview()
            }
        }

       //_playerList = [NSObject]()

        _timerLastFrame?.invalidate()
    }
    
    func modificaClick(sender : UIBarButtonItem ){
        
        for pl in _playerList		{
            if (pl.isKindOfClass(UIWebView))
            {
               // var p = pl as! VKPlayerController
            }
            else{
                let p = pl as! VLCMediaPlayer
                p.pause()
            }
        }

        for a  in activityList		{
            a.stopAnimating()
        }
        
        
        self.addLoading()
        
        dispatch_async(dispatch_get_main_queue(), {
            
            let creaVistaViewController = self.storyboard?.instantiateViewControllerWithIdentifier("creaVistaViewController") as! CreaVistaViewController
            creaVistaViewController.isEdit = true
            creaVistaViewController.selectedVista = self.currentVista
            self.navigationController?.pushViewController(creaVistaViewController, animated: true)
            
        });
        
        
        
    }
    
    
    //timer per il reload
    
    func onTimerFired(timer : NSTimer)
    {
        
        
       /* for pl in _playerList		{
            if (pl.isKindOfClass(UIWebView))
            {
                let p = pl as! UIWebView
                p.delegate = nil
                p.removeFromSuperview()
            }
            else{

            }
        }
        
        activityListTemp = [UIActivityIndicatorView]()
        addPlayerIntoVista(scrollView , vista: currentVista! , pagenum: currentPage ,playerContainer : false	, viewContainer : false)
        _playerList = _playerListTemp
        activityList = activityListTemp
        */

        let  pagenumber : Int = Int (scrollView.contentOffset.x /  scrollView.frame.size.width)
        


        for pl in _playerList
        {
            if (pl.isKindOfClass(UIWebView))
            {
                let p = pl as! UIWebView
                p.removeFromSuperview()
                p.delegate = nil
            }
            else
            {
                let p = pl as! VLCMediaPlayer
                p.stop()
                p.delegate = nil


                if ( activityList[(p.drawable as! UIView).tag] as UIActivityIndicatorView? != nil )
                {
                    activityList[(p.drawable as! UIView).tag].removeFromSuperview()
                }
            }
        }

        //setta la current vista
        currentVista = self.visteList [pagenumber]

        _playerListTemp  = [VLCMediaPlayer]()
        activityListTemp = [UIActivityIndicatorView]()
        addPlayerIntoVista(scrollView , vista: currentVista! , pagenum: pagenumber ,playerContainer : false	, viewContainer : false)
        _playerList = _playerListTemp
        activityList = activityListTemp

        self.title = currentVista!.name
    }


    //WEBVIEW DELEGATE

    func webViewDidFinishLoad(webView: UIWebView) {


        let contentSize :CGSize = webView.scrollView.contentSize;
        let viewSize:CGSize = webView.bounds.size;

        let rw  = viewSize.width / contentSize.width;

        webView.scrollView.minimumZoomScale = rw;
        webView.scrollView.maximumZoomScale = rw;
        webView.scrollView.zoomScale = rw;

        activityList[webView.tag].stopAnimating()
    }


    
}











