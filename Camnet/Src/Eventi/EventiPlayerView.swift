//
//  EventiPlayerView.swift
//  Camnet
//
//  Created by giulio piana on 11/01/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation

public var PlayerInstance: VLCMediaPlayer? = nil;

class EventiPlayerView: UIViewController,UIGestureRecognizerDelegate,UIScrollViewDelegate,UIAlertViewDelegate,VLCMediaPlayerDelegate,VLCMediaDelegate
{

    var evento : Evento?

    var player : VLCMediaPlayer?
    var playerview : UIView?

    //zoom scrollview
    var scrollview : UIScrollView = UIScrollView()
    //barra player
    var barraPlayer : BarraPlayer = BarraPlayer()

    //
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.setHidesBackButton(true, animated: true)


        //nasconde la tab bar
        self.tabBarController?.tabBar.hidden = true
        self.navigationController?.interactivePopGestureRecognizer!.enabled = false;
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self

        var url :String
        if ( evento!.urlVideo != nil){
            url = evento!.urlVideo!
        }
        else if ( evento!.imageUrl != nil){
            url  = evento!.imageUrl!
        }
        else{
            url = ""
        }

        self.title = evento!.type

        print("CREO PLAYER")
        print(url, terminator: "")


        //crea video

        let arr = ["--extraintf="] as NSArray

        /*if(PlayerInstance != nil)
        {
            player = PlayerInstance
        }
        else {
            player  =  VLCMediaPlayer(options: arr as [AnyObject])
            PlayerInstance = player
        }
         */
        player  = VLCMediaPlayer() // VLCMediaPlayer(options: arr as [AnyObject])
        playerview = UIView()

        playerview!.frame = CGRect(x: 0,y: 0,width: self.view.frame.width , height: self.view.frame.height);
        playerview!.tag = 1

        self.player?.delegate = self
        self.player?.drawable = playerview!

        print (url)
        let nsurl = NSURL(string: url)
        let media = VLCMedia (URL: nsurl)
        self.player?.media = media
        self.player?.media.addOptions(mediaOptionDictionary())

        if( url == "" )
        {
            self.alertMessage("ErrorTitle".loc(), message: "ErrorNoEvent".loc(), button: "annulla".loc() , delegate: self, _tag: 21)
        }
        else{
            player!.play();
        }

        //aggiungo scrollview per zoom
        self.automaticallyAdjustsScrollViewInsets = false

        scrollview = UIScrollView()

        scrollview.backgroundColor = UIColor.sfondoScuro()
        self.view.addSubview(scrollview);
        scrollview.addSubview(playerview!)
        scrollview.delegate = self
        scrollview.bounces=true
        scrollview.bouncesZoom=true
        scrollview.zoomScale = 1


        let tapGesture :UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "onPlayerTapped:")
        tapGesture.numberOfTapsRequired = 1
        scrollview.addGestureRecognizer(tapGesture)
        playerview!.addGestureRecognizer(tapGesture)

        // Crea Barra PLAYER

        barraPlayer   = NSBundle.mainBundle().loadNibNamed("BarraPlayer", owner: self, options: nil)[0] as! BarraPlayer
        barraPlayer.frame = CGRect(x:0 , y: self.frame().height - 80 - 200 , width: self.frame().width, height: 80 )
        barraPlayer.eventi = self
        barraPlayer.player = player!
        self.view.addSubview(barraPlayer)
        self.view.bringSubviewToFront(barraPlayer)


        self.addLoading()
    }

    override  func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        setTabBarVisible( true  , animated: true)
    }


    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {

        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){

            playerview!.frame = CGRect(x: 0,y: 0,width: self.view.frame.width , height: self.view.frame.height - 56 );

            let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
            scrollview.contentSize = scrollSize

            barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 56  , width: self.view.frame.width, height: 56 )
            scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width  , height: self.view.frame.height - 56)

        }
        else{
            playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )

            let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
            scrollview.contentSize = scrollSize

            barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 56  , width: self.view.frame.width, height: 56 )

            scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width  , height: self.view.frame.height - 56)

        }

        let scrollViewFrame = scrollview.frame
        let scaleWidth = scrollViewFrame.size.width / scrollview.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / scrollview.contentSize.height
        let minScale = min(scaleHeight, scaleWidth)

        scrollview.minimumZoomScale = minScale
        scrollview.maximumZoomScale = 10
        scrollview.zoomScale = minScale


        setplayerAspectRatio(player!)
    }

    override func viewWillDisappear(animated: Bool) {


        isback = true;
        self.removeLoading()
        player!.stop()
        player!.delegate = nil
        player = nil

        self.barraPlayer.stopDurationTimer()
        super.viewWillDisappear(animated)
    }


    override func viewWillAppear(animated: Bool) {

        super.viewWillAppear(animated)

        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){

            playerview!.frame = CGRect(x: 0,y: 0,width: self.view.frame.width , height: self.view.frame.height - 56 );

            let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
            scrollview.contentSize = scrollSize
        }
        else{
            playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )

            let scrollSize = CGSize(width: playerview!.frame.size.width , height: playerview!.frame.size.height)
            scrollview.contentSize = scrollSize
        }

        scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y , width: self.view.frame.width  , height: self.view.frame.height - 56 )

        barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 56  , width: self.view.frame.width, height: 56 )

        let scrollViewFrame = scrollview.frame
        let scaleWidth = scrollViewFrame.size.width / scrollview.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / scrollview.contentSize.height
        let minScale = min(scaleHeight, scaleWidth)

        scrollview.minimumZoomScale = minScale
        scrollview.maximumZoomScale = 10
        scrollview.zoomScale = minScale


        setplayerAspectRatio(player!)
    }


    @IBAction func backClick(sender: UIButton)
    {

        isback = true;
        self.removeLoading()
        player!.stop()
        player!.delegate = nil
        player = nil

    }


    func restartPlayer(){

        barraPlayer.stopDurationTimer()
        barraPlayer.Slider.value = 0

        let url = evento!.urlVideo!

        if (player != nil)
        {
            player?.delegate=nil
            player=nil
        }

        let arr = ["--extraintf="] as NSArray
        player  =  VLCMediaPlayer(options: arr as [AnyObject])

        self.player?.delegate = self
        self.player?.drawable = playerview!

        let nsurl = NSURL(string: url)
        let media = VLCMedia (URL: nsurl)
        self.player?.media = media
        self.player?.media.addOptions(mediaOptionDictionary())

        if( url == "" )
        {
            self.alertMessage("ErrorTitle".loc(), message: "ErrorNoEvent".loc(), button: "annulla".loc() , delegate: self, _tag: 21)
        }
        else{
            player!.play();
        }
        barraPlayer.player = player!
        setplayerAspectRatio(player!)
    }

    //PLAYER DELEGATE

    func setplayerAspectRatio( player: VLCMediaPlayer )
    {
        if   let rect : CGRect = playerview?.frame
        {

            let h = (rect.height / rect.height) * 100
            let w = (rect.width / rect.height) * 100
            let ratio = "\( Int (w) ):\(  Int(h) )"

            CriptString.setaspectRatio(player , ratio)
        }
    }

    func mediaOptionDictionary()-> [NSObject:AnyObject] {

        let dict = ["network-caching" : 0 ];


        return dict
    }



    var isback : Bool = false;


    func mediaPlayerStateChanged(aNotification: NSNotification!) {
        let  currentstate : VLCMediaPlayerState = player!.state
        switch (currentstate.rawValue)
        {

        case  VLCMediaPlayerState.Error.rawValue  :
            print("player error " )
            self.removeLoading()
            self.alertMessage("ErrorTitle".loc(), message: "ErrorNoEvent".loc(), button: "annulla".loc() , delegate: self, _tag: 21)

        case  VLCMediaPlayerState.Buffering.rawValue  :
            print("player bufering " )

        case  VLCMediaPlayerState.Playing.rawValue  :
            print("player playng " )
            barraPlayer.startDurationTimer()
            self.removeLoading()
            self.navigationItem.setHidesBackButton(false, animated: true)

        case VLCMediaPlayerState.Stopped.rawValue :
            print("player stopped" )

            if(!isback){
                player!.stop()
                restartPlayer()
            }
        case VLCMediaPlayerState.Ended.rawValue :
            print("player ended" )

        case VLCMediaPlayerState.Paused.rawValue :
            print("player paaused" )
            barraPlayer.stopDurationTimer()
        default:print("")
        }
    }



    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if (alertView.tag == 21)
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }


    //SCROLL VIEW DELEGATE

    func centerScrollViewContents(){


    }

    func scrollViewDidZoom(scrollView: UIScrollView) {
        centerScrollViewContents()
    }

    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return playerview!
    }

    func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat) {

        if(scale == 1)
        {
            print("EndZoom")
        }

    }

    //HIDE PLAYER BAR

    func onPlayerTapped(sender:UITapGestureRecognizer)	{

        toggleBarVisible()

    }

    func toggleBarVisible()	{
        setTabBarVisible(!tabBarIsVisible(), animated: true)
    }

    func hideBar()	{
        setTabBarVisible(false, animated: true)
    }

    func setTabBarVisible(visible:Bool, animated:Bool) {

        // bail if the current state matches the desired state
        if (tabBarIsVisible() == visible) { return }

        let frame2 = self.navigationController?.navigationBar.frame
        let height2 = frame2!.size.height

        let duration2:NSTimeInterval = (animated ? 0.3 : 0.0)

        if (visible){ // VISIbile
            UIView.animateWithDuration(duration2, animations: { () -> Void in
                self.navigationController?.navigationBar.frame = CGRectOffset(frame2!, 0, height2)

                if(self.IsPad())
                {
                    self.automaticallyAdjustsScrollViewInsets = false
                    self.barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 56  , width: self.view.frame.width, height: 56 )
                    self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y  , width: self.view.frame.width , height: self.frameBetweenBar().height - 56  );

                }
                else{
                    self.barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height - 56  , width: self.view.frame.width, height: 56 )
                    self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y  , width: self.view.frame.width , height: self.frameBetweenBar().height - 56  );
                }

                return
                }, completion: { (bool) -> Void in
                    self.scrollview.zoomScale = 1

                    if(self.IsPad())
                    {
                        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height  - 56  )
                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }
                        else{
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }
                    }
                    else{

                        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height - 56  )

                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }
                        else{

                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )

                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }
                    }
            })
        }
        else{ // NON VISIBILE
            UIView.animateWithDuration(duration2, animations: { () -> Void in
                self.navigationController?.navigationBar.frame = CGRectOffset(frame2!, 0, -height2)
                
                if(self.IsPad())
                {
                    self.barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height   , width: self.view.frame.width, height: 70 )
                    self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y  , width: self.view.frame.width , height: self.view.frame.height );
                }
                else{
                    self.barraPlayer.frame = CGRect(x:0 , y: self.view.frame.height  , width: self.view.frame.width, height: 56 )
                    self.scrollview.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y  , width: self.view.frame.width , height: self.view.frame.height );
                }
                
                
                return
                }, completion: { (bool) -> Void in
                    self.scrollview.zoomScale = 1
                    
                    if(self.IsPad())
                    {
                        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height  )
                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }
                        else{
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }
                    }
                    else{
                        
                        if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width  , height:  self.frameBetweenBar().height  )
                            
                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }
                        else{
                            
                            self.playerview!.frame = CGRect(x: 0, y: 0 , width: self.view.frame.width , height: self.view.frame.width * 3 / 4 )
                            
                            let scrollSize = CGSize(width: self.playerview!.frame.size.width , height: self.playerview!.frame.size.height)
                            self.scrollview.contentSize = scrollSize
                        }
                        
                    }
                    
            })
        }
        
        setplayerAspectRatio(player!)
    }
    
    func tabBarIsVisible() ->Bool {
        return !(self.navigationController?.navigationBar.frame.origin.y < 0 )
    }
    
    func navbarvisible() -> Bool{
        
        return self.navigationController!.navigationBar.hidden
    }
    
    
}