//
//  LoadingScreenVC.swift
//  Camnet
//
//  Created by giulio piana on 27/01/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation
class LoadingScreenVC: UIViewController {
	
	@IBOutlet weak var loadingLbl: UILabel!

	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		loadingLbl.text =  NSLocalizedString("loading", comment:  "")
	}
	
	
/*	override func supportedInterfaceOrientations() -> Int {
		if(self.IsPad())
		{
			return super.supportedInterfaceOrientations()
		}
		else{
			return UIInterfaceOrientation.Portrait.rawValue
		}
		
	}
	
	override func shouldAutorotate() -> Bool {
		return false
		
		/*/if(self.IsPad())
		{
			return true
		}
		else{
			return false
		}*/
	}
	*/

}