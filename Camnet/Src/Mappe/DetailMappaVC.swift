
//  DetailMappaVC.swift
//  Camnet
//
//  Created by giulio piana on 26/01/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation

import UIKit

class  DetailMappaVC : UIViewController, UITableViewDelegate, UITableViewDataSource,UIWebViewDelegate,SaveViewProtocol,UIGestureRecognizerDelegate {
	
	var selectedVista : Vista?
	var selectedMap : Mappa?
	var selectedImpianto : Impianto?
    var selectedCamId : String?
	
	var menuTable : UITableView!
	
	var mapWebView: UIWebView!
	var MenuBtn: UIButton!
	var NewViewBtn: UIButton!
	
	var isMenuopen : Bool = false
	var loadInd : UIActivityIndicatorView?
	
	var isCreaVista:Bool = false
	var selectedCamsId : [String] = [String]()
	
	var border: CALayer!
	//VIEW LOAD
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		/**/
		self.tabBarController?.tabBar.hidden=true
		self.navigationController?.interactivePopGestureRecognizer!.enabled = false;
		self.navigationController?.interactivePopGestureRecognizer!.delegate = self
		
		
		border  = CALayer()
		border.backgroundColor = UIColor.sfondoChiaro().CGColor
		border.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 66 , width: 1024, height: 1)
		
		view.layer.addSublayer(border)
		
		//creo MenuBtn
		
		MenuBtn = UIButton(type: UIButtonType.System) //UIButton()
		MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
		MenuBtn.setTitle("Nome Vistea (10  viste)", forState: UIControlState.Normal)
		MenuBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left
		MenuBtn.tintColor = UIColor.sfondoScuro()
		view.addSubview(MenuBtn)
		
		MenuBtn.addTarget(self, action: Selector("ElencoVisteClick:"), forControlEvents: UIControlEvents.TouchUpInside)
		
		if(selectedMap!.contentIds.count > 0){
			selectedVista = selectedImpianto!.views_user[ selectedMap!.contentIds[0] ]



            var stringname = ""

            if(selectedVista != nil )
            {
                    MenuBtn.setTitle(selectedVista!.name + "(" +  selectedVista!.size + " Viste )" , forState: UIControlState.Normal)
            }
            else{
                MenuBtn.setTitle(  "( 0 Viste )" , forState: UIControlState.Normal)
            }

		}
		else	{
			MenuBtn.userInteractionEnabled	 = false
			MenuBtn.alpha = 0.5
			MenuBtn.setTitle("Nessuna vista" , forState: UIControlState.Normal)
		}
		
		//creo NewViewBtn
		NewViewBtn = UIButton(type: UIButtonType.System)
		NewViewBtn.frame = CGRect (x: view.frame.width-178, y: self.frameBetweenBar().origin.y + 8 , width: 170, height: 50)
		NewViewBtn.setTitle("creaVista".loc(), forState: UIControlState.Normal)
		NewViewBtn.tintColor = UIColor.arancio()
		view.addSubview(NewViewBtn)
		//bordo
		NewViewBtn.layer.masksToBounds = true;
		NewViewBtn.layer.cornerRadius = 24;
		NewViewBtn.layer.borderWidth = 2
		NewViewBtn.layer.borderColor =  UIColor.arancio().CGColor
		
		NewViewBtn.addTarget(self, action: Selector("CreaVistaClick:"), forControlEvents: UIControlEvents.TouchUpInside)
		
		//creo la tableview del menu
		
		menuTable = UITableView()
		if(self.IsPad()){
			menuTable.frame = CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width/5 	, height: 0)
		}
		else{
			menuTable.frame = CGRect(x: 0 , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width 	, height: 0)
			menuTable.separatorStyle = UITableViewCellSeparatorStyle.None
		}
		
		menuTable.separatorStyle = UITableViewCellSeparatorStyle.None
		menuTable.backgroundColor = UIColor.sfondoChiaro()
		
		self.view.addSubview(menuTable)
		self.view.bringSubviewToFront(menuTable)
		
		self.title = selectedMap?.name;
		menuTable.delegate = self
		menuTable.dataSource = self
		
		//WEBVIEW
		mapWebView = UIWebView(frame: CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.width, height: self.frameBetweenBar().height - 66))
		mapWebView.backgroundColor = UIColor.sfondoChiaro()
		view.addSubview(mapWebView)
		mapWebView.scalesPageToFit = true
		
		//LOAD URL
        var urls : String = selectedMap!.url
        if(selectedCamId != nil)
        {
            urls = selectedMap!.url + "&devices_id=" + selectedCamId!
        }

        print(urls, terminator: "")

		var url = NSURL(string: urls	)
		var request = NSURLRequest(URL: url!)
		mapWebView.loadRequest(request)
		mapWebView.delegate = self
		
		
		
		// loading indicator
		loadInd  = UIActivityIndicatorView(frame: view.frame)
		loadInd!.center = mapWebView.center
		loadInd!.hidesWhenStopped = true
		loadInd!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
		loadInd!.color = UIColor.arancio()
		loadInd!.startAnimating()
		
		view.addSubview(loadInd!)
		self.view.bringSubviewToFront(loadInd!)
		
	}

    override func viewDidAppear(animated: Bool) {
 
		super.viewWillAppear(animated)
		
		if(self.IsPad())
		{
			border.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 66 , width: 1024, height: 1)
			
			MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
			NewViewBtn.frame = CGRect (x: view.frame.width - 178, y: self.frameBetweenBar().origin.y + 8 , width: 170, height: 50)
			menuTable.frame =  CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width/5 	, height: 0)
			mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.width, height: self.frameBetweenBar().height - 66)
			loadInd!.center = mapWebView.center
			
		}
		else
		{
			border.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 66 , width: 1024, height: 1)
			
			MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
			NewViewBtn.frame = CGRect (x: view.frame.width - 178, y: self.frameBetweenBar().origin.y + 8 , width: 170, height: 50)
			menuTable.frame = CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width 	, height: 0)
			mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.width, height: self.frameBetweenBar().height - 66)
			loadInd!.center = mapWebView.center
		}
		
	}
	
	//ORIENTATION
	override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		
		if(self.IsPad())
		{
			/*if (  toInterfaceOrientation.isPortrait		){
				MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
				NewViewBtn.frame = CGRect (x: view.frame.height - 178, y: self.frameBetweenBar().origin.y + 8 , width: 170, height: 50)
				menuTable.frame = CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width/5 	, height: 0)
				mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.height, height: self.frameBetweenBar().width - 66)
				loadInd!.center = mapWebView.center
				
			}
			else{
				
				
				MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
				NewViewBtn.frame = CGRect (x: view.frame.height - 178, y: self.frameBetweenBar().origin.y + 8 , width: 170, height: 50)
				menuTable.frame = CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width/5 	, height: 0)
				mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.height, height: self.frameBetweenBar().width - 66)
				loadInd!.center = mapWebView.center
			}*/
			
		}
		else{//iphone
			
			
			
		}
	}
	
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {

        if(self.IsPad())
        {

            border.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 66 , width: 1024, height: 1)


            MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
            NewViewBtn.frame = CGRect (x: view.frame.width - 178, y: self.frameBetweenBar().origin.y + 8 , width: 170, height: 50)
            menuTable.frame = CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width/5 	, height: 0)
            mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.width, height: self.frameBetweenBar().height - 66)
            loadInd!.center = mapWebView.center


        }
		else //if(!self.IsPad())
		{
			border.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 66 , width: 1024, height: 1)
			
			MenuBtn.frame = CGRect(x: 8, y:  self.frameBetweenBar().origin.y + 8, width: 250, height: 50)
			NewViewBtn.frame = CGRect (x: view.frame.width - 178, y: self.frameBetweenBar().origin.y + 8 , width: 170, height: 50)
			menuTable.frame = CGRect(x: MenuBtn.frame.origin.x , y: MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:  self.view.frame.width 	, height: 0)
			mapWebView.frame = CGRect(x: 0, y: self.frameBetweenBar().origin.y + 67 , width: self.view.frame.width, height: self.frameBetweenBar().height - 66)
			loadInd!.center = mapWebView.center
		}
	}
	
	
	
	
	//TABLE DATA SOURCE
	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return selectedMap!.contentIds.count
		
	}
	
	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		//	return UITableViewCell()
		
		
		var cell :UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("CellTitleDescription") as UITableViewCell?
		if(cell == nil)
		{
			let nib = UINib(nibName: "CellTitleDescription", bundle: nil)
			tableView.registerNib(nib, forCellReuseIdentifier: "CellTitleDescription")
			cell = tableView.dequeueReusableCellWithIdentifier("CellTitleDescription") as UITableViewCell?
		}
		
		let bgColorView = UIView()
		bgColorView.backgroundColor = UIColor.arancioSelezione()
		cell.selectedBackgroundView = bgColorView
		
		let vista = selectedImpianto!.views_user[ selectedMap!.contentIds[indexPath.row] ] //as Vista
		
		let title : UILabel =  cell.viewWithTag(1) as! UILabel
		title.text = vista?.name  // DataManager.Instance.impianti[ indexPath.row ].name
		
		let desc : UILabel = cell.viewWithTag(2) as! UILabel
		desc.text = vista?.description // "description"
		
		
		
		cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, tableView.frame.width  , 65 )
		
		let border : CALayer = CALayer()
		border.borderColor = UIColor.sfondoChiaro().CGColor
		border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: 1)
		border.borderWidth = 1
		cell.layer.addSublayer(border)
		
		return cell
	}
	
	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		
		//CARICA MAPPA CON CAM
		print("carica mappa con cam")
	}
	//heigt row
	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 65
		
	}
	
	
	//WEBVIEW
	// web view map event
	
	func webViewDidFinishLoad(webView: UIWebView) {
		
		self.loadInd!.stopAnimating()
		
		print( "web view finish load" )
		print( webView.request?.URL )
		// selectedCam()
	}
	
	var firstTime : Bool = true
	func webViewDidStartLoad(webView: UIWebView) {
		print( "web view start load" )
		print(webView.request?.URL  )
		
	}
	
	func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
		if (firstTime)
		{
			firstTime = false
		}
		else{
			
			if (isCreaVista)
			{
				if ( navigationType == UIWebViewNavigationType.LinkClicked ) {
					
					print( "link clickerd" )
					print(request.URL  )
					
					
					parseUrlCamSelected(request.URL!.absoluteString)
					
					return false;
				}
			}
			else{
				if ( navigationType == UIWebViewNavigationType.LinkClicked ) {
					
//					let string = request.URL!.absoluteString!
					parseUrlCamSelected2(request.URL!.absoluteString )
					
					return false;
				}
			}
			
		}
		
		
		return true
	}
	
	func parseUrlCamSelected2(url :String)
	{
		
        firstTime=true
        mapWebView.reload()
        loadInd!.startAnimating()
        view.addSubview(loadInd!)

		
		let myStringArr = url.componentsSeparatedByString("/")
		//let id = myStringArr.last
		
		if let id = myStringArr.last {
			
			let playerGreandeVc = PlayerLiveGrandeVC()
			
			let cam : Cam = DataManager.Instance.allCam[ id ]!
			playerGreandeVc.selectedCam = cam

            //playerGreandeVc.urlViedeo = cam.urlBigVideo!  //  _playerList[ id ].contentURLString
			
			navigationController?.pushViewController(playerGreandeVc , animated: true)  //showViewController(playerGreandeVc, sender: nil)
			
		}

		
	}
	
	
	func parseUrlCamSelected(url :String)
	{
		
		print(url)
		//DA METTERE CAM GIUSTA
		let myStringArr = url.componentsSeparatedByString("/")
		
		//let id = myStringArr.last
		
		if let id = myStringArr.last {
			//let selectedCamId = selectedImpianto!.devices[id]
			
			if(  selectedCamsId.contains(id) ){
				selectedCamsId.removeAtIndex( selectedCamsId.indexOf(id )! )
			}
			else{
				selectedCamsId.append( id )
			}
		}
		else{
			
		}
	}
	
	//ACTIOM
	
	
	
	@IBAction func CreaVistaClick(sender: UIButton) {
		print("Crea vista")
		
		if(!isCreaVista)
		{

            self.alertMessage("", message: "creaVistaMessage".loc(), button: "OK")
			//mapWebView.reload()
			
			self.navigationController?.navigationBar.hidden = true
			
			isCreaVista = true
			
			NewViewBtn.setTitle("salva".loc(), forState: UIControlState.Normal)
			NewViewBtn.removeTarget(self, action: Selector("CreaVistaClick:"), forControlEvents: UIControlEvents.TouchUpInside)
			NewViewBtn.addTarget(self, action: Selector("SalvaVistaClick:"), forControlEvents: UIControlEvents.TouchUpInside)
			
			
			MenuBtn.userInteractionEnabled	 = true
			MenuBtn.alpha = 1
			MenuBtn.removeTarget(self, action: Selector("ElencoVisteClick:"), forControlEvents: UIControlEvents.TouchUpInside)
			MenuBtn.addTarget(self, action: Selector("annullaSalvaVistaClick:"), forControlEvents: UIControlEvents.TouchUpInside)
			MenuBtn.setTitle("annulla".loc(), forState: UIControlState.Normal)
			
			UIView.animateWithDuration(0.4, animations: { () -> Void in
				self.view.frame = CGRect(x: self.view.frame.origin.x , y: self.view.frame.origin.y - self.navigationController!.navigationBar.frame.height ,  width:self.view.frame.width 	, height: self.view.frame.height + self.navigationController!.navigationBar.frame.height)
				}, completion: nil)
			
		}
	}
	
	func annullaSalvaVistaClick (sender : UIButton){
		
		print("annulla salva vista")
		
		UIView.animateWithDuration(0.4, animations: { () -> Void in
			self.view.frame = CGRect(x: self.view.frame.origin.x , y: self.view.frame.origin.y + self.navigationController!.navigationBar.frame.height ,  width:self.view.frame.width 	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height)
			}, completion: nil)
		
		firstTime = true
		mapWebView.reload()
		loadInd!.startAnimating()
		view.addSubview(loadInd!)
		
		isCreaVista = false
		selectedCamsId = [String]()
		self.navigationController?.navigationBar.hidden = false
		
		NewViewBtn.removeTarget(self, action: Selector("SalvaVistaClick:"), forControlEvents: UIControlEvents.TouchUpInside)
		NewViewBtn.addTarget(self, action: Selector("CreaVistaClick:"), forControlEvents: UIControlEvents.TouchUpInside)
		NewViewBtn.setTitle("creaVista".loc(), forState: UIControlState.Normal)
		
		MenuBtn.removeTarget(self, action: Selector("annullaSalvaVistaClick:"), forControlEvents: UIControlEvents.TouchUpInside)
		MenuBtn.addTarget(self, action: Selector("ElencoVisteClick:"), forControlEvents: UIControlEvents.TouchUpInside)
		
		if(selectedMap!.contentIds.count > 0){
            if(selectedVista != nil )
            {
                MenuBtn.setTitle(selectedVista!.name + "(" +  selectedVista!.size + " Viste )" , forState: UIControlState.Normal)
            }
            else{
                MenuBtn.setTitle(  "( 0 Viste )" , forState: UIControlState.Normal)
            }
		}
		else
		{
			MenuBtn.userInteractionEnabled	 = false
			MenuBtn.alpha = 0.5
			MenuBtn.setTitle("Nessuna vista" , forState: UIControlState.Normal)
		}
		
		
	}
	
	func SalvaVistaClick (sender : UIButton){
		
		print("salva vista")
		
		if (selectedCamsId.count > 0 ){
			
			
			let loadingViewVC = self.storyboard?.instantiateViewControllerWithIdentifier("SalvaVistaVC") as UIViewController?
			let salvavistaview = loadingViewVC!.view as! SalvaVistaView
			salvavistaview.frame = self.view.frame
			salvavistaview.tag = 81;
			salvavistaview.currentViewAttr = creaJsonAttr ()
			salvavistaview.currentInstallId = selectedImpianto!.id // getInstallId ()
			salvavistaview.currentViewId = nil
			salvavistaview.viewController = self
			salvavistaview.delegate = self
			self.view.addSubview(salvavistaview)
			self.view.bringSubviewToFront(salvavistaview)
			
		}
	}
	
	
	var viewmode = "4"
	func creaJsonAttr () -> String {
		
		viewmode = "4"
		
		if(selectedCamsId.count > 4 )
		{
			viewmode = "6"
		}
		if(selectedCamsId.count > 6 )
		{
			viewmode = "9"
		}
		if(selectedCamsId.count > 9 )
		{
			viewmode = "16"
		}
		
		var tempAttr = "{\"view_name\":\" <viewName> \",\"view_mode\":\"\(viewmode)\",\"view_details\":\"vista personalizzata\",\"hide_to_operator\":\"0\",\"map_id\":\" \( selectedMap!.id )  \",\"view_prop\":\"0\",\"cams\":{"
		
		var tempCamString = ""
		var camnum = 0
		for c:String in selectedCamsId {  // creo al lista delle cam
			tempCamString = tempCamString + "\"\(camnum)\" : \" \(c) \" ,"
			camnum++
		}
		tempCamString = tempCamString.substringToIndex( tempCamString.endIndex.predecessor() )   //tolgo la virgola in fondo
		
		tempAttr = tempAttr + tempCamString + "}}"
		
		return tempAttr
	}
	
	
	//Delegate SAVE VIEW

    func callBackSaveCancel() {
    
    }

	func callBackSave(vistaName :String  , vistaid : String) {
		
		
		self.view.viewWithTag(81)?.removeFromSuperview()
		self.removeLoading()
		
		
		
		let randNum = Int(arc4random_uniform(10000))

        var id_ : String = "\(randNum)"
        if( vistaid != "0"){
            id_ = vistaid
        }

        let vista = Vista(_id: id_ , _name: vistaName , _desc: "", _type: "user", _size: "\(viewmode)" )  //( _id: "100" , _name: vistaName , _desc: "", _type: "user", _size: selectedCam.count )
		
		for cam :String in selectedCamsId
		{
			vista.content[cam] = selectedImpianto!.devices[cam]
			vista.contentIds.append( cam)
		}

        vista.install = selectedImpianto!.id
		selectedImpianto!.views_user[ vista.id ] = vista
		DataManager.Instance.allUserView [ vista.id ] = vista


		
		annullaSalvaVistaClick(UIButton())
		
	}
	
	
	@IBAction func ElencoVisteClick(sender: UIButton) {
		
		if ( isMenuopen ){
			isMenuopen = false
			
			UIView.animateWithDuration(0.4, animations: { () -> Void in
				self.menuTable.frame = CGRect(x: self.MenuBtn.frame.origin.x, y: self.MenuBtn.frame.origin.y + self.MenuBtn.frame.height,  width:self.view.frame.width/5 	, height:0)
				}, completion: nil)
		}
		else{
			isMenuopen = true
			
			UIView.animateWithDuration(0.4, animations: { () -> Void in
				self.menuTable.frame = CGRect(x: self.MenuBtn.frame.origin.x, y: self.MenuBtn.frame.origin.y + self.MenuBtn.frame.height , width:self.view.frame.width/5 , height: self.view.frame.height/2)
				
				}, completion: nil)
		}
		
	}
	
	@IBAction func camButtonClick(sender: UIButton) {
	}
}
  