//
//  uicolorExtension.swift
//  Camnet
//
//  Created by giulio piana on 19/01/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//


 extension UIColor {

	
	 class func arancio() ->UIColor
	{
		return UIColor(red: 233/255, green: 94/255, blue: 39/255, alpha: 1)

	}
	
	class func arancioSelezione() ->UIColor
	{
		return UIColor(red: 255/255, green: 176/255, blue: 140/255, alpha: 1)
		
	}
	
	class func sfondoChiaro() ->UIColor
	{
		return UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
	}
	class func sfondoScuro() ->UIColor
	{
		return UIColor(red: 45/255, green: 45/255, blue: 45/255, alpha: 1)
	}
	class func sfondoPlayer() ->UIColor
	{
		return UIColor(red: 85/255, green: 85/255, blue: 87/255, alpha: 1)
	}
	class func sfondoHeader() ->UIColor
	{
		return UIColor(red: 169/255, green: 169/255, blue: 169/255, alpha: 1)
	}
	
}
