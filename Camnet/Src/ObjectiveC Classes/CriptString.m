//
//  CriptString.m
//  Camnet
//
//  Created by giulio piana on 17/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//


#import "CriptString.h"


@implementation CriptString

- (id) init
{
	self = [super init];
	
	return self;
	
	
}

-(NSString*) sha1:(NSString*)input
{
 const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
 NSData *data = [NSData dataWithBytes:cstr length:input.length];
 
 uint8_t digest[CC_SHA1_DIGEST_LENGTH];
 
 CC_SHA1(data.bytes, data.length, digest);
 
 NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
 
 for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
	 [output appendFormat:@"%02x", digest[i]];
 
 return output;
 
}


+(void ) setaspectRatio:( VLCMediaPlayer *) mp  {

     mp.videoAspectRatio = (char *)[@"4:3" UTF8String];
    mp.videoCropGeometry = "4:3";
}

+(void ) setaspectRatio:( VLCMediaPlayer *) mp : (NSString *)input  {

  //  mp.videoAspectRatio = (char *)[@"1.3:1" UTF8String]; //(char *) [ input UTF8String ] ;// (char *)[@"1:1" UTF8String];
    //mp.videoCropGeometry = (char *)[input UTF8String]; //(char *) [ input UTF8String ] ;
    mp.videoAspectRatio = (char *)[input UTF8String];
}







@end