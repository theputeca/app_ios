//
//  UIViewControllerExtension.swift
//  Camnet
//
//  Created by giulio piana on 19/01/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//



/*

proUser tag 98
loadingIntro tag 99
loading tag 95
*/

extension UIViewController {
	
	func frameBetweenBar() -> CGRect {
		
		var topRect : CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
		var bottRect :CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
		
		
		if ( self.navigationController != nil){
			if ( !self.navigationController!.navigationBar.hidden ){
			topRect = self.navigationController!.navigationBar.frame
			}
		}
		else{
			topRect = CGRect.zero
		}
		
		if ( self.tabBarController != nil){
			if ( !self.tabBarController!.tabBar.hidden ){
				bottRect = self.tabBarController!.tabBar.frame
			}
		}
		else{
			topRect = CGRect.zero
		}
		///
		//let  isIOS8 : Bool =  SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("8.0");
		
		let bounds : CGRect = self.view.frame
		
		/*let sharedApplication : UIApplication = UIApplication.sharedApplication();
		if (UIInterfaceOrientationIsLandscape(sharedApplication.statusBarOrientation)) {
		let height : CGFloat = CGRectGetWidth(bounds);
		let width : CGFloat  = CGRectGetHeight(bounds);
		
		let asd : CGFloat = (isIOS8) ? width : height;
		let asd2 : CGFloat = (isIOS8) ? height : width;
		
		let bounds : CGRect = CGRect (x: bounds.origin.x, y: bounds.origin.y, width: asd2 , height: asd)
		}*/
		///
		
		let posy : CGFloat =  topRect.height + topRect.origin.y
		
		let h : CGFloat = bounds.height -  bottRect.height - topRect.origin.y - topRect.height
		let asd =  CGRect(x: 0 , y: posy , width: bounds.width , height: h  )
		
		//
		
		return asd
	}
	
	func frame () -> CGRect {
		let  isIOS8 : Bool =  SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO("8.0");
		
		var bounds : CGRect = self.view.frame
		
		let sharedApplication : UIApplication = UIApplication.sharedApplication();
		if (UIInterfaceOrientationIsLandscape(sharedApplication.statusBarOrientation)) {
			let width : CGFloat = CGRectGetWidth(bounds);
			let height : CGFloat  = CGRectGetHeight(bounds);
			
			let asd : CGFloat = (isIOS8) ? width : height;
			let asd2 : CGFloat = (isIOS8) ? height : width;
			
			bounds  = CGRect (x: bounds.origin.x, y: bounds.origin.y, width: asd , height: asd2)
		}
		
		/*if (! legacyScreenHeightEnabled) {
		// In iOS <= 6.1 the container view is already offset below the status bar.
		// so no need to offset it if we use shouldAdjustChildViewHeightForStatusBar in iOS 7+.
		bounds.origin.y += statusBarHeight;
		}
		
		bounds.size.height -= statusBarHeight;*/
		
		print("\(bounds.width)  \(bounds.height)")
		
		return bounds;
	}
	
	func SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version: NSString) -> Bool {
		return UIDevice.currentDevice().systemVersion.compare(version as String,
			options: NSStringCompareOptions.NumericSearch) != NSComparisonResult.OrderedAscending
	}
	
	
	///ALERT
	
	func alertMessage( title: String , message:String , button:String ){
		
		
		
		if objc_getClass("UIAlertController") != nil {
			
			dispatch_async(dispatch_get_main_queue(), {
				if #available(iOS 8.0, *) {
				    let alert = UIAlertController(title: title , message: message, preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: button , style: UIAlertActionStyle.Default, handler: nil))
                    alert.view.tintColor = UIColor.arancio()
                    self.presentViewController(alert, animated: true, completion: nil)

				} else {
				    // Fallback on earlier versions
				}
							});
			
		}
		else {
			
			dispatch_async(dispatch_get_main_queue(), {
				print("UIAlertController can NOT be instantiated")
				
				//make and use a UIAlertView
				let alert: UIAlertView = UIAlertView()
				alert.delegate = self
				alert.title = title
				alert.message = message
				alert.addButtonWithTitle(button)
				//alert.addButtonWithTitle("Retry")
				alert.show()
				
			});
		}
	}
	
	func alertMessage( title: String , message:String , button:String , delegate : AnyObject?  , _tag : Int){
		
		dispatch_async(dispatch_get_main_queue(), {
			let alert: UIAlertView = UIAlertView()
			alert.delegate = self
			alert.title = title
			alert.message = message
			alert.addButtonWithTitle(button)
			
			alert.delegate = delegate
			alert.tag = _tag
			alert.show()
			
		});
		//self.view.bringSubviewToFront(alert)
	}
	
	func alertMessage( title: String , message:String , button:String,button2:String , delegate : AnyObject?  , _tag : Int){
		
		dispatch_async(dispatch_get_main_queue(), {
			let alert: UIAlertView = UIAlertView()
			alert.delegate = self
			alert.title = title
			alert.message = message
			alert.addButtonWithTitle(button)
			alert.addButtonWithTitle(button2)
			
			alert.delegate = delegate
			alert.tag = _tag
			alert.show()
		});
		
		//self.view.bringSubviewToFront(alert)
	}
	
	// PRO SCREEN
	
	func addProUserView(){
		
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		
		let premiumScreenVC = storyboard.instantiateViewControllerWithIdentifier("PremiumScreenVC") as! PremiumScreenVC
		premiumScreenVC.view.tag = 98
		self.view.addSubview(premiumScreenVC.view)
		
		
	}
	
	
	func addProUserView(frame:CGRect){

        if(self.view.viewWithTag(97) != nil )
        {
            self.view.viewWithTag(97)?.removeFromSuperview()
        }

		if ( self.view.viewWithTag(98) == nil )
		{
			
			let storyboard = UIStoryboard(name: "Main", bundle: nil)
			
			let premiumScreenVC = storyboard.instantiateViewControllerWithIdentifier("PremiumScreenVC") as! PremiumScreenVC
			premiumScreenVC.view.tag = 98
			
			premiumScreenVC.view.frame = frame
			self.view.addSubview(premiumScreenVC.view)
			self.view.bringSubviewToFront(premiumScreenVC.view)
			
			
		}
		
	}

    func addProUserView(frame:CGRect, isstoredVideo: Bool ){

        if(self.view.viewWithTag(98) != nil )
        {
            self.view.viewWithTag(98)?.removeFromSuperview()
        }

        if ( self.view.viewWithTag(97) == nil )
        {

            let storyboard = UIStoryboard(name: "Main", bundle: nil)

            let premiumScreenVC = storyboard.instantiateViewControllerWithIdentifier("PremiumScreenVC") as! PremiumScreenVC
              premiumScreenVC.isStoredVideo = isstoredVideo
            premiumScreenVC.view.tag = 97

           
            premiumScreenVC.view.frame = frame
            self.view.addSubview(premiumScreenVC.view)
            self.view.bringSubviewToFront(premiumScreenVC.view)
            
            
        }
        
    }

	func removeProUserView(){
		
		if(self.view.viewWithTag(98) != nil )
		{
			self.view.viewWithTag(98)?.removeFromSuperview()
		}
        if(self.view.viewWithTag(97) != nil )
        {
            self.view.viewWithTag(97)?.removeFromSuperview()
        }
	}
	
	//LoadingScreen
	
	func addLoadingView ()	{
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		
		let loadingViewVC = storyboard.instantiateViewControllerWithIdentifier("LoadinScreenVC") as! LoadingScreenVC
		loadingViewVC.view.tag = 99
		self.view.addSubview(loadingViewVC.view)
		
	}
	func removeLoadinView ()	{
		self.view.viewWithTag(99)?.alpha = 0
		self.view.viewWithTag(99)?.removeFromSuperview()
	}
	
	
	// LOADING VIEW
	func addLoading ()
	{
		
		//let currentWindow :UIWindow = UIApplication.sharedApplication().keyWindow!
		if( self.view.viewWithTag(95) == nil )
		{
			let loadingView  = NSBundle.mainBundle().loadNibNamed("LoadingView", owner: self, options: nil)[0] as! UIView
			loadingView.frame =  self.view.frame
			loadingView.tag = 95
			self.view.addSubview(loadingView)
			
			
			if(self.navigationItem.backBarButtonItem != nil)	{
				self.navigationItem.hidesBackButton = true;
			}
			if(self.navigationItem.rightBarButtonItem != nil ){
				self.navigationItem.rightBarButtonItem!.enabled = false
			}

            self.view.bringSubviewToFront(loadingView)
		}
        else{
            if let v = self.view.viewWithTag(95){
                self.view.bringSubviewToFront(v)
            }
        }
	}
	
	func addLoading (_frame : CGRect)
	{
		
		//let currentWindow :UIWindow = UIApplication.sharedApplication().keyWindow!
		if( self.view.viewWithTag(95) == nil )
		{
			let loadingView  = NSBundle.mainBundle().loadNibNamed("LoadingView", owner: self, options: nil)[0] as! UIView
			loadingView.frame = _frame
			loadingView.tag = 95
			self.view.addSubview(loadingView)
			
			
		}
	}
	
	
	
	func removeLoading ()
	{
		
		
		//self.view.bringSubviewToFront(self.view)
		
		if let v = self.view.viewWithTag(95){
			v.alpha = 0
			v.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
			v.removeFromSuperview()
		}
		if(self.navigationItem.backBarButtonItem != nil)	{
			self.navigationItem.hidesBackButton = false;
		}
		if(self.navigationItem.rightBarButtonItem != nil ){
			self.navigationItem.rightBarButtonItem!.enabled = true
		}
		
	}
	//LOG OUT
	
	func logout(){



		print("LOGOUT CLICK")


        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()

        // rimuove l'autostart autostart
        defaults.setBool( false , forKey: "doneAutostart")

		User.logout()

		defaults.removeObjectForKey("loginCode")
		defaults.removeObjectForKey("userServer")
		
		User.Instance.resetInstance()
		DataManager.Instance.resetInstance()
		
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		let secondViewController = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
		self.presentViewController(secondViewController, animated: true, completion: nil)
		
	}
	
	func logout2(){
		
		print("LOGOUT CLICK")
		
		//User.logout()
		
		
		//let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
		//defaults.removeObjectForKey("loginCode")
		//defaults.removeObjectForKey("userServer")
		
		User.Instance.resetInstance()
		DataManager.Instance.resetInstance()
		
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		let secondViewController = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
		self.presentViewController(secondViewController, animated: true, completion: nil)
		
	}
	///
	
	
	func IsPad()->Bool{
		if(	UIDevice.currentDevice().userInterfaceIdiom == .Pad)
		{
			return true
		}
		return false
	}
}

extension String{
	func loc() -> String{
		return NSLocalizedString(self, comment:  "")
	}
	
}


enum UIUserInterfaceIdiom : Int {
	case Unspecified
	
	case Phone // iPhone and iPod touch style UI
	case Pad // iPad style UI
}
























