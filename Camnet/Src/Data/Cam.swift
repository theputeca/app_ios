//
//  Evento.swift
//  Camnet
//
//  Created by giulio piana on 16/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation


class Cam{
	
	
	var id : String
	var name : String
	var description : String
	var mapId : String

	var install : String?
	
	//actionrs
	var status : [String: String ] = [String : String ]()
	/*
		<video>off</video>
		<audio>off</audio>
		<luce>nd</luce>
		<ptz>nd</ptz>
		<external>nd</external>
    
    
    
    <webViewVideo_1 />
    <webViewVideo_4 />
    <webViewVideo_6 />
    <webViewVideo_9 />
    <webViewVideo_16 />
    <video_1>http://root:W225601C@192.168.0.15:80/mjpg/1/video.mjpg?rotation=0&resolution=4CIF&fps=10&compression=10&date=0&clock=0</video_1>
    <video_4>http://root:W225601C@192.168.0.15:80/mjpg/1/video.mjpg?rotation=0&resolution=CIF&fps=3&compression=20&date=0&clock=0</video_4>
    <video_6>http://root:W225601C@192.168.0.15:80/mjpg/1/video.mjpg?rotation=0&resolution=CIF&fps=3&compression=20&date=0&clock=0</video_6>
    <video_9>http://root:W225601C@192.168.0.15:80/mjpg/1/video.mjpg?rotation=0&resolution=CIF&fps=3&compression=20&date=0&clock=0</video_9>
    <video_16>


	*/

	//Action
	/*
	<action>
	<recordingVideoOn></recordingVideoOn>
	<recordingVideoOff></recordingVideoOff>
	
	<recordingAudioOn></recordingAudioOn>
	<recordingAudioOff></recordingAudioOff>
	
	<lightOn></lightOn>
	<lightOff></lightOff>
	
	<external></external>
	
	</action>
	*/
	//url video
	var urlHdVideo : String?
	var urlBigVideo : String?
	var urlSmallVideo : String?
	var urlImg : String?

    //urlVideo new 1.0.1

    var urlVideo_1 : String?
	var urlVideo_4 : String?
    var urlVideo_6 : String?
    var urlVideo_9 : String?
    var urlVideo_16 : String?

//url per web view 

    var webViewVideoMedium : String?
    var webViewVideoBig : String?
    var webViewVideoHd : String?

    var webViewVideoVideo_1 : String?
    var webViewVideoVideo_4 : String?
    var webViewVideoVideo_6 : String?
    var webViewVideoVideo_9 : String?
    var webViewVideoVideo_16 : String?

	//var url Api 
	/*
	view_ptz
	view_map
	
	recordingAudio On/ Off
	recordingVideo On/Off
	externalCommand
	light On/Off
	*/
	var recordingVideoOn : String?
	var  recordingVideoOff : String?
	var recordingAudioOn : String?
	var recordingAudioOff : String?
	var lightOn : String?
	var lightOff : String?
	var viewPtz : String?
	var externalCommand : String?
	
	
	
	init ()
	{
		id = ""
		name = ""
		description = ""
		mapId = ""
		
		urlHdVideo = nil
		urlBigVideo = ""
		urlSmallVideo = ""
		urlImg = ""

        urlVideo_1=""
        urlVideo_4=""
        urlVideo_6=""
        urlVideo_9=""
        urlVideo_16=""
	}
	
	
	init (_id : String , _name : String? , _desc : String? , _mapId : String? )
	{
		id = _id
		
		
		name=""
		if(_name != nil){ name = _name! }
		description = ""
		if(_desc != nil){ description = _desc! }
		
		mapId = "0"
		if(_mapId != nil){	mapId = _mapId! }
		
		
		urlHdVideo = nil
		urlBigVideo = ""
		urlSmallVideo = ""
		urlImg = ""

        urlVideo_1=""
        urlVideo_4=""
        urlVideo_6=""
        urlVideo_9=""
        urlVideo_16=""
		
		recordingVideoOn = nil
		recordingVideoOff = nil
		recordingAudioOn = nil
		 recordingAudioOff = nil
		 lightOn = nil
		 lightOff = nil
		 viewPtz = nil
		 externalCommand = nil

		
	}
	
	
}