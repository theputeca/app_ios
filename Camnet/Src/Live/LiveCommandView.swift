//
//  LiveCommandView.swift
//  Camnet
//
//  Created by giulio piana on 16/02/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation


class LiveCommandView : UIView{
	
	
	var audioBtn : UIButton!
	var videoBtn : UIButton!
	var ptzBtn : UIButton!
	var lightBtn : UIButton!
	var extBtn : UIButton!
	
	var infoBtn : UIButton!
	var storicoBtn : UIButton!
	var mappaBtn : UIButton!
	var volumeBtn : UIButton!
	var zoomBtn : UIButton!
	
	
	
	
	override init(frame: CGRect) {
		
		super.init(frame: frame)
		
		self.backgroundColor =  UIColor.sfondoScuro()
		
		//audio
		audioBtn = UIButton(type: UIButtonType.System) //UIButton()
		
		let image = UIImage(named: "icoAudioRec.png") as UIImage?
		audioBtn.setImage(image, forState: UIControlState.Normal)
		audioBtn.acceso(false)
		self.addSubview(audioBtn)
		
		//video
		videoBtn = UIButton(type: UIButtonType.System) //UIButton()
		
		let imagevideoBtn = UIImage(named: "icoVideo.png") as UIImage?
		videoBtn.setImage(imagevideoBtn, forState: UIControlState.Normal)
		videoBtn.acceso(false)
		self.addSubview(videoBtn)
		//ptz
		ptzBtn = UIButton(type: UIButtonType.System) //UIButton()
		let imageptzBtn = UIImage(named: "icoPtz.png") as UIImage?
		ptzBtn.setImage(imageptzBtn, forState: UIControlState.Normal)
		ptzBtn.acceso(false)
		self.addSubview(ptzBtn)
		
		//light
		lightBtn = UIButton(type: UIButtonType.System) //UIButton()
		
		let imagelightBtn = UIImage(named: "icoLight.png") as UIImage?
		lightBtn.setImage(imagelightBtn, forState: UIControlState.Normal)
		lightBtn.acceso(false)
		self.addSubview(lightBtn)
		//ext
		extBtn = UIButton(type: UIButtonType.System) //UIButton()
		
		let imageextBtn = UIImage(named: "icoFlash.png") as UIImage?
		extBtn.setImage(imageextBtn, forState: UIControlState.Normal)
		extBtn.acceso(false)
		self.addSubview(extBtn)
		
		
		//info
		infoBtn = UIButton(type: UIButtonType.System) //UIButton()
		
		let imageinfoBtn = UIImage(named: "icoInfo.png") as UIImage?
		infoBtn.setImage(imageinfoBtn, forState: UIControlState.Normal)
		infoBtn.acceso(false)
		self.addSubview(infoBtn)
		
		//storico
		storicoBtn = UIButton(type: UIButtonType.System) //UIButton()
		
		let imagestoricoBtn = UIImage(named: "icoBook.png") as UIImage?
		storicoBtn.setImage(imagestoricoBtn, forState: UIControlState.Normal)
		storicoBtn.acceso(false)
		self.addSubview(storicoBtn)
		
		//mappa
		mappaBtn = UIButton(type: UIButtonType.System) //UIButton()
		
		let imagemappaBtn = UIImage(named: "icoLoc.png") as UIImage?
		mappaBtn.setImage(imagemappaBtn, forState: UIControlState.Normal)
		mappaBtn.acceso(false)
		self.addSubview(mappaBtn)
		
		//volume
		volumeBtn = UIButton(type: UIButtonType.System) //UIButton()
		
		let imagevolumeBtn = UIImage(named: "icoAudio.png") as UIImage?
		volumeBtn.setImage(imagevolumeBtn, forState: UIControlState.Normal)
		volumeBtn.acceso(false)
		self.addSubview(volumeBtn)
		
		//zoom
		zoomBtn = UIButton(type: UIButtonType.System) //UIButton()
		
		let imagezoomBtn = UIImage(named: "icoFindO.png") as UIImage?
		zoomBtn.setImage(imagezoomBtn, forState: UIControlState.Normal)
		zoomBtn.acceso(false)
		self.addSubview(zoomBtn)

		rotate()

	}
	
	func IsPad()->Bool{
		if(	UIDevice.currentDevice().userInterfaceIdiom == .Pad)
		{
			return true
		}
		return false
	}
	
	func rotate(){
		if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
			
			
			

			
			//land
			if( IsPad() ){
				audioBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.09 - 25 , width: 50	, height: 50)
				videoBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.18 - 25 , width: 50	, height: 50)
				ptzBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.27 - 25, width: 50	, height: 50)
				lightBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.36 - 25, width: 50	, height: 50)
				extBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.45 - 25, width: 50	, height: 50)
				
				storicoBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.54 - 25 , width: 50	, height: 50)
				mappaBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.63 - 25 , width: 50	, height: 50)
				volumeBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.72 - 25, width: 50	, height: 50)
				infoBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.81 - 25, width: 50	, height: 50)
				zoomBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.90 - 25, width: 50	, height: 50)
			}else{
				
				audioBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.1 - 25 , width: 50	, height: 50)
				videoBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.3 - 25 , width: 50	, height: 50)
				ptzBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.5 - 25, width: 50	, height: 50)
				lightBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.7 - 25, width: 50	, height: 50)
				extBtn.frame = CGRect(x: 8, y:  self.frame.height * 0.9 - 25, width: 50	, height: 50)
				
				storicoBtn.frame = CGRect(x: 66, y:  self.frame.height * 0.1 - 25 , width: 50	, height: 50)
				mappaBtn.frame = CGRect(x: 66, y:  self.frame.height * 0.3 - 25 , width: 50	, height: 50)
				volumeBtn.frame = CGRect(x: 66, y:  self.frame.height * 0.5 - 25, width: 50	, height: 50)
				infoBtn.frame = CGRect(x: 66, y:  self.frame.height * 0.7 - 25, width: 50	, height: 50)
				zoomBtn.frame = CGRect(x: 66, y:  self.frame.height * 0.9 - 25, width: 50	, height: 50)
				
			}
			
			
		}
		else{//Protrait
			if( IsPad() ){
				audioBtn.frame = CGRect(x: self.frame.width * 0.09 - 25, y:  8, width: 50	, height: 50)
				videoBtn.frame = CGRect(x: self.frame.width * 0.18 - 25, y:  8, width: 50	, height: 50)
				ptzBtn.frame = CGRect(x: self.frame.width * 0.27 - 25, y:  8, width: 50	, height: 50)
				lightBtn.frame = CGRect(x: self.frame.width * 0.36 - 25, y:  8, width: 50	, height: 50)
				extBtn.frame = CGRect(x: self.frame.width * 0.45 - 25, y:  8, width: 50	, height: 50)
				
				storicoBtn.frame = CGRect(x: self.frame.width * 0.54 - 25, y:  8, width: 50	, height: 50)
				mappaBtn.frame = CGRect(x: self.frame.width * 0.63 - 25, y:  8, width: 50	, height: 50)
				volumeBtn.frame = CGRect(x: self.frame.width * 0.72 - 25, y:  8, width: 50	, height: 50)
				infoBtn.frame = CGRect(x: self.frame.width * 0.81 - 25, y:  8, width: 50	, height: 50)
				zoomBtn.frame = CGRect(x: self.frame.width * 0.90 - 25, y:  8, width: 50	, height: 50)
			}
				else{
				audioBtn.frame = CGRect(x: self.frame.width * 0.1 - 25, y:  8 , width: 50	, height: 50)
				videoBtn.frame = CGRect(x: self.frame.width * 0.3 - 25, y:  8 , width: 50	, height: 50)
				ptzBtn.frame = CGRect(x: self.frame.width * 0.5 - 25, y:  8 , width: 50	, height: 50)
				lightBtn.frame = CGRect(x: self.frame.width * 0.7 - 25, y:  8 , width: 50	, height: 50)
				extBtn.frame = CGRect(x: self.frame.width * 0.9 - 25, y:  8 , width: 50	, height: 50)
				
				storicoBtn.frame = CGRect(x: self.frame.width * 0.1 - 25, y:  66 , width: 50	, height: 50)
				mappaBtn.frame = CGRect(x: self.frame.width * 0.3 - 25, y:  66, width: 50	, height: 50)
				volumeBtn.frame = CGRect(x: self.frame.width * 0.5 - 25, y:  66, width: 50	, height: 50)
				infoBtn.frame = CGRect(x: self.frame.width * 0.7 - 25, y: 66, width: 50	, height: 50)
				zoomBtn.frame = CGRect(x: self.frame.width * 0.9 - 25, y: 66, width: 50	, height: 50)
			}
		}
		
		if(loadIndAudio != nil){
			loadIndAudio!.center = audioBtn.center
		}		
		if(loadIndVideo != nil){
			loadIndVideo!.center = videoBtn.center
		}

	}
	
	convenience  init () {
		self.init(frame:CGRectZero)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("This class does not support NSCoding")
	}
	
	
	var loadIndAudio :UIActivityIndicatorView? 
	var loadIndVideo :UIActivityIndicatorView?
	
	func addLoadingOnButtonAudio(){
		
		
		//audio
		loadIndAudio  = UIActivityIndicatorView(frame: self.frame)
		loadIndAudio!.center = audioBtn.center
		//loadIndAudio!.hidesWhenStopped = true
		loadIndAudio!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
		loadIndAudio!.color = UIColor.arancio()
	
		self.addSubview(loadIndAudio!)
		loadIndAudio!.startAnimating()
		self.bringSubviewToFront(loadIndAudio!)
		
						
	}
	func addLoadingOnButtonVideo(){
			
		//video
		loadIndVideo  = UIActivityIndicatorView(frame: self.frame)
		loadIndVideo!.center = videoBtn.center
		//loadIndVideo!.hidesWhenStopped = true
		loadIndVideo!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
		loadIndVideo!.color = UIColor.arancio()
		
		self.addSubview(loadIndVideo!)
		loadIndVideo!.startAnimating()
		self.bringSubviewToFront(loadIndVideo!)
		
	}

	func RemoveLoadingOnButton(){
		if(loadIndAudio != nil){
			
			loadIndAudio!.stopAnimating()
			loadIndAudio!.removeFromSuperview()
			loadIndAudio = nil
		}
		
		if(loadIndVideo != nil){
			
			loadIndVideo!.stopAnimating()
			loadIndVideo!.removeFromSuperview()
			loadIndVideo = nil
		}
	}
	
	
	
}