//
//  MainViewController.swift
//  Camnet
//
//  Created by giulio piana on 12/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import UIKit




private var _notificationData : NSDictionary? = nil


private var _Instance : MainViewController? = nil //= EventiVC()


class MainViewController: UITabBarController
{

    class var Instance : MainViewController? {
        return _Instance
    }

	var timer : NSTimer? = nil 
	
	var  internetConnectionReach : Reachability = Reachability.reachabilityForInternetConnection()
	
	var isError : Bool = false

    var mainNavigationController : UINavigationController?

    class var notificationData : NSDictionary? {
        return _notificationData
    }

    class func SetNotificationData  ( dict : NSDictionary? ){
         _notificationData = dict
    }


    class func clearNotData (){
        _notificationData = nil

      //  var appDelegate : AppDelegate? = UIApplication.sharedApplication().delegate as? AppDelegate
      //  appDelegate!.mainController.clearBadge()

    }

	override func viewDidLoad() {

        UIApplication.sharedApplication().idleTimerDisabled = false
        UIApplication.sharedApplication().idleTimerDisabled = true
        
        _Instance = self

        let appDelegate : AppDelegate? = UIApplication.sharedApplication().delegate as? AppDelegate
        appDelegate!.mainController = self;
		
		super.viewDidLoad()
		 timer = NSTimer.scheduledTimerWithTimeInterval( 300 , target: self, selector:  Selector("timerForToken"), userInfo: nil, repeats: true)
		
		self.tabBar.selectedImageTintColor = UIColor(red: 233/255, green: 94/255, blue: 39/255, alpha: 1)
		
		let defCenter = NSNotificationCenter.defaultCenter()
		defCenter.addObserver(self, selector: Selector("reachabilityChanged:"), name: kReachabilityChangedNotification , object: nil)
		
		
		
		internetConnectionReach  = Reachability.reachabilityForInternetConnection()
		
		
		self.internetConnectionReach.reachableBlock = { reachability in
			
			print("RECHIABLE blok")
			
		};
		
		self.internetConnectionReach.unreachableBlock = { reachability in
			
			print("NON RECHIABLE blok")
		}
		
		self.internetConnectionReach.startNotifier()







        NSNotificationCenter.defaultCenter().addObserver(
            self,
            selector: "didbecomeActive:",
            name: UIApplicationDidBecomeActiveNotification,
            object: nil)

        //UIApplicationWillEnterForegroundNotification



	}


    func didbecomeActive(notification: NSNotification)
    {

        print("DIDBECOMEACTIVE")
        timerForToken()

        if (UIApplication.sharedApplication().applicationState == UIApplicationState.Background) {

            print("ATORAAT SU DA BACK")
        }



        if  (_notificationData != nil )// let  notificationDictionary : NSDictionary  = NSUserDefaults.standardUserDefaults().objectForKey("notification") as! NSDictionary?
        {

            //_notificationData = notificationDictionary

            if( _notificationData!.objectForKey("mode" ) as! String == "event"){
                openEventFromNotification()
            }
            else if (_notificationData!.objectForKey("mode") as! String == "news")
            {
                openNewsFromNotification()
            }
            
        }
    }



   


    override func  viewWillAppear(animated: Bool) {

        print("DIDBECOMEACTIVE")
        timerForToken()

        if (UIApplication.sharedApplication().applicationState == UIApplicationState.Background) {

            print("ATORAAT SU DA BACK")
        }



        if  (_notificationData != nil )// let  notificationDictionary : NSDictionary  = NSUserDefaults.standardUserDefaults().objectForKey("notification") as! NSDictionary?
        {
        //_notificationData = notificationDictionary
            if( _notificationData!.objectForKey("mode" ) as! String == "event"){
                openEventFromNotification()
            }
            else if (_notificationData!.objectForKey("mode") as! String == "news")
            {
                openNewsFromNotification()
            }
            
        }
    }


    func openEventFromNotification()
    {

        self.mainNavigationController?.popToRootViewControllerAnimated(false)
        self.selectedIndex = 2

    }

    func openNewsFromNotification()
    {

        self.mainNavigationController?.popToRootViewControllerAnimated(false)
        self.selectedIndex = 4

    }


    var not1 :Int  = 0
    var not2 :Int  = 0
    func openEventFromNotificationIfActive()
    {

        not1++

        let  item :UITabBarItem = self.tabBar.items![2] 
        item.badgeValue = "\(not1)"

        if( self.selectedIndex != 2 ){

            self.mainNavigationController?.popToRootViewControllerAnimated(false)

            if  (_notificationData != nil )// let  notificationDictionary : NSDictionary  = NSUserDefaults.standardUserDefaults().objectForKey("notification") as! NSDictionary?
            {
                if( _notificationData!.objectForKey("mode" ) as! String == "event"){
                    openEventFromNotification()
                }
            }
        }
        else{
            // reload event !

            if(self.IsPad())
            {
                if( EventiVC.Instance != nil)
                {
                    //var  eventiVc : EventiVC = self.selectedViewController as! EventiVC
                    EventiVC.Instance!.reloadEventFromNotification()
                }
            }
            else{
                self.selectedIndex = 0
                self.selectedIndex = 2
            }
            
        }


    }

    func openNewsFromNotificationIfActive()
    {

        not2++
        if( self.selectedIndex != 4 )
        {

            self.mainNavigationController?.popToRootViewControllerAnimated(false)


            let  item :UITabBarItem = self.tabBar.items![4]
            item.badgeValue = "\(not2)"
            self.selectedIndex = 4
        }
        else
        {
            //reload news
            NewsViewController.Instance!.reloadWebViewFroNotification()
        }

        
    }
    

    func clearBadge(){
        tabBarItem.badgeValue = nil
    }



    let taskfoToken:NSURLSessionTask = NSURLSessionTask();

	func timerForToken()
	{
		// /*
		print("CALL FOR TOKEN ")
		
		let url = NSURL(string:  User.Instance.serverUrl + User.keepAliveApiUrl() )//+"&udid=1234")
		
		//println( url )
		
		let request1 = NSMutableURLRequest(URL: url! )
		let session1 = NSURLSession.sharedSession()
		request1.HTTPMethod = "GET"
        request1.timeoutInterval = 30


		let taskfoToken = session1.dataTaskWithRequest(request1, completionHandler: {data, response, error -> Void in
			
			//var error: NSError?
            if((error) != nil){
                print("ERROR CALL FOR TOKEN ")

                return

            }
			if(data == nil )
            {
                print("DATA TOKEN NULL")
                return
            }

			do {				
				let xmlDoc = try AEXMLDocument(xmlData: data!)
				
				print(xmlDoc.xmlString)
				
				if let result =  xmlDoc.root["result"].value as String? {
					//println(result)					
					if (result == "false"){
						
						if( self.timer != nil  )
						{
							if ( self.timer!.valid ) {
								self.timer!.invalidate()
							}							
							self.timer = nil;		
							self.logout2()
						}	

									
					}
					else{
						
					}
				}
			} catch _ {

                 print("ERRORE DATA TOKEN")
			}	

		})

		taskfoToken.resume()
		


      //  task.cancel()

        // */
 	}
	
	
	func reachabilityChanged(note : NSNotification)
	{
		let reach : Reachability = note.object as! Reachability
		
		if (reach == self.internetConnectionReach)
		{
			if(reach.isReachable())
			{
				print("RECHIABLE changed")
				if(isError	)
				{
					isError = false
					//vai a login // ogout
					self.view.viewWithTag(97)?.removeFromSuperview()
					
					/*let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
					defaults.removeObjectForKey("loginCode")
					defaults.removeObjectForKey("userServer")
					
					User.Instance.resetInstance()
					DataManager.Instance.resetInstance()
					
					let storyboard = UIStoryboard(name: "Main", bundle: nil)
					let secondViewController = storyboard.instantiateViewControllerWithIdentifier("LoginViewController") as LoginViewController
					self.presentViewController(secondViewController, animated: true, completion: nil)
					*/
				}
			}
			else
			{
				print("NON RECHIABLE changed")
				if(!isError)
				{
					isError = true
					let storyboard = UIStoryboard(name: "Main", bundle: nil)
					let loadingViewVC = storyboard.instantiateViewControllerWithIdentifier("ErrorScreenVC") as! ErrorScreenVC
					loadingViewVC.view.tag = 97
					self.view.addSubview(loadingViewVC.view)
					self.view.bringSubviewToFront(loadingViewVC.view)
				}
			}
		}
		
	}


}