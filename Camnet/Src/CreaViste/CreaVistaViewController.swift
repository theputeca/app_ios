//
//  CreaVistaViewController.swift
//  Camnet
//
//  Created by giulio piana on 15/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation
import UIKit

class CreaVistaViewController: UIViewController   ,UICollectionViewDelegate,UICollectionViewDataSource,callBackFromSelectionProtocol,SaveViewProtocol,UIGestureRecognizerDelegate
{
	
	
	
	var theCollectionView: UICollectionView!
	var BarraLaterale: UIView!
	
	//@IBOutlet weak var theCollectionView2: UICollectionView!
	//@IBOutlet weak var BarraLaterale2: UIView!
	
	@IBOutlet weak var saveBtn: UIButton!
	
	
	
	var cells = [UICollectionViewCell] ()
	var selectedIndexPathCell : NSIndexPath?
	
	var selectedCam = [Cam?] ()
	
	var tc :[Cam?] = [nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]

    var imagesCache :[UIImage?] = [nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil]
	
	let kBgQueue  = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
	//let kBgQueue3  = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
	//var imageCahce : [Int : UIImage] = [Int : UIImage]()
	
	//edit
	var selectedVista : Vista?
	var isEdit : Bool = false
	
	///dragged view (over cells)
	var draggingView : UIImageView?
	
	///the point we first clicked
	var  dragViewStartLocation : CGPoint?
	
	///the indexpath for the first item to move
	var startIndex : NSIndexPath = NSIndexPath()
	var moveToIndexPath : NSIndexPath = NSIndexPath()
	var numbers : NSMutableArray = []
	
	var dimBarra : CGFloat = 81
	var dimBarraIphone : CGFloat = 61

    var isSaving:Bool = false;
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		//self.alertMessage("", message: "creaVistaMessage".loc(), button: "OK")
		// Do any additional setup after loading the view, typically from a nib.
		//nasconde la tab bar
		
		self.tabBarController?.tabBar.hidden=true
		
		self.automaticallyAdjustsScrollViewInsets = false;
		
		self.navigationController?.interactivePopGestureRecognizer!.enabled = false;
		self.navigationController?.interactivePopGestureRecognizer!.delegate = self
		
		//localization
		self.title = NSLocalizedString("creaVista", comment:  "")
		saveBtn.setTitle(NSLocalizedString("salva", comment:  ""), forState: UIControlState.Normal)
		saveBtn.enabled = false
		saveBtn.alpha = 0.5
		saveBtn.tintColor = UIColor.sfondoPlayer()
		saveBtn.setTitleColor(UIColor.sfondoPlayer() , forState: UIControlState.Normal)
		
		//create che collection view
		let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
		layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		layout.minimumInteritemSpacing = 2 // 1.0
		layout.minimumLineSpacing = 2 //1.0
		
		
		if(self.IsPad())
		{
			theCollectionView = UICollectionView(frame: CGRect(x: 0 , y: self.frameBetweenBar().origin.y ,width: (self.view.frame.width - dimBarra)	, height: self.frameBetweenBar().height ), collectionViewLayout: layout)
		}
		else{
			theCollectionView = UICollectionView(frame: CGRect(x: 0 , y: self.frameBetweenBar().origin.y , width: self.view.frame.width 	, height: self.frameBetweenBar().height - 80 ), collectionViewLayout: layout)
		}
		
		theCollectionView.backgroundColor = UIColor.sfondoChiaro()
		self.view.addSubview(theCollectionView)
		self.theCollectionView.dataSource = self;
		self.theCollectionView.delegate = self;
		theCollectionView.scrollEnabled=false;
		
		
		var nib = UINib(nibName: "CollCellCreteVista", bundle: nil)
		theCollectionView.registerNib(nib, forCellWithReuseIdentifier: "Cell")

		
		//CreaBarra LAterale
		if(self.IsPad())
		{
			BarraLaterale = NSBundle.mainBundle().loadNibNamed("BarraComandiCrationVista", owner: self, options: nil)[0] as? UIView
			BarraLaterale.frame = CGRect(x: self.view.frame.width - dimBarra , y: self.frameBetweenBar().origin.y ,width: dimBarra , height: self.frameBetweenBar().height)
			
			
		}
		else{
			BarraLaterale = NSBundle.mainBundle().loadNibNamed("BarraComandiCrationVistaIphone", owner: self, options: nil)[0] as? UIView
			BarraLaterale.frame = CGRect(x: 0  , y: self.view.frame.height - 80 ,width: self.view.frame.width , height: 80)
			
			let border : CALayer = CALayer()
			border.borderColor = UIColor.sfondoChiaro().CGColor
			border.frame = CGRect(x: 0, y: 0, width:  BarraLaterale.frame.size.width , height: 2 )
			border.borderWidth = 2
			BarraLaterale.layer.addSublayer(border)
			
		}
		self.view.addSubview(BarraLaterale)
		self.view.bringSubviewToFront(BarraLaterale)
		
		
		let btn4 : UIButton = BarraLaterale.viewWithTag(1) as! UIButton
		btn4.addTarget(self, action: Selector("bnt4click:") , forControlEvents: UIControlEvents.TouchUpInside)
		let btn6 : UIButton = BarraLaterale.viewWithTag(2) as! UIButton
		btn6.addTarget(self, action: Selector("bnt6click:") , forControlEvents: UIControlEvents.TouchUpInside)
		let btn9 : UIButton = BarraLaterale.viewWithTag(3) as! UIButton
		btn9.addTarget(self, action: Selector("bnt9click:") , forControlEvents: UIControlEvents.TouchUpInside)
		let btn16 : UIButton = BarraLaterale.viewWithTag(4) as! UIButton
		btn16.addTarget(self, action: Selector("bnt16click:") , forControlEvents: UIControlEvents.TouchUpInside)
		
		
		
		btn4.frame = CGRect(x: 0, y: 0, width: 100, height: 100);
		
		
		// crea datasource
		self.numbers = [0,1,2,3]
		selectedCam = [nil,nil,nil,nil]
		
		if(isEdit)
		{
			saveBtn.enabled = true
			saveBtn.alpha = 1
			saveBtn.tintColor = UIColor.arancio()
			saveBtn.setTitleColor(UIColor.arancio() , forState: UIControlState.Normal)
			
			
			let cams = [Cam ] (selectedVista!.content.values)
			self.numbers = []
			selectedCam = []
			
			let quante =  Int(selectedVista!.size)
			var i = 0
			for (i = 0; i < quante ; i++)
			{
				self.numbers.addObject( i )
				
				
				
				if (cams.count > i  )
				{
					let str : String = selectedVista!.contentIds[i]
					let cam : Cam = selectedVista!.content[ str ]!
					
					selectedCam.append(cam)//cams[i])
					tc[i] =  cam //cams[i]
				}
				else{
					selectedCam.append(nil)
					tc[i] = nil
				}
				
			}
			
		}
		
	}
	
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        isSaving = false;
		self.removeLoading()
		
		if (self.IsPad()){
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				theCollectionView.frame = CGRect(x: 0 , y: self.frameBetweenBar().origin.y  ,width: self.view.frame.width - dimBarra	, height:self.frameBetweenBar().height)
				self.BarraLaterale?.frame = CGRect(x: self.view.frame.width - dimBarra	 , y: self.frameBetweenBar().origin.y  , width: dimBarra , height: self.view.frame.height)
				rotate(true)
			}
				//protrait
				
			else{
				theCollectionView.frame = CGRect(x: 0 , y: self.frameBetweenBar().origin.y , width: self.view.frame.width	, height: self.frameBetweenBar().height - dimBarra - 30 )
				self.BarraLaterale?.frame = CGRect(x: 0 /*self.view.frame.height - dimBarra*/	 , y:  self.frameBetweenBar().height - dimBarra   , width: self.view.frame.height , height:dimBarra + 30 )
				
				rotate(false)
				
			}
			
			theCollectionView.reloadData()
		}
		else{//IPHONE
			
			if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
				
				//theCollectionView.frame =  CGRect(x: 0 , y: self.frameBetweenBar().origin.y , width: self.view.frame.width - dimBarraIphone 	, height: self.frameBetweenBar().height  )
				//BarraLaterale.frame = CGRect(x:  self.view.frame.width - dimBarra  , y: self.frameBetweenBar().origin.y  ,width: dimBarra , height: self.view.frame.height)
				
				theCollectionView.frame =  CGRect(x: 0 , y: self.frameBetweenBar().origin.y , width: self.view.frame.width 	, height: self.frameBetweenBar().height - 65 )
				BarraLaterale.frame = CGRect(x: 0  , y: self.view.frame.height - 65 ,width: self.view.frame.width , height: 65)
				
				//rotate(false)
			}
			else{
				theCollectionView.frame =  CGRect(x: 0 , y: self.frameBetweenBar().origin.y , width: self.view.frame.width 	, height: self.frameBetweenBar().height - 80 )
				BarraLaterale.frame = CGRect(x: 0  , y: self.view.frame.height - 80 ,width: self.view.frame.width , height: 80)
				
				//	rotate(false)
			}
			
			theCollectionView.reloadData()
		}
	}
	
	override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		
		if (self.IsPad()){
			if (  toInterfaceOrientation.isPortrait		){
				theCollectionView.frame = CGRect(x: 0 , y: self.frameBetweenBar().origin.y , width: self.view.frame.height	, height: self.frameBetweenBar().width - dimBarra - 30 - self.frameBetweenBar().origin.y )
				self.BarraLaterale?.frame = CGRect(x: 0 /*self.view.frame.height - dimBarra*/	 , y:  self.frameBetweenBar().width - dimBarra - 30  , width: self.view.frame.width , height:dimBarra + 30 )
				rotate(false)
			}
			else{
				theCollectionView.frame = CGRect(x: 0 , y: self.frameBetweenBar().origin.y  ,width: self.view.frame.height - dimBarra	, height:self.frameBetweenBar().width - self.frameBetweenBar().origin.y)
				self.BarraLaterale?.frame = CGRect(x: self.view.frame.height - dimBarra	 , y: self.frameBetweenBar().origin.y  ,width: dimBarra , height:self.frameBetweenBar().width)
				rotate(true)
			}
			
			theCollectionView.reloadData()
		}
		
	}
	
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
		if (self.IsPad()){
			
		}
		else{//IPHONE
			if (  fromInterfaceOrientation.isPortrait		){
				theCollectionView.frame = CGRect(x: 0 , y: self.frameBetweenBar().origin.y , width: self.view.frame.width 	, height: self.frameBetweenBar().height - 65 )
				BarraLaterale.frame = CGRect(x: 0  , y: self.view.frame.height - 65 ,width: self.view.frame.width , height: 65)
			}
			else{
				theCollectionView.frame =  CGRect(x: 0 , y: self.frameBetweenBar().origin.y , width: self.view.frame.width 	, height: self.frameBetweenBar().height - 80 )
				BarraLaterale.frame = CGRect(x: 0  , y: self.view.frame.height - 80 ,width: self.view.frame.width , height: 80)
			}
			
			theCollectionView.reloadData()
		}
	}
	
	
	
	func rotate(isLandscape : Bool ){
		
		let btn4 : UIButton = BarraLaterale.viewWithTag(1) as! UIButton
		let btn6 : UIButton = BarraLaterale.viewWithTag(2) as! UIButton
		let btn9 : UIButton = BarraLaterale.viewWithTag(3) as! UIButton
		let btn16 : UIButton = BarraLaterale.viewWithTag(4) as! UIButton
		
		let lbl4 : UILabel = BarraLaterale.viewWithTag(5) as! UILabel
		let lbl6 : UILabel = BarraLaterale.viewWithTag(6) as! UILabel
		let lbl9 : UILabel = BarraLaterale.viewWithTag(7) as! UILabel
		let lbl16 : UILabel = BarraLaterale.viewWithTag(8) as! UILabel
		
		if ( isLandscape ){
			
			btn4.frame = CGRect(x: 8, y:  BarraLaterale.frame.height * 0.1 - 0 , width: 65	, height: 65)
			btn6.frame = CGRect(x: 8, y:  BarraLaterale.frame.height * 0.3 - 0 , width: 65	, height: 65)
			btn9.frame = CGRect(x: 8, y:  BarraLaterale.frame.height * 0.5 - 0 , width: 65	, height: 65)
			btn16.frame = CGRect(x: 8, y:  BarraLaterale.frame.height * 0.7 - 0 , width: 65 , height: 65)
			
			lbl4.frame	 =  CGRect(x: btn4.frame.origin.x , y: btn4.frame.origin.y + btn4.frame.height , width:  btn4.frame.width	, height: 30 )
			lbl6.frame	 =  CGRect(x: btn6.frame.origin.x , y: btn6.frame.origin.y + btn6.frame.height , width:  btn6.frame.width	, height: 30 )
			lbl9.frame	 =  CGRect(x: btn9.frame.origin.x , y: btn9.frame.origin.y + btn9.frame.height , width:  btn9.frame.width	, height: 30 )
			lbl16.frame	 =  CGRect(x: btn16.frame.origin.x , y: btn16.frame.origin.y + btn16.frame.height , width:  btn16.frame.width	, height: 30 )
			
		}
		else{
			btn4.frame = CGRect(x: BarraLaterale.frame.width * 0.1 - 65, y:  8, width: 65	, height: 65)
			btn6.frame = CGRect(x: BarraLaterale.frame.width * 0.3 - 65, y:  8, width: 65	, height: 65)
			btn9.frame = CGRect(x: BarraLaterale.frame.width * 0.5 - 65, y:  8, width: 65	, height: 65)
			btn16.frame = CGRect(x: BarraLaterale.frame.width * 0.7 - 65, y:  8, width: 65	, height: 65)
			
			lbl4.frame	 =  CGRect(x: btn4.frame.origin.x , y: btn4.frame.origin.y + btn4.frame.height , width:  btn4.frame.width	, height: 30 )
			lbl6.frame	 =  CGRect(x: btn6.frame.origin.x , y: btn6.frame.origin.y + btn6.frame.height , width:  btn6.frame.width	, height: 30 )
			lbl9.frame	 =  CGRect(x: btn9.frame.origin.x , y: btn9.frame.origin.y + btn9.frame.height , width:  btn9.frame.width	, height: 30 )
			lbl16.frame	 =  CGRect(x: btn16.frame.origin.x , y: btn16.frame.origin.y + btn16.frame.height , width:  btn16.frame.width	, height: 30 )
			
		}
	}
	
	//////
	
	@IBAction func LongPressGesture(sender: UILongPressGestureRecognizer) {
		
        if(isSaving){
            return  ;
        }
		
		var loc  : CGPoint = sender.locationInView(self.theCollectionView) as CGPoint
		
		let loc44  : CGPoint = CGPoint(x: loc.x, y: loc.y + 44)
		if ( !CGRectContainsPoint(self.theCollectionView.frame, loc44))
		{
			return
		}
		
		
		let heightInSection : CGFloat = fmod((loc.y - self.theCollectionView.contentOffset.y), CGRectGetHeight(self.theCollectionView.frame))
		
		let locInScreen : CGPoint =  CGPointMake((loc.x - self.theCollectionView.contentOffset.x), heightInSection )
		
		if ( sender.state == UIGestureRecognizerState.Began ) {
			self.startIndex = self.theCollectionView.indexPathForItemAtPoint(loc)!
			
			if (self.startIndex != NSIndexPath()) {
				
				let cell : SDraggingCell  = self.theCollectionView.cellForItemAtIndexPath(self.startIndex) as! SDraggingCell
				self.draggingView = UIImageView(image: cell.getRasterizedImageCopy());
				
				cell.contentView.alpha = 0.0
				self.view.addSubview(self.draggingView!)
				self.draggingView!.center = locInScreen;
				self.dragViewStartLocation = self.draggingView!.center;
				self.view.bringSubviewToFront(self.draggingView!)
				
				UIView.animateWithDuration(0.4, animations: { () -> Void in
					let transform = CGAffineTransformMakeScale(1.2, 1.2);
					self.draggingView!.transform = transform;
					}, completion: nil)
				
				
			}
		}
		
		if (sender.state == UIGestureRecognizerState.Changed ) {
			self.draggingView!.center = locInScreen;
		}
		
		if (sender.state == UIGestureRecognizerState.Ended) {
			if let a = self.draggingView  {
				if let b = self.theCollectionView.indexPathForItemAtPoint(loc){
					
					self.moveToIndexPath = self.theCollectionView.indexPathForItemAtPoint(loc)!
					//update date source
					let thisNumber : NSNumber  = self.numbers.objectAtIndex( self.startIndex.row  ) as! NSNumber
					self.numbers.removeObjectAtIndex(self.startIndex.row)
					
					if (self.moveToIndexPath.row < self.startIndex.row) {
						self.numbers.insertObject(thisNumber , atIndex:self.moveToIndexPath.row);
					} else {
						self.numbers.insertObject(thisNumber , atIndex:self.moveToIndexPath.row);
					}
					
					//sposta le cam negli array di selezione
					let cam = selectedCam [ self.startIndex.row ]
					selectedCam [ self.startIndex.row ] = selectedCam [ self.moveToIndexPath.row ]
					selectedCam [ self.moveToIndexPath.row ] = cam
					
					
					
					UIView.animateWithDuration(0.4, animations: { () -> Void in
						self.draggingView!.transform = CGAffineTransformIdentity
						},completion: { (asd) -> Void in
							
							//change items
							let weakSelf = self;
							self.theCollectionView.performBatchUpdates({ () -> Void in
								
								self.theCollectionView.moveItemAtIndexPath(self.startIndex , toIndexPath:self.moveToIndexPath);
								self.theCollectionView.moveItemAtIndexPath(self.moveToIndexPath , toIndexPath:self.startIndex );
								
								}, completion: { (finished) -> Void in
									let  movedCell : SDraggingCell = self.theCollectionView.cellForItemAtIndexPath(self.moveToIndexPath) as! SDraggingCell;
									movedCell.contentView.alpha = 1.0
									
									//cambio index del button cancella
									let btn :UIButtonVisteSelection = movedCell.viewWithTag(3) as! UIButtonVisteSelection
									btn.indexPath = self.moveToIndexPath
									
									
									let  oldIndexCell : SDraggingCell = self.theCollectionView.cellForItemAtIndexPath(self.startIndex) as! SDraggingCell
									oldIndexCell.contentView.alpha = 1.0;
									
									let btn2 :UIButtonVisteSelection = oldIndexCell.viewWithTag(3) as! UIButtonVisteSelection
									btn2.indexPath = self.startIndex
									
									
							})
							
							self.draggingView!.removeFromSuperview()
					})
				}
				else	{	// return the item in inittial position
					
					UIView.animateWithDuration(0.4, animations: { () -> Void in
						self.draggingView!.transform = CGAffineTransformIdentity;
						}, completion: { (finished) -> Void in
							
							let cell : SDraggingCell = self.theCollectionView.cellForItemAtIndexPath(self.startIndex) as! SDraggingCell
							cell.contentView.alpha = 1.0
							self.draggingView!.removeFromSuperview()
							
					})
				}
				loc = CGPointZero;
			}
		}
	}
	
	
	//mark - UICollectionViewDataSource
	
	func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.numbers.count
	}
	
	func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		

		var cell :SDraggingCell!  = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as? SDraggingCell
		
		//set the  size of the cell
		let w = theCollectionView.frame.width
		let h = theCollectionView.frame.height
		
		if (self.numbers.count == 4){
			cell.frame = CGRect( x:cell.frame.origin.x  , y: cell.frame.origin.y , width:  (w / 2) - 1  , height: (h / 2 ) - 1   );
		}else if (self.numbers.count == 6){
			cell.frame = CGRect( x:cell.frame.origin.x  , y: cell.frame.origin.y , width:  (w / 3)  - 2 , height: (h / 2 ) - 1 );
		}else if (self.numbers.count == 9){
			cell.frame = CGRect( x:cell.frame.origin.x  , y: cell.frame.origin.y , width:  (w / 3)   - 2 , height: (h / 3 ) - 2  );
		}else if (self.numbers.count == 16){
			cell.frame = CGRect( x:cell.frame.origin.x  , y: cell.frame.origin.y , width:  (w / 4)   - 2 , height: (h / 4 )  - 2   );
		}
		
		//set the parameter of the cel
		let titleLbl : UILabel = cell.viewWithTag(1) as! UILabel
		let imageView : UIImageView = cell.viewWithTag(2) as! UIImageView
		let btnX : UIButton	= cell!.viewWithTag(3) as! UIButton
		let addLabel : UILabel = cell.viewWithTag(4) as! UILabel
		
		let imageCam : UIImageView = cell!.viewWithTag(5) as! UIImageView
		
		
		
		titleLbl.frame = CGRect(x: 0 /*titleLbl.frame.origin.x*/ , y: titleLbl.frame.origin.y, width: titleLbl.frame.width , height: titleLbl.frame.height)
		btnX.frame = CGRect(x: cell!.frame.width - btnX.frame.width  /*titleLbl.frame.origin.x*/ , y: btnX.frame.origin.y, width: btnX.frame.width , height: btnX.frame.height)
		
		titleLbl.numberOfLines = 1
		//titleLbl.lineBreakMode	 = NSLineBreakMode.ByCharWrapping
		titleLbl.adjustsFontSizeToFitWidth = true
		
		if let cam = selectedCam [indexPath.row]  {   // ce la cam
			
			titleLbl.text = cam.name
			titleLbl.alpha = 1
			btnX.alpha  = 1
			addLabel.alpha = 0
			imageView.alpha = 0
			imageCam.alpha = 1
			
			
			let imageView : UIImageView = cell.viewWithTag(2) as! UIImageView
		
			if let image = UIImage(named: "ITEM-3.png") {
				imageView.image = image
			}


            if (cam.urlImg != nil){
                if let img = DataManager.Instance.imgCache[ cam.id ]
                {
                    imageCam.image = img
                }
                else{
                let url = NSURL(string: cam.urlImg! )
                var timeoutInterval: NSTimeInterval = 5
                var chachepol : NSURLRequestCachePolicy = NSURLRequestCachePolicy.UseProtocolCachePolicy
                let request = NSURLRequest(URL: url!, cachePolicy: chachepol, timeoutInterval: timeoutInterval)

                NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in

                    if (data != nil){
                        if let image = UIImage(data: data!){

                            if let updateCell : UICollectionViewCell = self.theCollectionView.cellForItemAtIndexPath(indexPath) { //  tableView cellForRowAtIndexPath:indexPath];
                                (updateCell.viewWithTag(5) as! UIImageView).image = image;
                                DataManager.Instance.imgCache[ cam.id ] = image
                            }
                        }
                    }
            }
            }
            }


			
			imageCam.frame = CGRect(x: 0 , y: titleLbl.frame.height + 8, width: cell!.frame.width , height: cell!.frame.height - titleLbl.frame.height )
		}
		else		{
			
			titleLbl.alpha = 0
			btnX.alpha  = 0
			addLabel.alpha = 1
			addLabel.text = "aggiungiCam".loc()
			
			addLabel.numberOfLines = 2
			addLabel.lineBreakMode	 = NSLineBreakMode.ByWordWrapping
			addLabel.adjustsFontSizeToFitWidth = true
			
			//addLabel.frame = CGRect(x: addLabel.frame.origin.x , y: addLabel.frame.origin.y, width: 50 , height: addLabel.frame.height)
			
			if (self.numbers.count == 16){
				//	addLabel.alpha = 0
			}
			
			imageView.alpha = 1
			imageCam.alpha = 0
			//image
			let bundle = NSBundle.mainBundle()
			if let image = UIImage(named: "+.png") {
				imageView.image = image
			}
			
			
		}
		
		//
		
		let btn :UIButtonVisteSelection = cell.viewWithTag(3) as! UIButtonVisteSelection
		btn.indexPath = indexPath
		btn.addTarget(self, action: "removeCam:", forControlEvents: .TouchUpInside)
		
		return cell;
	}
	
	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
		
		let w = theCollectionView.frame.width
		let h = theCollectionView.frame.height
		
		if (self.numbers.count == 4){
			return CGSizeMake(w * 0.5  - 1  , h *  0.5  - 1)
			
		}else if (self.numbers.count == 6){
			return CGSizeMake(w * (1 / 3)   - 2 , h *  0.5   - 1)
		}else if (self.numbers.count == 9){
			return CGSizeMake(w  * (1 / 3)  - 2  , h *  (1 / 3)   - 2)
		}else if (self.numbers.count == 16){
			return CGSizeMake(w  * 0.25    - 2, h *  0.25  - 2 )
		}
		return CGSizeMake(w  * 0.5  , h *  0.5)
	}
	
	// collection view delegate
	
	func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
		
		print(indexPath.row )
		
		selectedIndexPathCell = indexPath
		
		//theCollectionView
		
		let selectionViewContorller = self.storyboard?.instantiateViewControllerWithIdentifier("selectionCreateVC") as!  SelectionCreateVC
		selectionViewContorller.delegate = self
		
		if(isEdit){
			selectionViewContorller.isModifica =  isEdit
		}
		
		navigationController?.pushViewController(selectionViewContorller , animated: true)  //showViewController(selectionViewContorller, sender: nil)
	}
	
	// iboutlett button
	
	@IBAction func bnt4click(sender: UIButton) {
		self.numbers = [0,1,2,3]
		selectedCam = [tc[0],tc[1],tc[2],tc[3]]
		
		self.theCollectionView.reloadData()
	}
	
	@IBAction func bnt6click(sender: UIButton) {
		self.numbers = [0,1,2,3,4,5]
		selectedCam = [tc[0],tc[1],tc[2],tc[3],tc[4],tc[5]]
		
		self.theCollectionView.reloadData()
	}
	
	@IBAction func bnt9click(sender: UIButton) {
		self.numbers = [0,1,2,3,4,5,6,7,8]
		selectedCam = [tc[0],tc[1],tc[2],tc[3],tc[4],tc[5],tc[6],tc[7],tc[8]]
		
		self.theCollectionView.reloadData()
	}
	
	@IBAction func bnt16click(sender: UIButton) {
		self.numbers = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
		selectedCam = [tc[0],tc[1],tc[2],tc[3],tc[4],tc[5],tc[6],tc[7],tc[8],tc[9],tc[10],tc[11],tc[12],tc[13],tc[14],tc[15]]
		
		self.theCollectionView.reloadData()
	}
	
	@IBAction func btnSalvaClick(sender: UIButton) {
		print("salvaView")

        isSaving = true;
		
		if( self.view.viewWithTag(21) == nil  )
		{
			
			let loadingViewVC = self.storyboard?.instantiateViewControllerWithIdentifier("SalvaVistaVC") as UIViewController?
			let salvavistaview = loadingViewVC!.view as! SalvaVistaView
			salvavistaview.frame = self.view.frame
			
			salvavistaview.currentViewAttr = creaJsonAttr ()
			salvavistaview.currentInstallId = getInstallId ()
			if(isEdit)
			{
				salvavistaview.currentViewId = selectedVista!.id
				salvavistaview.currentViewName = selectedVista!.name
			}
			else{
				salvavistaview.currentViewId = nil
				salvavistaview.currentViewName = nil
			}
			
			salvavistaview.viewController = self
			salvavistaview.delegate = self
			salvavistaview.tag = 21
			self.view.addSubview(salvavistaview)
			self.view.bringSubviewToFront(salvavistaview)
		}
		
	}
	
	func creaJsonAttr () -> String {
		
		let viewmode = self.numbers.count
		var tempAttr = ""  //= "{\"view_name\":\" <viewName> \",\"view_mode\":\"\(viewmode)\",\"view_details\":\"vista personalizzata\",\"hide_to_operator\":\"0\",\"map_id\":\"\",\"view_prop\":\"0\",\"cams\":{" //\"1\":\"10632\", \"2\":\"10633\"   }}"
		
		tempAttr = "{\"view_name\":\" <viewName> \",\"view_mode\":\"\(viewmode)\",\"view_details\":\"vista personalizzata\",\"hide_to_operator\":\"0\",\"map_id\":\"\",\"view_prop\":\"0\",\"cams\":{" //\"1\":\"10632\", \"2\":\"10633\"   }}"
		
		
		var tempCamString = ""
		var camnum = 0
		for c:Cam? in selectedCam {  // creo al lista delle cam
			if (c != nil)
			{
				tempCamString = tempCamString + "\"\(camnum)\" : \" \(c!.id) \" ,"
				camnum++
			}
		}
		tempCamString = tempCamString.substringToIndex( tempCamString.endIndex.predecessor() )   //tolgo la virgola in fondo
		
		tempAttr = tempAttr + tempCamString + "}}"
		
		return tempAttr
	}
	
	var tempInstallId = ""
	func getInstallId () ->String {
		tempInstallId = ""
		for (var i = 0  ; i < selectedCam.count ; i++)
		{
			if (selectedCam[i] != nil)
			{
				if( tempInstallId == "" ) {tempInstallId = selectedCam[i]!.install!}  // assegna lid dell impianto
				if ( selectedCam[i]!.install! != tempInstallId  ) {tempInstallId = "0"}  // se le cam hanno impianti diversi mette multiimpianto = = 0
			}
		}
		return tempInstallId
	}
	
	
	
	
	/// delegateBack from selection
	
	func callBackFromSelection(cam: Cam) {
		//set the camera
		//imageCahce  = [Int : UIImage]()
		
		
		print(cam.name)
		
		saveBtn.enabled = true
		saveBtn.alpha = 1
		saveBtn.tintColor = UIColor.arancio()
		saveBtn.setTitleColor(UIColor.arancio() , forState: UIControlState.Normal)
		
		
		selectedCam [selectedIndexPathCell!.row] = cam
		tc [selectedIndexPathCell!.row] = cam
		
	}
	
	func removeCam(sender:UIButtonVisteSelection!)	{
		
		selectedCam [sender.indexPath!.row] = nil
		tc [sender.indexPath!.row] = nil
		
		//attiva save button se ce piu di una cam selezionata
		saveBtn.enabled = false
		saveBtn.alpha = 0.5
		saveBtn.tintColor = UIColor.sfondoPlayer()
		saveBtn.setTitleColor(UIColor.sfondoPlayer() , forState: UIControlState.Normal)
		
		
		for (var i = 0 ; i < selectedCam.count ; i++)
		{
			if (selectedCam[i] != nil ){
				saveBtn.enabled = true
				saveBtn.alpha = 1
				saveBtn.tintColor = UIColor.arancio()
				saveBtn.setTitleColor(UIColor.arancio() , forState: UIControlState.Normal)
				
			}
		}
		
		//resetta la cell senza cam
		let cell = theCollectionView.cellForItemAtIndexPath( sender.indexPath! ) as UICollectionViewCell?
		let titleLbl : UILabel = cell!.viewWithTag(1) as! UILabel
		titleLbl.text = "Add a cam"
		titleLbl.alpha 	= 0
		
		let btnX : UIButton	= cell!.viewWithTag(3) as! UIButton
		btnX.alpha  = 0
		
		let addLabel : UILabel = cell!.viewWithTag(4) as! UILabel
		addLabel.alpha = 1
		
		addLabel.text = "aggiungiCam".loc()
		
		addLabel.numberOfLines = 2
		addLabel.lineBreakMode	 = NSLineBreakMode.ByWordWrapping
		addLabel.adjustsFontSizeToFitWidth = true
		
		
		let imageView : UIImageView = cell!.viewWithTag(2) as! UIImageView
		let imageCam : UIImageView = cell!.viewWithTag(5) as! UIImageView
		
		imageView.alpha = 1
		imageCam.alpha = 0
		
		
		let bundle = NSBundle.mainBundle()
		let path : String? = bundle.pathForResource("+", ofType: "png")
		if let image = UIImage(named: "+.png") {
			imageView.image = image
		}
		
	}
	
	//Delegate SAVE VIEW
	
	
	
	func dataReloaded()
	{
		self.removeLoading()
		let vc = self.navigationController!.viewControllers [0] 
		self.navigationController?.popToViewController(  vc , animated: true)
	}


    func callBackSaveCancel ()
    {
        isSaving = false;
    }

    func callBackSave(vistaName :String  , vistaid : String) {
		  isSaving = false;

		if(isEdit)
		{
			self.removeLoading()
			
			selectedVista!.name = vistaName
			
			selectedVista!.size = "\(self.numbers.count)"
			
			selectedVista!.content = [String: Cam] ()
			selectedVista!.contentIds = [String]()
			
			var i  : Int = 0 ;
			for cam : Cam? in selectedCam
			{
				if (cam != nil)
				{
					//selectedVista!.content[cam!.id] = cam
					
					if let c = selectedVista!.content[cam!.id] {
						selectedVista!.content[ cam!.id + "\(i)" ] = cam
					}
					else{
						selectedVista!.content[cam!.id] = cam
					}
					
					i++
					
					selectedVista!.contentIds.append( cam!.id)
				}
			}
			
			let vc = self.navigationController!.viewControllers [0] 
			self.navigationController?.popToViewController(  vc , animated: true)
			
			
			
		}
		else {
			
			let randNum = Int(arc4random_uniform(10000))

            var id_ : String = "\(randNum)"
            if( vistaid != "0"){
                id_ = vistaid
            }

			let vista = Vista(_id: id_ , _name: vistaName , _desc: "", _type: "user", _size: "\(selectedCam.count)" )
			//creo la visa da mettere nei miei dati locali
			
			var i  : Int = 0 ;
			for cam : Cam? in selectedCam
			{
				if (cam != nil)
				{
					//vista.content[cam!.id] = cam
					
					if let c = vista.content[cam!.id] {
						vista.content[ cam!.id + "\(i)" ] = cam
					}
					else{
						vista.content[cam!.id] = cam
					}
					
					i++
					
					vista.contentIds.append( cam!.id)
				}
			}
			
			//FIX IT BUG imianto sbagliato
			
			for imp in DataManager.Instance.impianti
			{
				if (imp.id == tempInstallId	)
				{

                    vista.install = imp.id
					imp.views_user[ vista.id ] = vista


					DataManager.Instance.allUserView [ vista.id ] = vista
					
					let vc = self.navigationController!.viewControllers [0] 
					self.navigationController?.popToViewController(  vc , animated: true)
					
					return
				}
			}
			
			//se nn trova l,impianto lo mette nello 0
			let imp = DataManager.Instance.impianti[ 0 ]
             vista.install = imp.id
			imp.views_user[ vista.id ] = vista
			DataManager.Instance.allUserView [ vista.id ] = vista


			let vc = self.navigationController!.viewControllers [0] 
			self.navigationController?.popToViewController(  vc , animated: true)
			
		}
	}
	
}
























