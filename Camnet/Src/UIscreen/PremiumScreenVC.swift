//
//  LoadingScreenVC.swift
//  Camnet
//
//  Created by giulio piana on 27/01/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation
class PremiumScreenVC: UIViewController {
	
	
	
	@IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var descLbl: UILabel!
	
	@IBOutlet weak var serverBtn: UIButton!


    var isStoredVideo : Bool = false

	override func viewDidLoad() {
		super.viewDidLoad()
		
		//loginBtn.layer.frame = CGRect(x: loginBtn.layer.frame.origin.x , y: loginBtn.layer.frame.origin.y, width: loginBtn.layer.frame.width, height: 80)
		serverBtn.layer.masksToBounds = true;
		serverBtn.layer.cornerRadius = 23;
		
		serverBtn.setTitle("contattaci".loc(), forState: UIControlState.Normal)
		//localize lbl and button
		
		titleLbl.text = "serverAbTitle".loc()
		descLbl.text = "serverAbDesc".loc()

        serverBtn.enabled = true;
        serverBtn.alpha = 1

        if(isStoredVideo){
            titleLbl.text = "storedAbTitle".loc()
            descLbl.text = "storedAbDesc".loc()

            serverBtn.enabled = false;
            serverBtn.alpha = 0
        }


		// Do any additional setup after loading the view, typically from a nib.
	}
	

	@IBAction func btnContactClick(sender: UIButton) {
		
		print("BTN CONTATTI " + User.Instance.paypage)
		UIApplication.sharedApplication().openURL(NSURL(string: User.Instance.paypage)!)
		
		
	}
}