//
//  Evento.swift
//  Camnet
//
//  Created by giulio piana on 16/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation


class Vista{
	
	var id : String
	var name : String
	var description : String?
	var type : String
	var size : String
	
	var content = [String: Cam] ()
	var contentIds = [String]()
	
	var install : String?
	
	init ()
	{
		id = ""
		name = ""
		description = ""
		type = ""
		size = ""
		
	}
	
	init (_id:String, _name:String?, _desc:String? , _type:String , _size:String? )
	{
		id = _id
		
		name = ""
		if (_name != nil ) { name = _name! }
		
		description = _desc
		type = _type
		
		size="4"
		if (_size != nil ) { size = _size! }
	}
	
	
}