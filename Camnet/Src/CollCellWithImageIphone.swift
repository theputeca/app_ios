//
//  CollCellWithImageIphone.swift
//  Camnet
//
//  Created by giulio piana on 18/04/15.
//  Copyright (c) 2015 giulio piana. All rights reserved.
//

import Foundation
import UIKit

class CollCellWithImageIphone : UICollectionViewCell
{
	
	override init(frame: CGRect) {
		super.init(frame: frame)
	}
	
	
	required init?(coder aDecoder: NSCoder) {
		
		super.init(coder: aDecoder)
	}
	
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
	/*	if( imageUrl != nil){
			loadImage()
		}*/
	}
	
	
	
	
	var imageUrl : String?
	var image : UIImage?
	var imageView : UIImageView?
	
    var title : UIView?
	
	func loadImage(){
		let imageView : UIImageView = self.viewWithTag(2) as! UIImageView
		
		dispatch_async(dispatch_get_main_queue(), {
			if ( self.imageUrl != nil){
				let url = NSURL(string: self.imageUrl!)!
				if let data = NSData(contentsOfURL: url) {
					if let image2 = UIImage(data: data){
						imageView.image = image2
					}
				}
			}

		});
		
		
	}

    func animate(){

/*
        title = viewWithTag(12)
        title!.backgroundColor = UIColor.arancio()

        UIView.animateWithDuration(0.5 , delay: 0.0 ,
                options: [.Repeat, .Autoreverse, .CurveEaseInOut] , animations: {
                    UIView.setAnimationRepeatCount(2.5)
                    self.title!.backgroundColor =  UIColor.sfondoChiaro()
                }, completion: nil)
*/
    }


}