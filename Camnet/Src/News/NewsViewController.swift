//
//  NewsViewController.swift
//  Camnet
//
//  Created by giulio piana on 21/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation


import UIKit


private var _Instance : NewsViewController? = nil

class NewsViewController: UIViewController,UIWebViewDelegate {

    class var Instance : NewsViewController? {
        return _Instance
    }

	
	
	
	var versionLabel :UILabel!
	
	var newsWebView: UIWebView!
	var loadInd : UIActivityIndicatorView?
	override func viewDidLoad() {
		super.viewDidLoad()

        _Instance = self

        MainViewController.Instance?.mainNavigationController = self.navigationController
        (self.tabBarController?.tabBar.items![4]  as UITabBarItem? )!.badgeValue = nil
		
		//WEBVIEW
		newsWebView = UIWebView(frame: CGRect(x: 0, y: self.frameBetweenBar().origin.y  , width: self.view.frame.width, height: self.frameBetweenBar().height ))
		newsWebView.backgroundColor = UIColor.sfondoChiaro()
		view.addSubview(newsWebView)
		newsWebView.delegate = self
		


       var url = NSURL(string: User.Instance.newspage)

        // url da push notification
        if(MainViewController.notificationData != nil)
        {
            let notNewsUrl : String = MainViewController.notificationData!.objectForKey("content") as! String
            url = NSURL(string: notNewsUrl)
           MainViewController.clearNotData()
        }

			let request = NSURLRequest(URL: url!)
			self.newsWebView.loadRequest(request)
			

		
		
		newsWebView.scalesPageToFit = true
		
		// loading indicator
		loadInd  = UIActivityIndicatorView(frame: view.frame)
		loadInd!.center = newsWebView.center
		loadInd!.hidesWhenStopped = true
		loadInd!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
		loadInd!.color = UIColor.arancio()
		loadInd!.startAnimating()
		
		view.addSubview(loadInd!)
		self.view.bringSubviewToFront(loadInd!)
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		if(self.IsPad())
		{
			self.automaticallyAdjustsScrollViewInsets = false
			newsWebView.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.height  , width: self.view.frame.width, height: self.frameBetweenBar().height )
			loadInd!.center = newsWebView.center
		}
		else
		{
			self.automaticallyAdjustsScrollViewInsets = false
			newsWebView.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.height  , width: self.view.frame.width, height: self.frameBetweenBar().height )
			loadInd!.center = newsWebView.center
		}


        var url = NSURL(string: User.Instance.newspage)

        // url da push notification
        if(MainViewController.notificationData != nil)
        {
            let notNewsUrl : String = MainViewController.notificationData!.objectForKey("content") as! String
            url = NSURL(string: notNewsUrl)
            MainViewController.clearNotData()
            let request = NSURLRequest(URL: url!)
            self.newsWebView.loadRequest(request)

            (self.tabBarController?.tabBar.items![4]  as UITabBarItem? )!.badgeValue = nil
            loadInd!.startAnimating()

        }




	}
	
	//ORIENTATION
	override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
		
		if(self.IsPad())
		{
			if (  toInterfaceOrientation.isPortrait		){
				self.automaticallyAdjustsScrollViewInsets = false
				newsWebView.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.height  , width: self.view.frame.height, height: self.frameBetweenBar().width)
				loadInd!.center = newsWebView.center
			}
			else{
				newsWebView.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.height  , width: self.view.frame.height, height: self.frameBetweenBar().width  )
				loadInd!.center = newsWebView.center
			}
			
		}
		
	}
	
	override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
		if(!self.IsPad())
		{
			newsWebView.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.height  , width: self.view.frame.width, height: self.frameBetweenBar().height )
			loadInd!.center = newsWebView.center
		}
	}
	
	
	//WEBVIEW
	// web view map event

    func reloadWebViewFroNotification()
    {
        var url = NSURL(string: User.Instance.newspage)

        // url da push notification
        if(MainViewController.notificationData != nil)
        {
            let notNewsUrl : String = MainViewController.notificationData!.objectForKey("content") as! String
            url = NSURL(string: notNewsUrl)
            MainViewController.clearNotData()
        }

        let request = NSURLRequest(URL: url!)
        self.newsWebView.loadRequest(request)

        (self.tabBarController?.tabBar.items![4]  as UITabBarItem? )!.badgeValue = nil


        loadInd!.startAnimating()
    }
	
	func webViewDidFinishLoad(webView: UIWebView) {
		self.loadInd!.stopAnimating()
		
	}
	
	@IBAction func logOutClick(sender: UIButton) {
		self.logout()
	}
}
