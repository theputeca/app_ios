//
//  GlobalData.swift
//  Camnet
//
//  Created by giulio piana on 15/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation
import UIKit


//define a global instance of Data to store xml parsed data
private let _Instance = DataManager()


class DataManager {
	
	
	var impianti = [Impianto] ()
	
	var installs = [String : Impianto]()
	
	var allCam = [String : Cam]()
	var allMapp = [String : Mappa] ()
	var allUserView = [String : Vista]()
	var allAutoView = [String : Vista]()


    var eventXmlForInstall = [String : AEXMLDocument]();

	var imgCache = [String : UIImage?]()
    var imgCacheEventi = [String : UIImage?]()

	let kBgQueue  = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
	
	var allEvents : [String : [Evento]] = [String : [Evento]]()

	//lista di tutte le cam di tutti gli impianti
	var camlist : [String: Cam] {
		get{
			var tempArr : [String: Cam] = [String :Cam]()
			for i in impianti{
				
				for (a,b) in i.devices{
					tempArr.updateValue(b, forKey: a)
				}
			}
			return tempArr
		}
	}
	
	class var Instance : DataManager {
		return _Instance
	}
	
	 func resetInstance () {
		
		 impianti = [Impianto] ()
			installs = [String : Impianto]()

         eventXmlForInstall = [String : AEXMLDocument]();
		 allCam = [String : Cam]()
		 allMapp = [String : Mappa] ()
		 allUserView = [String : Vista]()
		 allAutoView = [String : Vista]()
		
	}
	
	init() {
		//println("BBB");
	}
	
	func ParseXmlData(xmlDoc: AEXMLDocument)
	{
		print("PARSEXMLDATA")
		
			for imp in xmlDoc.root["installs"]["install"].all!
			{

					// creo gli impianti
					print(imp["id"].value)
					print(imp["name"].value)
					print(imp["description"].value)
					
					let impianto=Impianto(_id: imp["id"].value!, _name: imp["name"].value, _description: imp["description"].value)
					impianti.append(impianto)
				
					installs[impianto.id] = impianto
				
					impianto.isPro = imp["is_pro"].value!

                if (imp["stored_video"].value != nil){
                    impianto.storedVideo =  imp["stored_video"].value!
                }
                else{
                    impianto.storedVideo = "false"
                }

               //   impianto.storedVideo = "false"
					//aggiungo le cam
					
					for cam in imp["devices"].children
					{
						let c = Cam(_id: cam["id"].value!, _name: cam["name"].value, _desc: cam["description"].value, _mapId: cam["map"].value)
						
						
						//load action //possono ono eststere
						
						if(cam["action"]["recordingVideoOn"].name != "error"){ c.recordingVideoOn = cam["action"]["recordingVideoOn"].value }
						if(cam["action"]["recordingVideoOff"].name != "error"){ c.recordingVideoOff = cam["action"]["recordingVideoOff"].value}
						if(cam["action"]["recordingAudioOn"].name != "error"){c.recordingAudioOn = cam["action"]["recordingAudioOn"].value}
						if(cam["action"]["recordingAudioOff"].name != "error"){c.recordingAudioOff = cam["action"]["recordingAudioOff"].value }
						if(cam["action"]["lightOn"].name != "error"){c.lightOn = cam["action"]["lightOn"].value}
						if(cam["action"]["lightOff"].name != "error"){c.lightOff = cam["action"]["lightOff"].value}
						if(cam["action"]["viewPtz"].name != "error"){c.viewPtz	= cam["action"]["viewPtz"].value}
						if(cam["action"]["externalCommand"].name != "error"){c.externalCommand = cam["action"]["externalCommand"].value}
						

						//lad action status
						c.status["video"]=cam["status"]["recordingVideo"].value
						c.status["audio"]=cam["status"]["recordingAudio"].value
						c.status["luce"]=cam["status"]["luce"].value
						c.status["ptz"]=cam["status"]["ptz"].value
						c.status["external"]=cam["status"]["external"].value
						//
						c.install = impianto.id
						//load url
						c.urlBigVideo = cam["urls"]["hdVideo"].value
						c.urlBigVideo = cam["urls"]["bigVideo"].value
						c.urlSmallVideo = cam["urls"]["mediumVideo"].value

                        c.webViewVideoMedium = cam["urls"]["webViewVideoMedium"].value
                        c.webViewVideoBig = cam["urls"]["webViewVideoBig"].value
                        c.webViewVideoHd = cam["urls"]["webViewVideoHd"].value


                        //video nuovi
                         c.urlVideo_1 = cam["urls"]["video_1"].value
                         c.urlVideo_4 = cam["urls"]["video_4"].value
                         c.urlVideo_6 = cam["urls"]["video_6"].value
                         c.urlVideo_9 = cam["urls"]["video_9"].value
                         c.urlVideo_16 = cam["urls"]["video_16"].value

                        c.webViewVideoVideo_1 = cam["urls"]["webViewVideo_1"].value
                        c.webViewVideoVideo_4 = cam["urls"]["webViewVideo_4"].value
                        c.webViewVideoVideo_6 = cam["urls"]["webViewVideo_6"].value
                        c.webViewVideoVideo_9 = cam["urls"]["webViewVideo_9"].value
                        c.webViewVideoVideo_16 = cam["urls"]["webViewVideo_16"].value

						c.urlImg = cam["urls"]["image"].value
						
						impianto.devices [c.id] = c
						self.allCam[c.id]=c

                        print(c.urlVideo_1, terminator: "" );
					}
				
				
				
				
					//aggiungo le mappe
					
					for map in imp["maps"].children
					{
						//println(map.name)
						let m = Mappa(_id: map["id"].value! , _name: map["name"].value , _desc: map["description"].value , _url: map["viewMap"].value)
						
						for viewId in map["content"].children
						{
							m.contentIds.append( viewId.value!)
						}
						
						impianto.maps [m.id] = m
						self.allMapp[m.id] = m
					}
					
					//aggiungo le viste
					//auto
					for view in imp["views_auto"].children
					{
						let v = Vista(_id: (view["id"].value! ) , _name: view["name"].value! , _desc: view["description"].value, _type: "auto", _size: view["size"].value!)
						
						v.install = impianto.id
						//aggiunge le cam alla vista
						for camId in view["content"].children
						{
							v.content[ camId.value! ] = impianto.devices[camId.value!]
							v.contentIds.append( camId.value!)
						}
						
						impianto.views_auto[v.id] = v
						self.allAutoView [v.id ] = v
					}
					
					//aggiungo le viste user ma solo gli id peche potrebbero avere cam di altri impianti
				
				for view in  imp["views_user"].children //imp["views_user"].children
					{
						//println(view.name)
						let v = Vista(_id: view["id"].value!, _name: view["name"].value, _desc: view["description"].value, _type: "user", _size: view["size"].value)
						
						v.install = impianto.id
						//aggiunge le cam alla vista
						for camId in view["content"].children
						{
							//v.content[camId.value] = self.camlist[camId.value]
							v.contentIds.append( camId.value!)
						}
						
						impianto.views_user[view["id"].value!] = v
						self.allUserView [ v.id ] = v
						
					}
				
				
					
				}
		
		
		
			//impianti.sort({ ($0.name as  NSString).lowercaseString < ($1.name as NSString).lowercaseString })
		
			//}
			
			//associo le viste user agli impianti lo faccio dopo perche potrebbero avere cam di altri imipianti
			// gli id delle cam devono essere univoci
			
			for  v : Vista in self.allUserView.values
			{
				var i  : Int = 0 ;
				for s: String in v.contentIds{
					
					if let c = v.content[s] {
						v.content[ s + "\(i)" ] = self.allCam[s]
						
					}
					else{
						v.content[s] = self.allCam[s]
					}
					
					i++
				}
			}
		
			//debug
			/*for im in impianti
			{
				
				println("nome impianto :  " + im.name)
				for v : Vista in im.views_auto.values
				{
					println("viste auto : " + v.name)
				}
				
				for v : Vista in im.views_user.values
				{
					println("viste user : " + v.name)
					for c : Cam in v.content.values
					{
						println("cam Nella VISTA  : " + c.id	)
					}

				}
			}
			*/
		
		
		//imagechahe
		/*dispatch_async(dispatch_get_main_queue(), {
			
			let cams = [Cam] (self.allCam.values)
			for cam in cams
			{
			
			
			dispatch_async(self.kBgQueue, {
				let url = NSURL(string: cam.urlImg!)
				if let data = NSData(contentsOfURL: url!) {//make sure your image in this url does exist, otherwise unwrap in a if let check
					if let image = UIImage(data: data){
						
						println("caricato immagin \(cam.id)")
						self.imgCache[ cam.id ] = image
					}
					else{
						println("nul immagin \(cam.id)")
						self.imgCache[ cam.id ] = nil
					}
				}
			});
			
			}
		})*/
		
		
		

	}
	
/*	func ParseXmlData(data: NSData)
	{
		println("PARSEXML DATA")
		
		//Xml TEST
		
		let bundle = NSBundle.mainBundle()
		let path : String? = bundle.pathForResource("test1", ofType: "xml")  //testXmlFile
		var dataTest : NSData? = NSData(contentsOfFile: path!) as NSData?
		
		var error: NSError?
		if let xmlDoc = AEXMLDocument(xmlData: dataTest!, error: &error) {
			
			for install in xmlDoc.root["installs"].all!
			{
				
				println(install.value)
				
				for imp in install.children
				{
					
					//for imp in xmlDoc.rootElement["installs"]["install"].all
					//{
					// creo gli impianti
					println(imp["id"].value)
					println(imp["name"].value)
					println(imp["description"].value)
					
					let impianto=Impianto(_id: imp["id"].value!, _name: imp["name"].value!, _description: imp["description"].value!)
					impianti.append(impianto)
					installs[impianto.id] = impianto
					impianto.isPro = imp["is_pro"].value!
					
					//aggiungo le cam
					
					for cam in imp["devices"].children
					{
						let c = Cam(_id: cam["id"].value!, _name: cam["name"].value!, _desc: cam["description"].value!, _mapId: cam["map"].value!)
						
						//lad action
						c.status["video"]=cam["status"]["video"].value
						c.status["audio"]=cam["status"]["audio"].value
						c.status["luce"]=cam["status"]["luce"].value
						c.status["ptz"]=cam["status"]["ptz"].value
						
						
						//load url
						c.urlBigVideo = cam["urls"]["hdVideo"].value
						c.urlBigVideo = cam["urls"]["bigVideo"].value
						c.urlSmallVideo = cam["urls"]["mediumVideo"].value
						c.urlImg = cam["urls"]["image"].value
						
						impianto.devices [c.id] = c
						self.allCam[c.id]=c
						
					}
					
					//aggiungo le mappe
					
					for map in imp["maps"].children
					{
						//println(map.name)
						let m = Mappa(_id: map["id"].value!, _name: map["name"].value!, _desc: map["description"].value!, _url: map["viewMap"].value!)
						
						for viewId in map["content"].children
						{
							m.contentIds.append( viewId.value!)
						}
						
						impianto.maps [m.id] = m
						self.allMapp[m.id] = m
					}
					
					//aggiungo le viste
					//auto
					for view in imp["views_auto"].children
					{
						let v = Vista(_id: view["id"].value!, _name: view["name"].value!, _desc: view["description"].value!, _type: "auto", _size: view["size"].value!)
						
						//aggiunge le cam alla vista
						for camId in view["content"].children
						{
							v.content[ camId.value! ] = impianto.devices[camId.value!]
						}
						
						impianto.views_auto[v.id] = v
						self.allAutoView [v.id ] = v
					}
					
					//aggiungo le viste user ma solo gli id peche potrebbero avere cam di altri impianti
					for view in imp["views_user"].children
					{
						//println(view.name)
						let v = Vista(_id: view["id"].value!, _name: view["name"].value!, _desc: view["description"].value!, _type: "user", _size: view["size"].value!)
						
						//aggiunge le cam alla vista
						for camId in view["content"].children
						{
							//v.content[camId.value] = self.camlist[camId.value]
							v.contentIds.append( camId.value!)
						}
						
						impianto.views_user[view["id"].value!] = v
						self.allUserView [ v.id ] = v
						
					}

					
				}
			}
			
			//associo le viste user agli impianti lo faccio dopo perche potrebbero avere cam di altri imipianti
			// gli id delle cam devono essere univoci
			
			for  v : Vista in self.allUserView.values
			{
				for s:String in v.contentIds{
					v.content[s] = self.allCam[s]
				}
			}
			
		
			
			//debug
			for im in impianti
			{
				
				println("nome impianto :  " + im.name)
				for v : Vista in im.views_auto.values
				{
					println("viste auto : " + v.name)
				}
				
				for v : Vista in im.views_user.values
				{
					println("viste user : " + v.name)
					for c : Cam in v.content.values
					{
						println("cam Nella VISTA  : " + c.id	)
					}
					
				
				}
				
				
			/*	for c : Cam in im.devices.values
				{
					println("cam : " + c.name)
				}
				
				for m : Mappa in im.maps.values
				{
					println("mappe : " + m.name)
				}*/
			}
			
			
		}else
		{
			println(error);
		}
	}
	*/
	
	
	func removeViewFromImp( impId : String  ,  viewId : String)
	{
		for imp in impianti
		{
			if(imp.id == impId)
			{
				imp.views_user.removeValueForKey(viewId)
				break
			}
		}
	}
	
	
	func reloadData( caller : CreaVistaViewController){
		
		
		let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
		let loginCode = defaults.objectForKey("loginCode") as! String
		let url = NSURL(string:   User.Instance.serverUrl + "api_mobile.php?login="+loginCode+"&udid=0000")

		var request = NSMutableURLRequest(URL: url!)
		var session = NSURLSession.sharedSession()
		request.HTTPMethod = "GET"
		
	
		var task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
			
			print("Response: \(response)")
			var error: NSError?
			
			if ( response == nil )
			{
				//errore
				return
			}
			
			do {
				let xmlDoc = try AEXMLDocument(xmlData: data!)  // AEXMLDocument(xmlData: data, error: &error)
				
				print(xmlDoc.xmlString)
				if let result = xmlDoc.root["result"].value! as String? {
					
					if (result == "false"){
						//self.alertErrorLogin() // login error
					}
					else	{
						
						self.resetInstance ();
						DataManager.Instance.ParseXmlData(xmlDoc)
						
						caller.dataReloaded()
					}
				}
				else
				{
					//self.alertErrorLogin() //data error
				}
			} catch _ {
			}
			
		})
		task.resume()
	}
	
}





































 