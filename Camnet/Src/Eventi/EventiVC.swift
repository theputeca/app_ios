//
//  NEW.swift
//  Camnet
//
//  Created by giulio piana on 21/12/14.
//  Copyright (c) 2014 giulio piana. All rights reserved.
//

import Foundation
import UIKit


private var _Instance : EventiVC? = nil //= EventiVC()

class EventiVC:  UIViewController , UITableViewDataSource ,UITableViewDelegate , UICollectionViewDataSource , UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIAlertViewDelegate
{


    class var Instance : EventiVC? {
        return _Instance
    }

    var ImpiantiTable : UITableView!
    var CamCollectionView: UICollectionView!
    var selectedImpianto : Impianto?
    var selectedIndex : Int = 0
    var events : [Evento] = [Evento]()
    var loadInd : UIActivityIndicatorView?
    var isShowCollection : Bool = false

    //let kBgQueue  = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)

    var imageCahce : [Int : UIImage] = [Int : UIImage]()
    ////////////////

    var isPushNotification  : Bool = false
    var pushInstallId : String?
    var pushEventId : String?

    var isLoadedNewEvent : Bool = false


    @IBAction func logOutClick(sender: UIButton) {
        self.logout()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        isGoInPlayer = false
        _Instance = self

        MainViewController.Instance?.mainNavigationController = self.navigationController
        self.title = "eventi".loc()
        self.navigationController!.navigationBar.tintColor = UIColor.arancio()


        CamCollectionView = NSBundle.mainBundle().loadNibNamed("CVSelection", owner: self, options: nil)[0] as? UICollectionView
        CamCollectionView.frame = CGRect(x: 300 , y: 0 , width: self.frame().width - 300	, height: self.frame().height)
        CamCollectionView.backgroundColor = UIColor.whiteColor()




        if(self.IsPad())
        {
            self.automaticallyAdjustsScrollViewInsets = false
            let nib = UINib(nibName: "CVHeaderEventiCell", bundle: nil) // CVHeaderEventiCell
            CamCollectionView.registerNib(nib , forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
        }
        else
        {//IPHIONE
            let nib = UINib(nibName: "CVHeaderEventiCellIphone", bundle: nil) //CVHeaderCellIphone
            CamCollectionView.registerNib(nib , forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView")
        }

        self.view.addSubview(CamCollectionView)
        CamCollectionView.delegate = self
        CamCollectionView.dataSource = self

        //table view
        if (!isShowCollection )
        {
            ImpiantiTable   = UITableView()

            if(self.IsPad()){
                ImpiantiTable.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 300	, height: self.frame().height)
                ImpiantiTable.separatorStyle = UITableViewCellSeparatorStyle.None

            }
            else
            {//IPHIONE
                ImpiantiTable.frame = CGRect(x: 0 , y:  self.navigationController!.navigationBar.frame.height  , width: self.frame().width	, height: self.frame().height)
                ImpiantiTable.separatorStyle = UITableViewCellSeparatorStyle.None

            }


            self.view.addSubview(ImpiantiTable)
            self.view.bringSubviewToFront(ImpiantiTable)
            ImpiantiTable.delegate=self
            ImpiantiTable.dataSource = self

            //bordo
            if(self.IsPad())
            {
                let border = CALayer()
                let width = CGFloat(0.5)
                border.borderColor = UIColor.darkGrayColor().CGColor
                border.frame = CGRect(x: 0 , y:0 , width:  width, height: ImpiantiTable.frame.size.height) //

                border.borderWidth = width
                CamCollectionView.layer.addSublayer(border)
                CamCollectionView.layer.masksToBounds = true
            }

            ImpiantiTable.layer.borderColor =  UIColor.sfondoHeader().CGColor
        }





        // loading indicator
        loadInd  = UIActivityIndicatorView(frame: view.frame)
        loadInd!.center = CamCollectionView.center
        loadInd!.hidesWhenStopped = true
        loadInd!.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        loadInd!.color = UIColor.arancio()




    }

    func handlingEnterView()
    {

        isPushNotification = false

        if(MainViewController.notificationData != nil)
        {
            isPushNotification = true

            pushInstallId  = MainViewController.notificationData!.objectForKey("install") as? String
            pushEventId  = MainViewController.notificationData!.objectForKey("content") as? String

            print(pushInstallId)
            print(pushEventId)
        }



        if(self.IsPad())
        {
            if(isPushNotification){
                selectedImpianto = DataManager.Instance.installs[ pushInstallId! ]
            }
            else
            {

                if(selectedImpianto == nil ){
                    for  imp : Impianto in DataManager.Instance.impianti {
                        if (imp.devices.count >  0){
                            selectedImpianto = imp
                            break
                        }
                    }
                }
            }

            if (selectedImpianto?.isPro == "false"){
                self.addProUserView( CamCollectionView.frame )

                events = [Evento]()
                arrayEvents = Array<Array<Evento>>()
                CamCollectionView.reloadData()

            }
            else
            {
                loadInd!.center = CamCollectionView.center
                loadInd!.startAnimating()
                view.addSubview(loadInd!)
                self.view.bringSubviewToFront(loadInd!)

                if(isPushNotification)
                {
                    var int = 0;
                    for  imp : Impianto in DataManager.Instance.impianti {
                        if (imp.id == pushInstallId ){
                            break
                        }
                        int++;
                    }

                    let index =  NSIndexPath(forRow: int, inSection: 0)
                    if ( ImpiantiTable.indexPathForSelectedRow?.row == int  ){
                        reqEventAsinc()
                    }
                    else
                    {
                        ImpiantiTable.selectRowAtIndexPath(index, animated: false, scrollPosition: UITableViewScrollPosition.Middle)
                        ImpiantiTable.delegate!.tableView!( ImpiantiTable , didSelectRowAtIndexPath: index )
                    }
                }
                else
                {
                    reqEventAsinc()
                }


            }


        }
        else
        {//IPHONE
            if ( isShowCollection ){
                CamCollectionView.frame = CGRect(x: 0 , y: 0 , width: self.frame().width	, height: self.frame().height)
                //  ImpiantiTable.alpha = 0
                CamCollectionView.alpha = 1

                if (selectedImpianto?.isPro == "false"){

                    self.addProUserView( CamCollectionView.frame )
                    if(isPushNotification){
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                }
                else
                {
                    loadInd!.center = CamCollectionView.center
                    loadInd!.startAnimating()
                    view.addSubview(loadInd!)
                    self.view.bringSubviewToFront(loadInd!)

                    reqEventAsinc()
                }

                if(selectedImpianto != nil){
                    self.title = "eventi".loc() + ": " + selectedImpianto!.name
                }

            }
            else
            {
                ImpiantiTable.alpha = 1
                CamCollectionView.alpha = 0

                if(isPushNotification){
                    selectedImpianto = DataManager.Instance.installs[ pushInstallId! ]

                    NavigateToCollection(selectedImpianto )
                    //carica
                }
                else
                {
                    for  imp : Impianto in DataManager.Instance.impianti {
                        if (imp.devices.count >  0){
                            selectedImpianto = imp
                            break
                        }
                    }
                }


            }
        }


    }


    var isGoInPlayer = false
    override func viewWillDisappear(animated: Bool) {

        // taskforEvent.cancel()

        if (isShowCollection == true && isGoInPlayer == false)
        {
            //       isShowCollection = false
        }

    }

    override func viewWillAppear(animated: Bool) {

        if(self.IsPad())
        {


        }
        else
        {//IPHONE

            //handlingEnterView()

            if(isShowCollection){
                CamCollectionView.reloadData()
                CamCollectionView.frame = CGRect(x: 0 , y: CamCollectionView.frame.origin.y , width: self.view.frame.width	, height: self.view.frame.height)
            }
            else
            {
                ImpiantiTable.reloadData()
                ImpiantiTable.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: self.view.frame.width	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height )
            }

        }
    }

    override func viewDidAppear(animated: Bool) {

        isLoadedNewEvent = false;
        //handlingEnterView()

        self.tabBarController?.tabBar.hidden = false

        (self.tabBarController?.tabBar.items![2]  as UITabBarItem? )!.badgeValue = nil

        if(self.IsPad())
        {
            handlingEnterView()
            if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) )
            {
                ImpiantiTable.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 300	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height )
                CamCollectionView.frame = CGRect(x: 300, y:  self.navigationController!.navigationBar.frame.height  , width:  self.view.frame.width - 300	, height: self.frameBetweenBar().height ) // self.view.frame.height )

            }
            else
            {
                ImpiantiTable.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 200	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height )
                CamCollectionView.frame = CGRect(x: 200 , y:  self.navigationController!.navigationBar.frame.height  , width:  self.view.frame.width - 200	, height: self.frameBetweenBar().height ) //

            }

            ImpiantiTable.reloadData()
            CamCollectionView.reloadData()
            loadInd!.center = CamCollectionView.center

            if let viewpro = self.view.viewWithTag(98)
            {
                self.removeProUserView()
                self.addProUserView( CamCollectionView.frame )
            }

        }
        else
        {//IPHONE

            handlingEnterView()

            if(isShowCollection){
                CamCollectionView.reloadData()
                CamCollectionView.frame = CGRect(x: 0 , y: CamCollectionView.frame.origin.y , width: self.view.frame.width	, height: self.view.frame.height)
            }
            else
            {
                ImpiantiTable.reloadData()
                ImpiantiTable.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: self.view.frame.width	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height )
            }

        }


    }

    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {


        if(self.IsPad())
        {
            if (  toInterfaceOrientation.isPortrait		){
                ImpiantiTable.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 200	, height: self.view.frame.width - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height )
                CamCollectionView.frame = CGRect(x: 200 , y: CamCollectionView.frame.origin.y , width:  self.view.frame.height - 200	, height: self.view.frame.width - self.navigationController!.navigationBar.frame.height - self.tabBarController!.tabBar.frame.height  )

            }
            else
            {
                ImpiantiTable.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: 300	, height: self.view.frame.width - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height )
                CamCollectionView.frame = CGRect(x: 300, y: CamCollectionView.frame.origin.y , width:  self.view.frame.height - 300	, height: self.view.frame.width - self.navigationController!.navigationBar.frame.height - self.tabBarController!.tabBar.frame.height  )

            }

            loadInd!.center = CamCollectionView.center
            ImpiantiTable.reloadData()
            CamCollectionView.reloadData()
        }
    }
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        if (self.IsPad()){

            if let viewpro = self.view.viewWithTag(98)
            {
                self.removeProUserView()
                self.addProUserView( CamCollectionView.frame )
            }



        }
        else
        {//IPHONE
            if(isShowCollection){
                CamCollectionView.frame = CGRect(x: 0 , y:  0 , width: self.view.frame.width	, height: self.view.frame.height)
                CamCollectionView.reloadData()
            }
            else
            {
                ImpiantiTable.frame = CGRect(x: 0 , y: self.navigationController!.navigationBar.frame.height , width: self.view.frame.width	, height: self.view.frame.height - self.navigationController!.navigationBar.frame.height  - self.tabBarController!.tabBar.frame.height )
                ImpiantiTable.reloadData()
                loadInd!.center = CamCollectionView.center

            }




        }
    }


    //CAL API
var taskforEvent:NSURLSessionTask  = NSURLSessionTask();

    func reqEventAsinc()
    {
        //carico gli eventi
        //dispatch_async(dispatch_get_main_queue(), {

        if( selectedImpianto == nil ){
            return
        }


        let xmlDoc = DataManager.Instance.eventXmlForInstall[ self.selectedImpianto!.id ]

        if  (xmlDoc != nil && !isPushNotification)  {

            var eventi : [Evento] = [Evento]()
            if(xmlDoc!.root["event"].all != nil)
            {

                for e in xmlDoc!.root["event"].all!  {
                    if ( e.name != "error" ){
                        var vid : String? = nil
                        var img : String? = nil
                        if  (e["video"].name != "error") {   vid = e["video"].value    }
                        if  (e["image"].name != "error") {   img = e["image"].value    }

                        let evento : Evento = Evento (_id: e["eventId"].value! , _deviceId: e["deviceId"].value!, _deviceName: e["deviceName"].value!, _eventDetail: e["eventDetails"].value!, _imageUrl: img , _mode: e["mode"].value!, _type: e["type"].value! , _urlVideo: vid )
                        evento.time = e["time"].value!
                        eventi.append(evento)
                    }
                }
            }

            DataManager.Instance.allEvents [self.selectedImpianto!.id] = eventi

            dispatch_async(dispatch_get_main_queue(), {

                self.getAllEvent()
                self.isLoadedNewEvent = true;

            })
        }
        else
        {
            let urlapi : String = User.getEventsApiUrl( self.selectedImpianto!.id )
            let url = NSURL(string:  User.Instance.serverUrl + urlapi  )

            print("RICHIESTA EVENTI:  \(url) ")


            let request1 = NSMutableURLRequest(URL: url! )
            let session1 = NSURLSession.sharedSession()
            request1.HTTPMethod = "GET"
            request1.timeoutInterval = 30



             taskforEvent = session1.dataTaskWithRequest(request1, completionHandler: {data, response, error -> Void in


                if((error) != nil){
                    print("ERROR CALL FOR EVENT ")

                    return

                }
                if(data == nil )
                {
                    print("DATA EVENT NULL")
                    return
                }

                //var error: NSError?
                do {
                    let xmlDoc = try AEXMLDocument(xmlData: data!)

                    print("xml: \(xmlDoc.xmlString)")

                    DataManager.Instance.eventXmlForInstall[ self.selectedImpianto!.id ] = xmlDoc

                    var eventi : [Evento] = [Evento]()

                    if(xmlDoc.root["event"].all != nil)
                    {

                        for e in xmlDoc.root["event"].all!  {
                            if ( e.name != "error" ){
                                var vid : String? = nil
                                var img : String? = nil
                                if  (e["video"].name != "error") {   vid = e["video"].value    }
                                if  (e["image"].name != "error") {   img = e["image"].value    }

                                let evento : Evento = Evento (_id: e["eventId"].value! , _deviceId: e["deviceId"].value!, _deviceName: e["deviceName"].value!, _eventDetail: e["eventDetails"].value!, _imageUrl: img , _mode: e["mode"].value!, _type: e["type"].value! , _urlVideo: vid )
                                evento.time = e["time"].value!
                                eventi.append(evento)
                            }
                        }
                    }

                    DataManager.Instance.allEvents [self.selectedImpianto!.id] = eventi

                    dispatch_async(dispatch_get_main_queue(), {

                        self.getAllEvent()
                        self.isLoadedNewEvent = true;

                    })
                }
                catch _ {
                    print("ERROR DATA EVENT ")
                }
            })

            taskforEvent.resume()
        }
    }


    var arrayEvents : Array<Array<Evento>> = Array<Array<Evento>>()

    func getAllEvent(){

        imageCahce = [Int : UIImage]()
        events = [Evento]()
        arrayEvents = Array<Array<Evento>>()

        if(selectedImpianto != nil ){
            self.events = DataManager.Instance.allEvents[selectedImpianto!.id]!

            self.events.sortInPlace({ ($0.time as NSString).doubleValue > ($1.time as NSString).doubleValue })


            // arrayEvents = Array<Array<Evento>>()

            for e : Evento in events {
                //e.time
                let n = (e.time as NSString).doubleValue
                let dataEvento : NSDate = NSDate(timeIntervalSince1970: n)

                if (arrayEvents.count == 0 ){

                    var arr = [Evento]()
                    arr.append(e)
                    arrayEvents.append(arr)

                    continue
                }

                if let lastEvent = arrayEvents [arrayEvents.count - 1 ].last as Evento?
                {
                    let n2 = (lastEvent.time as NSString).doubleValue
                    let dataUltimo : NSDate = NSDate(timeIntervalSince1970: n2)

                    if ( dataEvento.month()	> dataUltimo.month() )
                    {
                        //crea nuovo atrrai
                        var arr = [Evento]()
                        arr.append(e)
                        arrayEvents.append(arr)
                    }
                    else
                    {
                        //metti in quello esistente
                        arrayEvents [arrayEvents.count - 1 ].append(e)
                    }
                }
                else
                {
                    //crea nuovo atrrai
                    var arr = [Evento]()
                    arr.append(e)
                    arrayEvents.append(arr)
                }
            }


            //dispatch_async(dispatch_get_main_queue(), {

            if(self.arrayEvents.count == 0 )
            {
                self.alertMessage("ErrorTitle".loc()	, message: "noEventsMessage".loc(), button: "annulla".loc()  , delegate: self, _tag: 21)

            }

            self.loadInd!.stopAnimating()
            self.CamCollectionView.reloadData()
            //})

        }
    }


    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {

        if (alertView.tag == 21)
        {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }


    //TABEL VIEW

    //number row
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.Instance.impianti.count
    }
    //row at index
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        var cell :UITableViewCell! = tableView.dequeueReusableCellWithIdentifier("CellTitleDescription") as UITableViewCell?
        if(cell == nil)
        {
            let nib = UINib(nibName: "CellTitleDescription", bundle: nil)
            tableView.registerNib(nib, forCellReuseIdentifier: "CellTitleDescription")
            cell = tableView.dequeueReusableCellWithIdentifier("CellTitleDescription") as UITableViewCell?
        }

        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor.arancioSelezione()
        cell.selectedBackgroundView = bgColorView

        let title : UILabel =  cell.viewWithTag(1) as! UILabel
        title.text = DataManager.Instance.impianti[ indexPath.row ].name

        let desc : UILabel = cell.viewWithTag(2) as! UILabel
        desc.text = DataManager.Instance.impianti[ indexPath.row ].description

        /// nasconde le cell per impanti vuoti (multiimp)
        if( DataManager.Instance.impianti[ indexPath.row ].id == "0" 	)
        {

            if (selectedIndex == indexPath.row) { selectedIndex++ }

            title.alpha = 0
            desc.alpha = 0
            cell.alpha = 0
            let freccia  : UIImageView = cell.viewWithTag(3) as! UIImageView
            freccia.alpha = 0
        }
        else
        {
            title.alpha = 1
            desc.alpha = 0
            cell.alpha = 1
            let freccia  : UIImageView = cell.viewWithTag(3) as! UIImageView
            freccia.alpha = 1
        }

        //if(!self.IsPad()){//IPHONE
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, tableView.frame.width  , 65 )


        let border : CALayer = CALayer()
        border.borderColor = UIColor.sfondoChiaro().CGColor
        border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: 1)
        border.borderWidth = 1
        cell.layer.addSublayer(border)
        //}

        if(self.IsPad()){
            if(indexPath.row == selectedIndex )	{
                tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
            }
        }
        return cell
    }
    //heigt row
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

        //diensione 0 per celle di impianti vuoti
        if( DataManager.Instance.impianti[ indexPath.row ].id == "0" 	) { return 0 }

        return 65
    }

    // table view header
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerview : UIView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
        headerview.backgroundColor = UIColor.sfondoHeader()

        let title : UILabel = UILabel (frame: headerview.frame)
        title.text = "IMPIANTI"
        title.textAlignment = NSTextAlignment.Center
        title.textColor = UIColor.sfondoChiaro()

        headerview.addSubview(title)

        return  headerview

    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }

    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(self.IsPad()){
            return 30
        }
        else
        {
            return 0
        }
    }

    //tableView Delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        isLoadedNewEvent  = false

        if(IsPad() ){
            if(selectedIndex == indexPath.row) { return }

            selectedIndex = indexPath.row
        }

        selectedImpianto = DataManager.Instance.impianti[ indexPath.row ]

        if(self.IsPad()){
            if (selectedImpianto?.isPro == "false"){
                self.addProUserView( CamCollectionView.frame )

                events = [Evento]()
                arrayEvents = Array<Array<Evento>>()
                CamCollectionView.reloadData()

            }
            else
            {
                self.removeProUserView()

                loadInd!.startAnimating()
                view.addSubview(loadInd!)
                self.view.bringSubviewToFront(loadInd!)

                reqEventAsinc()
            }

        }
        else
        {

            NavigateToCollection(selectedImpianto )

        }


        //    NSTimer.scheduledTimerWithTimeInterval( 10 , target: self, selector: Selector("reloadEventFromNotification" ), userInfo: nil, repeats: false	)
    }


    func NavigateToCollection(instal : Impianto? )
    {
        let eventiVC = EventiVC()
        eventiVC.selectedImpianto = instal
        eventiVC.isShowCollection = true
        self.navigationController?.pushViewController(eventiVC , animated: true)

    }

    // COLLECTION VIEW

    //Data
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        //return 1

        return arrayEvents.count
    }
    //number of item
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        //return events.count

        return arrayEvents[section].count
    }
    //cellforitem
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {





        if (selectedImpianto != nil){

            if(self.IsPad()){

                var nib = UINib(nibName: "CollCellWithImageEvent", bundle: nil)
                collectionView.registerNib(nib, forCellWithReuseIdentifier: "CollCellWithImageEvent")
                var cell :CollCellWithImageIphone!  = collectionView.dequeueReusableCellWithReuseIdentifier("CollCellWithImageEvent", forIndexPath: indexPath) as? CollCellWithImageIphone

                if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
                    cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, CamCollectionView.frame.width  - 10 , 125)
                }
                else
                {
                    cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, CamCollectionView.frame.width  - 10 , 125 )
                }

                //arrotonda bordi
                cell.layer.masksToBounds = true;
                cell.layer.cornerRadius = 25;

                //seleziona evento
                let event  = arrayEvents[indexPath.section][indexPath.row]

                //set the title of cam
                let title : UILabel = cell.viewWithTag(1) as! UILabel
                title.text = event.type //event.deviceName



                let ora : UILabel = cell.viewWithTag(4) as! UILabel

                let time : NSString = event.time as NSString
                let timestamp : Double = time.doubleValue

                //set the description of cam
                let desc : UILabel = cell.viewWithTag(3) as! UILabel
                desc.text = event.deviceName + " (" + (dateFormatter( NSDate(timeIntervalSince1970: timestamp ) ) as String)  + ")"//event.type

                ora.text = event.eventDetail//   dateFormatter( NSDate(timeIntervalSince1970: timestamp ) ) as String

                //let info : UILabel = cell.viewWithTag(5) as! UILabel
                //	info.text = event.eventDetail


                //    cell.animate()

                cell.backgroundColor = UIColor.sfondoChiaro()
                if(isPushNotification && isLoadedNewEvent){
                    if(event.id  == pushEventId ){
                        isPushNotification = false
                        MainViewController.clearNotData()

                        cell.backgroundColor = UIColor.arancio()
                        UIView.animateWithDuration(0.5 , delay: 0.0 ,
                            options: [.Repeat, .Autoreverse, .CurveEaseInOut] , animations: {
                                UIView.setAnimationRepeatCount(2.5)
                                cell.backgroundColor =  UIColor.sfondoChiaro()
                            }, completion: nil)

                        MainViewController.clearNotData()
                    }
                }

                //set the image of the cell

                let imageView : UIImageView = cell.viewWithTag(2) as! UIImageView
                let bundle = NSBundle.mainBundle()

                if let image = UIImage(named: "ITEM-3.png") {
                    imageView.image = image
                }

                if (event.imageUrl != nil){

                    // let UrlNew : String  = event.imageUrl!.stringByReplacingOccurrencesOfString("http", withString: "https", options: nil, range: nil)

                    if let img = DataManager.Instance.imgCacheEventi[ event.id ]
                    {
                        imageView.image = img
                    }
                    else
                    {

                        let url = NSURL(string: event.imageUrl! )  //event.imageUrl!
                        var timeoutInterval: NSTimeInterval = 5
                        var chachepol : NSURLRequestCachePolicy = NSURLRequestCachePolicy.UseProtocolCachePolicy
                        let request = NSURLRequest(URL: url!, cachePolicy: chachepol, timeoutInterval: timeoutInterval)

                        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in

                            if (data != nil){
                                if let image = UIImage(data: data!){
                                    DataManager.Instance.imgCacheEventi[ event.id ] = image
                                    if let updateCell : UICollectionViewCell = self.CamCollectionView.cellForItemAtIndexPath(indexPath) { //  tableView cellForRowAtIndexPath:indexPath];
                                        (updateCell.viewWithTag(2) as! UIImageView).image = image;
                                    }
                                }
                            }
                        }
                    }
                }


                return cell;
            }
                //IPHONE
            else
            {
                var nib = UINib(nibName: "CollCellWithImageIphone", bundle: nil)
                collectionView.registerNib(nib, forCellWithReuseIdentifier: "CollCellWithImageIphone")

                var cell :CollCellWithImageIphone!  = collectionView.dequeueReusableCellWithReuseIdentifier("CollCellWithImageIphone", forIndexPath: indexPath) as? CollCellWithImageIphone //UICollectionViewCell
                //resize the cell frame

                cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, CamCollectionView.frame.width  , 65 )

                let event = arrayEvents[indexPath.section][indexPath.row] // events[ indexPath.row ]
                //set the title of cam
                let title : UILabel = cell.viewWithTag(1) as! UILabel
                title.text = event.type // event.deviceName


                //ora
                let ora : UILabel = cell.viewWithTag(4) as! UILabel
                ora.alpha = 1

                let time : NSString = event.time as NSString
                let timestamp : Double = time.doubleValue


                ora.text = event.deviceName + " (" + (dateFormatter( NSDate(timeIntervalSince1970: timestamp ) ) as String)  + ")" // dateFormatter ( NSDate(timeIntervalSince1970: timestamp ) ) as String

                //set the description of cam
                let desc : UILabel = cell.viewWithTag(3) as! UILabel
                desc.text = event.eventDetail



                cell.backgroundColor = UIColor.whiteColor()

                if(isPushNotification){
                    MainViewController.clearNotData()
                    if(event.id  == pushEventId && isLoadedNewEvent)
                    {
                        isPushNotification = false
                        MainViewController.clearNotData()

                        cell.backgroundColor = UIColor.arancio()
                        UIView.animateWithDuration(0.5 , delay: 0.0 ,
                            options: [.Repeat, .Autoreverse, .CurveEaseInOut] , animations: {

                                UIView.setAnimationRepeatCount(2.5)
                                cell.backgroundColor =  UIColor.whiteColor()
                            }, completion: nil)

                        MainViewController.clearNotData()
                    }
                    else if(indexPath.row >= 5)
                    {
                        isPushNotification = false
                        MainViewController.clearNotData()
                    }
                }



                //set the image of the cell

                let imageView : UIImageView = cell.viewWithTag(2) as! UIImageView
                let bundle = NSBundle.mainBundle()
                if let image = UIImage(named: "ITEM-3.png") {
                    imageView.image = image
                }

                if (event.imageUrl != nil){
                    if let img = imageCahce[ indexPath.row ]
                    {
                        imageView.image = img
                    }
                    else
                    {

                        let url = NSURL(string: event.imageUrl! )
                        print(event.imageUrl!)
                        var timeoutInterval: NSTimeInterval = 5
                        var chachepol : NSURLRequestCachePolicy = NSURLRequestCachePolicy.UseProtocolCachePolicy
                        let request = NSURLRequest(URL: url!, cachePolicy: chachepol, timeoutInterval: timeoutInterval)

                        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, error) in

                            if (data != nil){
                                if let image = UIImage(data: data!){
                                    self.imageCahce[ indexPath.row ] = image
                                    if let updateCell : UICollectionViewCell = self.CamCollectionView.cellForItemAtIndexPath(indexPath) { //  tableView cellForRowAtIndexPath:indexPath];
                                        (updateCell.viewWithTag(2) as! UIImageView).image = image;
                                    }
                                }
                            }
                        }
                    }
                }


                let border : CALayer = CALayer()
                border.borderColor = UIColor.sfondoChiaro().CGColor
                border.frame = CGRect(x: 0, y: cell.frame.size.height - 1, width:  cell.frame.size.width, height: cell.frame.size.height)
                border.borderWidth = 1
                cell.layer.addSublayer(border)

                return cell;

            }

        }
        else {
            return UICollectionViewCell( )
        }
    }



    //size for row
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

        if(self.IsPad()){
            if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
                return CGSizeMake(CamCollectionView.frame.width  - 10  , 125)

            }
            else
            {
                return CGSizeMake(CamCollectionView.frame.width  - 10  , 125 )

            }
        }
            //IPHONE
        else
        {
            //if ( UIInterfaceOrientationIsLandscape(UIApplication.sharedApplication().statusBarOrientation) ){
            return CGSizeMake(CamCollectionView.frame.width   ,65)
        }

        
    }
    //header footer
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if(self.IsPad()){
            
            if( section==0){
                
                return CGSizeMake(CamCollectionView.frame.width  , 108 )
            }
            else
            {
                
                return CGSizeMake(CamCollectionView.frame.width  , 32 )
            }
            
            //	return CGSizeMake(CamCollectionView.frame.width  , 85 )
            
        }
            //IPHONE
        else
        {
            return CGSize(width: 0, height: 32)
            
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        if (kind == UICollectionElementKindSectionHeader){
            let header :UICollectionReusableView = collectionView.dequeueReusableSupplementaryViewOfKind(UICollectionElementKindSectionHeader, withReuseIdentifier: "headerView", forIndexPath: indexPath) 
            
            if(self.IsPad()){
                let title : UILabel = header.viewWithTag(1) as! UILabel
                //let view : UIView = header.viewWithTag(2)!
                let titledata : UILabel = header.viewWithTag(3) as! UILabel
                
                
                let event  = arrayEvents[indexPath.section][0]
                let n2 = (event.time as NSString).doubleValue
                let data : NSDate = NSDate(timeIntervalSince1970: n2)
                
                if (indexPath.section==0){
                    
                    title.alpha = 1
                    title.text = "eventi".loc() + " (" +  String(events.count) + ")"
                    titledata.text = meseFormatterDate(data) as String
                }
                else
                {
                    
                    
                    
                    title.alpha = 0
                    titledata.text = meseFormatterDate(data) as String
                }
                
                
            }
            else
            {
                let titledata : UILabel = header.viewWithTag(1) as! UILabel
                
                let event  = arrayEvents[indexPath.section][0]
                let n2 = (event.time as NSString).doubleValue
                let data : NSDate = NSDate(timeIntervalSince1970: n2)
                
                titledata.text = meseFormatterDate(data) as String
            }
            
            return header
        }
        
        return UICollectionReusableView()
    }
    
    // CollectionView Delegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        isGoInPlayer = true
        let eventPlayerController : EventiPlayerView =  EventiPlayerView()
        
        eventPlayerController.evento = arrayEvents[indexPath.section][indexPath.row]  //  events[indexPath.row]
        
        // let backItem = UIBarButtonItem()
        //backItem.title = "indietro".loc() + " " + eventPlayerController.evento!.type //"Something Else"
        // navigationItem.backBarButtonItem = backItem
        
        self.navigationController?.pushViewController(eventPlayerController, animated: true)
        
    }
    
    
    
    //
    
    func oraformatterDate(date: NSDate) -> NSString	{
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        return dateFormatter.stringFromDate(date)
        
    }
    
    func meseFormatterDate(date: NSDate) -> NSString	{
        let df : NSDateFormatter = NSDateFormatter()
        
        df.dateStyle = NSDateFormatterStyle.LongStyle
        df.locale = NSLocale.currentLocale()
        //df.dateFormat = "mm - yyyy "
        
        let arr = df.stringFromDate(date).componentsSeparatedByString(" ")
        
        let mese: String = arr[1]+" "+arr[2]
        return mese // df.stringFromDate(date)
    }
    
    func dateFormatter (date :NSDate )->NSString{
        
        
        let df : NSDateFormatter = NSDateFormatter()
        
        df.dateStyle = NSDateFormatterStyle.ShortStyle
        df.locale = NSLocale.currentLocale()
        //df.dateFormat = "mm - yyyy "
        
        let s : String = oraformatterDate(date) as String
        let string : String = df.stringFromDate(date) + " " + s
        return string
        /*[dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        [dateormatter setTimeStyle:NSDateFormatterNoStyle];
        [dateFormatter setLocale:[NSLocale currentLocale]];
        NSDate *date = [NSDate date];
        NSString *dateString = [dateFormatter stringFromDate:date];*/
    }
    
    func reloadEventFromNotification()
    {
        
        print ("reload event from notification", terminator: "")
        
        (self.tabBarController?.tabBar.items![2]  as UITabBarItem? )!.badgeValue = nil
        
        isPushNotification = false
        
        if(MainViewController.notificationData != nil)
        {
            isPushNotification = true
            pushInstallId  = MainViewController.notificationData!.objectForKey("install") as? String
            pushEventId  = MainViewController.notificationData!.objectForKey("content") as? String
        }
        
        if(self.IsPad())
        {
            if(isPushNotification){
                selectedImpianto = DataManager.Instance.installs[ pushInstallId! ]
            }
            
            if (selectedImpianto?.isPro == "false"){
                self.addProUserView( CamCollectionView.frame )
                
                events = [Evento]()
                arrayEvents = Array<Array<Evento>>()
                CamCollectionView.reloadData()
            }
            else
            {
                loadInd!.center = CamCollectionView.center
                loadInd!.startAnimating()
                view.addSubview(loadInd!)
                self.view.bringSubviewToFront(loadInd!)
                
                
                var int = 0;
                for  imp : Impianto in DataManager.Instance.impianti {
                    if (imp.id == pushInstallId ){
                        break
                    }
                    int++;
                }
                
                let index =  NSIndexPath(forRow: int, inSection: 0)
                ImpiantiTable.selectRowAtIndexPath(index, animated: false, scrollPosition: UITableViewScrollPosition.Middle)
                ImpiantiTable.delegate!.tableView!( ImpiantiTable , didSelectRowAtIndexPath: index )
                
                reqEventAsinc()
            }
        }
            
        else
        {
            
            if ( isShowCollection ){
                CamCollectionView.frame = CGRect(x: 0 , y: 0 , width: self.frame().width	, height: self.frame().height)
                ImpiantiTable.alpha = 0
                CamCollectionView.alpha = 1
                
                if (selectedImpianto?.isPro == "false"){
                    self.addProUserView( CamCollectionView.frame )
                    
                    self.navigationController?.popViewControllerAnimated(true)
                }
                else
                {
                    loadInd!.center = CamCollectionView.center
                    loadInd!.startAnimating()
                    view.addSubview(loadInd!)
                    self.view.bringSubviewToFront(loadInd!)
                    
                    reqEventAsinc()
                }
                
                if(selectedImpianto != nil){
                    self.title = "eventi".loc() + ": " + selectedImpianto!.name
                }
                
            }
            else
            {
                ImpiantiTable.alpha = 1
                CamCollectionView.alpha = 0
                
                if(isPushNotification){
                    selectedImpianto = DataManager.Instance.installs[ pushInstallId! ]
                    
                    self.navigationController?.popViewControllerAnimated(true)
                    
                    NavigateToCollection(selectedImpianto )
                    
                }
                else
                {
                    for  imp : Impianto in DataManager.Instance.impianti {
                        if (imp.devices.count >  0){
                            selectedImpianto = imp
                            break
                        }
                    }
                }
                
                
            }
        }
    }
    
}

